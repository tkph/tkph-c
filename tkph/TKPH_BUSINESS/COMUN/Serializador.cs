﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace TKPH_BUSINESS.COMUN
{
    /// <summary>
    /// Clase con metodos para serializar y desserializar variables
    /// </summary>
    public class Serializador
    {
        /// <summary>
        /// Metodo de Serializacion del Objeto
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="objeto">Objeto a Serializar.</param>
        /// <returns>Retorna Objeto Serializado</returns>
        public static string SerializeObjeto<T>(T objeto)
        {
            string dtoSerializado = string.Empty;
            System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(T));
            System.IO.StringWriter sw = new System.IO.StringWriter();
            serializer.Serialize(sw, objeto);
            dtoSerializado = sw.ToString();
            sw.Close();

            return dtoSerializado;
        }

        /// <summary>
        /// Metodo de Deserializacion del Objeto
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="objeto">Objeto a Deserializar.</param>
        /// <returns>Retorna Objeto Deserializado</returns>
        public static T DesSerializeObjeto<T>(string xmlObject)
        {
            T dtoSerialzado;
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            StringReader sr = new StringReader(xmlObject);
            XmlTextReader xmlReader = new XmlTextReader(sr);
            dtoSerialzado = (T)serializer.Deserialize(xmlReader);
            xmlReader.Close();
            sr.Close();
            return dtoSerialzado;
        }
    }
}
