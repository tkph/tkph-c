﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace TKPH_BUSINESS.COMUN
{
    /// <summary>
    /// Clase con metodos para encriptar y desencriptar cadena de caracteres
    /// </summary>
    public static class Seguridad
    {
        /// <summary>
        /// Metodo para encriptacion de cadena de caracteres.
        /// </summary>
        /// <param name="_cadenaAencriptar">Cadena de caracteres a Encriptar.</param>
        /// <returns>Retorna cadena encriptada en base a algoritmo de 64bits</returns>
        public static string EncriptarString(this string _cadenaAencriptar)
        {
            string result = string.Empty;
            byte[] encryted = System.Text.Encoding.Unicode.GetBytes(_cadenaAencriptar);
            result = Convert.ToBase64String(encryted);
            return result;
        }


        /// <summary>
        /// Metodo para desencriptacion de cadena de caracteres.
        /// </summary>
        /// <param name="_cadenaAencriptar">Cadena de caracteres a Desencriptar.</param>
        /// <returns>Retorna cadena Desencriptada</returns>
        public static string DesEncriptarString(this string _cadenaAdesencriptar)
        {
            string result = string.Empty;
            byte[] decryted = Convert.FromBase64String(_cadenaAdesencriptar);
            result = System.Text.Encoding.Unicode.GetString(decryted);
            return result;
        }
    }
}
