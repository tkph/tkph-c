﻿using Com.Vmica.Data;
using Com.Vmica.View.ControlSistema;
using Com.Vmica.Wrapper.ControlSistema;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TKPH_BUSINESS.BUSINESS_PAGE.ControlSistema
{
    public class bControlSistema
    {
        Orquestador _data = Orquestador.Instance;

        #region Encapsula Instancia
        private const string CLASS_NAME = "bControlSistema";

        private static readonly bControlSistema instance = new bControlSistema();
        private bControlSistema() { }
        public static bControlSistema Instance
        {
            get { return instance; }
        }
        #endregion

        public IList<vEstadisticas> GetEstadisticas(String IdProceso, Int64 TipoProp, String IdArea)
        {
            IList<vEstadisticas> list = new List<vEstadisticas>();

            try
            {
                list = wEstadisticas.ConvertData(_data.GetEstadisticas(IdProceso,TipoProp,IdArea));
            }
            catch (Exception e)
            {
                throw e;
            }

            return list;

        }

        public IList<vDetallePro> GetProceso(Int64 Flag, String IdProceso)
        {
            IList<vDetallePro> list = new List<vDetallePro>();

            try
            {
                list = wDetallePro.ConvertData(_data.GetProceso(Flag, IdProceso));
            }
            catch (Exception e)
            {
                throw e;
            }

            return list;

        }
    }
}
