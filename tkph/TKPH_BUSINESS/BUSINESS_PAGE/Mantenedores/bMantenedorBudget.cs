﻿
using Com.Vmica.View.Mantenedores;
using Com.Vmica.Wrapper.Mantenedores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TKPH_DATA;

namespace TKPH_BUSINESS.BUSINESS_PAGE.Mantenedores
{
    /// <summary>
    /// Clase con metodos y definiciones para administrar las configuraciones de Mantenedores.
    /// </summary>
    public class bMantenedorBudget
    {
        /// <summary>
        /// Llamada a instancia clase "Orquestador" que administra las llamadas desde otros objetos.
        /// </summary>
        Orquestador _data = Orquestador.Instance;


        /// <summary>
        /// Encapsulamiento de instancia para clase "bMantenedorBudget"
        /// </summary>
        #region Encapsula Instancia
        private const string CLASS_NAME = "bMantenedorBudget";

        private static readonly bMantenedorBudget instance = new bMantenedorBudget();
        private bMantenedorBudget() { }
        public static bMantenedorBudget Instance
        {
            get { return instance; }
        }
        #endregion

        /// <summary>
        /// Metodo para obtener los datos de configuración del modulo de Carga.
        /// </summary>
        /// <param name="Tipo">Valor correspondiente al tipo de Mantendor</param>
        /// <param name="Nivel_1">Valor correspondinte al primer nivel de Jerarquico</param>
        /// <param name="Fiscal">Año fiscal al cual corresponden los datos.</param>
        /// <param name="Flag">Valor correspondiente a estado de configuración</param>
        /// <param name="Fecha">Valor correspondiente a la fecha o periodo consultado</param>
        /// <returns>Retorna Interfaz de lista tipo "vMantenedorCarga" con datos de carga.</returns>
        public IList<vMantenedorCarga> GetDatosBudget(Int64 Tipo, Int64 Nivel_1, String Fiscal, String Flag, String Fecha)
        {
            IList<vMantenedorCarga> list = new List<vMantenedorCarga>();

            try
            {
                list = wMantenedorCarga.ConvertData(_data.GetDatosBudget(Tipo, Nivel_1, Fiscal, Flag, Fecha));
            }
            catch (Exception e)
            {
                throw e;
            }

            return list;

        }


        /// <summary>
        /// Metodo para persisitir valor de configuración.
        /// </summary>
        /// <param name="Ident">Valor correspondiente a identificador de  parametro a guardar.</param>
        /// <param name="ValorT">Valor a guardar.</param>
        /// <param name="Afisc">Año fiscal al cual corresponde.</param>
        /// <returns></returns>
        public IList<vInsertDatosMantenedor> SetDatosBudget(String Ident, String @ValorT, String Afisc)
        {
            IList<vInsertDatosMantenedor> list = new List<vInsertDatosMantenedor>();

            try
            {
                list = wInsertDatosMantenedor.ConvertData(_data.SetDatosBudget(Ident, @ValorT, Afisc));
            }
            catch (Exception e)
            {
                throw e;
            }

            return list;

        }

    }
}
