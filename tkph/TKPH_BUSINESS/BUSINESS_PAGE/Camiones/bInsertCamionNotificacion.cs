﻿
using Com.Vmica.View;
using Com.Vmica.Wrapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TKPH_DATA;

namespace TKPH_BUSINESS.BUSINESS_PAGE
{
    /// <summary>
    /// Clase con metodos y definiciones para Insercion de datos Notificación en Pantalla Detalle Camión
    /// </summary>
    public class bInsertCamionNotificacion
    {

        /// <summary>
        /// Llamada a instancia clase "Orquestador" que administra las llamadas desde otros objetos.
        /// </summary>
        Orquestador _data = Orquestador.Instance;


        /// <summary>
        /// Encapsulamiento de instancia para clase "bInsertCamionNotificacion"
        /// </summary>
         #region Encapsula Instancia
        private const string CLASS_NAME = "bInsertCamionNotificacion";

        private static readonly bInsertCamionNotificacion instance = new bInsertCamionNotificacion();
        private bInsertCamionNotificacion() { }
        public static bInsertCamionNotificacion Instance
        {
            get { return instance; }
        }
        #endregion

        /// <summary>
        /// Sets the insert camion notificacion.
        /// </summary>
        /// <param name="idcamion">Corresponde al identificador del Camión.</param>
        /// <param name="comentario">Valor que corresponde al tipo de Notificación.</param>
        /// <returns>Retorna Interfaz de Lista de "vInsertCamionNotificacion".</returns>
        public IList<vInsertCamionNotificacion> setInsertCamionNotificacion(Int64 idcamion, String comentario)
        {
            IList<vInsertCamionNotificacion> c = new List<vInsertCamionNotificacion>();

            try 
            {
                c = wInsertCamionNotificacion.ConvertData(_data.setInsertCamionNotificacion(idcamion, comentario));
            }
             catch (Exception e)
            {
                throw e;
            }

            return c;
        }
    }
}
