﻿
using Com.Vmica.View.Camiones;
using Com.Vmica.Wrapper.Camiones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TKPH_DATA;
using TKPH_DATA.DTO.Camiones;

namespace TKPH_BUSINESS.BUSINESS_PAGE.Camiones
{
    /// <summary>
    /// Clase con metodos y definiciones para obtener datos de Camión en Pantalla Detalle Camión
    /// </summary>
    public class bDetalleCamion
    {
        /// <summary>
        /// Llamada a instancia clase "Orquestador" que administra las llamadas desde otros objetos.
        /// </summary>
        Orquestador _data = Orquestador.Instance;

        /// <summary>
        /// Encapsulamiento de instancia para clase "bDetalleCamion"
        /// </summary>
         #region Encapsula Instancia
        private const string CLASS_NAME = "bDetalleCamion";

        private static readonly bDetalleCamion instance = new bDetalleCamion();
        private bDetalleCamion() { }
        public static bDetalleCamion Instance
        {
            get { return instance; }
        }
        #endregion


        /// <summary>
        /// Metodo para obtener datos agrupados de presion y temperatura asociadas al Camión.
        /// </summary>
        /// <param name="idcamion">Corresponde al identificador del Camión.</param>
        /// <returns>Retorna Interfaz de Lista de "vPromAvgTempPres".</returns>
        public IList<vPromAvgTempPres> getTempPres(Int64 idcamion)
        {
            IList<vPromAvgTempPres> c = new List<vPromAvgTempPres>();

            try 
            {
                IList<PromAvgTempPresDTO> res = new List<PromAvgTempPresDTO>();
                res=_data.getTempPres(idcamion);
                c = wPromAvgTempPres.ConvertData(res);
            }
             catch (Exception e)
            {
                throw e;
            }

            return c;
        }
    }
}
