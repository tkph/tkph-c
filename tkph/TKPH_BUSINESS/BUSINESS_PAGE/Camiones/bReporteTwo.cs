﻿

using Com.Vmica.View;
using Com.Vmica.Wrapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TKPH_DATA;
using TKPH_DATA.DTO.Camiones;

namespace TKPH_BUSINESS.BUSINESS_PAGE
{
    /// <summary>
    /// Clase con metodos y definiciones para obtener datos asociados a la flota.
    /// </summary>
    public class bReporteTwo
    {

        /// <summary>
        /// Llamada a instancia clase "Orquestador" que administra las llamadas desde otros objetos.
        /// </summary>
        Orquestador _data = Orquestador.Instance;

        /// <summary>
        /// Metodo para obtener la información individual de la flota de camiones.
        /// </summary>
        /// <param name="StatusRequestSingle">Objeto tipo "vStatusRequest" que contiene los datos del camión.</param>
        /// <returns>Retorna una Interfaz de Lista de tipo "vCamionDetails" con los camiones asociados a la flota</returns>
        public IList<vCamionDetails> GetCamionSingle(vStatusRequest StatusRequestSingle)
        {
            IList<vCamionDetails> _list = new List<vCamionDetails>();

            try
            {
                IList<CamionDetailsDTO> _lResponse = _data.DetailsByStatus(wStatusRequest.ConvertData(StatusRequestSingle));

                _list = wCamionDetails.ConvertData(_lResponse);
            }
            catch (Exception e)
            {
                throw e;
            }

            return _list;
        }

        /// <summary>
        /// Metodo que obtiene la imagen correspondiente al icono del camión según su alarma.
        /// </summary>
        /// <param name="Alerta">Valor correspondiente  a la alarma del camión.</param>
        /// <returns>Retorna la ruta de la imagen correspondiente a los parametros ingresados.</returns>
        public String ColorCamion(Int64 Alerta)
        {
            
            switch (Alerta)
            {

                case 1:
                    return @"~\Imagenes\truck_r.png";                  
                case 2:
                    return @"~\Imagenes\truck_a.png";
                case 3:
                    return @"~\Imagenes\truck_v.png";
                case 4:
                    return @"~\Imagenes\truck_g.png";
                case 5:
                    return @"~\Imagenes\truck_rp.gif";
                case 6:
                    return @"~\Imagenes\truck_ap.gif";
                default:
                    return @"~\Imagenes\truck_g.png";
            }
           
        }


        /// <summary>
        /// Metodo para obtener el color estandar de la alarma
        /// </summary>
        /// <param name="Alerta">Valor correspondiente a la alarma.</param>
        /// <returns></returns>
        public Color ColorAlarma(Int64 Alerta)
        {
            switch (Alerta)
            {
                case 1:
                    return Color.FromArgb(255, 255, 37, 37);
                case 2:
                    return Color.FromArgb(255, 255, 217, 0);
                case 3:
                    return Color.FromArgb(255, 120, 182, 1);
                default:
                    return Color.FromArgb(255, 191, 191, 191);
            }
        }


    }
}