﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Com.Vmica.View;

using Com.Vmica.Wrapper;
using TKPH_DATA;

namespace TKPH_BUSINESS.BUSINESS_PAGE.Neumatico
{
    /// <summary>
    /// Clase con metodos y definiciones para obtener datos Historicos Camión en Pantalla Tendencias
    /// </summary>
    public class bNivel4
    {
        /// <summary>
        /// Llamada a instancia clase "Orquestador" que administra las llamadas desde otros objetos.
        /// </summary>
        Orquestador _data = Orquestador.Instance;

        /// <summary>
        /// Encapsulamiento de instancia para clase "bNivel4"
        /// </summary>
        #region Encapsula Instancia
        private const string CLASS_NAME = "bNivel4";

        private static readonly bNivel4 instance = new bNivel4();
        private bNivel4() { }
        public static bNivel4 Instance
        {
            get { return instance; }
        }
        #endregion


        /// <summary>
        /// Metodo para obtener datos historicos de carga y TKPH asociadas al Camión.
        /// </summary>
        /// <param name="Request">Corresponde al objeto de tipo "vHistorico" para insertar datos.</param>
        /// <returns>Retorna Interfaz de Lista de "vHistorico".</returns>
        public IList<vHistorico> GetHistoricoFlag(vHistorico Request)
        {
            IList<vHistorico> _list = new List<vHistorico>();
 
            try
            {
                _list = wHistorico.ConvertData(_data.GetHistorico(wHistorico.ConvertData(Request)));
            }
            catch (Exception e)
            {
                throw e;
            }

            return _list;
        }


        /// <summary>
        /// Metodo para obtener datos historicos de presión y Temperatura asociadas al Camión.
        /// </summary>
        /// <param name="Request">Corresponde al objeto de tipo "vHistoricoPT" para insertar datos.</param>
        /// <returns>Retorna Interfaz de Lista de "vHistoricoPT".</returns>
        public IList<vHistoricoPT> GetHistoricoPT(vHistoricoPT Request)
        {
            IList<vHistoricoPT> _list = new List<vHistoricoPT>();

            try
            {
                _list = wHistoricoPT.ConvertData(_data.GetHistoricoPTO(wHistoricoPT.ConvertData(Request)));
            }
            catch (Exception e)
            {
                throw e;
            }

            return _list;
        }
    }
}
