﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Com.Vmica.Data;
using Com.Vmica.View;
using Com.Vmica.Wrapper;
using TKPH_BUSINESS.COMUN;

namespace TKPH_BUSINESS.BUSINESS_PAGE
{
    public class bReporteOne
    {
        Orquestador _data = Orquestador.Instance;

        #region Encapsula Instancia
        private const string CLASS_NAME = "bReporteOne";

        private static readonly bReporteOne instance = new bReporteOne();
        private bReporteOne() { }
        public static bReporteOne Instance
        {
            get { return instance; }
        }
        #endregion

        #region Metodos
        public IList<vComparaTonelada> GetComparaTonelada()
        {
            IList<vComparaTonelada> _lComparaTonelada = new List<vComparaTonelada>();

            _lComparaTonelada.Add(new vComparaTonelada() { Mes = "201412", Destino = "31", Carga = "302" });
            _lComparaTonelada.Add(new vComparaTonelada() { Mes = "201412", Destino = "BO", Carga = "11136566" });
            _lComparaTonelada.Add(new vComparaTonelada() { Mes = "201412", Destino = "CH", Carga = "6773111" });
            _lComparaTonelada.Add(new vComparaTonelada() { Mes = "201412", Destino = "P2", Carga = "2407379" });
            _lComparaTonelada.Add(new vComparaTonelada() { Mes = "201412", Destino = "P4", Carga = "2998145" });
            _lComparaTonelada.Add(new vComparaTonelada() { Mes = "201412", Destino = "SA", Carga = "1304171" });
            _lComparaTonelada.Add(new vComparaTonelada() { Mes = "201412", Destino = "SB", Carga = "49039" });
            _lComparaTonelada.Add(new vComparaTonelada() { Mes = "201412", Destino = "SL", Carga = "1068516" });
            _lComparaTonelada.Add(new vComparaTonelada() { Mes = "201412", Destino = "SM", Carga = "2581657" });
            _lComparaTonelada.Add(new vComparaTonelada() { Mes = "201412", Destino = "SO", Carga = "1575515" });

            return _lComparaTonelada;
        }

        public IList<vEstadoNeumatico> GetEstadoNeumatico()
        {
            IList<vEstadoNeumatico> _lEstadoNeumatico = new List<vEstadoNeumatico>();

            try
            {
                _lEstadoNeumatico = wNeumaticoGroup.ConvertData(_data.GroupByStatus());
            }
            catch (Exception e)
            {
                throw e;
            }

            return _lEstadoNeumatico;
        }

        public Int64 GetTotalCamion(IList<vEstadoNeumatico> ListStatus)
        {
            try
            {
                return ListStatus.Sum(p => p.Total);
            }
            catch
            {
                throw new Exception("ERR.t02: Calculo de totales con errores");
            }
        }

        public Int64 GetTotalCamion(IList<vEstadoNeumatico> ListStatus, Int64 Status)
        {
            try
            {
                return ListStatus.Where(p => p.Valor.Equals(Status)).ToList().Sum(p => p.Total);
            }
            catch
            {
                throw new Exception("ERR.t03: Calculo de totales con errores Status");
            }
        }
        #endregion

        public string GetUrlPie(String Status)
        {
            return "ReportTwo.aspx?status=" + Seguridad.EncriptarString(Status);
        }

        public IList<vDisponible> GetFlotaDisponible()
        {
            IList<vDisponible> _lEstadoNeumatico = new List<vDisponible>();

            try
            {
                _lEstadoNeumatico = wDisponilidad.ConvertData( _data.GetFlotaDisponible());
            }
            catch (Exception e)
            {
                throw e;
            }

            return _lEstadoNeumatico;
        }
    }
}
