﻿
using Com.Vmica.View;
using Com.Vmica.Wrapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TKPH_DATA;
using TKPH_DATA.DTO.Camiones;

namespace TKPH_BUSINESS.BUSINESS_PAGE
{
    /// <summary>
    /// Clase con metodos y definiciones para obtener datos de Notificación en Pantalla Detalle Camión
    /// </summary>
    public class bCamionNotificacion
    {
        /// <summary>
        /// Llamada a instancia clase "Orquestador" que administra las llamadas desde otros objetos.
        /// </summary>
        Orquestador _data = Orquestador.Instance;

        /// <summary>
        /// Encapsulamiento de instancia para clase "bCamionNotificacion"
        /// </summary>
         #region Encapsula Instancia
        private const string CLASS_NAME = "bCamionNotificacion";

        private static readonly bCamionNotificacion instance = new bCamionNotificacion();
        private bCamionNotificacion() { }
        public static bCamionNotificacion Instance
        {
            get { return instance; }
        }
        #endregion

        /// <summary>
        /// Metodo para obtener datos de notificaciones asociadas al Camión.
        /// </summary>
        /// <param name="idcamion">Corresponde al identificador del Camión.</param>
        /// <returns>Retorna Interfaz de Lista de "vCamionNotificacion".</returns>
        public IList<vCamionNotificacion> getCamionNotificacion(String idcamion)
        {
            IList<vCamionNotificacion> c = new List<vCamionNotificacion>();

            try 
            {
                IList<CamionNotificacionDTO> res = new List<CamionNotificacionDTO>();
                res = _data.getCamionNotificacion(idcamion);
                c = wCamionNotificacion.ConvertData(res);
            }
             catch (Exception e)
            {
                throw e;
            }

            return c;
        }
    }
}
