﻿
using Com.Vmica.View;
using Com.Vmica.Wrapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TKPH_DATA;

namespace TKPH_BUSINESS.BUSINESS_PAGE
{
    public class bLogin
    {
        /// <summary>
        /// Llamada a instancia de Objeto Orquestador
        /// </summary>
        Orquestador _data = Orquestador.Instance;

        /// <summary>
        /// Encapsulacion de instancia para Objeto "bLogin" para ser accedido desde otras clases.
        /// </summary>
        #region Encapsula Instancia
        private const string CLASS_NAME = "bLogin";

        private static readonly bLogin instance = new bLogin();
        private bLogin() { }
        public static bLogin Instance
        {
            get { return instance; }
        }
        #endregion


        /// <summary>
        /// Invoca al metodo de consulta de existencia de datos en el sistema
        /// </summary>
        /// <param name="Usuario">Corresponde al Usuario registrado</param>
        /// <param name="Password">Corresponde a la password correspondiente al usuario</param>
        /// <returns></returns>
        public vLogin getLogin(String Usuario, String Password)
        {
            vLogin _list = new vLogin();

            try
            {
                _list = wLogin.ConvertData(_data.getLogin(Usuario,Password));
            }
            catch (Exception e)
            {
                throw e;
            }

            return _list;
        }

       


    }
}
