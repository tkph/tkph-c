﻿
using Com.Vmica.View.BI;
using Com.Vmica.Wrapper.BI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TKPH_DATA;

namespace TKPH_BUSINESS.BUSINESS_PAGE.BI
{
    /// <summary>
    /// Clase con metodos y definiciones para Graficos Dashboard TKPH
    /// </summary>
    public class bTkphGraficos
    {
        /// <summary>
        /// Llamada a instancia clase "Orquestador" que administra las llamadas desde otros objetos.
        /// </summary>
        Orquestador _data = Orquestador.Instance;


        /// <summary>
        /// Encapsulamiento de instancia para clase "bTkphGraficos"
        /// </summary>
         #region Encapsula Instancia
        private const string CLASS_NAME = "bTkphGraficos";

        private static readonly bTkphGraficos instance = new bTkphGraficos();
        private bTkphGraficos() { }
        public static bTkphGraficos Instance
        {
            get { return instance; }
        }
        #endregion


        /// <summary>
        /// Metodo para importar datos Flota TKPH para generar Graficos.
        /// </summary>
        /// <param name="flag">Valor que indica el tipo de dato a retornar.</param>
        /// <param name="idTurno">Valor correspondiente al turno.</param>
        /// <param name="fDesde">Fecha inicio para consulta de datos.</param>
        /// <param name="fHasta">Fecha termino para consulta de datos.</param>
        /// <param name="Objeto">Valor correspondiente a identificador de dato para realizar la busqueda</param>
        /// <returns>Retorna Interfaz de Lista de "vTkphFlota".</returns>
        public IList<vTkphFlota> getTkphFlota(Int64 flag, Int64 idTurno, String fDesde, String fHasta, String Objeto)
        {
            IList<vTkphFlota> _list = new List<vTkphFlota>();

            try
            {
                _list = wTkphFlota.ConvertData(_data.getTkphFlota(flag, idTurno, fDesde, fHasta, Objeto));
            }
            catch (Exception e)
            {
                throw e;
            }

            return _list;
        }


        /// <summary>
        /// Metodo para importar datos Destinos TKPH para generar Graficos.
        /// </summary>
        /// <param name="flag">Valor que indica el tipo de dato a retornar.</param>
        /// <param name="idTurno">Valor correspondiente al turno.</param>
        /// <param name="fDesde">Fecha inicio para consulta de datos.</param>
        /// <param name="fHasta">Fecha termino para consulta de datos.</param>
        /// <param name="Objeto">Valor correspondiente a identificador de dato para realizar la busqueda</param>
        /// <returns>Retorna Interfaz de Lista de "vTkphDestino".</returns>
        public IList<vTkphDestino> getTkphDestino(Int64 flag, Int64 idTurno, String fDesde, String fHasta, String Objeto)
        {
            IList<vTkphDestino> _list = new List<vTkphDestino>();

            try
            {
                _list = wTkphDestino.ConvertData(_data.getTkphDestino(flag, idTurno, fDesde, fHasta, Objeto));
            }
            catch (Exception e)
            {
                throw e;
            }

            return _list;
        }


        /// <summary>
        /// Metodo para importar datos Histograma TKPH para generar Graficos.
        /// </summary>
        /// <param name="flag">Valor que indica el tipo de dato a retornar.</param>
        /// <param name="idTurno">Valor correspondiente al turno.</param>
        /// <param name="fDesde">Fecha inicio para consulta de datos.</param>
        /// <param name="fHasta">Fecha termino para consulta de datos.</param>
        /// <param name="Objeto">Valor correspondiente a identificador de dato para realizar la busqueda</param>
        /// <returns>Retorna Interfaz de Lista de "vTkphHistograma".</returns>
        public IList<vTkphHistograma> getTkphHistograma(Int64 flag, Int64 idTurno, String fDesde, String fHasta, String Objeto)
        {
            IList<vTkphHistograma> _list = new List<vTkphHistograma>();

            try
            {
                _list = wTkphHistograma.ConvertData(_data.getTkphHistograma(flag, idTurno, fDesde, fHasta, Objeto));
            }
            catch (Exception e)
            {
                throw e;
            }

            return _list;
        }


        /// <summary>
        /// Metodo para importar datos Ranking TKPH para generar Graficos.
        /// </summary>
        /// <param name="flag">Valor que indica el tipo de dato a retornar.</param>
        /// <param name="idTurno">Valor correspondiente al turno.</param>
        /// <param name="fDesde">Fecha inicio para consulta de datos.</param>
        /// <param name="fHasta">Fecha termino para consulta de datos.</param>
        /// <param name="Objeto">Valor correspondiente a identificador de dato para realizar la busqueda</param>
        /// <returns>Retorna Interfaz de Lista de "vTkphRanking".</returns>
        public IList<vTkphRanking> getTkphRanking(Int64 flag, Int64 idTurno, String fDesde, String fHasta, String Objeto)
        {
            IList<vTkphRanking> _list = new List<vTkphRanking>();

            try
            {
                _list = wTkphRanking.ConvertData(_data.getTkphRanking(flag, idTurno, fDesde, fHasta, Objeto));
            }
            catch (Exception e)
            {
                throw e;
            }

            return _list;
        }


        /// <summary>
        /// Metodo para importar datos Tendencia TKPH para generar Graficos.
        /// </summary>
        /// <param name="flag">Valor que indica el tipo de dato a retornar.</param>
        /// <param name="idTurno">Valor correspondiente al turno.</param>
        /// <param name="fDesde">Fecha inicio para consulta de datos.</param>
        /// <param name="fHasta">Fecha termino para consulta de datos.</param>
        /// <param name="Objeto">Valor correspondiente a identificador de dato para realizar la busqueda</param>
        /// <returns>Retorna Interfaz de Lista de "vTkphTendencia".</returns>
        public IList<vTkphTendencia> getTkphTendencia(Int64 flag, Int64 idTurno, String fDesde, String fHasta, String Objeto)
        {
            IList<vTkphTendencia> _list = new List<vTkphTendencia>();

            try
            {
                _list = wTkphTendencia.ConvertData(_data.getTkphTendencia(flag, idTurno, fDesde, fHasta, Objeto));
            }
            catch (Exception e)
            {
                throw e;
            }

            return _list;
        }

    }
}
