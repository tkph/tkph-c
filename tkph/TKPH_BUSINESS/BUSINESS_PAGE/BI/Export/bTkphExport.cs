﻿
using Com.Vmica.View.BI;
using Com.Vmica.View.BI.Export;
using Com.Vmica.Wrapper.BI;
using Com.Vmica.Wrapper.BI.Export;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TKPH_DATA;

namespace COM_TKPH_DATA.BUSINESS_PAGE.BI
{
    /// <summary>
    /// Clase con metodos y definiciones para Exportar datos desde Dashboard TKPH
    /// </summary>
    public class bTkphExport
    {
        /// <summary>
        /// Llamada a instancia clase "Orquestador" que administra las llamadas desde otros objetos.
        /// </summary>
        Orquestador _data = Orquestador.Instance;

        /// <summary>
        /// Encapsulamiento de instancia para clase "bTkphExport"
        /// </summary>
         #region Encapsula Instancia
        private const string CLASS_NAME = "bTkphExport";

        private static readonly bTkphExport instance = new bTkphExport();
        private bTkphExport() { }
        public static bTkphExport Instance
        {
            get { return instance; }
        }
        #endregion


        /// <summary>
        /// Metodo para importar datos Histograma Tkph para exportar a Excel.
        /// </summary>
        /// <param name="flag">Valor que indica el tipo de dato a retornar.</param>
        /// <param name="idTurno">Valor correspondiente al turno.</param>
        /// <param name="fDesde">Fecha inicio para consulta de datos.</param>
        /// <param name="fHasta">Fecha termino para consulta de datos.</param>
        /// <param name="Objeto">Valor correspondiente a identificador de dato para realizar la busqueda</param>
        /// <returns>Retorna Interfaz de Lista de "vTkphHistogramaExp" con datos a exportar.</returns>
        public IList<vTkphHistogramaExp> getTkphHistogramaData(Int64 flag, Int64 idTurno, String fDesde, String fHasta, String Objeto)
        {
            IList<vTkphHistogramaExp> _list = new List<vTkphHistogramaExp>();

            try
            {
                _list = wTkphHistogramaExp.ConvertData(_data.getTkphHistogramaData(flag, idTurno, fDesde, fHasta, Objeto));
            }
            catch (Exception e)
            {
                throw e;
            }

            return _list;
        }


        /// <summary>
        /// Metodo para importar datos Tendencia Tkph para exportar a Excel.
        /// </summary>
        /// <param name="flag">Valor que indica el tipo de dato a retornar.</param>
        /// <param name="idTurno">Valor correspondiente al turno.</param>
        /// <param name="fDesde">Fecha inicio para consulta de datos.</param>
        /// <param name="fHasta">Fecha termino para consulta de datos.</param>
        /// <param name="Objeto">Valor correspondiente a identificador de dato para realizar la busqueda</param>
        /// <returns>Retorna Interfaz de Lista de "vTkphTendenciaExp" con datos a exportar.</returns>
        public IList<vTkphTendenciaExp> getTkphTendenciaData(Int64 flag, Int64 idTurno, String fDesde, String fHasta, String Objeto)
        {
            IList<vTkphTendenciaExp> _list = new List<vTkphTendenciaExp>();

            try
            {
                _list = wTkphTendenciaExp.ConvertData(_data.getTkphTendenciaData(flag, idTurno, fDesde, fHasta, Objeto));
            }
            catch (Exception e)
            {
                throw e;
            }

            return _list;
        }


        /// <summary>
        /// Metodo para importar datos Camión Tkph para exportar a Excel.
        /// </summary>
        /// <param name="flag">Valor que indica el tipo de dato a retornar.</param>
        /// <param name="idTurno">Valor correspondiente al turno.</param>
        /// <param name="fDesde">Fecha inicio para consulta de datos.</param>
        /// <param name="fHasta">Fecha termino para consulta de datos.</param>
        /// <param name="Objeto">Valor correspondiente a identificador de dato para realizar la busqueda</param>
        /// <returns>Retorna Interfaz de Lista de "vTkphCamionExp" con datos a exportar.</returns>
        public IList<vTkphCamionExp> getTkphCamionData(Int64 flag, Int64 idTurno, String fDesde, String fHasta, String Objeto)
        {
            IList<vTkphCamionExp> _list = new List<vTkphCamionExp>();

            try
            {
                _list = wTkphCamionExp.ConvertData(_data.getTkphCamionData(flag, idTurno, fDesde, fHasta, Objeto));
            }
            catch (Exception e)
            {
                throw e;
            }

            return _list;
        }

        /// <summary>
        /// Metodo para importar datos Operador Tkph para exportar a Excel.
        /// </summary>
        /// <param name="flag">Valor que indica el tipo de dato a retornar.</param>
        /// <param name="idTurno">Valor correspondiente al turno.</param>
        /// <param name="fDesde">Fecha inicio para consulta de datos.</param>
        /// <param name="fHasta">Fecha termino para consulta de datos.</param>
        /// <param name="Objeto">Valor correspondiente a identificador de dato para realizar la busqueda</param>
        /// <returns>Retorna Interfaz de Lista de "vTkphOperadorExp" con datos a exportar.</returns>
        public IList<vTkphOperadorExp> getTkphOperadorData(Int64 flag, Int64 idTurno, String fDesde, String fHasta, String Objeto)
        {
            IList<vTkphOperadorExp> _list = new List<vTkphOperadorExp>();

            try
            {
                _list = wTkphOperadorExp.ConvertData(_data.getTkphOperadorData(flag, idTurno, fDesde, fHasta, Objeto));
            }
            catch (Exception e)
            {
                throw e;
            }

            return _list;
        }

        /// <summary>
        /// Metodo para importar datos Destino Tkph para exportar a Excel.
        /// </summary>
        /// <param name="flag">Valor que indica el tipo de dato a retornar.</param>
        /// <param name="idTurno">Valor correspondiente al turno.</param>
        /// <param name="fDesde">Fecha inicio para consulta de datos.</param>
        /// <param name="fHasta">Fecha termino para consulta de datos.</param>
        /// <param name="Objeto">Valor correspondiente a identificador de dato para realizar la busqueda</param>
        /// <returns>Retorna Interfaz de Lista de "vTkphDestinoExp" con datos a exportar.</returns>
        public IList<vTkphDestinoExp> getTkphDestinoData(Int64 flag, Int64 idTurno, String fDesde, String fHasta, String Objeto)
        {
            IList<vTkphDestinoExp> _list = new List<vTkphDestinoExp>();

            try
            {
                _list = wTkphDestinoExp.ConvertData(_data.getTkphDestinoData(flag, idTurno, fDesde, fHasta, Objeto));
            }
            catch (Exception e)
            {
                throw e;
            }

            return _list;
        }

    }
}
