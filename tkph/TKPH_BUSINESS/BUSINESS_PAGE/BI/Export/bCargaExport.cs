﻿
using Com.Vmica.View.BI;
using Com.Vmica.View.BI.Export;
using Com.Vmica.Wrapper.BI;
using Com.Vmica.Wrapper.BI.Export;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TKPH_DATA;

namespace TKPH_BUSINESS.BUSINESS_PAGE.BI
{
    /// <summary>
    /// Clase con metodos y definiciones para Exportar datos desde Dashboard Carga
    /// </summary>
    public class bCargaExport
    {

        /// <summary>
        /// Llamada a instancia clase "Orquestador" que administra las llamadas desde otros objetos.
        /// </summary>
        Orquestador _data = Orquestador.Instance;

        /// <summary>
        /// Encapsulamiento de instancia para clase "bCargaExport"
        /// </summary>
        #region Encapsula Instancia
        private const string CLASS_NAME = "bCargaExport";

        private static readonly bCargaExport instance = new bCargaExport();
        private bCargaExport() { }
        public static bCargaExport Instance
        {
            get { return instance; }
        }
        #endregion


        /// <summary>
        /// Metodo para importar datos Histograma Carga para exportar a Excel.
        /// </summary>
        /// <param name="flag">Valor que indica el tipo de dato a retornar.</param>
        /// <param name="idTurno">Valor correspondiente al turno.</param>
        /// <param name="fDesde">Fecha inicio para consulta de datos.</param>
        /// <param name="fHasta">Fecha termino para consulta de datos.</param>
        /// <param name="Objeto">Valor correspondiente a identificador de dato para realizar la busqueda</param>
        /// <returns>Retorna Interfaz de Lista de "vCargaHistogramaExp" con datos a exportar.</returns>
        public IList<vCargaHistogramaExp> getCargaHistogramaData(Int64 flag, Int64 idTurno, String fDesde, String fHasta, String Objeto)
        {
            IList<vCargaHistogramaExp> _list = new List<vCargaHistogramaExp>();

            try
            {
                _list = wCargaHistogramaExp.ConvertData(_data.getCargaHistogramaData(flag, idTurno, fDesde, fHasta, Objeto));
            }
            catch (Exception e)
            {
                throw e;
            }

            return _list;
        }


        /// <summary>
        /// Metodo para importar datos Tendencia Carga para exportar a Excel.
        /// </summary>
        /// <param name="flag">Valor que indica el tipo de dato a retornar.</param>
        /// <param name="idTurno">Valor correspondiente al turno.</param>
        /// <param name="fDesde">Fecha inicio para consulta de datos.</param>
        /// <param name="fHasta">Fecha termino para consulta de datos.</param>
        /// <param name="Objeto">Valor correspondiente a identificador de dato para realizar la busqueda</param>
        /// <returns>Retorna Interfaz de Lista de "vCargaTendenciaExp" con datos a exportar.</returns>
        public IList<vCargaTendenciaExp> getCargaTendenciaData(Int64 flag, Int64 idTurno, String fDesde, String fHasta, String Objeto)
        {
            IList<vCargaTendenciaExp> _list = new List<vCargaTendenciaExp>();

            try
            {
                _list = wCargaTendenciaExp.ConvertData(_data.getCargaTendenciaData(flag, idTurno, fDesde, fHasta, Objeto));
            }
            catch (Exception e)
            {
                throw e;
            }

            return _list;
        }


        /// <summary>
        /// Metodo para importar datos Camión Carga para exportar a Excel.
        /// </summary>
        /// <param name="flag">Valor que indica el tipo de dato a retornar.</param>
        /// <param name="idTurno">Valor correspondiente al turno.</param>
        /// <param name="fDesde">Fecha inicio para consulta de datos.</param>
        /// <param name="fHasta">Fecha termino para consulta de datos.</param>
        /// <param name="Objeto">Valor correspondiente a identificador de dato para realizar la busqueda</param>
        /// <returns>Retorna Interfaz de Lista de "vCargaCamionExp" con datos a exportar.</returns>
        public IList<vCargaCamionExp> getCargaCamionData(Int64 flag, Int64 idTurno, String fDesde, String fHasta, String Objeto)
        {
            IList<vCargaCamionExp> _list = new List<vCargaCamionExp>();

            try
            {
                _list = wCargaCamionExp.ConvertData(_data.getCargaCamionData(flag, idTurno, fDesde, fHasta, Objeto));
            }
            catch (Exception e)
            {
                throw e;
            }

            return _list;
        }


        /// <summary>
        /// Metodo para importar datos Pala Carga para exportar a Excel.
        /// </summary>
        /// <param name="flag">Valor que indica el tipo de dato a retornar.</param>
        /// <param name="idTurno">Valor correspondiente al turno.</param>
        /// <param name="fDesde">Fecha inicio para consulta de datos.</param>
        /// <param name="fHasta">Fecha termino para consulta de datos.</param>
        /// <param name="Objeto">Valor correspondiente a identificador de dato para realizar la busqueda</param>
        /// <returns>Retorna Interfaz de Lista de "vCargaPalaExp" con datos a exportar.</returns>
        public IList<vCargaPalaExp> getCargaPalaData(Int64 flag, Int64 idTurno, String fDesde, String fHasta, String Objeto)
        {
            IList<vCargaPalaExp> _list = new List<vCargaPalaExp>();

            try
            {
                _list = wCargaPalaExp.ConvertData(_data.getCargaPalaData(flag, idTurno, fDesde, fHasta, Objeto));
            }
            catch (Exception e)
            {
                throw e;
            }

            return _list;
        }

    }
}
