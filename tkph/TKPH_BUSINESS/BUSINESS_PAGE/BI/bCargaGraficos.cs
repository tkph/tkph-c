﻿
using Com.Vmica.View.BI;
using Com.Vmica.Wrapper.BI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TKPH_DATA;

namespace TKPH_BUSINESS.BUSINESS_PAGE.BI
{
    /// <summary>
    /// Clase con metodos y definiciones para Graficos Dashboard TKPH
    /// </summary>
    public class bCargaGraficos
    {
        /// <summary>
        /// Llamada a instancia clase "Orquestador" que administra las llamadas desde otros objetos.
        /// </summary>
        Orquestador _data = Orquestador.Instance;

        /// <summary>
        /// Encapsulamiento de instancia para clase "bCargaGraficos"
        /// </summary>
         #region Encapsula Instancia
        private const string CLASS_NAME = "bCargaGraficos";

        private static readonly bCargaGraficos instance = new bCargaGraficos();
        private bCargaGraficos() { }
        public static bCargaGraficos Instance
        {
            get { return instance; }
        }
        #endregion


        /// <summary>
        /// Metodo para importar datos Carga Flota para generar Graficos.
        /// </summary>
        /// <param name="flag">Valor que indica el tipo de dato a retornar.</param>
        /// <param name="idTurno">Valor correspondiente al turno.</param>
        /// <param name="fDesde">Fecha inicio para consulta de datos.</param>
        /// <param name="fHasta">Fecha termino para consulta de datos.</param>
        /// <param name="Objeto">Valor correspondiente a identificador de dato para realizar la busqueda</param>
        /// <returns>Retorna Interfaz de Lista de "vCargaFlota".</returns>
        public IList<vCargaFlota> getCargaFlota(Int64 flag, Int64 idTurno, String fDesde, String fHasta, String Objeto)
        {
            IList<vCargaFlota> _list = new List<vCargaFlota>();

            try
            {
                _list = wCargaFlota.ConvertData(_data.getCargaFlota(flag, idTurno, fDesde, fHasta, Objeto));
            }
            catch (Exception e)
            {
                throw e;
            }

            return _list;
        }


        /// <summary>
        /// Metodo para importar datos Carga Tendencia para generar Graficos.
        /// </summary>
        /// <param name="flag">Valor que indica el tipo de dato a retornar.</param>
        /// <param name="idTurno">Valor correspondiente al turno.</param>
        /// <param name="fDesde">Fecha inicio para consulta de datos.</param>
        /// <param name="fHasta">Fecha termino para consulta de datos.</param>
        /// <param name="Objeto">Valor correspondiente a identificador de dato para realizar la busqueda</param>
        /// <returns>Retorna Interfaz de Lista de "vCargaTendencia".</returns>
        public IList<vCargaTendencia> getCargaTendencia(Int64 flag, Int64 idTurno, String fDesde, String fHasta, String Objeto)
        {
            IList<vCargaTendencia> _list = new List<vCargaTendencia>();

            try
            {
                _list = wCargaTendencia.ConvertData(_data.getCargaTendencia(flag, idTurno, fDesde, fHasta, Objeto));
            }
            catch (Exception e)
            {
                throw e;
            }

            return _list;
        }


        /// <summary>
        /// Metodo para importar datos Histograma Carga para generar Graficos.
        /// </summary>
        /// <param name="flag">Valor que indica el tipo de dato a retornar.</param>
        /// <param name="idTurno">Valor correspondiente al turno.</param>
        /// <param name="fDesde">Fecha inicio para consulta de datos.</param>
        /// <param name="fHasta">Fecha termino para consulta de datos.</param>
        /// <param name="Objeto">Valor correspondiente a identificador de dato para realizar la busqueda</param>
        /// <returns>Retorna Interfaz de Lista de "vCargaHistograma".</returns>
        public IList<vCargaHistograma> getCargaHistograma(Int64 flag, Int64 idTurno, String fDesde, String fHasta, String Objeto)
        {
            IList<vCargaHistograma> _list = new List<vCargaHistograma>();

            try
            {
                _list = wCargaHistograma.ConvertData(_data.getCargaHistograma(flag, idTurno, fDesde, fHasta, Objeto));
            }
            catch (Exception e)
            {
                throw e;
            }

            return _list;
        }


        /// <summary>
        /// Metodo para importar datos Ranking Carga para generar Graficos.
        /// </summary>
        /// <param name="flag">Valor que indica el tipo de dato a retornar.</param>
        /// <param name="idTurno">Valor correspondiente al turno.</param>
        /// <param name="fDesde">Fecha inicio para consulta de datos.</param>
        /// <param name="fHasta">Fecha termino para consulta de datos.</param>
        /// <param name="Objeto">Valor correspondiente a identificador de dato para realizar la busqueda</param>
        /// <returns>Retorna Interfaz de Lista de "vCargaRanking".</returns>
        public IList<vCargaRanking> getCargaRanking(Int64 flag, Int64 idTurno, String fDesde, String fHasta, String Objeto)
        {
            IList<vCargaRanking> _list = new List<vCargaRanking>();

            try
            {
                _list = wCargaRanking.ConvertData(_data.getCargaRanking(flag, idTurno, fDesde, fHasta, Objeto));
            }
            catch (Exception e)
            {
                throw e;
            }

            return _list;
        }

    }
}
