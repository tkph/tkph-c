﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Com.Vmica.View.ControlSistema
{
    [Serializable]
    public class vEstadisticas
    {
        public Int64 IdProceso { get; set; }
        public String Proceso { get; set; }
        public String Fecha { get; set; }
        public Int64 ProcesosOk { get; set; }
        public Int64 ProcesosError { get; set; }
        public Int64 Estado { get; set; }
        public Decimal Porcentaje { get; set; }
        public Int64 RegCargados { get; set; }
        public Int64 IdArea { get; set; }
        public String Area { get; set; }
    }
}
