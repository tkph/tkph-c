﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Com.Vmica.View
{
    /// <summary>
    /// Clase Base con atributos para mostrar Datos de Presión y Temperatura del Camión
    /// </summary>
    [Serializable]
    public class vHistoricoPT
    {
        public DateTime FECHA { get; set; }
        public String IDCAMION { get; set; }
        public String LOGICALID { get; set; }
        public Double TEMPERATURA { get; set; }
        public Double PRESION { get; set; }
        public String FechaDesde { get; set; }
        public String FechaHasta { get; set; }

        public string IdCamion { get; set; }

        public int Flag { get; set; }

        public object Fecha { get; set; }
    }
}
