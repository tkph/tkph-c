﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Com.Vmica.View
{
    /// <summary>
    /// Clase Base con atributos para mostrar Datos de Camión
    /// </summary>
    [Serializable]
    public class vCamionDatos
    {
        public String IdCamion { get; set; }
        public String Carga { get; set; }
        public String CargaEsperada { get; set; }
        public Int32 AlertaCarga { get; set; }
        public String VM { get; set; }
        public String DistRec { get; set; }
        public String TKPHOperacionalT { get; set; }
        public Int32 AlertaTKPH_T { get; set; }
        public String TKPHOperacionalD { get; set; }
        public Int32 AlertaTKPH_D { get; set; }
        public String TipoCalculo { get; set; }
        public String DescPosicionEjeT { get; set; }
        public String DescPosicionEjeD { get; set; }
        public String TKPHNominal_C { get; set; }
        public String Flota { get; set; }
        public String Turno { get; set; }
        public String Operador { get; set; }
        public String OpePala { get; set; }
        public String Pala { get; set; }
        public String CargaActual { get; set; }
        public String VelocidadActual { get; set; }
        public String id_destino { get; set; }
        public String carga_turno { get; set; }
        public String km_turno { get; set; }
        public String TKPH_avg_D { get; set; }
        public String TKPH_avg_T { get; set; }
        public String f_ult_lectura { get; set; }
        public String Temp_ambiente { get; set; }
        public String fecha_fin_de_ciclo { get; set; }
        public Int32 aler_not_alto { get; set; }
        public Int32 aler_not_medio { get; set; }
    }
}
