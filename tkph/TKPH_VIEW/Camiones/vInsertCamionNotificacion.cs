﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Com.Vmica.View
{
    /// <summary>
    /// Clase Base con atributos para persistencia de Datos de notificación
    /// </summary>
    public class vInsertCamionNotificacion
    {
        public Int64 ID_CAMION { get; set; }
        public String COMENTARIO { get; set; }
    }
}
