﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Com.Vmica.View
{
    [Serializable]
    public class vProduccionUC
    {
        public DateTime Fecha { get; set; }
        public Double OxidoRel { get; set; }
        public Double OxidoFor { get; set; }
        public Double SulfuroRel { get; set; }
        public Double SulfuroFor { get; set; }
        public Int32 Flag { get; set; }
        public Int32 Status { get; set; } 
    }  
}
