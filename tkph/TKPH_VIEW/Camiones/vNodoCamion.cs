﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Vmica.View
{
    [Serializable]
    public class vNodoCamion
    {
        public Int64 IdCamion { get; set; }
        public String Flota { get; set; }
        public Int64 Valor { get; set; }
    }
}
