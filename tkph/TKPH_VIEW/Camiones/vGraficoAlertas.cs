﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Com.Vmica.View
{
    /// <summary>
    /// Clase Base con atributos para Generar graficos de alertas
    /// </summary>
    [Serializable]
    public class vGraficoAlertas
    {
        public String Tagname { get; set; }
        public DateTime Fecha { get; set; }
        public String Units { get; set; }
        public Double Value { get; set; }
        public Int64 RangoInferior { get; set; }
        public Int64 RangoSuperior { get; set; }
        public Int32 Alerta { get; set; }
        public Int32 Flag { get; set; }

    }
}
