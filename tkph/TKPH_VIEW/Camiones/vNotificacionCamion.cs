﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Vmica.View
{
    /// <summary>
    /// Clase Base con atributos para mostrar Datos de Notificaciones del Camión
    /// </summary>
    public class vNotificacionCamion
    {
        public String Fecha { get; set; }
        public String IdHora { get; set; }
        public String IdCamion { get; set; }
        public String Operador { get; set; }
        public String DescAlarma { get; set; }
        public Int64 RowN { get; set; }
    }
}
