﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Vmica.View
{
    /// <summary>
    /// Clase Base con atributos para mostrar Datos de Estado Neumaticos Camión
    /// </summary>
    [Serializable]
    public class vEstadoNeumatico
    {
        public Int64 Valor { get; set; }
        public String Alerta { get; set; }
        public Int64 Total { get; set; }
    }
}
