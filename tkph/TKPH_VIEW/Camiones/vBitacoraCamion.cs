﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Vmica.View
{
    /// <summary>
    /// Clase Base con atributos para mostrar Datos de Bitacora Camión
    /// </summary>
    [Serializable]
    public class vBitacoraCamion
    {
        public string Fecha { get; set; }
        public DateTime IdDia { get; set; }
        public DateTime IdHora { get; set; }
        public int RowN { get; set; }
        public string Comentario { get; set; }
    }
}
