﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Vmica.View
{
    [Serializable]
    public class vComparaTonelada
    {
        public String Mes { get; set; }
        public String Destino { get; set; }
        public String Carga { get; set; }
    }
}
