﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Com.Vmica.View
{
    /// <summary>
    /// Clase Base con atributos para mostrar Datos de Tkph y Carga del Camión
    /// </summary>
    [Serializable]
    public class vHistorico
    {
        public DateTime Fecha { get; set; }
        public Int64 Nonimal { get; set; }
        public Int64 Operacional { get; set; }
        public String IdCamion { get; set; }
        public Int64 Flag { get; set; }
        public String FechaDesde { get; set; }
        public String FechaHasta { get; set; }

    }
}
