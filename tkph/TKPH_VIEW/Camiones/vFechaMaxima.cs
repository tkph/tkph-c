﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Com.Vmica.View
{
    /// <summary>
    /// Clase Base con atributos para mostrar Datos de Fechas minimas y maximas de información Camión
    /// </summary>
    [Serializable]
    public class vFechaMaxima
    {
        public String IdCamion { get; set; }
        public String FUAME { get; set; }
        public String FUAMI { get; set; }
    }
}
