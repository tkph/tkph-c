﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Com.Vmica.View
{
    [Serializable]
    public class vProduccionCatodos
    {
        public DateTime Fecha { get; set; }
        public Double Esox { get; set; }
        public Double Off1 { get; set; }
        public Double Scrap { get; set; }
    }
}
