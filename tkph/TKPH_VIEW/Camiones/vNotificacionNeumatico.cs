﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Vmica.View
{
    [Serializable]
    public class vNotificacionNeumatico
    {
        public String Fecha { get; set; }
        public String Hora { get; set; }
        public String Equipo { get; set; }
        public String Operador { get; set; }
        public String Alarma { get; set; }
    }
}
