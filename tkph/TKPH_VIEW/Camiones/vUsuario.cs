﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Vmica.View
{
    /// <summary>
    /// Clase Base con atributos para mostrar Datos de Usuario de la aplicación
    /// </summary>
    [Serializable]
    public class vUsuario
    {
        public String Usuario { get; set; }
        public String Password { get; set; }
        public Int64 Nivel { get; set; }
        public String Status { get; set; }
        public Boolean Error { get; set; }
    }
}
