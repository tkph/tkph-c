﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Vmica.View
{
    /// <summary>
    /// Clase Base con atributos para mostrar Datos de Ciclo Camión
    /// </summary>
    [Serializable]
    public class vCamionCiclo
    {
        public String IdCamion { get; set; }
        public String IdCamionDispatch { get; set; }
        public DateTime Fecha { get; set; }
        public DateTime Hora { get; set; }
        public Double CargaTotal { get; set; }
        public Int64 Carga { get; set; }
        public Int64 Ciclo { get; set; }
        public Double K2 { get; set; }
        public Double Vm { get; set; }
        public Double Qm { get; set; }
        public Double K1 { get; set; }
        public Double F { get; set; }
        public Double TkphOperacional { get; set; }
        public String TipoCalculo { get; set; }
        public String DescPosicionEje { get; set; }
        public Double TkphNominalC { get; set; }
    }
}
