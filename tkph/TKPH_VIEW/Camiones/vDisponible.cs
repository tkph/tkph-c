﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Com.Vmica.View
{
    /// <summary>
    /// Clase Base con atributos para mostrar Disponibilidad Camión
    /// </summary>
    [Serializable]
    public class vDisponible
    {
        public String Fecha { get; set; }
        public String Turno { get; set; }
        public String Flota { get; set; }
        public Double Operativo { get; set; }
        public Double Armado { get; set; }
        public Double Porcentaje { get; set; }
    }
}
