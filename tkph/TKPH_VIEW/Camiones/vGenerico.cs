﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Com.Vmica.View
{
    [Serializable]
    public class vGenerico
    {
        public String TipoParam { get; set; }
        public String OrigenParam { get; set; }
        public String DescOrigenParam { get; set; }
        public String DescInternaParam { get; set; }
        public String DescTipoParam { get; set; }
        public String Gerencia { get; set; }
        public Int64 Nivel { get; set; }
        public String Estado { get; set; }
        public Boolean StatusRojo { get; set; }
        public Boolean StatusAmarillo { get; set; }
        public Boolean StatusVerde { get; set; }
        public Boolean StatusGris { get; set; }
    }
}
