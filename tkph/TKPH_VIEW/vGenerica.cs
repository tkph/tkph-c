﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Com.Vmica.View
{
    /// <summary>
    /// Clase Base con atributos genericos para creacion de datos para Graficos
    /// </summary>
    [Serializable]
    public class vGenerica
    {
        public dynamic Valor1 { get; set; }
        public dynamic Valor2 { get; set; }
        public dynamic Valor3 { get; set; }
        public dynamic Valor4 { get; set; }
        public dynamic Valor5 { get; set; }
        public dynamic Valor6 { get; set; }
        public dynamic Valor7 { get; set; }
        public dynamic Valor8 { get; set; }
        public dynamic Valor9 { get; set; }
        public dynamic Valor10 { get; set; }
    }
}
