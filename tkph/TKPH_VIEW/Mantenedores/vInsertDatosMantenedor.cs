﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Com.Vmica.View.Mantenedores
   
{
    /// <summary>
    /// Clase Base con atributos para Persistencia de valores mantenedores de Configuración
    /// </summary>
    public class vInsertDatosMantenedor
    {
        public String Ident { get; set; }
        public String ValorT { get; set; }
        public String Afisc { get; set; }
    }
}
