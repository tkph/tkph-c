﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Com.Vmica.View.Mantenedores
{
    /// <summary>
    /// Clase Base con atributos para mostrar datos de valores en mantenedores de Configuración
    /// </summary>
    public class vMantenedorCarga
    {
        public String Mes { get; set; }
        public DateTime Dia { get; set; }
        public Int64 Item { get; set; }
        public String Tipo { get; set; }
        public String Nivel_1 { get; set; }
        public String Nivel_2 { get; set; }
        public String Nivel_3 { get; set; }
        public Double valor { get; set; }
        public String AFiscal { get; set; }
        public String Mes_pl { get; set; }
        public String Id_tipo { get; set; }
        public String Id_nivel_1 { get; set; }
        public String Unidad { get; set; }
        public String Period { get; set; }
    }
}
