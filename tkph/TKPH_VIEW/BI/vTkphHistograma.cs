﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Com.Vmica.View.BI
{
    /// <summary>
    /// Clase Base con atributos para mostrar Datos de Histograma TKPH
    /// </summary>
    [Serializable]
    public class vTkphHistograma
    {
        public String FlotaCamion { get; set; }
        public Int64 Valor { get; set; }
        public String Tipo { get; set; }
        public Int64 Orden { get; set; }
        public Int64 AlarmaPunto { get; set; }
    }
}
