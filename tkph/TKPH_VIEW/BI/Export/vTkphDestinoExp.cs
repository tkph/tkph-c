﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Com.Vmica.View.BI.Export
{
    /// <summary>
    /// Clase Base con atributos para exportar Datos de TKPH por Destino
    /// </summary>
    [Serializable]
    public class vTkphDestinoExp
    {    
        public DateTime Fecha { get; set; }
        public String Flota { get; set; }
        public String IdDestino { get; set; }
        public Double Operacional { get; set; }
        public Double Nominal { get; set; }
    }
}
