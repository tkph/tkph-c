﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Com.Vmica.View.BI.Export
{
    /// <summary>
    /// Clase Base con atributos para exportar Datos de TKPH  por Camión
    /// </summary>
    [Serializable]
    public class vTkphCamionExp
    {
        public DateTime Fecha { get; set; }
        public DateTime FechaCarguio { get; set; }
        public String Flota { get; set; }
        public Int64 IdCamion { get; set; }
        public Int64 Carga { get; set; }
        public Double CargaMedia { get; set; }
        public Double TiempoTotal { get; set; }
        public Double ConstanteK2 { get; set; }
        public Double VelocidadMedia { get; set; }
        public Int64 Temperatura { get; set; }
        public Double Operacional { get; set; }
        public Double Nominal { get; set; }

    }
}
