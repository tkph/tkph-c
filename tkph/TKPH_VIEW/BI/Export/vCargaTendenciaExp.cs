﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Com.Vmica.View.BI.Export
{
    /// <summary>
    /// Clase Base con atributos para exportar Datos de Carga por Tendencia
    /// </summary>
    [Serializable]
    public class vCargaTendenciaExp
    {
        public String IdCamion { get; set; }
        public String FlotaCamion { get; set; }
        public DateTime Fecha { get; set; }
        public Int64 Carga { get; set; }
        public Int64 Nominal { get; set; }
    }
}
