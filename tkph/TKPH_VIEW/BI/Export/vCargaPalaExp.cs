﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Com.Vmica.View.BI.Export
{
    /// <summary>
    /// Clase Base con atributos para exportar Datos de Carga Pala
    /// </summary>
    [Serializable]
    public class vCargaPalaExp
    {
        public DateTime Fecha { get; set; }
        public String Pala { get; set; }
        public Int64 Carga { get; set; }
        public String Flota { get; set; }
    }
}
