﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Com.Vmica.View.BI
{
    /// <summary>
    /// Clase Base con atributos para mostrar Datos de Tendencia Carga 
    /// </summary>
    [Serializable]
    public class vCargaTendencia
    {
        public String FlotaCamion { get; set; }
        public DateTime Fecha { get; set; }
        public Int64 Valor { get; set; }
        public String Tipo { get; set; }
    }
}
