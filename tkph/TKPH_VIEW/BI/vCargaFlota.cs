﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Com.Vmica.View.BI
{
    /// <summary>
    /// Clase Base con atributos para mostrar Datos de Carga por Flota
    /// </summary>
    [Serializable]
    public class vCargaFlota
    {
        public String FlotaCamion { get; set; }
        public Double Promedio { get; set; }
        public Int64 CargaTotal { get; set; }
        public Double Porcentaje { get; set; }
        public Int64 AlarmaPunto { get; set; }
    }
}
