﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Com.Vmica.View.BI
{
    /// <summary>
    /// Clase Base con atributos para mostrar Datos de Ranking Carga 
    /// </summary>
    [Serializable]
    public class vCargaRanking
    {
        public String FlotaCamion { get; set; }
        public Double Valor { get; set; }
        public String Tipo { get; set; }
        public String Flota { get; set; }
        public Int64 AlarmaPunto { get; set; }

    }
}
