﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Com.Vmica.View.BI
{
    /// <summary>
    /// Clase Base con atributos para mostrar Datos de Tendencia TKPH
    /// </summary>
    [Serializable]
    public class vTkphTendencia
    {
        public String FlotaCamion { get; set; }
        public DateTime Fecha { get; set; }
        public Double Promedio { get; set; }
        public Int64 Nominal { get; set; }
        public Double Porcentaje { get; set; }
        public Int64 AlarmaPunto { get; set; }
    }
}
