﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Com.Vmica.View
{
    /// <summary>
    /// Clase Base con atributos para Logueo de usuarios en la aplicación
    /// </summary>
    [Serializable]
    public class vLogin
    {
        public String Usuario { get; set; }
        public String Password { get; set; }
        public String Nombre { get; set; }
        public DateTime Vigencia { get; set; }
        public Boolean Vigente { get; set; }
    }
}
