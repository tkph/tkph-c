﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Tkph.aspx.cs" Inherits="TKPH_WEB.Camiones.Tkph" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="BodyC" runat="server">

    <table border="0" width="90%" align="center">
        <tr>
            <td width="50%" class="alineado_up_centro">
                <br />
                <div class="SubTitAppBlanco">
                    Ranking Tkph
                </div>
                <br />
                <div style="background-color: gray; width: 95%; height: 570px; overflow-y: scroll; scrollbar-face-color: aliceblue;" class="BordeForm">
                    <br />
                    Filtrar por Flota:
                    <asp:DropDownList runat="server" ID="cboFlota" AutoPostBack="true" OnSelectedIndexChanged="cboFlota_SelectedIndexChanged" ViewStateMode="Enabled" CssClass="InputForm" Width="200px" />
                    <br />
                    <br />
                    <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="cboFlota" EventName="SelectedIndexChanged" />
                        </Triggers>
                        <ContentTemplate>
                            <asp:GridView runat="server" ID="gvEventosTkph" Width="80%" HorizontalAlign="Center" AutoGenerateColumns="false" OnRowDataBound="gvEventosTkph_RowDataBound" HeaderStyle-CssClass="Acum_agua" BorderColor="DarkGray">
                                <Columns>
                                    <asp:BoundField HeaderText="Camión" DataField="Camion" />
                                    <asp:BoundField HeaderText="Q Eventos" DataField="qEventos" />
                                    <asp:TemplateField HeaderText="Tendencia">
                                        <ItemTemplate>
                                            <asp:Chart runat="server" ID="chTendencia" Width="300" Height="30">
                                                <ChartAreas>
                                                    <asp:ChartArea Name="cArea" />
                                                </ChartAreas>
                                            </asp:Chart>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <br />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </td>
            <td width="50%" class="alineado_up_centro">
                <br />
                <div class="SubTitAppBlanco">Tkph por Destino</div>
                <br />
                <div style="background-color: gray; width: 95%;" class="BordeForm">
                    <br />
                    <asp:GridView runat="server" ID="gvDestinos" Width="80%" HorizontalAlign="Center" AutoGenerateColumns="false" OnRowCommand="gvDestinos_RowCommand" DataKeyNames="Destino" HeaderStyle-CssClass="Acum_agua" BorderColor="DarkGray" ShowHeader="false" GridLines="None" OnRowDataBound="gvDestinos_RowDataBound">
                        <Columns>
                            <asp:ButtonField HeaderText="Destino" DataTextField="Destino" ItemStyle-HorizontalAlign="Left" />
                            <asp:TemplateField ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblPtkph" CssClass="SubTitAppBlanco" />
                                    <asp:ImageButton runat="server" ID="imgPtkph" ImageUrl="~/StyleSheet/Image/bV.png" Width="200" Height="16" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <br />
                    <asp:UpdatePanel runat="server" ID="upChartDestino" UpdateMode="Conditional">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="gvDestinos" EventName="RowCommand" />
                        </Triggers>
                        <ContentTemplate>
                            <asp:Chart runat="server" ID="chTkphDestino" Width="500" Height="280">
                                <Titles>
                                    <asp:Title Text="" />
                                </Titles>
                                <ChartAreas>
                                    <asp:ChartArea Name="cArea" />
                                </ChartAreas>
                            </asp:Chart>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </td>
        </tr>
    </table>

</asp:Content>
