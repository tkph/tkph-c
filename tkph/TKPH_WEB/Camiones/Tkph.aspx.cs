﻿using TKPH_BUSINESS.COMUN;
using TKPH_WEB.Comun;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;

namespace TKPH_WEB.Camiones
{
    public partial class Tkph : System.Web.UI.Page
    {



        IList<Eventos> Eve = new List<Eventos>();
        IList<Destinos> Des = new List<Destinos>();
        IList<DestinoTkph> DesTkph = new List<DestinoTkph>();
        List<Eventos> lstEventos;

        String[] Flotas = new String[5];
        String[] Destinos = new String[10];




        protected void Page_Load(object sender, EventArgs e)
        {
           
            if (!IsPostBack)
            {
                CrearData();

                HttpContext.Current.Session["Titulo"] = "Tkph Mes Actual";
                HttpContext.Current.Session["SubTitulo"] = "";

                //if (Request.QueryString["idc"] == null)
                //{
                //    Response.Redirect("DetalleCamion.aspx");
                //}

                gvEventosTkph.DataSource = lstEventos.ToList();
                gvEventosTkph.DataBind();

                cboFlota.DataSource = Flotas;
                cboFlota.DataBind();
                cboFlota.Items.Insert(0, "Todas las Flotas");


                gvDestinos.DataSource = Des.ToList();
                gvDestinos.DataBind();

               

                graficoDestino(Des.First().Destino);



            }



        }

        protected void cboFlota_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (cboFlota.SelectedValue.ToString() == "Todas las Flotas")
            {
                lstEventos = Eve.ToList();
            }
            else
            {
                lstEventos = Eve.Where(f => f.Flota.Equals(cboFlota.SelectedValue.ToString())).ToList();
               
            }
            
                 lstEventos = lstEventos.OrderByDescending(o => o.qEventos).ToList();
            gvEventosTkph.DataSource = lstEventos.ToList();
            gvEventosTkph.DataBind();
        }

        public void CrearData()
        {
            Random rnd = new Random(DateTime.Now.Millisecond);

            Flotas[0] = "CAT797B";
            Flotas[1] = "CAT797F";
            Flotas[2] = "CAT793F";
            Flotas[3] = "KOMATSU930E";
            Flotas[4] = "KOMATSU960E";

            for (int i = 120; i < 140; i++)
            {
                Eventos veve = new Eventos();
                veve.Camion = i;
                veve.qEventos = rnd.Next(1, 30);
                veve.Flota = Flotas[rnd.Next(0, 4)];
                Eve.Add(veve);
            }

            lstEventos = Eve.OrderByDescending(o => o.qEventos).ToList();


            Destinos[0] = "Chancador 1";
            Destinos[1] = "Chancador 2";
            Destinos[2] = "Chancador 3";
            Destinos[3] = "Chancador 4";
            Destinos[4] = "Chancador 5";
            Destinos[5] = "Botadero 1";
            Destinos[6] = "Botadero 2";
            Destinos[7] = "Botadero 3";
            Destinos[8] = "Botadero 4";

            for (int i = 0; i < 9; i++)
            {
                Destinos Dest = new Destinos();
                Dest.Destino = Destinos[i];
                Dest.Porcentaje = rnd.Next(10, 100);
                Des.Add(Dest);
            }


            Des = Des.OrderByDescending(o => o.Porcentaje).ToList();

        }


        #region ViewState: Manejo de variables estables en view State
        protected override object SaveViewState()
        {
            object[] SavedState = new object[18];

            SavedState[0] = base.SaveViewState();

            if (Eve != null) SavedState[1] = Serializador.SerializeObjeto<Eventos[]>(Eve.ToArray());
            if (Flotas != null) SavedState[2] = Flotas;


            return SavedState;
        }

        protected override void LoadViewState(object SavedState)
        {
            if (SavedState != null)
            {
                object[] state = (object[])SavedState;

                if (state[0] != null) base.LoadViewState(state[0]);
                if (state[1] != null) Eve = Serializador.DesSerializeObjeto<Eventos[]>((string)state[1]).ToList();
                if (state[2] != null) Flotas = (String[])state[2];
            }
        }
        #endregion

        protected void gvEventosTkph_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType.ToString().Equals("DataRow"))
            {
                Chart graf = (Chart)e.Row.FindControl("chTendencia");
                CreaTendencia(12, graf);
            }
        }


        private void CreaTendencia(Int64 Puntos, Chart Grafico)
        {
            try
            {
                Random r = new Random(DateTime.Now.Millisecond);
                Series _slin1 = new Series();

                _slin1.ChartType = SeriesChartType.Line;
                
                for (int i = 0; i < Puntos; i++)
                {
                    _slin1.Points.Add(r.Next(1,5));
                }

                Grafico.Series.Add(_slin1);

                AjustaGrafico(Grafico);


            }
            catch (Exception e)
            {
                throw e;
            }
        }


        protected void AjustaGrafico(Chart Grafico)
        {
            Grafico.ChartAreas[0].AxisX.Interval = 1;
            Grafico.ChartAreas[0].AxisX.MajorGrid.Enabled = false;
            Grafico.ChartAreas[0].AxisY.MajorGrid.Enabled = false;


            Grafico.ChartAreas[0].AxisY.Enabled = AxisEnabled.False;
            Grafico.ChartAreas[0].AxisX.Enabled = AxisEnabled.False;

            

            //Grafico.ChartAreas[0].AxisY.LabelStyle.Enabled = false;
            //Grafico.ChartAreas[0].AxisX.LabelStyle.Enabled = false;

            Grafico.Series[0].BorderWidth = 2;
            //Grafico.Series[0].MarkerBorderWidth = 2;


            Grafico.ChartAreas[0].BackColor = Color.Gray; //Color.FromArgb(255, 29, 29, 29);
            Grafico.BackColor = Color.Gray; //Color.FromArgb(255, 29, 29, 29);
            Grafico.Series[0].Color = Color.White; //Color.FromArgb(255, 144, 198, 83);


            //Grafico.Series[0].MarkerColor = Color.FromArgb(255, 255, 215, 110); //Color.FromArgb(255, 68, 134, 244);
            Grafico.Series[0].MarkerBorderColor = Color.FromArgb(255, 138, 198, 211);
            Grafico.Series[0].MarkerBorderWidth = 1;
            Grafico.Series[0].MarkerStyle = MarkerStyle.Circle;
            Grafico.Series[0].MarkerSize = 3;


        }

        protected void gvDestinos_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int currentRowIndex = int.Parse(e.CommandArgument.ToString());
            String Destino = gvDestinos.DataKeys[currentRowIndex].Value.ToString();

            graficoDestino(Destino);
           
        }


        protected void graficoDestino(String Destino)
        {
            chTkphDestino.Titles[0].Text = "Destino " + Destino;
            Random rnd = new Random(DateTime.Now.Millisecond);

            Series S1 = new Series();
            Series S2 = new Series();

            S1.ChartType = SeriesChartType.Line;
            S2.ChartType = SeriesChartType.Line;

            S1.Name = "Tkph Esperado";
            S2.Name = "Tkph Nominal";

  
            for (int i = 0; i < 5; i++)
            { 
                DestinoTkph DT = new DestinoTkph();

                DT.Flota = Flotas[i].ToString();
                DT.Necesario = rnd.Next(500, 2500);
                DT.Nominal = rnd.Next(500, 2500);
                DesTkph.Add(DT);
            }


            int cont = 0;

            foreach (DestinoTkph row in DesTkph)
            {
                S1.Points.Add(row.Necesario);
                var p1 = S1.Points[cont];
                p1.AxisLabel = row.Flota;
                cont++;
            }


            cont = 0;

            foreach (DestinoTkph row in DesTkph)
            {
                S2.Points.Add(row.Nominal);
                var p1 = S2.Points[cont];
                p1.AxisLabel = row.Flota;
                cont++;
            }


            chTkphDestino.Series.Add(S1);
            chTkphDestino.Series.Add(S2);

            chTkphDestino.ChartAreas[0].AxisX.MajorGrid.Enabled = false;
            chTkphDestino.ChartAreas[0].AxisY.MajorGrid.LineColor = Color.DarkGray;
            chTkphDestino.ChartAreas[0].AxisX.LabelStyle.Angle = -45;

            chTkphDestino.ChartAreas[0].AxisY.TitleForeColor = Color.White;
            chTkphDestino.ChartAreas[0].AxisY.Title = "TKPH";

            chTkphDestino.BackColor = Color.Gray;
            chTkphDestino.ChartAreas[0].BackColor = Color.Gray;

            chTkphDestino.Titles[0].ForeColor = Color.LightGray;

            chTkphDestino.Series[0].BorderWidth = 2;
            chTkphDestino.Series[1].BorderWidth = 2;
            chTkphDestino.Series[1].BorderDashStyle = ChartDashStyle.Dash;

            chTkphDestino.Series[0].Color = Color.FromArgb(255, 138, 198, 211);
            chTkphDestino.Series[1].Color = Color.FromArgb(255, 255, 215, 110);


            chTkphDestino.Legends.Add("Ley");
            chTkphDestino.Legends[0].Docking = Docking.Bottom;
            chTkphDestino.Legends[0].Alignment = StringAlignment.Center;
            chTkphDestino.Legends[0].LegendStyle = LegendStyle.Table;
            chTkphDestino.Legends[0].BackColor = Color.Gray;

            chTkphDestino.ChartAreas[0].AxisY.LabelStyle.Font = new System.Drawing.Font("Calibri", 7);
            chTkphDestino.ChartAreas[0].AxisX.LabelStyle.Font = new System.Drawing.Font("Calibri", 7);
            chTkphDestino.ChartAreas[0].AxisX2.LabelStyle.Font = new System.Drawing.Font("Calibri", 7);


        }

        protected void gvDestinos_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType.ToString().Equals("DataRow"))
            {
                Int64 porc;
                porc = (Int64)DataBinder.Eval(e.Row.DataItem, "Porcentaje");

                 Label lbl = (Label)e.Row.FindControl("lblPtkph");
                 lbl.Text = porc.ToString()+"%";

                ImageButton imag = (ImageButton)e.Row.FindControl("imgPtkph");
                imag.Height = 16;
                imag.Width = int.Parse(porc.ToString()) * 2;
                
 
                if (porc < 40)
                {
                    imag.ImageUrl = "~/StyleSheet/Image/bV.png";
                }
                else if ((porc > 41) && (porc < 79))
                {
                    imag.ImageUrl = "~/StyleSheet/Image/bA.png";
                }
                else
                {
                    imag.ImageUrl = "~/StyleSheet/Image/bR.png";
                }

                
            }
        }

    }


    
    // Clases //
    [Serializable]
    public class Eventos
    {
        public Int64 Camion { get; set; }
        public Int64 qEventos { get; set; }
        public String Flota { get; set; }
    }


    public class Destinos
    {
        public String Destino { get; set; }
        public Int64 Porcentaje { get; set; }
    }


    public class DestinoTkph
    {
        public String Flota { get; set; }
        public Int64 Necesario { get; set; }
        public Int64 Nominal { get; set; }
    }


    // Fin Clases //


}