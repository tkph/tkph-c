﻿using Com.Vmica.View;
using TKPH_BUSINESS.BUSINESS_PAGE;
using TKPH_BUSINESS.BUSINESS_PAGE.Neumatico;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;
using TKPH_BUSINESS.COMUN;
using TKPH_WEB.Comun;
using TKPH_BUSINESS.BUSINESS_PAGE.Camiones;
using Com.Vmica.View.Camiones;
using System.Data;
using System.IO;
using TKPH_BUSINESS.BUSINESS_PAGE.Mantenedores;
using Com.Vmica.View.Mantenedores;
using System.Web.Services;
using System.Web.Script.Services;

namespace TKPH_WEB.Camiones
{
    /// <summary>
    /// Pantalla Vista Detalle Camión.
    /// </summary>
    public partial class DetalleCamion : System.Web.UI.Page
    {

        // Llamada a instancia de Objeto "bMantenedorBudget"
        bMantenedorBudget _dataBudget = bMantenedorBudget.Instance;

        // Llamada a instancia de Objeto "bReporteThree"
        private bReporteThree _business = bReporteThree.Instance;


        public static IList<vMantenedorCarga> _DatosMant;

        vCamionSingle _dataCamion;
       
        bNivel4 _business1 = bNivel4.Instance;
        bReporteTwo _business3 = new bReporteTwo();
        bCamionNotificacion _business4 = bCamionNotificacion.Instance;
        bInsertCamionNotificacion _business5 = bInsertCamionNotificacion.Instance;
        bDetalleCamion _business2 = bDetalleCamion.Instance;
        IList<vCamionDetails> _Single;
        IList<vCamionNotificacion> listCamionNotificacion;
        IList<vCamionNotificacion> listAUX;

        String[] Notif = new String[5];

        public Double PorcentajeEjeDVAR;
        public Double PorcentajeEjeDVAR2;

        public String IdCamion;


        /// <summary>
        /// Metodo de carga Inicial de la Página
        /// </summary>
        /// <param name="sender">La fuente del evento.</param>
        /// <param name="e">La Instancia que contiene los datos del evento.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                HttpContext.Current.Session["Titulo"] = "Detalle Camión";

                if (HttpContext.Current.Session["pro-camion"] == null && Request.QueryString["nc"] == null)
                {
                    Response.Redirect("Flota.aspx");
                }
                else
                {
                    if (Request.QueryString["nc"] != null)
                    {
                        HttpContext.Current.Session["pro-camion"] = Request.QueryString["nc"].ToString();
                    }

                    lblAct.Text = "Actualizado: " + DateTime.Now.ToString("HH:mm:ss");

                    vStatusRequest _req = new vStatusRequest()
                    {
                        IdCamion = HttpContext.Current.Session["pro-camion"].ToString(),
                        DescripcionFlota = ""
                    };

                    try
                    {

                        _Single = _business3.GetCamionSingle(new vStatusRequest() { AlertaGeneral = "" }).Where(f => f.Alerta != 4).ToList();

                        _Single = _Single.OrderBy(o => o.IdCamion).ToList();

                        ddlCamiones.DataTextField = "IdCamion";
                        ddlCamiones.DataValueField = "IdCamion";
                        ddlCamiones.DataSource = _Single;

                        ddlCamiones.DataBind();

                        ddlCamiones.SelectedValue = HttpContext.Current.Session["pro-camion"].ToString();

                        CargarData();

                    }
                    catch (Exception ex)
                    {
                        lblMen.Text = ex.Message.ToString();
                        MenPOP.Show();
                    }
                }
            }
        }


        /// <summary>
        /// Confirms that an <see cref="T:System.Web.UI.HtmlControls.HtmlForm" /> control is rendered for the specified ASP.NET server control at run time.
        /// </summary>
        /// <param name="control">The ASP.NET server control that is required in the <see cref="T:System.Web.UI.HtmlControls.HtmlForm" /> control.</param>
        public override void VerifyRenderingInServerForm(Control control)
        {
            //base.VerifyRenderingInServerForm(control);
        }

        /// <summary>
        /// Ejecuta procesos de carga de datos para objetos presentados en la GUI
        /// </summary>
        public void CargarData()
        {
            vStatusRequest _req = new vStatusRequest()
            {
                IdCamion = HttpContext.Current.Session["pro-camion"].ToString(),
                DescripcionFlota = ""
            };

            try
            {

                vFechaMaxima _fecha = _business.GetFechasMaximas(_req.IdCamion);

                vCamionDatos _Dato = _business.GetCamionDato(_req.IdCamion, _req.DescripcionFlota);

                _DatosMant = _dataBudget.GetDatosBudget(1, 1, "", "12", "");

                if (_Dato.aler_not_medio.Equals(1))
                {
                    imgAlertaMedio.ImageUrl = "../Imagenes/Iconos/Alerta_Notif_Amarilla.gif";
                    //imgAlertaMedio.Visible = true;
                }
                else
                {
                    imgAlertaMedio.Visible = false;
                }


                if (_Dato.aler_not_alto.Equals(1))
                {
                    imgAlertaAlto.ImageUrl = "../Imagenes/Iconos/Alerta_Notif_Roja.gif";
                    //imgAlertaAlto.Visible = true;
                    imgAlertaMedio.Visible = false;
                }
                else
                {
                    imgAlertaAlto.Visible = false;
                }

                Random rnd = new Random();

                lblULectura.Text = _fecha.FUAME;
                lblUCiclo.Text = _Dato.fecha_fin_de_ciclo;

                lblCamion.Text = "Camión: " + _Dato.IdCamion;
                lblFlota.Text = _Dato.Flota;
                lblOperador.Text = _Dato.Operador;
                lblPala.Text = _Dato.Pala;
                lblDestino.Text = _Dato.id_destino;
                lblTurno.Text = _Dato.Turno;
                lblCargaTurno.Text = Math.Round(Convert.ToDouble(_Dato.carga_turno), 2).ToString();
                lblKmTurno.Text = Math.Round(Convert.ToDouble(_Dato.km_turno), 0).ToString();


                lblCarga.Text = _Dato.Carga;
                lblCargaNom.Text = _Dato.CargaEsperada;
                lblCarga.ForeColor = _business.GetColorText(_Dato.AlertaCarga.ToString());

                lblTKPHD.Text = Math.Round(Convert.ToDouble(_Dato.TKPHOperacionalD), 0).ToString();
                lblTKPHT.Text = Math.Round(Convert.ToDouble(_Dato.TKPHOperacionalT), 0).ToString();
                lblNominal.Text = Math.Round(Convert.ToDouble(_Dato.TKPHNominal_C), 0).ToString();
                lblNominal2.Text = Math.Round(Convert.ToDouble(_Dato.TKPHNominal_C), 0).ToString();
                PorcentajeEjeDVAR = (Convert.ToDouble(_Dato.TKPHOperacionalD) * (100)) / (Convert.ToDouble(_Dato.TKPHNominal_C));
                PorcentajeEjeDVAR2 = (Convert.ToDouble(_Dato.TKPHOperacionalT) * (100)) / (Convert.ToDouble(_Dato.TKPHNominal_C));

                lblPorcentajeEjeD.Text = Math.Round(PorcentajeEjeDVAR, 0).ToString() + "%";
                lblPorcentajeEjeT.Text = Math.Round(PorcentajeEjeDVAR2, 0).ToString() + "%";

                lblPromTurnoD.Text = Math.Round(Convert.ToDouble(_Dato.TKPH_avg_D), 0).ToString();
                lblPromTurnoT.Text = Math.Round(Convert.ToDouble(_Dato.TKPH_avg_T), 0).ToString();
                lblTempA.Text = _Dato.Temp_ambiente.ToString();

                lblVelocidad.Text = _Dato.VM;
                lblDist.Text = _Dato.DistRec;

                _dataCamion = _business.GetDataSingle(_req);

                imgRueda1.ImageUrl = _business.GetColorRueda(_dataCamion, 1);
                imgRueda2.ImageUrl = _business.GetColorRueda(_dataCamion, 2);
                imgRueda3.ImageUrl = _business.GetColorRueda(_dataCamion, 3);
                imgRueda4.ImageUrl = _business.GetColorRueda(_dataCamion, 4);
                imgRueda5.ImageUrl = _business.GetColorRueda(_dataCamion, 5);
                imgRueda6.ImageUrl = _business.GetColorRueda(_dataCamion, 6);

                lblTempR1.Text = _business.GetTemperatura(_dataCamion, 1);
                lblTempR2.Text = _business.GetTemperatura(_dataCamion, 2);
                lblTempR3.Text = _business.GetTemperatura(_dataCamion, 3);
                lblTempR4.Text = _business.GetTemperatura(_dataCamion, 4);
                lblTempR5.Text = _business.GetTemperatura(_dataCamion, 5);
                lblTempR6.Text = _business.GetTemperatura(_dataCamion, 6);

                lblPresionR1.Text = _business.GetPresion(_dataCamion, 1);
                lblPresionR2.Text = _business.GetPresion(_dataCamion, 2);
                lblPresionR3.Text = _business.GetPresion(_dataCamion, 3);
                lblPresionR4.Text = _business.GetPresion(_dataCamion, 4);
                lblPresionR5.Text = _business.GetPresion(_dataCamion, 5);
                lblPresionR6.Text = _business.GetPresion(_dataCamion, 6);

                lblPresionR1.ForeColor = _business.GetColorText(_dataCamion.AlertaPres_1);
                lblPresionR2.ForeColor = _business.GetColorText(_dataCamion.AlertaPres_2);
                lblPresionR3.ForeColor = _business.GetColorText(_dataCamion.AlertaPres_3);
                lblPresionR4.ForeColor = _business.GetColorText(_dataCamion.AlertaPres_4);
                lblPresionR5.ForeColor = _business.GetColorText(_dataCamion.AlertaPres_5);
                lblPresionR6.ForeColor = _business.GetColorText(_dataCamion.AlertaPres_6);

                lblTempR1.ForeColor = _business.GetColorText(_dataCamion.AlertaTemp_1);
                lblTempR2.ForeColor = _business.GetColorText(_dataCamion.AlertaTemp_2);
                lblTempR3.ForeColor = _business.GetColorText(_dataCamion.AlertaTemp_3);
                lblTempR4.ForeColor = _business.GetColorText(_dataCamion.AlertaTemp_4);
                lblTempR5.ForeColor = _business.GetColorText(_dataCamion.AlertaTemp_5);
                lblTempR6.ForeColor = _business.GetColorText(_dataCamion.AlertaTemp_6);

                lblPorcentajeEjeD.ForeColor = _business.GetColorText(_Dato.AlertaTKPH_D.ToString());
                lblPorcentajeEjeT.ForeColor = _business.GetColorText(_Dato.AlertaTKPH_T.ToString());

                Int64 ColCam=Int64.Parse(_DatosMant.First().valor.ToString());

                pnlRTras.CssClass = _business.ColorEje(_Dato.AlertaTKPH_T, "T", ColCam);

                pnlAdelante.CssClass = _business.ColorEje(_Dato.AlertaTKPH_D, "D", ColCam);

                imgCabina.ImageUrl = _business.ColorCabina(_Dato.AlertaCarga);


                vPromAvgTempPres _DatosPT = _business2.getTempPres(Convert.ToInt64(_req.IdCamion)).First();

                lblpMaxR1.Text = _DatosPT.max_presion_psi_1;
                lblpMaxR2.Text = _DatosPT.max_presion_psi_2;
                lblpMaxR3.Text = _DatosPT.max_presion_psi_3;
                lblpMaxR4.Text = _DatosPT.max_presion_psi_4;
                lblpMaxR5.Text = _DatosPT.max_presion_psi_5;
                lblpMaxR6.Text = _DatosPT.max_presion_psi_6;

                lblpMinR1.Text = _DatosPT.min_presion_psi_1;
                lblpMinR2.Text = _DatosPT.min_presion_psi_2;
                lblpMinR3.Text = _DatosPT.min_presion_psi_3;
                lblpMinR4.Text = _DatosPT.min_presion_psi_4;
                lblpMinR5.Text = _DatosPT.min_presion_psi_5;
                lblpMinR6.Text = _DatosPT.min_presion_psi_6;

                lblpAvgR1.Text = _DatosPT.avg_presion_psi_1;
                lblpAvgR2.Text = _DatosPT.avg_presion_psi_2;
                lblpAvgR3.Text = _DatosPT.avg_presion_psi_3;
                lblpAvgR4.Text = _DatosPT.avg_presion_psi_4;
                lblpAvgR5.Text = _DatosPT.avg_presion_psi_5;
                lblpAvgR6.Text = _DatosPT.avg_presion_psi_6;

                lbltMaxR1.Text = _DatosPT.max_temperatura_c_1;
                lbltMaxR2.Text = _DatosPT.max_temperatura_c_2;
                lbltMaxR3.Text = _DatosPT.max_temperatura_c_3;
                lbltMaxR4.Text = _DatosPT.max_temperatura_c_4;
                lbltMaxR5.Text = _DatosPT.max_temperatura_c_5;
                lbltMaxR6.Text = _DatosPT.max_temperatura_c_6;

                lbltMinR1.Text = _DatosPT.min_temperatura_c_1;
                lbltMinR2.Text = _DatosPT.min_temperatura_c_2;
                lbltMinR3.Text = _DatosPT.min_temperatura_c_3;
                lbltMinR4.Text = _DatosPT.min_temperatura_c_4;
                lbltMinR5.Text = _DatosPT.min_temperatura_c_5;
                lbltMinR6.Text = _DatosPT.min_temperatura_c_6;

                lbltAvgR1.Text = _DatosPT.avg_temperatura_c_1;
                lbltAvgR2.Text = _DatosPT.avg_temperatura_c_2;
                lbltAvgR3.Text = _DatosPT.avg_temperatura_c_3;
                lbltAvgR4.Text = _DatosPT.avg_temperatura_c_4;
                lbltAvgR5.Text = _DatosPT.avg_temperatura_c_5;
                lbltAvgR6.Text = _DatosPT.avg_temperatura_c_6;

                Random r = new Random();
                Random r2 = new Random();
                Random r3 = new Random();
                int pulso = r.Next(50, 100);
                int o2 = r2.Next(94, 100);
                int Ifatiga = r2.Next(20, 100);

                lblPulso.Text = pulso.ToString();
                lblO2.Text = o2.ToString() + "%";
                lblFatiga.Text = Ifatiga.ToString() + "%";

                lblFatiga.ForeColor = _business.GetColorText(_Dato.AlertaCarga.ToString());

                pnlAtras.CssClass=_business.ColorCamionA(Int64.Parse(_Dato.AlertaCarga.ToString()),ColCam);

                DespliegaNotif();

            }
            catch (Exception ex)
            {
                lblMen.Text = ex.Message.ToString();
                MenPOP.Show();
            }
        }


        /// <summary>
        /// Metodo que despliega ventana PopUp con Notificaciones
        /// </summary>
        public void DespliegaNotif()
        {
            Int64 vNotNull;

            listCamionNotificacion = _business4.getCamionNotificacion(HttpContext.Current.Session["pro-camion"].ToString());

            vNotNull = listCamionNotificacion.Count();

            if (vNotNull > 0)
            {

                cNeumatico.Visible = true;
                btnExportarNotificacion.Visible = true;
                btnNotificacion.Visible = true;
                ddlInsertComentario.Visible = true;
                lblTitNotif.Text = "Notificaciones Manuales";
                lblTitNotif.Font.Size = 14;
                lblTitNotif.ForeColor = Color.White;
                pnlDisclaimer.Width = 1000;
                gdvNotificacion.Visible = true;
                lblNotif.Visible = true;

                gdvNotificacion.DataSource = listCamionNotificacion.ToList();
                gdvNotificacion.DataBind();
                gdvNotificacion.Visible = false;


                String vfTKPH = listCamionNotificacion.Distinct().Count(p => p.TIPO_ALARMA.Equals("Alarma de Tkph")).ToString();
                String vfCARGA = listCamionNotificacion.Distinct().Count(p => p.TIPO_ALARMA.Equals("Alarma de Carga")).ToString();
                String vfPRESION = listCamionNotificacion.Distinct().Count(p => p.TIPO_ALARMA.Equals("Alarma de Presion")).ToString();
                String vfTEMPERATURA = listCamionNotificacion.Distinct().Count(p => p.TIPO_ALARMA.Equals("Alarma de Temperatura")).ToString();

                listAUX = _business4.getCamionNotificacion((Convert.ToString(" ")));
                listAUX.Add(new vCamionNotificacion() { TIPO_ALARMA = "Alarma de Tkph", VALOR = vfTKPH });
                listAUX.Add(new vCamionNotificacion() { TIPO_ALARMA = "Alarma de Carga", VALOR = vfCARGA });
                listAUX.Add(new vCamionNotificacion() { TIPO_ALARMA = "Alarma de Presion", VALOR = vfPRESION });
                listAUX.Add(new vCamionNotificacion() { TIPO_ALARMA = "Alarma de Temperatura", VALOR = vfTEMPERATURA });

                Notif[0] = "Se tomaron medidas de TKPH";
                Notif[1] = "Se tomaron medidas de Presion";
                Notif[2] = "Se tomaron medidas de Temperatura";
                Notif[3] = "Se tomaron medidas de Carga";
                Notif[4] = "Otros";

                ddlInsertComentario.DataSource = Notif;

                ddlInsertComentario.DataBind();


                //Proceso de carga de datos y pintado de Grafico Notificaciones.

                Series _slin2 = Comun.Comun.CreateSerie("", SeriesChartType.Pie);

                Comun.Comun.CreatePoint(listAUX, _slin2, false);

                cNeumatico.Series.Add(_slin2);
                cNeumatico.ChartAreas[0].AxisX.LabelStyle.Angle = -45;
                cNeumatico.Series[0].ChartType = SeriesChartType.Doughnut;
                cNeumatico.BackColor = Color.FromArgb(255, 30, 30, 30);

                cNeumatico.ChartAreas[0].BorderWidth = 0;
                cNeumatico.ChartAreas[0].BackColor = Color.FromArgb(255, 30, 30, 30);
                cNeumatico.ChartAreas[0].AxisX.LabelStyle.Angle = -45;
                cNeumatico.ChartAreas[0].AxisY.LabelStyle.Font = cNeumatico.ChartAreas[0].AxisX.LabelStyle.Font = new System.Drawing.Font("Calibri", 8);
                cNeumatico.ChartAreas[0].AxisY.LabelStyle.ForeColor = cNeumatico.ChartAreas[0].AxisX.LabelStyle.ForeColor = Color.White;

                cNeumatico.ChartAreas[0].AxisY.MajorGrid.LineColor = Color.FromArgb(43, 43, 43);
                cNeumatico.ChartAreas[0].AxisX.MajorGrid.Enabled = false;
                cNeumatico.ChartAreas[0].AxisY.MajorGrid.LineWidth = 1;

                cNeumatico.Legends.Add("Ley");
                cNeumatico.Legends[0].Docking = Docking.Top;
                cNeumatico.Legends[0].Font = new System.Drawing.Font("Calibri", 8);
                cNeumatico.Legends[0].Alignment = StringAlignment.Center;
                cNeumatico.Legends[0].BackColor = Color.FromArgb(255, 30, 30, 30);
                cNeumatico.Legends[0].ForeColor = Color.White;


                // Carga de datos en Grilla con notificaciones para ser exportadas

                gdvNotificaciones.DataSource = listCamionNotificacion.ToList();

                gdvNotificaciones.DataBind();

                gdvNotificaciones.HeaderRow.Cells[0].Visible = false;
                gdvNotificaciones.HeaderRow.Cells[2].Visible = false;
                gdvNotificaciones.HeaderRow.Cells[6].Visible = false;
                gdvNotificaciones.HeaderRow.Cells[7].Visible = false;
                gdvNotificaciones.HeaderRow.Cells[8].Visible = false;
                gdvNotificaciones.HeaderRow.Cells[9].Visible = false;
                gdvNotificaciones.HeaderRow.Cells[10].Visible = false;

                gdvNotificaciones.HeaderRow.Cells[1].CssClass = "cabecera";
                gdvNotificaciones.HeaderRow.Cells[3].CssClass = "cabecera";
                gdvNotificaciones.HeaderRow.Cells[4].CssClass = "cabecera";
                gdvNotificaciones.HeaderRow.Cells[5].CssClass = "cabecera";
                gdvNotificaciones.HeaderRow.Cells[11].CssClass = "cabecera";

                gdvNotificaciones.HeaderRow.Cells[3].Text = "RIESGO";
                gdvNotificaciones.HeaderRow.Cells[5].Text = "FECHA";

            }

            else
            {
                cNeumatico.Visible = false;
                btnExportarNotificacion.Visible = false;
                btnNotificacion.Visible = false;
                ddlInsertComentario.Visible = false;
                lblTitNotif.Text = "No Existen Notificaciones";
                lblTitNotif.Font.Size = 20;
                lblTitNotif.ForeColor = Color.Red;
                pnlDisclaimer.Width = 600;
                gdvNotificaciones.Visible = false;
                lblNotif.Visible = false;
            }

            /**
            lblTitNotif.Visible = false;
            ddlInsertComentario.Visible = false;
            btnNotificacion.Visible = false;
            ddlInsertComentario.Visible = false;
            **/
        }

        /// <summary>
        /// Evento gatillado al cambiar la seleccion del dropdownlist con el listado de camiones, 
        /// recarga datos segun camión seleccionado, para presentar en la GUI
        /// </summary>
        /// <param name="sender">La fuente del evento.</param>
        /// <param name="e">La Instancia que contiene los datos del evento.</param>
        protected void ddlCamiones_SelectedIndexChanged(object sender, EventArgs e)
        {
            HttpContext.Current.Session["pro-camion"] = ddlCamiones.SelectedValue.ToString();
            CargarData();
        }

        /// <summary>
        /// Evento click del botón Tendencia para direccionar a pantalla de Tendencias.
        /// </summary>
        /// <param name="sender">La fuente del evento.</param>
        /// <param name="e">La Instancia que contiene los datos del evento.</param>
        protected void btnTendenciaTkph_Click(object sender, EventArgs e)
        {
            Response.Redirect("Tendencia.aspx");
        }


        /// <summary>
        /// Evento Databound gridview con listado de notificaciones, para ocultar o mostrar columnas.
        /// </summary>
        /// <param name="sender">La fuente del evento.</param>
        /// <param name="e">La Instancia que contiene los datos del evento.</param>
        protected void dgvNotificaciones_DataBound(object sender, EventArgs e)
        {
            GridView gv = (GridView)sender;

            GridViewRowCollection growadd = (GridViewRowCollection)gv.Rows;

            foreach (GridViewRow Rows in growadd)
            {
                Rows.Cells[0].Visible = false;
                Rows.Cells[2].Visible = false;
                Rows.Cells[6].Visible = false;
                Rows.Cells[7].Visible = false;
                Rows.Cells[8].Visible = false;
                Rows.Cells[9].Visible = false;
                Rows.Cells[10].Visible = false;

                Rows.Cells[1].CssClass = "celda";
                Rows.Cells[3].CssClass = "celda";
                Rows.Cells[4].CssClass = "celda";
                Rows.Cells[5].CssClass = "celda";
                Rows.Cells[11].CssClass = "celda";

            }
        }


        /// <summary>
        /// Evento click para registrar la notificacion en la base de datos
        /// </summary>
        /// <param name="sender">La fuente del evento.</param>
        /// <param name="e">La Instancia que contiene los datos del evento.</param>
        protected void btnGuardarNotificacion_Click(object sender, EventArgs e)
        {
            _business5.setInsertCamionNotificacion(Convert.ToInt64(HttpContext.Current.Session["pro-camion"].ToString()), ddlInsertComentario.SelectedValue);
            Response.Write("");
            DespliegaNotif();
            ModalPopupExtender1.Show();
        }

        /// <summary>
        /// Evento click para desplegar ventana Popup con las notificaciones asociadas al camión.
        /// </summary>
        /// <param name="sender">La fuente del evento.</param>
        /// <param name="e">La Instancia que contiene los datos del evento.</param>
        protected void btnNotificacion_Click(object sender, EventArgs e)
        {
            ModalPopupExtender1.Show();
            ModalPopupExtender1.DataBind();
            DespliegaNotif();
        }


        /// <summary>
        /// Evento RowDataBound gridview con listado de notificaciones, para dar formato a las celdas
        /// </summary>
        /// <param name="sender">La fuente del evento.</param>
        /// <param name="e">La Instancia que contiene los datos del evento.</param>
        protected void dgvNotificaciones_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (DataBinder.Eval(e.Row.DataItem, "TIPO_ALARMA").Equals("Alarma de Carga"))
                {
                    e.Row.ControlStyle.BackColor = System.Drawing.Color.FromArgb(17, 42, 79);//Fondo
                    e.Row.ControlStyle.ForeColor = System.Drawing.Color.White;//Fuente
                }

                if (DataBinder.Eval(e.Row.DataItem, "TIPO_ALARMA").Equals("Alarma de Temperatura")) 
                {
                    e.Row.ControlStyle.BackColor = System.Drawing.Color.FromArgb(95, 133, 161); //Fondo
                    e.Row.ControlStyle.ForeColor = System.Drawing.Color.White; //Fuente
                }

                if (DataBinder.Eval(e.Row.DataItem, "TIPO_ALARMA").Equals("Alarma de Presion")) 
                {
                    e.Row.ControlStyle.BackColor = System.Drawing.Color.FromArgb(7, 81, 137); //Fondo
                    e.Row.ControlStyle.ForeColor = System.Drawing.Color.White; //Fuente
                }

                if (DataBinder.Eval(e.Row.DataItem, "TIPO_ALARMA").Equals("Alarma de Tkph"))
                {
                    e.Row.ControlStyle.BackColor = System.Drawing.Color.FromArgb(25, 147, 191); //Fondo
                    e.Row.ControlStyle.ForeColor = System.Drawing.Color.White; //Fuente
                }
            }
        }


        /// <summary>
        /// Evento click del botoón que permite exportar listado de notificaciones en formato Excel
        /// </summary>
        /// <param name="sender">La fuente del evento.</param>
        /// <param name="e">La Instancia que contiene los datos del evento.</param>
        protected void btnExportarNotificacion_Click(object sender, EventArgs e)
        {
            gdvNotificacion.Visible = true;

            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename=Notificaciones_" + Convert.ToInt64(HttpContext.Current.Session["pro-camion"].ToString()) + "_" + DateTime.Today.ToString("dd-MM-yyyy") + ".xls");
            Response.Charset = "UFT-8";
            this.EnableViewState = false;
            // Si se desea que el archivo no se guarde y solo se muestre comentar la linea siguiente.
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "application/vnd.ms-excel";
            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
            gdvNotificacion.RenderControl(htmlWrite);
            Response.Write(stringWrite.ToString());
            Response.End();

            gdvNotificacion.Visible = false;

        }


        /// <summary>
        /// Evento de cumplimiento de ciclo dentro del Timer para actualización de los datos.
        /// </summary>
        /// <param name="sender">La fuente del evento.</param>
        /// <param name="e">La Instancia que contiene los datos del evento.</param>
        protected void tmrAct_Tick(object sender, EventArgs e)
        {
            lblAct.Text = "Actualizado: " + DateTime.Now.ToString("HH:mm:ss");

            vStatusRequest _req = new vStatusRequest()
            {
                IdCamion = HttpContext.Current.Session["pro-camion"].ToString(),
                DescripcionFlota = ""
            };
            try
            {

                _Single = _business3.GetCamionSingle(new vStatusRequest() { AlertaGeneral = "" }).Where(f => f.Alerta != 4).ToList();

                _Single = _Single.OrderBy(o => o.IdCamion).ToList();

                ddlCamiones.DataTextField = "IdCamion";
                ddlCamiones.DataValueField = "IdCamion";
                ddlCamiones.DataSource = _Single;

                ddlCamiones.DataBind();


                ddlCamiones.Items.Insert(0, IdCamion);

                CargarData();

            }
            catch (Exception ex)
            {
                lblMen.Text = ex.Message.ToString();
                MenPOP.Show();
            }
        }


    }
}