﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Com.Vmica.View;
using TKPH_BUSINESS.BUSINESS_PAGE.Neumatico;
using System.Web.UI.DataVisualization.Charting;
using TKPH_BUSINESS.COMUN;
using System.Drawing;
using TKPH_WEB;
using System.Windows.Forms;
using System.IO;
using System.Collections;
using TKPH_WEB.Comun;


namespace TKPH_WEB.Camiones
{
    /// <summary>
    /// Pantalla Vista Graficos de tendencia de camión seleccionado.
    /// </summary>
    public partial class Tendencia : System.Web.UI.Page
    {
        // Llamada a instancia de Objeto "bNivel4"
        bNivel4 _business = bNivel4.Instance;

        IList<vHistorico> list0;
        IList<vHistorico> list1;
        IList<vHistorico> list2;
        IList<vHistoricoPT> list3;

        IList<vHistoricoPT> listFin = new List<vHistoricoPT>();

        String _idtruck="0";


        String _Fdesde, _Fhasta = "";

        int m = 0;

        /// <summary>
        /// Metodo de carga Inicial de la Página
        /// </summary>
        /// <param name="sender">La fuente del evento.</param>
        /// <param name="e">La Instancia que contiene los datos del evento.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                HttpContext.Current.Session["Titulo"] = "Datos Históricos de TKPH, Carga, Presión, Temperatura ";
                HttpContext.Current.Session["SubTitulo"] = "";
                lblIdCam.Text = "Camión: " + HttpContext.Current.Session["pro-camion"].ToString();

                if (HttpContext.Current.Session["pro-camion"] == null)
                {
                    Response.Redirect("DetalleCamion.aspx");
                }
                else
                {
                    
                    lblAct.Text = "Actualizado: " + DateTime.Now.ToString("HH:mm:ss");

                    vStatusRequest _req = new vStatusRequest()
                    {
                        IdCamion = HttpContext.Current.Session["pro-camion"].ToString(),
                        DescripcionFlota = ""
                    };

                    try
                    {

                        _idtruck = HttpContext.Current.Session["pro-camion"].ToString();

                        CargarGraficos(8);
                        btn8Hrs.CssClass = "BotonSelect";


                    }
                    catch (Exception ex)
                    {
                        lblMen.Text = ex.Message.ToString();
                        MenPOP.Show();
                    }
                }
            }

        }


        protected override void LoadViewState(object SavedState)
        {
            if (SavedState != null)
            {
                object[] state = (object[])SavedState;

                if (state[0] != null) base.LoadViewState(state[0]);
                if (state[1] != null) list1 = Serializador.DesSerializeObjeto<vHistorico[]>((string)state[1]).ToList();
                if (state[2] != null) _idtruck = state[2].ToString();
                //if (state[3] != null) _rideID = state[3].ToString();
                //if (state[2] != null) _listDisponible = Serializador.DesSerializeObjeto<vDisponible[]>((string)state[2]).ToList();
            }
        }

        protected override object SaveViewState()
        {
            object[] SavedState = new object[18];
            SavedState[0] = base.SaveViewState();

            if (list1 != null) SavedState[1] = Serializador.SerializeObjeto<vHistorico[]>(list1.ToArray());
            if (_idtruck != null) SavedState[2] = _idtruck.ToString();
            //if (_rideID != null) SavedState[3] = _rideID.ToString();
            //if (_listDisponible != null) SavedState[2] = Serializador.SerializeObjeto<vDisponible[]>(_listDisponible.ToArray());

            return SavedState;
        }

        /// <summary>
        /// Evento Click del botón para desplegar datos de las ultimas 2 Horas
        /// </summary>
        /// <param name="sender">La fuente del evento.</param>
        /// <param name="e">La Instancia que contiene los datos del evento.</param>
        protected void btn2Hrs_Click(object sender, EventArgs e)
        {
            CargarGraficos(2);
            btn2Hrs.CssClass = "BotonSelect";
            btn4Hrs.CssClass = "input";
            btn8Hrs.CssClass = "input";
        }


        /// <summary>
        /// Evento Click del botón para desplegar datos de las ultimas 4 Horas
        /// </summary>
        /// <param name="sender">La fuente del evento.</param>
        /// <param name="e">La Instancia que contiene los datos del evento.</param>
        protected void btn4Hrs_Click(object sender, EventArgs e)
        {
            CargarGraficos(4);
            btn4Hrs.CssClass = "BotonSelect";
            btn2Hrs.CssClass = "input";
            btn8Hrs.CssClass = "input";
        }


        /// <summary>
        /// Evento Click del botón para desplegar datos de las ultimas 8 Horas (Por Defecto)
        /// </summary>
        /// <param name="sender">La fuente del evento.</param>
        /// <param name="e">La Instancia que contiene los datos del evento.</param>
        protected void btn8Hrs_Click(object sender, EventArgs e)
        {
            CargarGraficos(8);
            btn8Hrs.CssClass = "BotonSelect";
            btn4Hrs.CssClass = "input";
            btn2Hrs.CssClass = "input";
        }


        /// <summary>
        /// Evento Click del botón para exportar los datos cargados del periodo seleccionado a Excel
        /// </summary>
        /// <param name="sender">La fuente del evento.</param>
        /// <param name="e">La Instancia que contiene los datos del evento.</param>
        protected void ExportToExcel(object sender, EventArgs e)
        {
            gdvGrillaDescarga.Visible = true;

            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename=Data_Camion_" + _idtruck + "_" + DateTime.Today.ToString("dd-MM-yyyy") + ".xls");
            Response.Charset = "UFT-8";
            this.EnableViewState = false;
            // Si se desea que el archivo no se guarde y solo se muestre, comentar la linea siguiente.
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "application/vnd.ms-excel";
            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
            gdvGrillaDescarga.RenderControl(htmlWrite);
            Response.Write(stringWrite.ToString());
            Response.End();

            gdvGrillaDescarga.Visible = false;
        }


        /// <summary>
        /// Confirms that an <see cref="T:System.Web.UI.HtmlControls.HtmlForm" /> control is rendered for the specified ASP.NET server control at run time.
        /// </summary>
        /// <param name="control">The ASP.NET server control that is required in the <see cref="T:System.Web.UI.HtmlControls.HtmlForm" /> control.</param>
        public override void VerifyRenderingInServerForm(System.Web.UI.Control control)
        {
            /* Verifies that the control is rendered */
        }


        /// <summary>
        /// Evento de carga de datos en grilla, se ocultan columns que no deben ser visualizadas.
        /// </summary>
        /// <param name="sender">La fuente del evento.</param>
        /// <param name="e">La Instancia que contiene los datos del evento.</param>
        protected void gdvGrillaDescarga_DataBound(object sender, EventArgs e)
        {

            gdvGrillaDescarga.HeaderRow.Cells[5].Visible = false;
            gdvGrillaDescarga.HeaderRow.Cells[6].Visible = false;
            gdvGrillaDescarga.HeaderRow.Cells[7].Visible = false;
            gdvGrillaDescarga.HeaderRow.Cells[8].Visible = false;

            GridView gv = (GridView)sender;
            GridViewRowCollection growArr = (GridViewRowCollection)gv.Rows;

            foreach (GridViewRow row in growArr)
            {

                row.Cells[5].Visible = false;
                row.Cells[6].Visible = false;
                row.Cells[7].Visible = false;
                row.Cells[8].Visible = false;
            }
        }


        /// <summary>
        /// Metodo que Genera graficos de TKPH, Carga, Temperatura y Presión. 
        /// </summary>
        /// <param name="Horas">Corresponde al intervalo de tiempo en horas.</param>
        public void CargarGraficos(int Horas)
        {

            try
            {

                int lashoras = Horas * -1;

                _Fdesde = DateTime.Now.AddHours(lashoras).ToString("yyyy-MM-dd HH:mm");
                _Fhasta = DateTime.Now.ToString("yyyy-MM-dd HH:mm");


                list0 = _business.GetHistoricoFlag(new vHistorico() { IdCamion = _idtruck, Flag = 4, FechaDesde = _Fdesde, FechaHasta = _Fhasta });
                list1 = _business.GetHistoricoFlag(new vHistorico() { IdCamion = _idtruck, Flag = 1, FechaDesde = _Fdesde, FechaHasta = _Fhasta });
                list2 = _business.GetHistoricoFlag(new vHistorico() { IdCamion = _idtruck, Flag = 2, FechaDesde = _Fdesde, FechaHasta = _Fhasta });


                for (int i = 1; i <= 6; i++)
                {
                    list3 = _business.GetHistoricoPT(new vHistoricoPT() { IDCAMION = _idtruck, LOGICALID = i.ToString(), FechaDesde = _Fdesde, FechaHasta = _Fhasta });
                    listFin = listFin.Concat(list3).ToList();
                }


                lblIntervalo.Text = "Ultimas " + Horas + " Horas.";

                gdvGrillaDescarga.DataSource = listFin.ToList();
                gdvGrillaDescarga.DataBind();
                gdvGrillaDescarga.Visible = false;

                // Grafico Historico TKPH
                Series s1 = Comun.Comun.CreateSerie("Nominal", SeriesChartType.Line);
                Series s2 = Comun.Comun.CreateSerie("Operacional Eje Delantero", SeriesChartType.Line);
                Series s3 = Comun.Comun.CreateSerie("Operacional Eje Trasero", SeriesChartType.Line);

                Comun.Comun.CreatePoint(list1, s1, true);
                Comun.Comun.CreatePoint(list1, s2, false);
                Comun.Comun.CreatePoint(list0, s3, false);

                gTkph.Series.Add(s1);
                gTkph.Series.Add(s2);
                gTkph.Series.Add(s3);

                m = int.Parse(list2.Min(o => o.Operacional).ToString()) / 10;
                FormatoGraficos(gTkph, m);

                // Grafico Historico Carga
                Series sC1 = Comun.Comun.CreateSerie("Nominal", SeriesChartType.Line);
                Series sC2 = Comun.Comun.CreateSerie("Operacional", SeriesChartType.Line);

                Comun.Comun.CreatePoint(list2, sC1, true);
                Comun.Comun.CreatePoint(list2, sC2, false);

                gCarga.Series.Add(sC1);
                gCarga.Series.Add(sC2);


                m = int.Parse(list2.Min(o => o.Operacional).ToString()) - 30;
                FormatoGraficos(gCarga, m);

                //Grafico Temperatura
                for (int i = 1; i <= 6; i++)
                {
                    Series nS = new Series();
                    nS = Comun.Comun.CreateSerie("P" + i.ToString(), SeriesChartType.Line);
                    Comun.Comun.CreatePoint(listFin.Where(r => r.LOGICALID.Equals(i.ToString())).ToList(), nS, true);
                    gTemp.Series.Add(nS);
                }

                m = int.Parse(listFin.Where(te => te.TEMPERATURA>0).Min(t => t.TEMPERATURA).ToString()) - 10 ;

                FormatoGraficos(gTemp, m);

                //Grafico Presion
                for (int i = 1; i <= 6; i++)
                {
                    Series nS = new Series();
                    nS = Comun.Comun.CreateSerie("P" + i.ToString(), SeriesChartType.Line);
                    Comun.Comun.CreatePoint(listFin.Where(r => r.LOGICALID.Equals(i.ToString())).ToList(), nS, false);
                    gPres.Series.Add(nS);
                }

                //m = int.Parse(listFin.Where(pr => pr.PRESION > 0.0).Min(p => p.PRESION).ToString()) - 10;
                m = int.Parse(Math.Round(listFin.Min(p => p.PRESION), 0).ToString()) - 10;
                FormatoGraficos(gPres, m);

            }
            catch (Exception ex)
            {
                lblMen.Text = ex.Message.ToString();
                MenPOP.Show();
            }
        }


        /// <summary>
        /// Aplica formatos a graficos.
        /// </summary>
        /// <param name="Grafico">Objeto tipo Chart</param>
        /// <param name="Minimo">Valor intervalo minimo a desplegar en el grafico.</param>
        public void FormatoGraficos(Chart Grafico, int Minimo)
        {
            Grafico.ChartAreas[0].AxisY.Minimum = Minimo;   
            Grafico.BackColor = Color.FromArgb(255, 30, 30, 30);

            Grafico.ChartAreas[0].BorderWidth = 0;
            Grafico.ChartAreas[0].BackColor = Color.FromArgb(255, 30, 30, 30);
            Grafico.ChartAreas[0].AxisX.LabelStyle.Angle = -45;
            Grafico.ChartAreas[0].AxisY.LabelStyle.Font = Grafico.ChartAreas[0].AxisX.LabelStyle.Font = new System.Drawing.Font("Calibri", 8);
            Grafico.ChartAreas[0].AxisY.LabelStyle.ForeColor = Grafico.ChartAreas[0].AxisX.LabelStyle.ForeColor = Color.White;

            Grafico.ChartAreas[0].AxisY.MajorGrid.LineColor = Color.FromArgb(43, 43, 43);
            Grafico.ChartAreas[0].AxisX.MajorGrid.Enabled = false;
            Grafico.ChartAreas[0].AxisY.MajorGrid.LineWidth = 1;

            Grafico.Legends.Add("Ley");
            Grafico.Legends[0].Docking = Docking.Top;
            Grafico.Legends[0].Font = new System.Drawing.Font("Calibri", 8);
            Grafico.Legends[0].Alignment = StringAlignment.Center;
            Grafico.Legends[0].BackColor = Color.FromArgb(255, 30, 30, 30);
            Grafico.Legends[0].ForeColor = Color.White;
            
            // Aplicar Colores Series

            int cnt = 1;
            foreach (Series Ser in Grafico.Series)
            {
                Ser.Color = Comun.Comun.Colores(cnt);
                cnt++;
            }
        }


        /// <summary>
        /// Evento de cumplimiento de ciclo dentro del Timer para actualización del estado general de la flota
        /// </summary>
        /// <param name="sender">La fuente del evento.</param>
        /// <param name="e">La Instancia que contiene los datos del evento.</param>
        protected void tmrActualizar_Tick(object sender, EventArgs e)
        {
            lblAct.Text = "Actualizado: " + DateTime.Now.ToString("HH:mm:ss");

            vStatusRequest _req = new vStatusRequest()
            {
                IdCamion = HttpContext.Current.Session["pro-camion"].ToString(),
                DescripcionFlota = ""
            };

            try
            {
                _idtruck = HttpContext.Current.Session["pro-camion"].ToString();

                CargarGraficos(8);
            }
            catch (Exception ex)
            {
                lblMen.Text = ex.Message.ToString();
                MenPOP.Show();
            }

        }


    }
}