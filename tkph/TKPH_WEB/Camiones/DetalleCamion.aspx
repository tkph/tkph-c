﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DetalleCamion.aspx.cs" Inherits="TKPH_WEB.Camiones.DetalleCamion" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="BodyC" runat="server">

    <div id="contenedor">
        <asp:UpdatePanel runat="server" ID="upDetalleCamion" UpdateMode="Conditional">
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="ddlCamiones" EventName="SelectedIndexChanged" />
                <asp:AsyncPostBackTrigger ControlID="btnGuardarNotificacion" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="btnNotificacion" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="tmrAct" EventName="Tick" />
                <asp:PostBackTrigger ControlID="btnExportarNotificacion" />
            </Triggers>
            <ContentTemplate>
                <asp:Timer ID="tmrAct" runat="server" Interval="600000" Enabled="true" OnTick="tmrAct_Tick" />
                <ul class="superior">
                    <li>
                        <asp:Label runat="server" ID="lblCamion" Text="Camión: " CssClass="titulo" />
                    </li>
                    <li>
                        <asp:Label runat="server" ID="lblFlota" Text="Flota: " CssClass="titulo" />
                    </li>
                    <li class="centro" style="height: 24px;">
                        <asp:UpdateProgress ID="UpdateProgress" runat="server">
                            <ProgressTemplate>
                                <asp:Image runat="server" ImageUrl="~/Imagenes/Iconos/Cargando2.gif" Width="24" Height="24" ImageAlign="AbsMiddle" ID="imgCargando" />
                                <asp:Label runat="server" Text="Cargando Datos..." ID="lblCargando"/>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </li>
                    <li class="der">
                        <asp:Label runat="server" ID="lblESeleccione" Text="Seleccione Camión: " CssClass="ocu">
                            <asp:DropDownList runat="server" ID="ddlCamiones" AutoPostBack="true" OnSelectedIndexChanged="ddlCamiones_SelectedIndexChanged" CssClass="input" Width="70" />
                        </asp:Label>
                    </li>
                    <li class="der">
                        <asp:Button runat="server" ID="btnTendenciaTkph" OnClick="btnTendenciaTkph_Click" Text="Tendencias" CssClass="input ocu" />
                    </li>
                    <li class="centro + der">
                        <asp:Button runat="server" ID="btnNotificacion" CssClass="input ocu" Text="Notificaciones" OnClick="btnNotificacion_Click" />
                    </li>
                    <li class="centro + der">
                        <asp:Image runat="server" ID="imgAlertaAlto" Visible="false" />
                        <asp:Image runat="server" ID="imgAlertaMedio" Visible="false" />
                    </li>
                </ul>


                <div class="contenedorDetalle">
                    <div>
                        <asp:Panel ID="pnlAdelante" runat="server" Width="320" align="center">
                            <table width="320" align="center">
                                <tr>
                                    <td>
                                        <div style="margin-top: -3px; margin-left: -3px; width: 0px; height: 0px">
                                            <asp:ImageButton ID="imgCabina" runat="server" />
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <div style="margin-top: 156px; margin-left: 20px; width: 45px; height: 95px;">
                                            <asp:ImageButton ID="imgRueda2" Width="45px" Height="96px" runat="server" />
                                        </div>
                                    </td>
                                    <td align="center">
                                        <div style="margin-top: 156px; margin-left: 128px; width: 45px; height: 95px;">
                                            <asp:ImageButton ID="imgRueda1" Width="45px" Height="96px" runat="server" />
                                        </div>
                                    </td>
                                </tr>
                            </table>

                        </asp:Panel>
                        <table align="center" width="260">
                            <tr>
                                <td align="center" width="45">2</td>
                                <td></td>
                                <td align="center" width="45">1</td>
                            </tr>
                            <tr>
                                <td class="celda">
                                    <asp:Label ID="lblTempR2" runat="server"></asp:Label>
                                </td>
                                <td>< Temp. (C°) ></td>
                                <td class="celda">
                                    <asp:Label ID="lblTempR1" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="celda">
                                    <asp:Label ID="lblPresionR2" runat="server"></asp:Label>
                                </td>
                                <td>< Presión (Psi) ></td>
                                <td class="celda">
                                    <asp:Label ID="lblPresionR1" runat="server"></asp:Label></td>
                            </tr>
                        </table>
                        <br />
                        <table width="300" align="center">
                            <tr>
                                <td colspan="2" class="titulostabs">TKPH Delantero</td>
                            </tr>
                            <tr>
                                <td class="cabecera" align="left" width="100">Porcentaje</td>
                                <td class="celda">

                                    <asp:Label ID="lblPorcentajeEjeD" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="cabecera" align="left">Nominal</td>
                                <td class="celda">
                                    <asp:Label ID="lblNominal" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="cabecera" align="left">Operacional</td>
                                <td class="celda">
                                    <asp:Label ID="lblTKPHD" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="cabecera" align="left">Prom. Turno</td>
                                <td class="celda">
                                    <asp:Label ID="lblPromTurnoD" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </table>
                        <br />
                        <table width="300" align="center" id="tblFatiga">
                            <tr>
                                <td colspan="2" class="titulostabs">Indicadores Fatiga</td>
                            </tr>
                            <tr>
                                <td class="cabecera" align="left">Indice Fatiga</td>
                                <td class="celda">
                                    <asp:Label ID="lblFatiga" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="cabecera" align="left">O2</td>
                                <td class="celda">
                                    <asp:Label ID="lblO2" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="cabecera" align="left" width="100">Pulso</td>
                                <td class="celda">

                                    <asp:Label ID="lblPulso" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </table>

                    </div>

                    <div>
                        <asp:Panel ID="pnlAtras" runat="server" Width="320" Height="260" CssClass="camion_atras_T">
                            <asp:Panel runat="server" ID="pnlRTras" CssClass="eje_T_a" Width="320">
                                <table width="320" align="center">
                                    <tr>
                                        <td align="center" width="55">
                                            <div style="margin-left: 4px; width: 45px; height: 125px;">
                                                <asp:ImageButton ID="imgRueda3" Width="45px" Height="128px" runat="server" />
                                            </div>
                                        </td>
                                        <td align="center" width="55">
                                            <div style="margin-left: 7px; width: 45px; height: 125px;">
                                                <asp:ImageButton ID="imgRueda5" Width="45px" Height="128px" runat="server" />
                                            </div>
                                        </td>
                                        <td width="100"></td>
                                        <td align="center" width="55">
                                            <div style="margin-left: 0px; width: 45px; height: 125px;">
                                                <asp:ImageButton ID="imgRueda4" Width="45px" Height="128px" runat="server" />
                                            </div>
                                        </td>
                                        <td align="center" width="55">
                                            <div style="margin-left: 0px; width: 45px; height: 125px;">
                                                <asp:ImageButton ID="imgRueda6" Width="45px" Height="128px" runat="server" />
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </asp:Panel>
                        <table width="320" align="center">
                            <tr>
                                <td width="55" align="center">3</td>
                                <td width="55" align="center">5</td>
                                <td></td>
                                <td width="55" align="center">4</td>
                                <td width="55" align="center">6</td>
                            </tr>
                            <tr>
                                <td class="celda">
                                    <asp:Label ID="lblTempR3" runat="server"></asp:Label>
                                </td>
                                <td class="celda">
                                    <asp:Label ID="lblTempR5" runat="server"></asp:Label>
                                </td>
                                <td align="center">< Temp. (C°) ></td>
                                <td class="celda">
                                    <asp:Label ID="lblTempR4" runat="server"></asp:Label>
                                </td>
                                <td class="celda">
                                    <asp:Label ID="lblTempR6" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="celda">
                                    <asp:Label ID="lblPresionR3" runat="server"></asp:Label>
                                </td>
                                <td class="celda">
                                    <asp:Label ID="lblPresionR5" runat="server"></asp:Label>
                                </td>
                                <td align="center">< Presión (Psi) ></td>
                                <td class="celda">
                                    <asp:Label ID="lblPresionR4" runat="server"></asp:Label>
                                </td>
                                <td class="celda">
                                    <asp:Label ID="lblPresionR6" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </table>
                        <br />
                        <table width="300" align="center">
                            <tr>
                                <td colspan="2" class="titulostabs">TKPH Trasero</td>
                            </tr>
                            <tr>
                                <td class="cabecera" align="left" width="100">Porcentaje</td>
                                <td class="celda">
                                    <asp:Label ID="lblPorcentajeEjeT" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="cabecera" align="left">Nominal</td>
                                <td class="celda">
                                    <asp:Label ID="lblNominal2" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="cabecera" align="left">Operacional</td>
                                <td class="celda">
                                    <asp:Label ID="lblTKPHT" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="cabecera" align="left">Prom. Turno</td>
                                <td class="celda">
                                    <asp:Label ID="lblPromTurnoT" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </table>
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                    </div>
                    <div>
                        <br />
                        <table width="90%" align="center">
                            <tr>
                                <td colspan="2" class="titulostabs">
                                    <!--
                                    <asp:Label ID="lblUL" runat="server" CssClass="SubTitApp4" Text="Datos Ciclo " />
                                    (
                                    <asp:Label ID="lblULectura" runat="server" />
                                    )-->
                                    <asp:Label ID="lblUC" runat="server" CssClass="SubTitApp4" Text="Ultimo Ciclo Cerrado " />
                                    (
                                    <asp:Label ID="lblUCiclo" runat="server" />
                                    )
                                </td>
                            </tr>
                            <tr>
                                <td class="cabecera" align="left" width="140">
                                    <asp:Label ID="lblOperador0" runat="server" Text="Operador "></asp:Label>
                                </td>
                                <td class="celda">
                                    <asp:Label ID="lblOperador" runat="server" Text="Operador: "></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="cabecera" align="left">
                                    <asp:Label ID="lblPala0" runat="server" Text="Pala "></asp:Label>
                                </td>
                                <td class="celda">
                                    <asp:Label ID="lblPala" runat="server" Text="Pala: "></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="cabecera" align="left">
                                    <asp:Label ID="lblDestino0" runat="server" Text="Destino" />
                                </td>
                                <td class="celda">
                                    <asp:Label ID="lblDestino" runat="server" Text="Destino " />
                                </td>
                            </tr>
                            <tr>
                                <td class="cabecera" align="left">
                                    <asp:Label ID="lblTurno0" runat="server" Text="Turno " />
                                </td>
                                <td class="celda">
                                    <asp:Label ID="lblTurno" runat="server" Text="Turno " />
                                </td>
                            </tr>
                            <tr>
                                <td class="cabecera" align="left" width="140">
                                    <asp:Label ID="Label43" runat="server" Text="Carga (Ton)"></asp:Label>
                                </td>
                                <td class="celda">
                                    <asp:Label ID="lblCarga" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="cabecera" align="left">
                                    <asp:Label ID="Label1" runat="server" Text="Carga Nominal (Ton)"></asp:Label>
                                </td>
                                <td class="celda">
                                    <asp:Label ID="lblCargaNom" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="cabecera" align="left">
                                    <asp:Label ID="Label41" runat="server" Text="Velocidad (Km/H)"></asp:Label>
                                </td>
                                <td class="celda">
                                    <asp:Label ID="lblVelocidad" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="cabecera" align="left">
                                    <asp:Label ID="Label42" runat="server" Text="Distancia (Km)"></asp:Label>
                                </td>
                                <td class="celda">
                                    <asp:Label ID="lblDist" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="cabecera" align="left">
                                    <asp:Label ID="Label2" runat="server" Text="Temp. Ambiente (C°)"></asp:Label>
                                </td>
                                <td class="celda">
                                    <asp:Label ID="lblTempA" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                        </table>

                        <br />

                        <br />
                        <table width="90%" align="center">
                            <tr>
                                <td colspan="2" class="titulostabs">Acumulado Ultimo Turno</td>
                            </tr>
                            <tr>
                                <td class="cabecera" align="left" width="140">
                                    <asp:Label ID="lblCargaTurno0" runat="server" Text="Carga (Ton) " />
                                </td>
                                <td class="celda">
                                    <asp:Label ID="lblCargaTurno" runat="server" Text="CargaT " />
                                </td>
                            </tr>
                            <tr>
                                <td class="cabecera" align="left">
                                    <asp:Label ID="lblKmTurno0" runat="server" Text="Distancia (Km) " />
                                </td>
                                <td class="celda">
                                    <asp:Label ID="lblKmTurno" runat="server" Text="KmT: " />
                                </td>
                            </tr>
                        </table>

                        <br />
                        <table width="90%" align="center">
                            <tr>
                                <td></td>
                                <td colspan="3" class="cabecera">
                                    <asp:Label runat="server" ID="lblPresT" Text="Presión (Psi)" /></td>
                                <td colspan="3" class="cabecera">
                                    <asp:Label runat="server" ID="lblTempT" Text="Temperatura (C°)" /></td>
                            </tr>
                            <tr>
                                <td class="cabecera">
                                    <asp:Label runat="server" ID="lblRuedas2" Text="Posición" /></td>
                                <td class="cabecera">
                                    <asp:Label ID="lblPresMin" runat="server" Text="Min." /></td>
                                <td class="cabecera">
                                    <asp:Label ID="lblPresMax" runat="server" Text="Max." /></td>
                                <td class="cabecera">
                                    <asp:Label ID="lblPresAvg" runat="server" Text="Prom." /></td>
                                <td class="cabecera">
                                    <asp:Label ID="lblTempMin" runat="server" Text="Min." /></td>
                                <td class="cabecera">
                                    <asp:Label ID="lblTempMax" runat="server" Text="Max." /></td>
                                <td class="cabecera">
                                    <asp:Label ID="lblTempAvg" runat="server" Text="Prom." /></td>
                            </tr>
                            <tr>
                                <td class="cabecera">1</td>
                                <td class="celda">
                                    <asp:Label ID="lblpMinR1" runat="server"></asp:Label></td>
                                <td class="celda">
                                    <asp:Label ID="lblpMaxR1" runat="server"></asp:Label></td>
                                <td class="celda">
                                    <asp:Label ID="lblpAvgR1" runat="server"></asp:Label></td>
                                <td class="celda">
                                    <asp:Label ID="lbltMinR1" runat="server"></asp:Label></td>
                                <td class="celda">
                                    <asp:Label ID="lbltMaxR1" runat="server"></asp:Label></td>
                                <td class="celda">
                                    <asp:Label ID="lbltAvgR1" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td class="cabecera">2</td>
                                <td class="celda">
                                    <asp:Label ID="lblpMinR2" runat="server"></asp:Label></td>
                                <td class="celda">
                                    <asp:Label ID="lblpMaxR2" runat="server"></asp:Label></td>
                                <td class="celda">
                                    <asp:Label ID="lblpAvgR2" runat="server"></asp:Label></td>
                                <td class="celda">
                                    <asp:Label ID="lbltMinR2" runat="server"></asp:Label></td>
                                <td class="celda">
                                    <asp:Label ID="lbltMaxR2" runat="server"></asp:Label></td>
                                <td class="celda">
                                    <asp:Label ID="lbltAvgR2" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td class="cabecera">3</td>
                                <td class="celda">
                                    <asp:Label ID="lblpMinR3" runat="server"></asp:Label></td>
                                <td class="celda">
                                    <asp:Label ID="lblpMaxR3" runat="server"></asp:Label></td>
                                <td class="celda">
                                    <asp:Label ID="lblpAvgR3" runat="server"></asp:Label></td>
                                <td class="celda">
                                    <asp:Label ID="lbltMinR3" runat="server"></asp:Label></td>
                                <td class="celda">
                                    <asp:Label ID="lbltMaxR3" runat="server"></asp:Label></td>
                                <td class="celda">
                                    <asp:Label ID="lbltAvgR3" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td class="cabecera">4</td>
                                <td class="celda">
                                    <asp:Label ID="lblpMinR4" runat="server"></asp:Label></td>
                                <td class="celda">
                                    <asp:Label ID="lblpMaxR4" runat="server"></asp:Label></td>
                                <td class="celda">
                                    <asp:Label ID="lblpAvgR4" runat="server"></asp:Label></td>
                                <td class="celda">
                                    <asp:Label ID="lbltMinR4" runat="server"></asp:Label></td>
                                <td class="celda">
                                    <asp:Label ID="lbltMaxR4" runat="server"></asp:Label></td>
                                <td class="celda">
                                    <asp:Label ID="lbltAvgR4" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td class="cabecera">5</td>
                                <td class="celda">
                                    <asp:Label ID="lblpMinR5" runat="server"></asp:Label></td>
                                <td class="celda">
                                    <asp:Label ID="lblpMaxR5" runat="server"></asp:Label></td>
                                <td class="celda">
                                    <asp:Label ID="lblpAvgR5" runat="server"></asp:Label></td>
                                <td class="celda">
                                    <asp:Label ID="lbltMinR5" runat="server"></asp:Label></td>
                                <td class="celda">
                                    <asp:Label ID="lbltMaxR5" runat="server"></asp:Label></td>
                                <td class="celda">
                                    <asp:Label ID="lbltAvgR5" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td class="cabecera">6</td>
                                <td class="celda">
                                    <asp:Label ID="lblpMinR6" runat="server"></asp:Label></td>
                                <td class="celda">
                                    <asp:Label ID="lblpMaxR6" runat="server"></asp:Label></td>
                                <td class="celda">
                                    <asp:Label ID="lblpAvgR6" runat="server"></asp:Label></td>
                                <td class="celda">
                                    <asp:Label ID="lbltMinR6" runat="server"></asp:Label></td>
                                <td class="celda">
                                    <asp:Label ID="lbltMaxR6" runat="server"></asp:Label></td>
                                <td class="celda">
                                    <asp:Label ID="lbltAvgR6" runat="server"></asp:Label></td>
                            </tr>
                        </table>
                        <br />
                        <br />
                        <br />
                    </div>
                    <br />
                    <br />

                </div>
                <div align="center">
                    <asp:Panel ID="pnlDisclaimer" runat="server" CssClass="backgroundPOPAP" Width="72%" Style="display: none;">
                        <div class="der">
                            <asp:ImageButton ID="btnOkCerrar" runat="server" ImageUrl="~/Imagenes/Iconos/close2.png" CausesValidation="false" />
                        </div>
                        <div>
                            <table>
                                <tr>
                                    <td colspan="2" align="center">
                                        <asp:Label runat="server" ID="lblNotif" Text="Notificaciones Automaticas" CssClass="SubTitAppBlancoG" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div style="height: 307px; width: 550px; overflow: auto;">
                                            <asp:GridView runat="server" ID="gdvNotificaciones" Style="height: 300px; width: 530px;" AutoGenerateColumns="true" OnDataBound="dgvNotificaciones_DataBound" OnRowDataBound="dgvNotificaciones_RowDataBound"></asp:GridView>
                                        </div>
                                    </td>
                                    <td class="alineado_up_centro">
                                        <div>
                                            <asp:Chart ID="cNeumatico" runat="server" CssClass="backgroundPOPAP" Width="390px">
                                                <ChartAreas>
                                                    <asp:ChartArea Name="cArea">
                                                        <Area3DStyle Enable3D="True" Inclination="48" />
                                                    </asp:ChartArea>
                                                </ChartAreas>
                                            </asp:Chart>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="center">
                                        <table style="width: 85%">
                                            <tr>
                                                <td align="center" colspan="2">
                                                    <br />
                                                    <asp:Label runat="server" ID="lblTitNotif" Text="Notificaciones Manuales" CssClass="SubTitAppBlancoG" />
                                                    <br />
                                                    <br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="2">
                                                    <asp:DropDownList runat="server" ID="ddlInsertComentario" CssClass="input" Style="width: auto;" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="2">
                                                    <asp:Button runat="server" ID="btnGuardarNotificacion" AutoPostBack="true" CssClass="input" Text="Guardar Notificacion" OnClick="btnGuardarNotificacion_Click" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                   
                                                </td>
                                                <td align="center">
                                                    <asp:Button ID="btnExportarNotificacion" runat="server" CssClass="input" OnClick="btnExportarNotificacion_Click" Text="Exportar a Excel" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <br />

                            <asp:GridView runat="server" ID="gdvNotificacion" AutoGenerateColumns="true" />
                            <br />
                        </div>
                    </asp:Panel>
                </div>
                <asp:Label runat="server" ID="lblAct" Text="Actualizado:" CssClass="der" />
                <asp:ModalPopupExtender ID="ModalPopupExtender1" runat="server" PopupControlID="pnlDisclaimer" TargetControlID="lblTitNotif" DropShadow="false" BackgroundCssClass="FondoAplicacion" OkControlID="btnOkCerrar" />
            </ContentTemplate>
        </asp:UpdatePanel>




    </div>

    <asp:UpdatePanel ID="udpMensajes" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Panel ID="pMensaje" runat="server" CssClass="BordeForm" Style="display: none; width: 500px;">
                <asp:Panel ID="pCabecera" runat="server" CssClass="TituloForm">
                    <asp:Label ID="Label7" runat="server" Text="Integridad" />
                </asp:Panel>
                <asp:Panel ID="pBody" runat="server">
                    <br />
                    <br />
                    <asp:Image ID="imgIcono" runat="server" ImageUrl="~/StyleSheet/Icon/info-icon.png" Width="24" Height="24" />
                    <asp:Label ID="lblMen" runat="server" /><br />
                    <br />
                    <asp:Button ID="btnAceptar" runat="server" Text="Aceptar" CssClass="input" /><br />
                    <br />
                </asp:Panel>
            </asp:Panel>
            <asp:ModalPopupExtender ID="MenPOP" runat="server" TargetControlID="lblMen" PopupControlID="pMensaje" DropShadow="false" BackgroundCssClass="FondoAplicacion" OnOkScript="btnAceptar" />
        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>
