﻿using Com.Vmica.View;
using Com.Vmica.View.Camiones;
using TKPH_BUSINESS.BUSINESS_PAGE;
using TKPH_BUSINESS.BUSINESS_PAGE.Camiones;
using TKPH_BUSINESS.COMUN;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TKPH_WEB.Camiones
{
    /// <summary>
    /// Pantalla Vista general FLota de Camiones.
    /// </summary>
    public partial class Flota : System.Web.UI.Page
    {

        // Llamada a instancia de Objeto "bReporteTwo"
        bReporteTwo _business = new bReporteTwo();
        

        IList<vCamionDetails> _Single;
        IList<vCamionDetails> _Filtrado;
        String Status = "";


        /// <summary>
        /// Metodo de carga Inicial de la Página
        /// </summary>
        /// <param name="sender">La fuente del evento.</param>
        /// <param name="e">La Instancia que contiene los datos del evento.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            
            HttpContext.Current.Session["Titulo"] = "Estado Flota Operativa";


            //Llamada a metodo de carga de listado de Camiones, con parametro "0" para cargar todas las alarmas.
            CreaCamiones(0);

            if (!IsPostBack)
            {

                // Setea datos para resumen agrupado por alarma.
                lblFR.Text = " " + (_Single.Where(a => a.Alerta.Equals(5)).Count() + _Single.Where(a => a.Alerta.Equals(1)).Count()).ToString();
                lblFA.Text = " " + (_Single.Where(a => a.Alerta.Equals(6)).Count() + _Single.Where(a => a.Alerta.Equals(2)).Count()).ToString();
                lblFV.Text = " " + _Single.Where(a => a.Alerta.Equals(3)).Count().ToString();
                lblFG.Text = " " + _Single.Where(a => a.Alerta.Equals(4)).Count().ToString();
                lblTC.Text = _Single.Count().ToString();

                lnkFR.CssClass = "camionResumen";
                lnkFA.CssClass = "camionResumen";
                lnkFV.CssClass = "camionResumen";
                lnkFG.CssClass = "camionResumen";
                lblAct.Text = "Actualizado: " + DateTime.Now.ToString("HH:mm:ss");

            }

        }

        /// <summary>
        /// Metodo de Creación y de opbjetos para representación del la Flota. 
        /// </summary>
        /// <param name="Alerta">Valor correspondiente a la alarma del camion (0: Todos, 1: Rojo, 2: Amarillo, 3: Verde, 4: Gris)</param>
        public void CreaCamiones(int Alerta)
        {
            try
            {
                pnlCamiones.Controls.Clear();

                if (_Single == null)
                {
                    _Single = _business.GetCamionSingle(new vStatusRequest() { AlertaGeneral = Status });
                }

                switch (Alerta)
                {
                    case 0:
                        _Filtrado = _Single;
                        break;
                    case 1:
                        _Filtrado = _Single.Where(a => a.Alerta.Equals(5)).Union(_Single.Where(b => b.Alerta.Equals(1))).ToList();
                        break;
                    case 2:
                        _Filtrado = _Single.Where(a => a.Alerta.Equals(6)).Union(_Single.Where(b => b.Alerta.Equals(2))).ToList();
                        break;
                    default:
                        _Filtrado = _Single.Where(a => a.Alerta.Equals(Alerta)).ToList();
                        break;

                }


                foreach (vCamionDetails cam in _Filtrado)
                {

                    Panel pnl = new Panel();
                    Label lblCamion = new Label();
                    Label lblCarga = new Label();
                    Label lblTkph = new Label();
                    Label lblNeum = new Label();
                    Label lblFatiga = new Label();


                    ImageButton imgCamion = new ImageButton();

                    if (cam.Alerta != 4)
                    {
                        imgCamion.Click += new ImageClickEventHandler(imgCamion_Click);
                        imgCamion.ID = cam.IdCamion.ToString();
                    }
                    else
                    {
                        imgCamion.ToolTip = "Información de Camión No Disponible";
                        imgCamion.Enabled = false;
                    }


                    imgCamion.ImageUrl = _business.ColorCamion(cam.Alerta);
                    imgCamion.CssClass = "ancho_camiones";

                    pnl.CssClass = "camiones";
                    lblCamion.Text = cam.IdCamion.ToString();

                    lblCarga.Text = "C ";
                    lblCarga.ForeColor = _business.ColorAlarma(cam.Carga);
                    lblCarga.Font.Bold = true;

                    lblFatiga.Text = "F ";
                    lblFatiga.ForeColor = _business.ColorAlarma(cam.Carga);
                    lblFatiga.Font.Bold = true;


                    lblNeum.Text = "N ";
                    lblNeum.ForeColor = _business.ColorAlarma(cam.Neumatico);
                    lblNeum.Font.Bold = true;

                    pnl.Controls.Add(imgCamion);
                    pnl.Controls.Add(new LiteralControl("<br>"));
                    pnl.Controls.Add(lblCamion);
                    pnl.Controls.Add(new LiteralControl("<br>"));
                    pnl.Controls.Add(lblCarga);         
                    pnl.Controls.Add(lblNeum);
                    pnl.Controls.Add(lblFatiga);

                    pnlCamiones.Controls.Add(pnl);
                }

            }
            catch (Exception e)
            {
                lblMen.Text = e.Message.ToString();
                MenPOP.Show();
            }



          

        }


        /// <summary>
        /// Metodo para realizar filtro en GUI según color alarma.
        /// </summary>
        /// <param name="Alerta">Corresponde al color o Alarma.</param>
        /// <param name="Link">Corresponde al "LinkButon" presionado para invocar al metodo.</param>
        public void Filtrar(int Alerta, LinkButton Link)
        {

            if (Link.CssClass == "camionResumen")
            {
                CreaCamiones(Alerta);
                lnkFR.CssClass = "camionResumen";
                lnkFA.CssClass = "camionResumen";
                lnkFV.CssClass = "camionResumen";
                lnkFG.CssClass = "camionResumen";
                Link.CssClass = "camionResumenSel";
             }
            else
            {
                CreaCamiones(0);
                lnkFR.CssClass = "camionResumen";
                lnkFA.CssClass = "camionResumen";
                lnkFV.CssClass = "camionResumen";
                lnkFG.CssClass = "camionResumen";

            }
        }


        #region ViewState: Manejo de variables estables en view State
        protected override object SaveViewState()
        {
            object[] SavedState = new object[18];

            SavedState[0] = base.SaveViewState();
            //if (_Single != null) SavedState[1] = Serializador.SerializeObjeto<vCamionDetails[]>(_Single.ToArray());

            return SavedState;
        }

        protected override void LoadViewState(object SavedState)
        {
            if (SavedState != null)
            {
                object[] state = (object[])SavedState;

                if (state[0] != null) base.LoadViewState(state[0]);
                //if (state[1] != null) _Single = Serializador.DesSerializeObjeto<vCamionDetails[]>((string)state[1]).ToList();

            }
        }
        #endregion 
       

        /// <summary>
        /// Evento Click del LinkButton para filtrar flota por Alarma Roja
        /// </summary>
        /// <param name="sender">La fuente del evento.</param>
        /// <param name="e">La Instancia que contiene los datos del evento.</param>
        protected void lnkFR_Click(object sender, EventArgs e)
        {
            Filtrar(1, (LinkButton)sender);
        }

        /// <summary>
        /// Evento Click del LinkButton para filtrar flota por Alarma Amarilla
        /// </summary>
        /// <param name="sender">La fuente del evento.</param>
        /// <param name="e">La Instancia que contiene los datos del evento.</param>
        protected void lnkFA_Click(object sender, EventArgs e)
        {
            Filtrar(2, (LinkButton)sender);
        }

        /// <summary>
        /// Evento Click del LinkButton para filtrar flota por Alarma Verde
        /// </summary>
        /// <param name="sender">La fuente del evento.</param>
        /// <param name="e">La Instancia que contiene los datos del evento.</param>
        protected void lnkFV_Click(object sender, EventArgs e)
        {
            Filtrar(3, (LinkButton)sender);
        }

        /// <summary>
        /// Evento Click del LinkButton para filtrar flota por Alarma Gris
        /// </summary>
        /// <param name="sender">La fuente del evento.</param>
        /// <param name="e">La Instancia que contiene los datos del evento.</param>
        protected void lnkFG_Click(object sender, EventArgs e)
        {
            Filtrar(4, (LinkButton)sender);
        }

        /// <summary>
        /// Evento Click de la imagen que representa al camion para visualizar su detalle
        /// </summary>
        /// <param name="sender">La fuente del evento.</param>
        /// <param name="e">La Instancia que contiene los datos del evento.</param>
        protected void imgCamion_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton cam = (ImageButton)sender;
            HttpContext.Current.Session["pro-camion"] = cam.ID.ToString();
            Response.Redirect("~/Camiones/DetalleCamion.aspx");
        }

        /// <summary>
        /// Evento de cumplimiento de ciclo dentro del Timer para actualización del estado general de la flota
        /// </summary>
        /// <param name="sender">La fuente del evento.</param>
        /// <param name="e">La Instancia que contiene los datos del evento.</param>
        protected void tmrAct_Tick(object sender, EventArgs e)
        {
            CreaCamiones(0);

            // Resumen Camiones

            lblFR.Text = " " + (_Single.Where(a => a.Alerta.Equals(5)).Count() + _Single.Where(a => a.Alerta.Equals(1)).Count()).ToString();
            lblFA.Text = " " + (_Single.Where(a => a.Alerta.Equals(6)).Count() + _Single.Where(a => a.Alerta.Equals(2)).Count()).ToString();
            lblFV.Text = " " + _Single.Where(a => a.Alerta.Equals(3)).Count().ToString();
            lblFG.Text = " " + _Single.Where(a => a.Alerta.Equals(4)).Count().ToString();
            lblTC.Text = _Single.Count().ToString();

            lnkFR.CssClass = "camionResumen";
            lnkFA.CssClass = "camionResumen";
            lnkFV.CssClass = "camionResumen";
            lnkFG.CssClass = "camionResumen";
            lblAct.Text = "Actualizado: " + DateTime.Now.ToString("HH:mm:ss");
        }

       

    }
}