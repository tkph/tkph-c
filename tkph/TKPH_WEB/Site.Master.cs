﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Com.Vmica.View;
using TKPH_WEB;
using TKPH_BUSINESS;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.Script.Serialization;
using TKPH_BUSINESS.BUSINESS_PAGE.Mantenedores;
using Com.Vmica.View.Mantenedores;
using TKPH_WEB.Camiones;
using System.IO;

namespace TKPH_WEB
{
    /// <summary>
    /// Pagina Maestra Aplicación TKPH
    /// </summary>
    public partial class Site : System.Web.UI.MasterPage
    {
        
        // Llamada a instancia de Objeto "bMantenedorBudget"
        bMantenedorBudget _dataBudget = bMantenedorBudget.Instance;
        public static IList<vMantenedorCarga> _DatosMant;
        public static String UrlVolver = "";
        List<String> UrlBack = new List<String>();


        /// <summary>
        /// Metodo de carga Inicial de la Página
        /// </summary>
        /// <param name="sender">La fuente del evento.</param>
        /// <param name="e">La Instancia que contiene los datos del evento.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["usuario"] == null)
            {
                Response.Redirect("~/Index.aspx");
            }
            else
            { 
            String TitApp = ConfigurationManager.AppSettings["APPNAME"].ToString();
            String TitAppD = ConfigurationManager.AppSettings["APPNAMED"].ToString();

            lblTitDisc.Text = TitAppD + " -  Disclaimer";
            lblDefault.Text = TitApp;
            lblTitPage.Text = HttpContext.Current.Session["Titulo"].ToString();

            lblDisclaimer.Text = "<b>" + TitApp + "</b><br/> La información desplegada tiene un desfase temporal máximo de 15 minutos, Considere que esta solución está capturando información <br/>generada automáticamente en su fuente origen, por lo tanto las alertas informadas dependen de la calidad de dicha fuente. Si tiene dudas, <br/> recomendamos contactar a los responsables de operación/mantención de las fuentes  de información.";
           
            }

        }

        /// <summary>
        /// Evento Click del LinkButton para cerrar mensaje Disclaimer
        /// </summary>
        /// <param name="sender">La fuente del evento.</param>
        /// <param name="e">La Instancia que contiene los datos del evento.</param>
        protected void lnkDisclaimer_Click(object sender, EventArgs e)
        {
            popDisclaimer.Show();
        }

        /// <summary>
        /// Evento Click del LinkButton Mantenedor para mostrar Popup con Opciones de Mantenedor
        /// </summary>
        /// <param name="sender">La fuente del evento.</param>
        /// <param name="e">La Instancia que contiene los datos del evento.</param>
        protected void lnkMantenedor_Click(object sender, EventArgs e)
        {
            ModalPopupExtender2.Show();
        }

        /// <summary>
        /// Evento Click del LinkButton Dashboard para direccionar a Pantalla Dashboard de Carga
        /// </summary>
        /// <param name="sender">La fuente del evento.</param>
        /// <param name="e">La Instancia que contiene los datos del evento.</param>
        protected void lnkAnalCarga_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/BI/DashboardCarga.aspx", true);
        }

        /// <summary>
        /// Evento Click del LinkButton Dashboard para direccionar a Pantalla Dashboard de Tkph
        /// </summary>
        /// <param name="sender">La fuente del evento.</param>
        /// <param name="e">La Instancia que contiene los datos del evento.</param>
        protected void lnkAnalTkph_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/BI/DashboardTkph.aspx", true);
        }

        /// <summary>
        /// Evento Click del LinkButton Dashboard para direccionar a Pantalla Flota
        /// </summary>
        /// <param name="sender">La fuente del evento.</param>
        /// <param name="e">La Instancia que contiene los datos del evento.</param>
        protected void lnkFlotaC_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Camiones/Flota.aspx", true);
        }

        /// <summary>
        /// Evento Click del LinkButton COnfiguración para mostrar Popup de Configuración
        /// </summary>
        /// <param name="sender">La fuente del evento.</param>
        /// <param name="e">La Instancia que contiene los datos del evento.</param>
        protected void lnkConfig_Click(object sender, EventArgs e)
        {
            _DatosMant = _dataBudget.GetDatosBudget(1, 1, "", "12", "");
            chkCamTrans.Checked = (_DatosMant.First().valor == 0) ? false : true;
            popCfg.Show();
        }


        /// <summary>
        /// Metodo para cambiar configuración al mostrar camión coloreado o en escala de grises,
        /// si se encuentra en pantalla detalle camion realiza cambio en la GUI.
        /// </summary>
        /// <param name="sender">La fuente del evento.</param>
        /// <param name="e">La Instancia que contiene los datos del evento.</param>
        protected void chkCamTrans_CheckedChanged(object sender, EventArgs e)
        {
            String valor = (chkCamTrans.Checked) ? "1" : "0";
            _dataBudget.SetDatosBudget("83", valor, "");
            if (Path.GetFileName(Request.Url.AbsolutePath)=="DetalleCamion.aspx")
          
            {
                Panel pnlD = BodyC.FindControl("pnlAdelante") as Panel;
                Panel pnlT = BodyC.FindControl("pnlAtras") as Panel;
                UpdatePanel up = BodyC.FindControl("upDetalleCamion") as UpdatePanel;

                String eD = pnlD.CssClass.ToString().Substring(0,pnlD.CssClass.ToString().Length-2);
                String eT = pnlT.CssClass.ToString().Substring(0, pnlT.CssClass.ToString().Length - 2);

                pnlD.CssClass = (valor == "1") ? eD + "_C" : eD + "_T";
                pnlT.CssClass = (valor == "1") ? eT + "_C" : eT + "_T";



                up.Update();
                
            }
        }



    }
}