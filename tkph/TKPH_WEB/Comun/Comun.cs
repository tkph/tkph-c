﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.DataVisualization.Charting;
using System.Drawing;
using TKPH_BUSINESS.COMUN;
using Com.Vmica.View;
using Com.Vmica.View.BI;

namespace TKPH_WEB.Comun
{

    /// <summary>
    /// Clase con metodos reutilizables por los Dashboards y Pantallas de la GUI.
    /// </summary>
    public class Comun
    {
        #region Nuevo

        /// <summary>
        /// Inicializa una nueva serie de grafico.
        /// </summary>
        /// <param name="Listas">Interfaz de Lista Tipo "vGrafica".</param>
        /// <param name="Type">Tipo de Serie.</param>
        /// <returns>Retorna una Array tipo Serie.</returns>
        public static Series[] CreateSerie(IList<vGrafica> Listas, SeriesChartType Type)
        {
            IList<Series> serie = new List<Series>();

            var lGroup = from p in Listas
                         group p by p.SerieGrafico into newGroup
                         orderby newGroup.Key
                         select newGroup;

            foreach (var newSerie in lGroup)
            {
                Series s = new Series(newSerie.Key);

                s.ChartType = Type;
                s.ChartArea = "cArea";

                serie.Add(s);
            }


            return serie.ToArray();
        }


        /// <summary>
        /// Inicializa una nuevo punto de la serie del grafico.
        /// </summary>
        /// <param name="Data">Interfaz de lista tipo "vGrafica" que contiene los datos</param>
        /// <param name="Serie">La serie inicializada.</param>
        /// <param name="Grafico">Objeto tipo Chart</param>
        public static void CreatePoint(IList<vGrafica> Data, Series[] Serie, Chart Grafico)
        {
            foreach (Series s in Serie)
            {
                int i = 0;

                foreach (vGrafica row in Data.Where(p => p.SerieGrafico.Equals(s.Name)).ToList())
                {
                    s.Points.Add(row.PointValue);

                    var p = s.Points[i];

                    p.AxisLabel = row.StringLabel;
                    i++;
                }

                Grafico.Series.Add(s);
            }
        }

        /// <summary>
        /// Inicializa una nueva serie de grafico.
        /// </summary>
        /// <param name="Name">Nombre de la Serie.</param>
        /// <param name="Type">Tipo de Serie.</param>
        /// <returns> Retorna un Objeto tipo Chart Series. </returns>
        public static Series CreateSerie(String Name, SeriesChartType Type)
        {
            Series _serie = new Series(Name);

            _serie.ChartType = Type;
            _serie.ChartArea = "cArea";
            return _serie;
        }

        public static void CreatePoint(IList<vEstadoNeumatico> Rows, Series Serie, Int64 Total)
        {
            foreach (vEstadoNeumatico row in Rows)
            {
                DataPoint _p = new DataPoint()
                {
                    Color = row.Valor.Equals(3) ? Color.Green : row.Valor.Equals(2) ? Color.Yellow : row.Valor.Equals(1) ? Color.Red : Color.Blue,
                    XValue = Total,
                    Url = "ReportTwo.aspx?status=" + Seguridad.EncriptarString(row.Valor.ToString()),
                    ToolTip = "#PERCENT{P0}",
                    LabelForeColor = Color.Black,      //!row.Valor.Equals(2) ? Color.White : Color.Black
                    MarkerSize = 14
                };

                _p.YValues[0] = Convert.ToInt32(row.Total);

                Serie.Points.Add(_p);
            }
        }
        public static void CreatePoint(IList<vDisponible> Rows, Series Serie)
        {
            int i = 0;
            foreach (vDisponible row in Rows)
            {
                DataPoint _p = new DataPoint()
                {
                    //Color = row.Valor.Equals(3) ? Color.Green : row.Valor.Equals(2) ? Color.Yellow : row.Valor.Equals(1) ? Color.Red : Color.Blue,
                    XValue = i,
                    //Url = "ReportTwo.aspx?status=" + Seguridad.EncriptarString(row.Valor.ToString()),
                    //ToolTip = "#PERCENT{P0}"
                };

                i++;

                _p.YValues[0] = row.Porcentaje; //Convert.ToInt32(Rows.Max(p => p.Porcentaje));

                Serie.Points.Add(_p);
            }
        }

        public static void CreatePoint(IList<vHistorico> Rows, Series Serie, Boolean vbool)
        {
            int i = 0;

            foreach (vHistorico row in Rows)
            {
                if (vbool)
                    Serie.Points.Add(row.Nonimal);
                else
                    Serie.Points.Add(row.Operacional);

                var p1 = Serie.Points[i];

                p1.ToolTip = "#AXISLABEL -> #VALY{N0}";

                p1.MarkerSize = 3;

                p1.MarkerStyle = MarkerStyle.Circle;

                p1.AxisLabel = row.Fecha.ToString("HH:mm");

                p1.LegendText = vbool ? "Nominal" : "Operacional";

                i++;
            }
        }

        public static void CreatePoint(IList<vCamionNotificacion> Rows, Series Serie, Boolean vbool)
        {
            int i = 0;

            foreach (vCamionNotificacion row in Rows)
            {
                if (vbool)
                    Serie.Points.Add(Convert.ToDouble(row.VALOR));
                else
                    Serie.Points.Add(Convert.ToDouble(row.VALOR));

                var p1 = Serie.Points[i];

                p1.ToolTip = "#AXISLABEL -> #VALY{N0}";

                p1.MarkerSize = 3;

                p1.MarkerStyle = MarkerStyle.Circle;

                p1.AxisLabel = row.VALOR;

                p1.LegendText = row.TIPO_ALARMA;

                p1.Color = row.TIPO_ALARMA.Equals("Alarma de Carga") ? Color.FromArgb(17, 42, 79) : row.TIPO_ALARMA.Equals("Alarma de Temperatura") ? Color.FromArgb(95, 133, 161) : row.TIPO_ALARMA.Equals("Alarma de Presion") ? Color.FromArgb(7, 81, 137) : Color.FromArgb(25, 147, 191);

                p1.LabelForeColor = Color.White;

                i++;
            }
        }

        public static void CreatePoint(IList<vHistoricoPT> Rows, Series Serie, Boolean vbool)
        {
            int i = 0;

            foreach (vHistoricoPT row in Rows)
            {
                if (vbool)
                    Serie.Points.Add(row.TEMPERATURA);
                else
                    Serie.Points.Add(row.PRESION);

                var p1 = Serie.Points[i];

                p1.ToolTip = "#AXISLABEL -> #VALY{N0}";

                p1.MarkerSize = 3;

                p1.MarkerStyle = MarkerStyle.Circle;

                p1.AxisLabel = row.FECHA.ToString("HH:mm");

                p1.LegendText = vbool ? "TEMPERATURA" : "PRESION";

                i++;
            }
        }



        # region Graficos BI Carga

        /// <summary>
        /// Inicializa una nuevo punto de la serie del grafico Flota Carga.
        /// </summary>
        /// <param name="Rows">Interfaz de lista tipo "vCargaFlota" con datos de puntos.</param>
        /// <param name="Serie">Serie del grafico donde se agregaran los puntos.</param>
        /// <param name="Flota">Nombre de la Flota.</param>
        public static void CreatePoint(IList<vCargaFlota> Rows, Series Serie, String Flota)
        {
            int i = 0;

            foreach (vCargaFlota row in Rows)
            {
                Serie.Points.Add(row.Porcentaje);

                var p1 = Serie.Points[i];

                p1.ToolTip = "Carga Nominal: " + row.Promedio+".";
                p1.AxisLabel = row.FlotaCamion.ToString();

                p1.Label = row.Porcentaje.ToString() + "%";
                p1.LabelForeColor = Color.White;

                int tr = (row.FlotaCamion.ToString() == Flota.ToString()) ? 255 : 100;

                p1.Color = Colores(int.Parse(row.AlarmaPunto.ToString()) + 10, tr);

                i++;
            }
        }

        /// <summary>
        /// Inicializa una nuevo punto de la serie del grafico Histograma Carga.
        /// </summary>
        /// <param name="Rows">Interfaz de lista tipo "vCargaHistograma" con datos de puntos.</param>
        /// <param name="Serie">Serie del grafico donde se agregaran los puntos.</param>
        public static void CreatePoint(IList<vCargaHistograma> Rows, Series Serie)
        {
            int i = 0;

            
            foreach (vCargaHistograma row in Rows)
            {
                Serie.Points.Add(row.Valor);

                var p1 = Serie.Points[i];

                p1.MarkerSize = 3;

                p1.MarkerStyle = MarkerStyle.Circle;

                p1.ToolTip = "#AXISLABEL: #VALY{N0} Eventos.";

                p1.AxisLabel = row.Tipo.ToString();

                p1.Color = Colores(int.Parse(row.AlarmaPunto.ToString()) + 10, 255);

                i++;
            }
        }


        /// <summary>
        /// Inicializa una nuevo punto de la serie del grafico Tendencia Carga.
        /// </summary>
        /// <param name="Rows">Interfaz de lista tipo "vCargaTendencia" con datos de puntos.</param>
        /// <param name="Serie">Serie del grafico donde se agregaran los puntos.</param>
        public static void CreatePoint(IList<vCargaTendencia> Rows, Series Serie)
        {
            int i = 0;

            foreach (vCargaTendencia row in Rows)
            {
                Serie.Points.Add(row.Valor);

                var p1 = Serie.Points[i];

                p1.ToolTip = Serie.Name.ToString()+":  #VALY{N0} eventos.";

                p1.AxisLabel = row.Fecha.ToString("dd-MM-yyyy");

                p1.MarkerSize = 4;

                p1.MarkerStyle = MarkerStyle.Circle;

                i++;
            }
        }


        /// <summary>
        /// Inicializa una nuevo punto de la serie del grafico Ranking Carga.
        /// </summary>
        /// <param name="Rows">Interfaz de lista tipo "vCargaRanking" con datos de puntos.</param>
        /// <param name="Serie">Serie del grafico donde se agregaran los puntos.</param>
        /// <param name="Porc">Verdadero: Se agrega texto "%", Falso: Se agrega texto "eventos".</param>
        public static void CreatePoint(IList<vCargaRanking> Rows, Series Serie, Boolean Porc)
        {
            int i = 0;

            foreach (vCargaRanking row in Rows)
            {
                Serie.Points.Add(row.Valor);

                var p1 = Serie.Points[i];

                String Escala = Porc ? "%" : " eventos.";

                p1.ToolTip = Serie.Name.ToString() + " #AXISLABEL: #VALY{N0}" + Escala;

                p1.AxisLabel = "" + row.FlotaCamion + "";

                p1.Label = Math.Round(row.Valor, 0).ToString();
                p1.LabelForeColor = Color.White;

                p1.Color = Colores(int.Parse(row.AlarmaPunto.ToString()) + 10, 255);

                i++;
            }
        }

        #endregion

        # region Graficos BI Tkph

        /// <summary>
        /// Inicializa una nuevo punto de la serie del grafico FLota TKPH.
        /// </summary>
        /// <param name="Rows">Interfaz de lista tipo "vTkphFlota" con datos de puntos.</param>
        /// <param name="Serie">Serie del grafico donde se agregaran los puntos.</param>
        /// <param name="Flota">Nombre de la Flota.</param>
        public static void CreatePoint(IList<vTkphFlota> Rows, Series Serie, String Flota)
        {
            int i = 0;

            foreach (vTkphFlota row in Rows)
            {
                Serie.Points.Add(row.Porcentaje);

                var p1 = Serie.Points[i];

                p1.ToolTip = "Tkph Nominal: "+row.TkphNominal;

                p1.AxisLabel = row.FlotaCamion.ToString();

                p1.Label = row.Porcentaje.ToString() + "%";
                p1.LabelForeColor = Color.White;

                int tr = (row.FlotaCamion.ToString() == Flota.ToString()) ? 255 : 100;

                p1.Color = Colores(int.Parse(row.AlarmaPunto.ToString()) + 10, tr);

                i++;
            }
        }

        /// <summary>
        /// Inicializa una nuevo punto de la serie del grafico Histograma TKPH.
        /// </summary>
        /// <param name="Rows">Interfaz de lista tipo "vTkphHistograma" con datos de puntos.</param>
        /// <param name="Serie">Serie del grafico donde se agregaran los puntos.</param>
        public static void CreatePoint(IList<vTkphHistograma> Rows, Series Serie)
        {
            int i = 0;


            foreach (vTkphHistograma row in Rows)
            {
                Serie.Points.Add(row.Valor);

                var p1 = Serie.Points[i];

                p1.MarkerSize = 3;

                p1.MarkerStyle = MarkerStyle.Circle;

                p1.ToolTip = "#AXISLABEL: #VALY{N0}";

                p1.AxisLabel = row.Tipo.ToString();

                p1.Color = Colores(int.Parse(row.AlarmaPunto.ToString())+10, 255);

                i++;
            }
        }


        /// <summary>
        /// Inicializa una nuevo punto de la serie del grafico Ranking Destino TKPH.
        /// </summary>
        /// <param name="Rows">Interfaz de lista tipo "vTkphDestino" con datos de puntos.</param>
        /// <param name="Serie">Serie del grafico donde se agregaran los puntos.</param>
        public static void CreatePoint(IList<vTkphDestino> Rows, Series Serie)
        {
            int i = 0;


            foreach (vTkphDestino row in Rows)
            {

                Serie.Points.Add(row.Porcentaje);

                var p1 = Serie.Points[i];

                p1.ToolTip = Serie.Name.ToString() + " #AXISLABEL: #VALY{N0}%";

                p1.AxisLabel = row.Destino.ToString();


                p1.Label = Math.Round(row.Porcentaje,0).ToString();

                p1.LabelForeColor = Color.White;

                p1.Color = Colores(int.Parse(row.AlarmaPunto.ToString()) + 10, 255);

                i++;
            }
        }


        /// <summary>
        /// Inicializa una nuevo punto de la serie del Ranking TKPH.
        /// </summary>
        /// <param name="Rows">Interfaz de lista tipo "vCargaFlota" con datos de puntos.</param>
        /// <param name="Serie">Serie del grafico donde se agregaran los puntos.</param>
        /// <param name="Porc">Verdadero: Se agrega texto "%", Falso: Se agrega texto "eventos".</param>
        public static void CreatePoint(IList<vTkphRanking> Rows, Series Serie, Boolean Porc)
        {
            int i = 0;


            foreach (vTkphRanking row in Rows)
            {

               Serie.Points.Add(row.Promedio); 
                
                var p1 = Serie.Points[i];

                String Escala = Porc ? "%" : " eventos.";

                p1.ToolTip = Serie.Name.ToString() + " #AXISLABEL: #VALY{N0}" + Escala;

                p1.AxisLabel = row.Objeto;

                p1.Label = Math.Round(row.Promedio, 0).ToString();
                
                p1.LabelForeColor = Color.White;

                p1.Color = Colores(int.Parse(row.AlarmaPunto.ToString()) + 10, 255);

                i++;
            }

            

        }



        /// <summary>
        /// Inicializa una nuevo punto de la serie del grafico Tendencia TKPH.
        /// </summary>
        /// <param name="Rows">Interfaz de lista tipo "vTkphDestino" con datos de puntos.</param>
        /// <param name="Serie">Serie del grafico donde se agregaran los puntos.</param>
        public static void CreatePoint(IList<vTkphTendencia> Rows, Series Serie)
        {
            int i = 0;


            foreach (vTkphTendencia row in Rows)
            {

                Serie.Points.Add(row.Porcentaje);

                var p1 = Serie.Points[i];

                p1.ToolTip = "(" + Math.Round(row.Porcentaje,0) + "%) " + Serie.Name.ToString() + ": " + row.Promedio;

                p1.AxisLabel = row.Fecha.ToString("dd-MM-yyyy");
                p1.MarkerStyle = MarkerStyle.Circle;
                if (row.Porcentaje > 100)
                {
                    p1.MarkerSize = 6;
                    p1.MarkerColor = Comun.Colores(11, 255);
                }
                else 
                {
                    p1.MarkerSize = 4;
                    
                }

                p1.Color = Colores(int.Parse(row.AlarmaPunto.ToString()) + 10, 255);

                i++;
            }



        }

        #endregion


        /// TODO: Generar Inclucion de clases para estadisticas y control Sistema

        /*
        public static void CreatePoint(IList<vEstadisticas> Rows, Series Serie)
        {
            int i = 0;

            foreach (vEstadisticas row in Rows)
            {

                //Serie.Points.Add(row.Periodo);
                Serie.Points.Add(row.RegCargados);
                Serie.BorderWidth = 3;
                var p1 = Serie.Points[i];

                p1.AxisLabel = row.Fecha.ToString();

                p1.LegendText = "Registros Cargados";

                p1.ToolTip = "#AXISLABEL -> #VALY{N0} Registros Cargados";
                p1.MarkerSize = 8;
                p1.MarkerColor = Color.SkyBlue;
                p1.MarkerBorderColor = Color.SteelBlue;
                p1.MarkerBorderWidth = 3;
                p1.MarkerStyle = MarkerStyle.Circle;

                i++;
            }
        }

        public static void CreatePoint(IList<vEstadisticas> Rows, Series Serie, Int64 QueSerie)
        {
            int i = 0;

            foreach (vEstadisticas row in Rows)
            {

                //Serie.Points.Add(row.Periodo);
                if (QueSerie == 1)
                {
                    Serie.Points.Add(row.ProcesosOk);
                }
                else
                {
                    Serie.Points.Add(row.ProcesosError);
                }

                //Serie.BorderWidth = 3;
                var p1 = Serie.Points[i];

                p1.AxisLabel = row.Fecha;

                p1.LegendText = "Periodo";

                if (QueSerie == 1)
                {
                    p1.ToolTip = "#AXISLABEL -> #VALY{N0} Procesos Exitosos";
                }
                else
                {
                    p1.ToolTip = "#AXISLABEL -> #VALY{N0} Procesos Erroneos";
                }


                i++;
            }
        }

         * */

        #region CreatPoint Barra
        public static void CreatePoint(IList<vGraficoAlertas> Rows, Series Serie)
        {
            int i = 0;

            foreach (vGraficoAlertas row in Rows)
            {
                Serie.Points.Add(row.Value);


                var p1 = Serie.Points[i];

                p1.AxisLabel = row.Fecha.ToString();
                //p1.LegendText = row.Fecha.ToString();
                //p1.Label = row.Fecha.ToString();
                //p1.BorderWidth = 2;

                i++;
            }

        }
        #endregion



        #endregion


        /// <summary>
        /// Metodo para establecer según valor de alarma el icono correspondiente.
        /// </summary>
        /// <param name="Alarma">Valor de la alarma. (1: Rojo, 2: Amarillo, 3: Verde, 4: Gris)</param>
        /// <returns>Retorna la ruta de la imagen correspondiente a la alarma.</returns>
        public static String IconoAlarma(Int64 Alarma)
        {
            String IMA;
            switch (Alarma)
            {
                case 1:
                    IMA = "~/Imagenes/Iconos/Circle-Red.png";
                    break;
                case 2:
                    IMA = "~/Imagenes/Iconos/Circle-Yellow.png";
                    break;
                case 3:
                    IMA = "~/Imagenes/Iconos/Circle-Green.png";
                    break;
                default:
                    IMA = "~/Imagenes/Iconos/Circle-Grey.png";
                    break;
            }


            return IMA;
        }

        /// <summary>
        /// Metodo que aplica formato estandar a graficos.
        /// </summary>
        /// <param name="Grafico">Objeto tipo Chart.</param>
        /// <param name="Angulo">Angulo de las etiquetas del eje X.</param>
        /// <param name="Minimo">Valor Minimo a mostrar en serie Y</param>
        /// <param name="TituloY">Titulo para la Serie Y.</param>
        /// <param name="TituloX">Titulo para la SerieX.</param>
        /// <param name="TitulaGraf">Titulo del Grafico.</param>
        /// <param name="Leyenda">Verdadero Muestra Leyenda, Falso Oculta Leyenda.</param>
        public static void FormatoGraficos(Chart Grafico, int Angulo, int Minimo, String TituloY, String TituloX, String TitulaGraf, Boolean Leyenda)
        {

            Grafico.ChartAreas[0].AxisY.Minimum = Minimo;

            //Grafico.Style.Add("width", "100%");
            Grafico.BackColor = Color.FromArgb(255, 30, 30, 30);

            Grafico.ChartAreas[0].BorderWidth = 0;
            Grafico.ChartAreas[0].BackColor = Color.FromArgb(255, 30, 30, 30);
            Grafico.ChartAreas[0].AxisX.LabelStyle.Angle = Angulo;
            Grafico.ChartAreas[0].AxisY.LabelStyle.Font = Grafico.ChartAreas[0].AxisX.LabelStyle.Font = new System.Drawing.Font("Calibri", 8);
            Grafico.ChartAreas[0].AxisY.LabelStyle.ForeColor = Grafico.ChartAreas[0].AxisX.LabelStyle.ForeColor = Color.White;

            Grafico.ChartAreas[0].AxisY.MajorGrid.LineColor = Color.FromArgb(43, 43, 43);
            Grafico.ChartAreas[0].AxisX.MajorGrid.Enabled = false;
            //Grafico.ChartAreas[0].AxisX.MajorGrid.LineColor = Color.FromArgb(43, 43, 43);
            Grafico.ChartAreas[0].AxisY.MajorGrid.LineWidth = 1;
            //Grafico.ChartAreas[0].AxisX.MajorGrid.LineWidth = 1;

            //Grafico.ChartAreas[0].AxisY.Minimum = Minimo;

            Grafico.ChartAreas[0].AxisY.Title = TituloY;
            Grafico.ChartAreas[0].AxisY.TitleForeColor = Color.White;

            Grafico.ChartAreas[0].AxisX.Title = TituloX;
            Grafico.ChartAreas[0].AxisX.TitleForeColor = Color.White;


            Grafico.ChartAreas[0].AxisX.Interval = 1;

            /* Titulo */
            if (TitulaGraf != "")
            {
                Grafico.Titles.Add("Tit").Text = TitulaGraf.ToString();
                Grafico.Titles[0].ForeColor = Color.White;
            }

            /* Leyenda */
            if (Leyenda)
            {
                Grafico.Legends.Add("Ley");
                Grafico.Legends[0].Docking = Docking.Right;
                Grafico.Legends[0].Font = new System.Drawing.Font("Calibri", 8);
                Grafico.Legends[0].Alignment = StringAlignment.Center;
                Grafico.Legends[0].BackColor = Color.FromArgb(255, 30, 30, 30);
                Grafico.Legends[0].ForeColor = Color.White;
            }


        }

        /// <summary>
        /// Metodo para establecer color en puntos y series.
        /// </summary>
        /// <param name="Numero">Valor para establecer Color.</param>
        /// <param name="Transparencia">Nivel de Transparencia o canal Alpha desde 0 a 255.</param>
        /// <returns>Devuelve Objeto Color con el valor correspondiente a los parametros ingresados.</returns>
        public static Color Colores(int Numero, int Transparencia)
        {
            switch (Numero)
            {
                case 1:
                    return Color.FromArgb(Transparencia, 68, 169, 168);
                case 2:
                    return Color.FromArgb(Transparencia, 91, 155, 213);
                case 3:
                    return Color.FromArgb(Transparencia, 165, 165, 165);
                case 4:
                    return Color.FromArgb(Transparencia, 255, 85, 0);
                case 5:
                    return Color.FromArgb(Transparencia, 184, 72, 255);
                case 6:
                    return Color.FromArgb(Transparencia, 151, 0, 0);
                case 11:
                    return Color.FromArgb(Transparencia, 255, 37, 37);
                case 12:
                    return Color.FromArgb(Transparencia, 255, 216, 0);
                case 13:
                    return Color.FromArgb(Transparencia, 122, 185, 0);
                default:
                    return Color.FromArgb(Transparencia, 68, 169, 168);
            }


        }


        /// <summary>
        /// Metodo para establecer color en puntos y series.
        /// </summary>
        /// <param name="Numero">Valor para establecer Color.</param>
        /// <returns>Devuelve Objeto Color con el valor correspondiente a los parametros ingresados.</returns>
        public static Color Colores(int Numero)
        {
            switch (Numero)
            {
                case 1:
                    return Color.FromArgb(146, 208, 80);
                case 2:
                    return Color.FromArgb(91, 155, 213);
                case 3:
                    return Color.FromArgb(165, 165, 165);
                case 4:
                    return Color.FromArgb(255, 85, 0);
                case 5:
                    return Color.FromArgb(184, 72, 255);
                case 6:
                    return Color.FromArgb(151, 0, 0);
                default:
                    return Color.FromArgb(246, 204, 24);
            }


        }


    }
}