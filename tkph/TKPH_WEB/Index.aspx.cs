﻿using Com.Vmica.View;
using TKPH_BUSINESS.BUSINESS_PAGE;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TKPH_WEB
{
    /// <summary>
    /// Pagina Inicio Sistema TKPH
    /// </summary>
    public partial class Index : System.Web.UI.Page
    {

        // Llamada a instancia de Objeto "bLogin"
        bLogin Login = bLogin.Instance;

        // Creacion de Objeto Usuario del tipo vLogin
        vLogin Usuario = new vLogin();

        /// <summary>
        /// Metodo de carga Inicial de la Página
        /// </summary>
        /// <param name="sender">La fuente del evento.</param>
        /// <param name="e">La Instancia que contiene los datos del evento.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        /// <summary>
        /// Llamada al metodo de consulta de usuario (Inicio de Sesión). 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnLogin_Click(object sender, EventArgs e)
        {
            
            lblMens.Visible = true;

            if (txtUsuario.Text.Length < 3 || txtPassword.Text.Length < 3)
            {
                lblMens.Text = "Debe Completar Usuario y Password";
            }
            else
            {
                lblMens.Text = "Iniciando Sesión Espere...";

                Usuario = Login.getLogin(txtUsuario.Text, txtPassword.Text);

                if (Usuario.Nombre=="")
                {
                    lblMens.Text = "Los datos ingresados no fueron encontrados.";
                }
                else
                {
                    if (!Usuario.Vigente)
                    {
                        lblMens.Text = "El usuario esta Caducado.";
                    }
                    else
                    {
                        Session["usuario"] = txtUsuario.Text;
                        Response.Redirect("~/Camiones/Flota.aspx");
                        
                    }
                }
                
            }
        }





    }
}