﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Mantenedores_notsm1.aspx.cs" Inherits="TKPH_WEB.Mantenedores.Mantenedores_notsm1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="../Css/Style_Negro.css" rel="stylesheet" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server" ID="sManager" />
        <div>
            <div id="contenedor">
                <div class="izq">
                    <div class="contenedorDetalle + izq + BordeFormMant" style="width: 100%;">
                        <table style="width: 100%; height: 40px;">
                            <tr>
                                <td style="width: 440px;">
                                    <asp:Label runat="server" ID="Label9" Font-Bold="true" ForeColor="#0066cc" Text="Seleccione Flota"></asp:Label>
                                    <asp:DropDownList ID="ddlAlerta" runat="server" AutoPostBack="true" CssClass="input" Style="width: auto;" OnSelectedIndexChanged="ddlAlerta_SelectedIndexChanged" />
                                </td>
                                <td>
                                    <asp:Label runat="server" ID="lblTitConfGen" ForeColor="#0066cc" Font-Bold="true" Text="Configuraciones Generales"></asp:Label>
                                </td>
                            </tr>

                        </table>
                    </div>

                    <asp:UpdatePanel ID="udpData" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="txtUmbralTkph1" EventName="TextChanged" />
                            <asp:AsyncPostBackTrigger ControlID="txtUmbralTkph2" EventName="TextChanged" />
                            <asp:AsyncPostBackTrigger ControlID="txtUmbralCarga1" EventName="TextChanged" />
                            <asp:AsyncPostBackTrigger ControlID="txtUmbralCarga2" EventName="TextChanged" />
                            <asp:AsyncPostBackTrigger ControlID="ddlAlerta" EventName="SelectedIndexChanged" />
                            <asp:AsyncPostBackTrigger ControlID="chkTemperatura" EventName="CheckedChanged" />
                        </Triggers>
                        <ContentTemplate>
                            <div class="contenedorDetalle + izq + BordeFormMant" style="width: 100%;">
                                <table width="100%">
                                    <tr>
                                        <td style="width: 450px">
                                            <div class="contenedorDetalle + izq" style="width: 100%; height: 185px;">
                                                <table align="center">
                                                    <tr>
                                                        <td class="alineado_centro_izq">
                                                            <asp:Label runat="server" ID="Label3" ForeColor="White" Font-Bold="true" Text="Configuracion Umbrales de TKPH"></asp:Label>

                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height: 10px">
                                                            <asp:Label runat="server" ID="Label10" ForeColor="#0066cc" Font-Bold="false" Text="Nominal: "></asp:Label>
                                                            <asp:Label runat="server" ID="lblTkphNominal" ForeColor="White" Font-Bold="True" class="Margen"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table align="center">
                                                    <%--                            <tr>
                                <td class="alineado_centro_izq"></td>
                                <td class="alineado_centro">
                                    <asp:Label runat="server" ID="SliderValue" ForeColor="White" Font-Bold="true"></asp:Label>
                                    <asp:Label runat="server" ID="Label1" ForeColor="White" Font-Bold="true" Text="  %"></asp:Label>
                                </td>
                                <td class="alineado_centro_izq">&nbsp;</td>
                                <td class="alineado_centro">
                                    <asp:Label runat="server" ID="SliderValue2" ForeColor="White" Font-Bold="true"></asp:Label>
                                    <asp:Label runat="server" ID="Label2" ForeColor="White" Font-Bold="true" Text="  %"></asp:Label>
                                </td>
                                <td class="alineado_centro"></td>
                            </tr>--%>
                                                    <tr>
                                                        <td class="alineado_centro_izq" style="width: auto">
                                                            <asp:Image runat="server" ID="Averde" ImageUrl="~/Imagenes/Iconos/Rect_Green.png" />

                                                        </td>
                                                        <td class="alineado_centro">

                                                            <table class="alineado_centro_izq" align="center" width="35px">
                                                                <tr>
                                                                    <td>
                                                                        <asp:ImageButton ID="btnUpTkph1" runat="server" ImageUrl="~/Imagenes/Iconos/Up.png" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="txtUmbralTkph1" runat="server" AutoPostBack="true" OnTextChanged="txtUmbralTkph1_TextChanged" CssClass="input2 + centro" Width="35" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:ImageButton ID="btnDownTkph1" runat="server" ImageUrl="~/Imagenes/Iconos/Down.png" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <asp:NumericUpDownExtender ID="Rangoi_carga" runat="server" Minimum="1" Maximum="100" TargetControlID="txtUmbralTkph1" Width="60" TargetButtonDownID="btnDownTkph1" TargetButtonUpID="btnUpTkph1" />
                                                            <%--                                    <asp:SliderExtender ID="SliderExtender1" runat="server"
                                        TargetControlID="Slider1"
                                        Minimum="1"
                                        Maximum="100"
                                        Length="200"
                                        BoundControlID="SliderValue"
                                        Steps="100"
                                        EnableHandleAnimation="true" />--%>
                                                        </td>
                                                        <td class="alineado_centro_izq" style="width: auto">
                                                            <asp:Image runat="server" ID="Image1" ImageUrl="~/Imagenes/Iconos/Rect_Yellow.png" />
                                                        </td>
                                                        <td class="alineado_centro_izq">
                                                            <table class="alineado_centro_izq" align="center" style="width: 35px">
                                                                <tr>
                                                                    <td>
                                                                        <asp:ImageButton ID="btnUpTkph2" runat="server" ImageUrl="~/Imagenes/Iconos/Up.png" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="txtUmbralTkph2" runat="server" AutoPostBack="true" OnTextChanged="txtUmbralTkph2_TextChanged" CssClass="input2 + centro" Width="35" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:ImageButton ID="btnDownTkph2" runat="server" ImageUrl="~/Imagenes/Iconos/Down.png" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <asp:NumericUpDownExtender ID="NumericUpDownExtender1" runat="server" TargetControlID="txtUmbralTkph2" Width="60" TargetButtonDownID="btnDownTkph2" TargetButtonUpID="btnUpTkph2" />
                                                            <%--                                    <asp:SliderExtender ID="SliderExtender2" runat="server"
                                        TargetControlID="Slider2"
                                        Minimum="1"
                                        Maximum="150"
                                        Length="200"
                                        BoundControlID="SliderValue2"
                                        Steps="150"
                                        EnableHandleAnimation="true" />--%>
                                                        </td>
                                                        <td class="alineado_centro_izq" style="width: auto">
                                                            <asp:Image runat="server" ID="Image3" ImageUrl="~/Imagenes/Iconos/Rect_Red.png" /></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="alineado_centro_izq"></td>
                                                        <td class="alineado_centro" style="width: 35px">
                                                            <asp:Label runat="server" ID="Label14" ForeColor="#0066cc" Font-Bold="false" Text="Rango Inferior: "></asp:Label>
                                                            <asp:Label runat="server" ID="lblTkphValor1" ForeColor="White" Font-Bold="True" Text="2000"></asp:Label></td>
                                                        <td class="alineado_centro_izq">&nbsp;</td>
                                                        <td class="alineado_centro" style="width: 35px">
                                                            <asp:Label runat="server" ID="Label15" ForeColor="#0066cc" Font-Bold="false" Text="Rango Superior: "></asp:Label>
                                                            <asp:Label runat="server" ID="lblTkphValor2" ForeColor="White" Font-Bold="True" Text="3000"></asp:Label></td>
                                                        <td class="alineado_centro_izq"></td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                        <td rowspan="2">
                                            <div class="contenedorDetalle" style="width: 100%; height: 365px;">
                                                <asp:Label runat="server" ID="Label1" ForeColor="White" Font-Bold="true" Text="Umbral de Notificacion"></asp:Label>
                                                <br />
                                                <br />
                                                <br />
                                                <br />
                                                <br />
                                                <table align="center">

                                                    <tr>
                                                        <td colspan="3"></td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td>
                                                            <asp:Image runat="server" ID="Image6" ImageUrl="~/Imagenes/Iconos/Box_Red.png" /></td>
                                                        <td>
                                                            <asp:Image ID="Image7" runat="server" ImageUrl="~/Imagenes/Iconos/Box_Yellow.png" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="LblCargaMant" runat="server" Text="Carga" ForeColor="#0066cc" Font-Bold="True" CssClass="izq"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlCargaR" runat="server" AutoPostBack="true" CssClass="input" Style="width: auto;"></asp:DropDownList></td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlCargaA" runat="server" AutoPostBack="true" CssClass="input" Style="width: auto;"></asp:DropDownList></td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="LblTkphMant" CssClass="izq" runat="server" ForeColor="#0066cc" Text="TKPH"> </asp:Label></td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlTkphR" runat="server" AutoPostBack="true" CssClass="input" Style="width: auto;"></asp:DropDownList></td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlTkphA" runat="server" AutoPostBack="true" CssClass="input" Style="width: auto;"></asp:DropDownList></td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="LblPresMant" runat="server" CssClass="izq" ForeColor="#0066cc" Text="Presion"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlPresR" runat="server" AutoPostBack="true" CssClass="input" Style="width: auto;"></asp:DropDownList></td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlPresA" runat="server" AutoPostBack="true" CssClass="input" Style="width: auto;"></asp:DropDownList></td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="LblTempMant" runat="server" CssClass="izq" Text="Temp" ForeColor="#0066cc"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlTempR" runat="server" AutoPostBack="true" CssClass="input" Style="width: auto;"></asp:DropDownList></td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlTempA" runat="server" AutoPostBack="true" CssClass="input" Style="width: auto;"></asp:DropDownList></td>
                                                    </tr>
                                                </table>

                                            </div>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 450px">
                                            <div class="contenedorDetalle + izq" style="width: 100%;">
                                                <table align="center">
                                                    <tr>
                                                        <td class="alineado_centro_izq">
                                                            <asp:Label runat="server" ID="Label4" ForeColor="White" Font-Bold="true" Text="Configuracion Umbrales de CARGA"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height: 10px">
                                                            <asp:Label runat="server" ID="Label11" ForeColor="#0066cc" Font-Bold="false" Text="Nominal: "></asp:Label>
                                                            <asp:Label runat="server" ID="lblCargaNominal" ForeColor="White" Font-Bold="True" class="Margen"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>

                                                <table align="center">
                                                    <%--                            <tr>
                                <td class="alineado_centro_izq"></td>
                                <td class="alineado_centro">
                                    <asp:Label runat="server" ID="Label5" ForeColor="White" Font-Bold="true"></asp:Label>
                                    <asp:Label runat="server" ID="Label6" ForeColor="White" Font-Bold="true" Text="  %"></asp:Label>
                                </td>
                                <td class="alineado_centro_izq">&nbsp;</td>
                                <td class="alineado_centro">
                                    <asp:Label runat="server" ID="Label7" ForeColor="White" Font-Bold="true"></asp:Label>
                                    <asp:Label runat="server" ID="Label8" ForeColor="White" Font-Bold="true" Text="  %"></asp:Label>
                                </td>
                                <td class="alineado_centro"></td>
                            </tr>--%>
                                                    <tr>
                                                        <td class="alineado_centro_izq" style="width: auto">
                                                            <asp:Image runat="server" ID="Image2" ImageUrl="~/Imagenes/Iconos/Rect_Green.png" />

                                                        </td>
                                                        <td class="alineado_centro_izq">

                                                            <table class="alineado_centro_izq" align="center" style="width: 35px">
                                                                <tr>
                                                                    <td>
                                                                        <asp:ImageButton ID="btnUpCarga1" runat="server" ImageUrl="~/Imagenes/Iconos/Up.png" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="txtUmbralCarga1" runat="server" AutoPostBack="true" OnTextChanged="txtUmbralCarga1_TextChanged" CssClass="input2 + centro" Width="35" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:ImageButton ID="btnDownCarga1" runat="server" ImageUrl="~/Imagenes/Iconos/Down.png" />
                                                                    </td>
                                                                </tr>
                                                            </table>

                                                            <asp:NumericUpDownExtender ID="NumericUpDownExtender2" runat="server" Minimum="1" Maximum="100" TargetControlID="txtUmbralCarga1" Width="60" TargetButtonDownID="btnDownCarga1" TargetButtonUpID="btnUpCarga1" />
                                                            <%--                                    <asp:SliderExtender ID="SliderExtender3" runat="server"
                                        TargetControlID="TextBox1"
                                        Minimum="1"
                                        Maximum="100"
                                        Length="200"
                                        BoundControlID="Label5"
                                        Steps="100"
                                        EnableHandleAnimation="true" />--%>
                                                        </td>
                                                        <td class="alineado_centro_izq" style="width: auto">
                                                            <asp:Image runat="server" ID="Image4" ImageUrl="~/Imagenes/Iconos/Rect_Yellow.png" />
                                                        </td>
                                                        <td class="alineado_centro_izq">

                                                            <table class="alineado_centro_izq" align="center" style="width: 35px">
                                                                <tr>
                                                                    <td>
                                                                        <asp:ImageButton ID="btnUpCarga2" runat="server" ImageUrl="~/Imagenes/Iconos/Up.png" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="txtUmbralCarga2" runat="server" AutoPostBack="true" OnTextChanged="txtUmbralCarga2_TextChanged" CssClass="input2 + centro" Width="35" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:ImageButton ID="btnDownCarga2" runat="server" ImageUrl="~/Imagenes/Iconos/Down.png" />
                                                                    </td>
                                                                </tr>
                                                            </table>

                                                            <asp:NumericUpDownExtender ID="NumericUpDownExtender3" runat="server" TargetControlID="txtUmbralCarga2" Width="60" TargetButtonDownID="btnDownCarga2" TargetButtonUpID="btnUpCarga2" />
                                                            <%--                                    <asp:SliderExtender ID="SliderExtender4" runat="server"
                                        TargetControlID="TextBox2"
                                        Minimum="1"
                                        Maximum="150"
                                        Length="200"
                                        BoundControlID="Label7"
                                        Steps="100"
                                        EnableHandleAnimation="true" />--%>
                                                        </td>
                                                        <td class="alineado_centro_izq" style="width: auto">
                                                            <asp:Image runat="server" ID="Image5" ImageUrl="~/Imagenes/Iconos/Rect_Red.png" /></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="alineado_centro_izq"></td>
                                                        <td class="alineado_centro" style="width: 35px">
                                                            <asp:Label runat="server" ID="Label12" ForeColor="#0066cc" Font-Bold="false" Text="Rango Inferior: "></asp:Label>
                                                            <asp:Label runat="server" ID="lblCargaValor1" ForeColor="White" Font-Bold="True" Text="2000"></asp:Label></td>
                                                        <td class="alineado_centro_izq">&nbsp;</td>
                                                        <td class="alineado_centro" style="width: 35px">
                                                            <asp:Label runat="server" ID="Label13" ForeColor="#0066cc" Font-Bold="false" Text="Rango Superior: "></asp:Label>
                                                            <asp:Label runat="server" ID="lblCargaValor2" ForeColor="White" Font-Bold="True" Text="2000"></asp:Label></td>
                                                        <td class="alineado_centro_izq"></td>
                                                    </tr>
                                                </table>

                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <div class="contenedorDetalle" style="width: 100%;">
                                                <table align="center">
                                                    <tr>
                                                        <td class="alineado_centro_izq">
                                                            <asp:Label runat="server" ID="Label16" ForeColor="White" Font-Bold="true" Text="Configuracion variable de temperatura"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="alineado_centro" style="height: 10px">
                                                            <asp:CheckBox ID="chkTemperatura" runat="server" OnCheckedChanged="chkTemperatura_CheckedChanged" AutoPostBack="true" />
                                                            <asp:Label runat="server" ID="Label17" ForeColor="White" Font-Bold="false" Text="Temperatura automatica "></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label runat="server" ID="lblTempConstante" ForeColor="White" Font-Bold="False" Visible="False" Text="Ingrese temperatura constante: "></asp:Label>
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator15" Text="*" ControlToValidate="txtTemperatura" runat="server" Display="Dynamic" ValidationExpression="\d{1,9}(,\d{1,9})?" ValidationGroup="Save" ForeColor="Red"></asp:RegularExpressionValidator>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator15" Text="*" ControlToValidate="txtTemperatura" runat="server" Display="Dynamic" ValidationGroup="Save" ForeColor="Red"></asp:RequiredFieldValidator>
                                                            <asp:TextBox ID="txtTemperatura" runat="server" AutoPostBack="false" CssClass="input2 + centro" Visible="false" Width="50"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>

                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="centro">
                                <asp:Button ID="btnGuardar" runat="server" CssClass="input" AutoPostBack="true" Text="Guardar Cambios" OnClick="btnGuardar_Click" ValidationGroup="Save" />
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>


                </div>
                <div>
                    <asp:Timer runat="server" ID="tmrMensaje" Interval="2000" Enabled="false" OnTick="tmrMensaje_Tick">
                    </asp:Timer>
                    <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnGuardar" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="tmrMensaje" EventName="Tick" />
                        </Triggers>
                        <ContentTemplate>
                            <asp:Panel runat="server" HorizontalAlign="Center">
                                <br />
                                <asp:Label ID="lblMensaje" runat="server" Text="" CssClass="Input_err" Visible="false" Font-Bold="true" />
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
