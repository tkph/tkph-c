﻿using Com.Vmica.View.Mantenedores;
using EPMS_BUSINESS.BUSINESS_PAGE.Mantenedores;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EMPS_WEB.Mantenedores
{
    public partial class MantenedorFlotas : System.Web.UI.Page
    {
        bMantenedorBudget _dataBudget = bMantenedorBudget.Instance;

        public static IList<vMantenedorCarga> _DatosMant;
        public static IList<vMantenedorCarga> _DatosMantAlertas;
        public static Double var_tkphnomporcin;
        public static Double var_tkphnomporcsu;
        public static Double var_carganomporcin;
        public static Double var_carganomporcsu;

        public static Int64 var_guardarc1;
        public static Int64 var_guardarc2;
        public static Int64 var_guardart1;
        public static Int64 var_guardart2;
        public static String Valores_numextSup;
        protected void Page_Load(object sender, EventArgs e)
        {



            if (!IsPostBack)
            {
                HttpContext.Current.Session["Titulo"] = "Mantenedor de Flotas";
                HttpContext.Current.Session["SubTitulo"] = "Configuración de Alarmas";

                _DatosMant = _dataBudget.GetDatosBudget(1, 1, "", "10", "");
                alerta.DataSource = _DatosMant.Where(p => p.Tipo.Equals("Umbral")).GroupBy(p => p.Nivel_2).Select(grp => grp.First()).ToList();
                alerta.DataTextField = "Nivel_2";                           // FieldName of Table in DataBase
                alerta.DataValueField = "Nivel_2";
                alerta.DataBind();
                //alerta.SelectedIndex = 0;

                Carga_datos_inicial();
            }
        }

        public void Carga_datos_inicial()
        {
            _DatosMantAlertas = _dataBudget.GetDatosBudget(1, 1, "", "11", "");

            String[] Notif = new String[100];

            for (int i = 1; i <= 100; i++)
            {
                Notif[i-1] = Convert.ToString(i);
            }

            CboxCargaR.DataSource = Notif;
            CboxCargaR.DataBind();
            CboxCargaA.DataSource = Notif;
            CboxCargaA.DataBind();
            CboxTkphR.DataSource = Notif;
            CboxTkphR.DataBind();
            CboxTkphA.DataSource = Notif;
            CboxTkphA.DataBind();
            CboxPresR.DataSource = Notif;
            CboxPresR.DataBind();
            CboxPresA.DataSource = Notif;
            CboxPresA.DataBind();
            CboxTempR.DataSource = Notif;
            CboxTempR.DataBind();
            CboxTempA.DataSource = Notif;
            CboxTempA.DataBind();

            CboxCargaR.SelectedValue = _DatosMantAlertas.Where(p => p.Item.Equals(75)).First().valor.ToString();
            CboxCargaA.SelectedValue = _DatosMantAlertas.Where(p => p.Item.Equals(79)).First().valor.ToString();

            CboxTkphR.SelectedValue = _DatosMantAlertas.Where(p => p.Item.Equals(76)).First().valor.ToString();
            CboxTkphA.SelectedValue = _DatosMantAlertas.Where(p => p.Item.Equals(80)).First().valor.ToString();

            CboxPresR.SelectedValue = _DatosMantAlertas.Where(p => p.Item.Equals(77)).First().valor.ToString();
            CboxPresA.SelectedValue = _DatosMantAlertas.Where(p => p.Item.Equals(81)).First().valor.ToString();

            CboxTempR.SelectedValue = _DatosMantAlertas.Where(p => p.Item.Equals(78)).First().valor.ToString();
            CboxTempA.SelectedValue = _DatosMantAlertas.Where(p => p.Item.Equals(82)).First().valor.ToString();

            _DatosMant = _dataBudget.GetDatosBudget(1, 1, "", "10", "");

            tkphvalor1.Text = _DatosMant.Where(p => p.Nivel_2.Equals(alerta.SelectedValue.ToString())).Where(p2 => p2.Nivel_3.Equals("limite inferior")).Where(p3 => p3.Nivel_1.Equals("Tkph")).First().valor.ToString();

            tkphvalor2.Text = _DatosMant.Where(p => p.Nivel_2.Equals(alerta.SelectedValue.ToString())).Where(p2 => p2.Nivel_3.Equals("limite superior")).Where(p3 => p3.Nivel_1.Equals("Tkph")).First().valor.ToString();

            cargavalor1.Text = _DatosMant.Where(p => p.Nivel_2.Equals(alerta.SelectedValue.ToString())).Where(p2 => p2.Nivel_3.Equals("limite inferior")).Where(p3 => p3.Nivel_1.Equals("Carga")).First().valor.ToString();

            cargavalor2.Text = _DatosMant.Where(p => p.Nivel_2.Equals(alerta.SelectedValue.ToString())).Where(p2 => p2.Nivel_3.Equals("limite superior")).Where(p3 => p3.Nivel_1.Equals("Carga")).First().valor.ToString();

            Labeltkphnom.Text = _DatosMant.Where(p => p.Nivel_2.Equals(alerta.SelectedValue.ToString())).Where(p2 => p2.Nivel_3.Equals("limite inferior")).Where(p3 => p3.Nivel_1.Equals("Tkph")).First().AFiscal.ToString();

            Labelcarganom.Text = _DatosMant.Where(p => p.Nivel_2.Equals(alerta.SelectedValue.ToString())).Where(p2 => p2.Nivel_3.Equals("limite inferior")).Where(p3 => p3.Nivel_1.Equals("Carga")).First().AFiscal.ToString();

            var_tkphnomporcin = Math.Ceiling((Convert.ToDouble(tkphvalor1.Text) * 100) / Convert.ToDouble(Labeltkphnom.Text));
            var_tkphnomporcsu = Math.Ceiling((Convert.ToDouble(tkphvalor2.Text) * 100) / Convert.ToDouble(Labeltkphnom.Text));

            var_carganomporcin = Math.Ceiling((Convert.ToDouble(cargavalor1.Text) * 100) / Convert.ToDouble(Labelcarganom.Text));
            var_carganomporcsu = Math.Ceiling((Convert.ToDouble(cargavalor2.Text) * 100) / Convert.ToDouble(Labelcarganom.Text));

            Slider1.Text = var_tkphnomporcin.ToString();
            Slider2.Text = var_tkphnomporcsu.ToString();

            TextBox1.Text = var_carganomporcin.ToString();
            TextBox2.Text = var_carganomporcsu.ToString();


            temperatura.Visible = true;
            Label19.Visible = true;
            temperatura.Text = _DatosMant.Where(p => p.Item.Equals(74)).First().valor.ToString();

            if(_DatosMant.Where(p => p.Item.Equals(73)).First().valor.ToString() == "1")
            {
                check_temp.Checked = true;
                temperatura.Visible = false;
                Label19.Visible = false;
            }



        }

        protected void Slider1_TextChanged(object sender, EventArgs e)
        {
            Valores_numextSup = "";
            tkphvalor1.Text = ((Convert.ToInt64(Slider1.Text) * Convert.ToInt64(Labeltkphnom.Text)) / 100).ToString();
            //SliderExtender2.Minimum = Convert.ToInt64(Slider1.Text);
            //NumericUpDownExtender1.Minimum = 50;
            //NumericUpDownExtender1.Maximum = 150;
            //Slider2.Text = (Convert.ToInt64(Slider1.Text) + 1).ToString();
            tkphvalor2.Text = ((Convert.ToInt64(Slider2.Text) * Convert.ToInt64(Labeltkphnom.Text)) / 100).ToString();

            for (int i = 150; i >= Convert.ToInt64(Slider1.Text); i--)
            {
                Valores_numextSup = i.ToString() + ";" + Valores_numextSup;
            }

            NumericUpDownExtender1.RefValues = Valores_numextSup.Substring(0,Valores_numextSup.Length - 1);

        }
        protected void Slider2_TextChanged(object sender, EventArgs e)
        {
            tkphvalor2.Text = ((Convert.ToInt64(Slider2.Text) * Convert.ToInt64(Labeltkphnom.Text)) / 100).ToString();

        }

        protected void TextBox1_TextChanged(object sender, EventArgs e)
        {
            Valores_numextSup = "";
            cargavalor1.Text = ((Convert.ToInt64(TextBox1.Text) * Convert.ToInt64(Labelcarganom.Text)) / 100).ToString();
            //SliderExtender4.Minimum = Convert.ToInt64(TextBox1.Text);
            //NumericUpDownExtender3.Minimum = Convert.ToInt64(TextBox1.Text);
            //TextBox2.Text = (Convert.ToInt64(TextBox1.Text) + 1).ToString();
            cargavalor2.Text = ((Convert.ToInt64(TextBox2.Text) * Convert.ToInt64(Labelcarganom.Text)) / 100).ToString();


            for (int i = 150; i >= Convert.ToInt64(TextBox1.Text); i--)
            {
                Valores_numextSup = i.ToString() + ";" + Valores_numextSup;
            }

            NumericUpDownExtender3.RefValues = Valores_numextSup.Substring(0, Valores_numextSup.Length - 1);

        }

        protected void TextBox2_TextChanged(object sender, EventArgs e)
        {
            cargavalor2.Text = ((Convert.ToInt64(TextBox2.Text) * Convert.ToInt64(Labelcarganom.Text)) / 100).ToString();

        }

        protected void alerta_SelectedIndexChanged(object sender, EventArgs e)
        {
            Carga_datos_inicial();
        }

        protected void guardar_Click(object sender, EventArgs e)
        {

             Int64 contguarda = 0;

            var_guardarc1 = _DatosMant.Where(p => p.Nivel_2.Equals(alerta.SelectedValue.ToString())).Where(p2 => p2.Nivel_3.Equals("limite superior")).Where(p3 => p3.Nivel_1.Equals("Carga")).First().Item;

            var_guardarc2 = _DatosMant.Where(p => p.Nivel_2.Equals(alerta.SelectedValue.ToString())).Where(p2 => p2.Nivel_3.Equals("limite inferior")).Where(p3 => p3.Nivel_1.Equals("Carga")).First().Item;

            var_guardart1 = _DatosMant.Where(p => p.Nivel_2.Equals(alerta.SelectedValue.ToString())).Where(p2 => p2.Nivel_3.Equals("limite superior")).Where(p3 => p3.Nivel_1.Equals("Tkph")).First().Item;

            var_guardart2 = _DatosMant.Where(p => p.Nivel_2.Equals(alerta.SelectedValue.ToString())).Where(p2 => p2.Nivel_3.Equals("limite inferior")).Where(p3 => p3.Nivel_1.Equals("Tkph")).First().Item;


            if (_DatosMant.Where(p => p.Item.Equals(var_guardarc2)).First().valor != Convert.ToInt64(cargavalor1.Text.ToString()))
            {
                _dataBudget.SetDatosBudget(var_guardarc2.ToString(), cargavalor1.Text.ToString(), "");
                //Mens.Text = "Datos Guadados OK";
                contguarda = contguarda + 1;

                //Mens.Visible = true;
                //Actu.Enabled = true;
            }

            if (_DatosMant.Where(p => p.Item.Equals(var_guardarc1)).First().valor != Convert.ToInt64(cargavalor2.Text.ToString()))
            {
                _dataBudget.SetDatosBudget(var_guardarc1.ToString(), cargavalor2.Text.ToString(), "");
                //Mens.Text = "Datos Guadados OK";
                contguarda = contguarda + 1;

                //Mens.Visible = true;
                //Actu.Enabled = true;
            }

            if (_DatosMant.Where(p => p.Item.Equals(var_guardart2)).First().valor != Convert.ToInt64(tkphvalor1.Text.ToString()))
            {
                _dataBudget.SetDatosBudget(var_guardart2.ToString(), tkphvalor1.Text.ToString(), "");
                //Mens.Text = "Datos Guadados OK";
                contguarda = contguarda + 1;

                //Mens.Visible = true;
                //Actu.Enabled = true;
            }

            if (_DatosMant.Where(p => p.Item.Equals(var_guardart1)).First().valor != Convert.ToInt64(tkphvalor2.Text.ToString()))
            {
                _dataBudget.SetDatosBudget(var_guardart1.ToString(), tkphvalor2.Text.ToString(), "");
                //Mens.Text = "Datos Guadados OK";
                contguarda = contguarda + 1;

                //Mens.Visible = true;
                //Actu.Enabled = true;
            }

            if (_DatosMant.Where(p => p.Item.Equals(74)).First().valor.ToString() != temperatura.Text)
            {
                _dataBudget.SetDatosBudget("74", temperatura.Text.ToString(), "");
                //Mens.Text = "Datos Guadados OK";
                contguarda = contguarda + 1;


            }



            if(_DatosMantAlertas.Where(p => p.Item.Equals(75)).First().valor.ToString() != CboxCargaR.SelectedValue)
            {
                _dataBudget.SetDatosBudget("75", CboxCargaR.SelectedValue.ToString(), "");
                contguarda = contguarda + 1;
            }


            if (_DatosMantAlertas.Where(p => p.Item.Equals(76)).First().valor.ToString() != CboxTkphR.SelectedValue)
            {
                _dataBudget.SetDatosBudget("76", CboxTkphR.SelectedValue.ToString(), "");
                contguarda = contguarda + 1;
            }


            if (_DatosMantAlertas.Where(p => p.Item.Equals(77)).First().valor.ToString() != CboxPresR.SelectedValue)
            {
                _dataBudget.SetDatosBudget("77", CboxPresR.SelectedValue.ToString(), "");
                contguarda = contguarda + 1;
            }

            if (_DatosMantAlertas.Where(p => p.Item.Equals(78)).First().valor.ToString() != CboxTempR.SelectedValue)
            {
                _dataBudget.SetDatosBudget("78", CboxTempR.SelectedValue.ToString(), "");
                contguarda = contguarda + 1;
            }



            if(_DatosMantAlertas.Where(p => p.Item.Equals(79)).First().valor.ToString() != CboxCargaA.SelectedValue)
            {
                _dataBudget.SetDatosBudget("79", CboxCargaA.SelectedValue.ToString(), "");
                contguarda = contguarda + 1;
            }


            if (_DatosMantAlertas.Where(p => p.Item.Equals(80)).First().valor.ToString() != CboxTkphA.SelectedValue)
            {
                _dataBudget.SetDatosBudget("80", CboxTkphA.SelectedValue.ToString(), "");
                contguarda = contguarda + 1;
            }


            if (_DatosMantAlertas.Where(p => p.Item.Equals(81)).First().valor.ToString() != CboxPresA.SelectedValue)
            {
                _dataBudget.SetDatosBudget("81", CboxPresA.SelectedValue.ToString(), "");
                contguarda = contguarda + 1;
            }

            if (_DatosMantAlertas.Where(p => p.Item.Equals(82)).First().valor.ToString() != CboxTempA.SelectedValue)
            {
                _dataBudget.SetDatosBudget("82", CboxTempA.SelectedValue.ToString(), "");
                contguarda = contguarda + 1;
            }




            if (contguarda > 0)
            {
                Mens.Text = "Datos Guadados OK";
                Mens.Visible = true;
                Timer.Enabled = true;
            }

            Carga_datos_inicial();
        }

        //protected void Actu_Tick(object sender, EventArgs e)
        //{
        //    Actu.Enabled = false;
        //    Mens.Visible = false;

        //}

        protected void check_temp_CheckedChanged(object sender, EventArgs e)
        {
            if (!check_temp.Checked)
            {
                temperatura.Visible = true;
                Label19.Visible = true;

                temperatura.Text = _DatosMant.Where(p => p.Item.Equals(74)).First().valor.ToString();

                _dataBudget.SetDatosBudget("73", "0", "");
            }
            else 
            {
                temperatura.Visible = false;
                Label19.Visible = false;

                _dataBudget.SetDatosBudget("73", "1", "");
            }
        }

      
        protected void Timer_Tick(object sender, EventArgs e) 
        {
           
            Mens.Visible = false;
            Timer.Enabled = false;
        }

    }
}