﻿using Com.Vmica.View.Mantenedores;
using TKPH_BUSINESS.BUSINESS_PAGE.Mantenedores;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace TKPH_WEB.Mantenedores
{
    /// <summary>
    /// Pantalla Formulario de Configuración.
    /// </summary>
    public partial class Mantenedores_notsm1 : System.Web.UI.Page
    {
        // Llamada a instancia de Objeto "bMantenedorBudget"
        bMantenedorBudget _dataBudget = bMantenedorBudget.Instance;

        public static IList<vMantenedorCarga> _DatosMant;
        public static IList<vMantenedorCarga> _DatosMantAlertas;
        public static Double var_tkphnomporcin;
        public static Double var_tkphnomporcsu;
        public static Double var_carganomporcin;
        public static Double var_carganomporcsu;

        public static Int64 var_guardarc1;
        public static Int64 var_guardarc2;
        public static Int64 var_guardart1;
        public static Int64 var_guardart2;
        public static String Valores_numextSup;

        /// <summary>
        /// Metodo de carga Inicial de la Página
        /// </summary>
        /// <param name="sender">La fuente del evento.</param>
        /// <param name="e">La Instancia que contiene los datos del evento.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                _DatosMant = _dataBudget.GetDatosBudget(1, 1, "", "10", "");
                ddlAlerta.DataSource = _DatosMant.Where(p => p.Tipo.Equals("Umbral")).GroupBy(p => p.Nivel_2).Select(grp => grp.First()).ToList();
                ddlAlerta.DataTextField = "Nivel_2";                          
                ddlAlerta.DataValueField = "Nivel_2";
                ddlAlerta.DataBind();

                Carga_datos_inicial();
            }
        }


        /// <summary>
        /// Metodo de inicialización de listas y variables.
        /// </summary>
        public void Carga_datos_inicial()
        {
            _DatosMantAlertas = _dataBudget.GetDatosBudget(1, 1, "", "11", "");

            String[] Notif = new String[100];

            for (int i = 1; i <= 100; i++)
            {
                Notif[i - 1] = Convert.ToString(i);
            }

            ddlCargaR.DataSource = Notif;
            ddlCargaR.DataBind();
            ddlCargaA.DataSource = Notif;
            ddlCargaA.DataBind();
            ddlTkphR.DataSource = Notif;
            ddlTkphR.DataBind();
            ddlTkphA.DataSource = Notif;
            ddlTkphA.DataBind();
            ddlPresR.DataSource = Notif;
            ddlPresR.DataBind();
            ddlPresA.DataSource = Notif;
            ddlPresA.DataBind();
            ddlTempR.DataSource = Notif;
            ddlTempR.DataBind();
            ddlTempA.DataSource = Notif;
            ddlTempA.DataBind();

            ddlCargaR.SelectedValue = _DatosMantAlertas.Where(p => p.Item.Equals(75)).First().valor.ToString();
            ddlCargaA.SelectedValue = _DatosMantAlertas.Where(p => p.Item.Equals(79)).First().valor.ToString();

            ddlTkphR.SelectedValue = _DatosMantAlertas.Where(p => p.Item.Equals(76)).First().valor.ToString();
            ddlTkphA.SelectedValue = _DatosMantAlertas.Where(p => p.Item.Equals(80)).First().valor.ToString();

            ddlPresR.SelectedValue = _DatosMantAlertas.Where(p => p.Item.Equals(77)).First().valor.ToString();
            ddlPresA.SelectedValue = _DatosMantAlertas.Where(p => p.Item.Equals(81)).First().valor.ToString();

            ddlTempR.SelectedValue = _DatosMantAlertas.Where(p => p.Item.Equals(78)).First().valor.ToString();
            ddlTempA.SelectedValue = _DatosMantAlertas.Where(p => p.Item.Equals(82)).First().valor.ToString();

            _DatosMant = _dataBudget.GetDatosBudget(1, 1, "", "10", "");

            lblTkphValor1.Text = _DatosMant.Where(p => p.Nivel_2.Equals(ddlAlerta.SelectedValue.ToString())).Where(p2 => p2.Nivel_3.Equals("limite inferior")).Where(p3 => p3.Nivel_1.Equals("Tkph")).First().valor.ToString();

            lblTkphValor2.Text = _DatosMant.Where(p => p.Nivel_2.Equals(ddlAlerta.SelectedValue.ToString())).Where(p2 => p2.Nivel_3.Equals("limite superior")).Where(p3 => p3.Nivel_1.Equals("Tkph")).First().valor.ToString();

            lblCargaValor1.Text = _DatosMant.Where(p => p.Nivel_2.Equals(ddlAlerta.SelectedValue.ToString())).Where(p2 => p2.Nivel_3.Equals("limite inferior")).Where(p3 => p3.Nivel_1.Equals("Carga")).First().valor.ToString();

            lblCargaValor2.Text = _DatosMant.Where(p => p.Nivel_2.Equals(ddlAlerta.SelectedValue.ToString())).Where(p2 => p2.Nivel_3.Equals("limite superior")).Where(p3 => p3.Nivel_1.Equals("Carga")).First().valor.ToString();

            lblTkphNominal.Text = _DatosMant.Where(p => p.Nivel_2.Equals(ddlAlerta.SelectedValue.ToString())).Where(p2 => p2.Nivel_3.Equals("limite inferior")).Where(p3 => p3.Nivel_1.Equals("Tkph")).First().AFiscal.ToString();

            lblCargaNominal.Text = _DatosMant.Where(p => p.Nivel_2.Equals(ddlAlerta.SelectedValue.ToString())).Where(p2 => p2.Nivel_3.Equals("limite inferior")).Where(p3 => p3.Nivel_1.Equals("Carga")).First().AFiscal.ToString();

            var_tkphnomporcin = Math.Ceiling((Convert.ToDouble(lblTkphValor1.Text) * 100) / Convert.ToDouble(lblTkphNominal.Text));
            var_tkphnomporcsu = Math.Ceiling((Convert.ToDouble(lblTkphValor2.Text) * 100) / Convert.ToDouble(lblTkphNominal.Text));

            var_carganomporcin = Math.Ceiling((Convert.ToDouble(lblCargaValor1.Text) * 100) / Convert.ToDouble(lblCargaNominal.Text));
            var_carganomporcsu = Math.Ceiling((Convert.ToDouble(lblCargaValor2.Text) * 100) / Convert.ToDouble(lblCargaNominal.Text));

            txtUmbralTkph1.Text = var_tkphnomporcin.ToString();
            txtUmbralTkph2.Text = var_tkphnomporcsu.ToString();

            txtUmbralCarga1.Text = var_carganomporcin.ToString();
            txtUmbralCarga2.Text = var_carganomporcsu.ToString();


            txtTemperatura.Visible = true;
            lblTempConstante.Visible = true;
            txtTemperatura.Text = _DatosMant.Where(p => p.Item.Equals(74)).First().valor.ToString();

            if (_DatosMant.Where(p => p.Item.Equals(73)).First().valor.ToString() == "1")
            {
                chkTemperatura.Checked = true;
                txtTemperatura.Visible = false;
                lblTempConstante.Visible = false;
            }



        }


        /// <summary>
        /// Evento TextChanged de txtUmbralTkph1 para recalculo de valores de primer umbral TKPH .
        /// </summary>
        /// <param name="sender">La fuente del evento.</param>
        /// <param name="e">La Instancia que contiene los datos del evento.</param>
        protected void txtUmbralTkph1_TextChanged(object sender, EventArgs e)
        {
            Valores_numextSup = "";
            lblTkphValor1.Text = ((Convert.ToInt64(txtUmbralTkph1.Text) * Convert.ToInt64(lblTkphNominal.Text)) / 100).ToString();
            lblTkphValor2.Text = ((Convert.ToInt64(txtUmbralTkph2.Text) * Convert.ToInt64(lblTkphNominal.Text)) / 100).ToString();

            for (int i = 150; i >= Convert.ToInt64(txtUmbralTkph1.Text); i--)
            {
                Valores_numextSup = i.ToString() + ";" + Valores_numextSup;
            }

            NumericUpDownExtender1.RefValues = Valores_numextSup.Substring(0, Valores_numextSup.Length - 1);

        }

        /// <summary>
        /// Evento TextChanged de txtUmbralTkph1 para recalculo de valores de segundo umbral TKPH .
        /// </summary>
        /// <param name="sender">La fuente del evento.</param>
        /// <param name="e">La Instancia que contiene los datos del evento.</param>
        protected void txtUmbralTkph2_TextChanged(object sender, EventArgs e)
        {
            lblTkphValor2.Text = ((Convert.ToInt64(txtUmbralTkph2.Text) * Convert.ToInt64(lblTkphNominal.Text)) / 100).ToString();

        }


        /// <summary>
        /// Evento TextChanged de txtUmbralTkph1 para recalculo de valores de primer umbral Carga .
        /// </summary>
        /// <param name="sender">La fuente del evento.</param>
        /// <param name="e">La Instancia que contiene los datos del evento.</param>
        protected void txtUmbralCarga1_TextChanged(object sender, EventArgs e)
        {
            Valores_numextSup = "";
            lblCargaValor1.Text = ((Convert.ToInt64(txtUmbralTkph1.Text) * Convert.ToInt64(lblCargaNominal.Text)) / 100).ToString();
            //SliderExtender4.Minimum = Convert.ToInt64(TextBox1.Text);
            //NumericUpDownExtender3.Minimum = Convert.ToInt64(TextBox1.Text);
            //TextBox2.Text = (Convert.ToInt64(TextBox1.Text) + 1).ToString();
            lblCargaValor2.Text = ((Convert.ToInt64(txtUmbralTkph2.Text) * Convert.ToInt64(lblCargaNominal.Text)) / 100).ToString();


            for (int i = 150; i >= Convert.ToInt64(txtUmbralTkph1.Text); i--)
            {
                Valores_numextSup = i.ToString() + ";" + Valores_numextSup;
            }

            NumericUpDownExtender3.RefValues = Valores_numextSup.Substring(0, Valores_numextSup.Length - 1);

        }


        /// <summary>
        /// Evento TextChanged de txtUmbralTkph1 para recalculo de valores de segundo umbral Carga .
        /// </summary>
        /// <param name="sender">La fuente del evento.</param>
        /// <param name="e">La Instancia que contiene los datos del evento.</param>
        protected void txtUmbralCarga2_TextChanged(object sender, EventArgs e)
        {
            lblCargaValor2.Text = ((Convert.ToInt64(txtUmbralCarga2.Text) * Convert.ToInt64(lblCargaNominal.Text)) / 100).ToString();

        }


        /// <summary>
        /// Evento SelectedIndexChanged de ddlAlerta para recarga de datos segun valor seleccionado.
        /// </summary>
        /// <param name="sender">La fuente del evento.</param>
        /// <param name="e">La Instancia que contiene los datos del evento.</param>
        protected void ddlAlerta_SelectedIndexChanged(object sender, EventArgs e)
        {
            Carga_datos_inicial();
        }


        /// <summary>
        /// Evento Click Botón Guardar para persistir datos en base de datos SQL.
        /// </summary>
        /// <param name="sender">La fuente del evento.</param>
        /// <param name="e">La Instancia que contiene los datos del evento.</param>
        protected void btnGuardar_Click(object sender, EventArgs e)
        {

            Int64 contguarda = 0;

            var_guardarc1 = _DatosMant.Where(p => p.Nivel_2.Equals(ddlAlerta.SelectedValue.ToString())).Where(p2 => p2.Nivel_3.Equals("limite superior")).Where(p3 => p3.Nivel_1.Equals("Carga")).First().Item;

            var_guardarc2 = _DatosMant.Where(p => p.Nivel_2.Equals(ddlAlerta.SelectedValue.ToString())).Where(p2 => p2.Nivel_3.Equals("limite inferior")).Where(p3 => p3.Nivel_1.Equals("Carga")).First().Item;

            var_guardart1 = _DatosMant.Where(p => p.Nivel_2.Equals(ddlAlerta.SelectedValue.ToString())).Where(p2 => p2.Nivel_3.Equals("limite superior")).Where(p3 => p3.Nivel_1.Equals("Tkph")).First().Item;

            var_guardart2 = _DatosMant.Where(p => p.Nivel_2.Equals(ddlAlerta.SelectedValue.ToString())).Where(p2 => p2.Nivel_3.Equals("limite inferior")).Where(p3 => p3.Nivel_1.Equals("Tkph")).First().Item;


            if (_DatosMant.Where(p => p.Item.Equals(var_guardarc2)).First().valor != Convert.ToInt64(lblCargaValor1.Text.ToString()))
            {
                _dataBudget.SetDatosBudget(var_guardarc2.ToString(), lblCargaValor1.Text.ToString(), "");
                //Mens.Text = "Datos Guadados OK";
                contguarda = contguarda + 1;

                //Mens.Visible = true;
                //Actu.Enabled = true;
            }

            if (_DatosMant.Where(p => p.Item.Equals(var_guardarc1)).First().valor != Convert.ToInt64(lblCargaValor2.Text.ToString()))
            {
                _dataBudget.SetDatosBudget(var_guardarc1.ToString(), lblCargaValor2.Text.ToString(), "");
                //Mens.Text = "Datos Guadados OK";
                contguarda = contguarda + 1;

                //Mens.Visible = true;
                //Actu.Enabled = true;
            }

            if (_DatosMant.Where(p => p.Item.Equals(var_guardart2)).First().valor != Convert.ToInt64(lblTkphValor1.Text.ToString()))
            {
                _dataBudget.SetDatosBudget(var_guardart2.ToString(), lblTkphValor1.Text.ToString(), "");
                //Mens.Text = "Datos Guadados OK";
                contguarda = contguarda + 1;

                //Mens.Visible = true;
                //Actu.Enabled = true;
            }

            if (_DatosMant.Where(p => p.Item.Equals(var_guardart1)).First().valor != Convert.ToInt64(lblTkphValor2.Text.ToString()))
            {
                _dataBudget.SetDatosBudget(var_guardart1.ToString(), lblTkphValor2.Text.ToString(), "");
                //Mens.Text = "Datos Guadados OK";
                contguarda = contguarda + 1;

                //Mens.Visible = true;
                //Actu.Enabled = true;
            }

            if (_DatosMant.Where(p => p.Item.Equals(74)).First().valor.ToString() != txtTemperatura.Text)
            {
                _dataBudget.SetDatosBudget("74", txtTemperatura.Text.ToString(), "");
                //Mens.Text = "Datos Guadados OK";
                contguarda = contguarda + 1;


            }



            if (_DatosMantAlertas.Where(p => p.Item.Equals(75)).First().valor.ToString() != ddlCargaR.SelectedValue)
            {
                _dataBudget.SetDatosBudget("75", ddlCargaR.SelectedValue.ToString(), "");
                contguarda = contguarda + 1;
            }


            if (_DatosMantAlertas.Where(p => p.Item.Equals(76)).First().valor.ToString() != ddlTkphR.SelectedValue)
            {
                _dataBudget.SetDatosBudget("76", ddlTkphR.SelectedValue.ToString(), "");
                contguarda = contguarda + 1;
            }


            if (_DatosMantAlertas.Where(p => p.Item.Equals(77)).First().valor.ToString() != ddlPresR.SelectedValue)
            {
                _dataBudget.SetDatosBudget("77", ddlPresR.SelectedValue.ToString(), "");
                contguarda = contguarda + 1;
            }

            if (_DatosMantAlertas.Where(p => p.Item.Equals(78)).First().valor.ToString() != ddlTempR.SelectedValue)
            {
                _dataBudget.SetDatosBudget("78", ddlTempR.SelectedValue.ToString(), "");
                contguarda = contguarda + 1;
            }



            if (_DatosMantAlertas.Where(p => p.Item.Equals(79)).First().valor.ToString() != ddlCargaA.SelectedValue)
            {
                _dataBudget.SetDatosBudget("79", ddlCargaA.SelectedValue.ToString(), "");
                contguarda = contguarda + 1;
            }


            if (_DatosMantAlertas.Where(p => p.Item.Equals(80)).First().valor.ToString() != ddlTkphA.SelectedValue)
            {
                _dataBudget.SetDatosBudget("80", ddlTkphA.SelectedValue.ToString(), "");
                contguarda = contguarda + 1;
            }


            if (_DatosMantAlertas.Where(p => p.Item.Equals(81)).First().valor.ToString() != ddlPresA.SelectedValue)
            {
                _dataBudget.SetDatosBudget("81", ddlPresA.SelectedValue.ToString(), "");
                contguarda = contguarda + 1;
            }

            if (_DatosMantAlertas.Where(p => p.Item.Equals(82)).First().valor.ToString() != ddlTempA.SelectedValue)
            {
                _dataBudget.SetDatosBudget("82", ddlTempA.SelectedValue.ToString(), "");
                contguarda = contguarda + 1;
            }




            if (contguarda > 0)
            {
                lblMensaje.Text = "Datos Guadados OK";
                lblMensaje.Visible = true;
                tmrMensaje.Enabled = true;
            }

            Carga_datos_inicial();
        }



        /// <summary>
        /// Evento CheckedChanged de chkTemperatura para mostrar u ocultar textbox para ingreso manual de temperatura.
        /// </summary>
        /// <param name="sender">La fuente del evento.</param>
        /// <param name="e">La Instancia que contiene los datos del evento.</param>
        protected void chkTemperatura_CheckedChanged(object sender, EventArgs e)
        {
            if (!chkTemperatura.Checked)
            {
                txtTemperatura.Visible = true;
                lblTempConstante.Visible = true;

                txtTemperatura.Text = _DatosMant.Where(p => p.Item.Equals(74)).First().valor.ToString();

                _dataBudget.SetDatosBudget("73", "0", "");
            }
            else
            {
                txtTemperatura.Visible = false;
                lblTempConstante.Visible = false;

                _dataBudget.SetDatosBudget("73", "1", "");
            }
        }


        /// <summary>
        /// Evento de cumplimiento de ciclo dentro del Timer para visualizacion de mensaje.
        /// </summary>
        /// <param name="sender">La fuente del evento.</param>
        /// <param name="e">La Instancia que contiene los datos del evento.</param>
        protected void tmrMensaje_Tick(object sender, EventArgs e)
        {
            lblMensaje.Visible = false;
            tmrMensaje.Enabled = false;
        }

    }

}