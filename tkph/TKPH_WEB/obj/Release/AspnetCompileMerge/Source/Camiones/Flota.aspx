﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Flota.aspx.cs" Inherits="EMPS_WEB.Camiones.Flota" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="BodyC" runat="server">
    <div id="contenedor">

        <asp:UpdatePanel runat="server" ID="upTotal" UpdateMode="Conditional">
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="lbFR" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="lbFA" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="lbFV" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="lbFG" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="TimerAct" EventName="Tick" />
            </Triggers>
            <ContentTemplate>
                <asp:Timer ID="TimerAct" runat="server" Interval="600000" Enabled="true" OnTick="TimerAct_Tick" />
                <asp:Panel runat="server" ID="pnlResumen">
                    <div class="camionResumen">
                        <asp:LinkButton ID="lbFR" runat="server" CausesValidation="false" OnClick="lbFR_Click">
                            <asp:Image ID="imgIdioma" runat="server" ImageUrl="~/Imagenes/truck_r.png" Width="24" />
                            <asp:Label runat="server" ID="lblFR" />
                        </asp:LinkButton></div><div class="camionResumen">
                        <asp:LinkButton ID="lbFA" runat="server" CausesValidation="false" OnClick="lbFA_Click">
                            <asp:Image ID="Image1" runat="server" ImageUrl="~/Imagenes/truck_a.png" Width="24" />
                            <asp:Label runat="server" ID="lblFA" />
                        </asp:LinkButton></div><div class="camionResumen">
                        <asp:LinkButton ID="lbFV" runat="server" CausesValidation="false" OnClick="lbFV_Click">
                            <asp:Image ID="Image2" runat="server" ImageUrl="~/Imagenes/truck_v.png" Width="24" />
                            <asp:Label runat="server" ID="lblFV" />
                        </asp:LinkButton></div><div class="camionResumen">
                        <asp:LinkButton ID="lbFG" runat="server" CausesValidation="false" OnClick="lbFG_Click">
                            <asp:Image ID="Image3" runat="server" ImageUrl="~/Imagenes/truck_g.png" Width="24" />
                            <asp:Label runat="server" ID="lblFG" />
                        </asp:LinkButton></div><div class="tCam">
                        <asp:Label runat="server" ID="lblTC" /><br />
                    </div>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlCamiones" CssClass="contenedorCamiones" Width="100%"></asp:Panel>
                <br />
                <asp:Label runat="server" ID="lblAct" Text="Actualizado:" CssClass="der" />

            </ContentTemplate>
        </asp:UpdatePanel>
        <br />
        <div>
            <asp:Label runat="server" ID="lblZona" Text="Zonas de Alerta" CssClass="titulo" />
            <br />
            <asp:Label runat="server" ID="leyC" Text="C: Carga" CssClass="leyenda" />
            <asp:Label runat="server" ID="LeyN" Text="N: Neumaticos" CssClass="leyenda" />
        </div>



    </div>


    <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Panel ID="pMensaje" runat="server" CssClass="BordeForm" Style="display: none; width: 500px;">
                <asp:Panel ID="pCabecera" runat="server" CssClass="TituloForm">
                    <asp:Label ID="Label7" runat="server" Text="Integridad" />
                </asp:Panel>
                <asp:Panel ID="pBody" runat="server">
                    <br />
                    <br />
                    <asp:Image ID="imgIcono" runat="server" ImageUrl="~/Imagenes/Iconos/info-icon.png" Width="24" Height="24" />
                    <asp:Label ID="lblMen" runat="server" /><br />
                    <br />
                    <asp:Button ID="btnAceptar" runat="server" Text="Aceptar" CssClass="input" /><br />
                    <br />
                </asp:Panel>
            </asp:Panel>

            <asp:ModalPopupExtender ID="MenPOP" runat="server" TargetControlID="lblMen" PopupControlID="pMensaje" DropShadow="false" BackgroundCssClass="FondoAplicacion" OnOkScript="btnAceptar" />
        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>
