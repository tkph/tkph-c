﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MantenedorFlotas.aspx.cs" Inherits="EMPS_WEB.WebPage.Reports.Mantenedores.MantenedorFlotas" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="BodyC" runat="server">
    <div id="contenedor">
        <h1></h1>
        <div class="centro">
            <div class="contenedorDetalle" style="width: 60%;">
                <table style="width: 100%; height: 40px;">
                    <tr>
                        <td style="width: 440px;">
                            <asp:Label runat="server" ID="Label9" Font-Bold="true" ForeColor="#0066cc" Text="Seleccione Flota"></asp:Label>
                            <asp:DropDownList ID="alerta" runat="server" AutoPostBack="true" CssClass="input" Style="width: auto;" OnSelectedIndexChanged="alerta_SelectedIndexChanged" />
                        </td>
                        <td>
                            <asp:Label runat="server" ID="lblTitConfGen" ForeColor="#0066cc" Font-Bold="true" Text="Configuraciones Generales"></asp:Label>
                        </td>
                    </tr>

                </table>
            </div>
            <asp:UpdatePanel ID="uData" runat="server" UpdateMode="Conditional">
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="Slider1" EventName="TextChanged" />
                    <asp:AsyncPostBackTrigger ControlID="Slider2" EventName="TextChanged" />
                    <asp:AsyncPostBackTrigger ControlID="TextBox1" EventName="TextChanged" />
                    <asp:AsyncPostBackTrigger ControlID="TextBox2" EventName="TextChanged" />
                    <asp:AsyncPostBackTrigger ControlID="alerta" EventName="SelectedIndexChanged" />
                    <asp:AsyncPostBackTrigger ControlID="check_temp" EventName="CheckedChanged" />
                </Triggers>
                <ContentTemplate>
                    <div class="contenedorDetalle" style="width: 60%;">
                        <table width="100%">
                            <tr>
                                <td style="width: 450px">
                                    <div class="contenedorDetalle + izq" style="width: 100%; height: 179px;">
                                        <table align="center">
                                            <tr>
                                                <td class="alineado_centro_izq">
                                                    <asp:Label runat="server" ID="Label3" ForeColor="White" Font-Bold="true" Text="Configuracion Umbrales de TKPH"></asp:Label>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height: 10px">
                                                    <asp:Label runat="server" ID="Label10" ForeColor="#0066cc" Font-Bold="false" Text="Nominal: "></asp:Label>
                                                    <asp:Label runat="server" ID="Labeltkphnom" ForeColor="White" Font-Bold="true" Text="" class="Margen"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                        <table align="center">
                                            <%--                            <tr>
                                <td class="alineado_centro_izq"></td>
                                <td class="alineado_centro">
                                    <asp:Label runat="server" ID="SliderValue" ForeColor="White" Font-Bold="true"></asp:Label>
                                    <asp:Label runat="server" ID="Label1" ForeColor="White" Font-Bold="true" Text="  %"></asp:Label>
                                </td>
                                <td class="alineado_centro_izq">&nbsp;</td>
                                <td class="alineado_centro">
                                    <asp:Label runat="server" ID="SliderValue2" ForeColor="White" Font-Bold="true"></asp:Label>
                                    <asp:Label runat="server" ID="Label2" ForeColor="White" Font-Bold="true" Text="  %"></asp:Label>
                                </td>
                                <td class="alineado_centro"></td>
                            </tr>--%>
                                            <tr>
                                                <td class="alineado_centro_izq" style="width: auto">
                                                    <asp:Image runat="server" ID="Averde" ImageUrl="~/Imagenes/Iconos/Rect_Green.png"/>

                                                </td>
                                                <td class="alineado_centro">

                                                    <table class="alineado_centro_izq" align="center" width="35px">
                                                        <tr>
                                                            <td>
                                                                <asp:ImageButton ID="Button1" runat="server" ImageUrl="~/Imagenes/Iconos/Up.png" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:TextBox ID="Slider1" runat="server" AutoPostBack="true" OnTextChanged="Slider1_TextChanged" CssClass="input + centro" Width="35" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:ImageButton ID="Button2" runat="server" ImageUrl="~/Imagenes/Iconos/Down.png" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <asp:NumericUpDownExtender ID="Rangoi_carga" runat="server" Minimum="1" Maximum="100" TargetControlID="Slider1" Width="60" TargetButtonDownID="Button2" TargetButtonUpID="Button1"/>
                                                    <%--                                    <asp:SliderExtender ID="SliderExtender1" runat="server"
                                        TargetControlID="Slider1"
                                        Minimum="1"
                                        Maximum="100"
                                        Length="200"
                                        BoundControlID="SliderValue"
                                        Steps="100"
                                        EnableHandleAnimation="true" />--%>
                                                </td>
                                                <td class="alineado_centro_izq" style="width: auto">
                                                    <asp:Image runat="server" ID="Image1" ImageUrl="~/Imagenes/Iconos/Rect_Yellow.png" />
                                                </td>
                                                <td class="alineado_centro_izq">
                                                    <table class="alineado_centro_izq" align="center" style="width: 35px">
                                                        <tr>
                                                            <td>
                                                                <asp:ImageButton ID="Button3" runat="server" ImageUrl="~/Imagenes/Iconos/Up.png" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:TextBox ID="Slider2" runat="server" AutoPostBack="true" OnTextChanged="Slider2_TextChanged" CssClass="input + centro" Width="35" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:ImageButton ID="Button4" runat="server" ImageUrl="~/Imagenes/Iconos/Down.png" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <asp:NumericUpDownExtender ID="NumericUpDownExtender1" runat="server" TargetControlID="Slider2" Width="60" TargetButtonDownID="Button4" TargetButtonUpID="Button3" />
                                                    <%--                                    <asp:SliderExtender ID="SliderExtender2" runat="server"
                                        TargetControlID="Slider2"
                                        Minimum="1"
                                        Maximum="150"
                                        Length="200"
                                        BoundControlID="SliderValue2"
                                        Steps="150"
                                        EnableHandleAnimation="true" />--%>
                                                </td>
                                                <td class="alineado_centro_izq" style="width: auto">
                                                    <asp:Image runat="server" ID="Image3" ImageUrl="~/Imagenes/Iconos/Rect_Red.png" /></td>
                                            </tr>
                                            <tr>
                                                <td class="alineado_centro_izq"></td>
                                                <td class="alineado_centro" style="width: 35px">
                                                    <asp:Label runat="server" ID="Label14" ForeColor="#0066cc" Font-Bold="false" Text="Rango Inferior: "></asp:Label>
                                                    <asp:Label runat="server" ID="tkphvalor1" ForeColor="White" Font-Bold="true" Text="2000"></asp:Label></td>
                                                <td class="alineado_centro_izq">&nbsp;</td>
                                                <td class="alineado_centro" style="width: 35px">
                                                    <asp:Label runat="server" ID="Label15" ForeColor="#0066cc" Font-Bold="false" Text="Rango Superior: "></asp:Label>
                                                    <asp:Label runat="server" ID="tkphvalor2" ForeColor="White" Font-Bold="true" Text="3000"></asp:Label></td>
                                                <td class="alineado_centro_izq"></td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                                <td rowspan="2">
                                    <div class="contenedorDetalle" style="width: 100%; height: 365px;">
                                        <asp:Label runat="server" ID="Label1" ForeColor="White" Font-Bold="true" Text="Umbral de Notificacion"></asp:Label>
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <table align="center">

                                            <tr>
                                                <td colspan="3"></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <asp:Image runat="server" ID="Image6" ImageUrl="~/Imagenes/Iconos/Box_Red.png" /></td>
                                                <td>
                                                    <asp:Image ID="Image7" runat="server" ImageUrl="~/Imagenes/Iconos/Box_Yellow.png" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="LblCargaMant" runat="server" Text="Carga" ForeColor="#0066cc" Font-Bold="True" CssClass="izq"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="CboxCargaR" runat="server" AutoPostBack="true" CssClass="input" Style="width: auto;"></asp:DropDownList></td>
                                                <td>
                                                    <asp:DropDownList ID="CboxCargaA" runat="server" AutoPostBack="true" CssClass="input" Style="width: auto;"></asp:DropDownList></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="LblTkphMant" CssClass="izq" runat="server" ForeColor="#0066cc" Text="TKPH"> </asp:Label></td>
                                                <td>
                                                    <asp:DropDownList ID="CboxTkphR" runat="server" AutoPostBack="true" CssClass="input" Style="width: auto;"></asp:DropDownList></td>
                                                <td>
                                                    <asp:DropDownList ID="CboxTkphA" runat="server" AutoPostBack="true" CssClass="input" Style="width: auto;"></asp:DropDownList></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="LblPresMant" runat="server" CssClass="izq" ForeColor="#0066cc" Text="Presion"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="CboxPresR" runat="server" AutoPostBack="true" CssClass="input" Style="width: auto;"></asp:DropDownList></td>
                                                <td>
                                                    <asp:DropDownList ID="CboxPresA" runat="server" AutoPostBack="true" CssClass="input" Style="width: auto;"></asp:DropDownList></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="LblTempMant" runat="server" CssClass="izq" Text="Temp" ForeColor="#0066cc"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="CboxTempR" runat="server" AutoPostBack="true" CssClass="input" Style="width: auto;"></asp:DropDownList></td>
                                                <td>
                                                    <asp:DropDownList ID="CboxTempA" runat="server" AutoPostBack="true" CssClass="input" Style="width: auto;"></asp:DropDownList></td>
                                            </tr>
                                        </table>

                                    </div>

                                </td>
                            </tr>
                            <tr>
                                <td style="width: 450px">
                                    <div class="contenedorDetalle + izq" style="width: 100%;">
                                        <table align="center">
                                            <tr>
                                                <td class="alineado_centro_izq">
                                                    <asp:Label runat="server" ID="Label4" ForeColor="White" Font-Bold="true" Text="Configuracion Umbrales de CARGA"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height: 10px">
                                                    <asp:Label runat="server" ID="Label11" ForeColor="#0066cc" Font-Bold="false" Text="Nominal: "></asp:Label>
                                                    <asp:Label runat="server" ID="Labelcarganom" ForeColor="White" Font-Bold="true" Text="" class="Margen"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>

                                        <table align="center">
                                            <%--                            <tr>
                                <td class="alineado_centro_izq"></td>
                                <td class="alineado_centro">
                                    <asp:Label runat="server" ID="Label5" ForeColor="White" Font-Bold="true"></asp:Label>
                                    <asp:Label runat="server" ID="Label6" ForeColor="White" Font-Bold="true" Text="  %"></asp:Label>
                                </td>
                                <td class="alineado_centro_izq">&nbsp;</td>
                                <td class="alineado_centro">
                                    <asp:Label runat="server" ID="Label7" ForeColor="White" Font-Bold="true"></asp:Label>
                                    <asp:Label runat="server" ID="Label8" ForeColor="White" Font-Bold="true" Text="  %"></asp:Label>
                                </td>
                                <td class="alineado_centro"></td>
                            </tr>--%>
                                            <tr>
                                                <td class="alineado_centro_izq" style="width: auto">
                                                    <asp:Image runat="server" ID="Image2" ImageUrl="~/Imagenes/Iconos/Rect_Green.png" />

                                                </td>
                                                <td class="alineado_centro_izq">

                                                    <table class="alineado_centro_izq" align="center" style="width: 35px">
                                                        <tr>
                                                            <td>
                                                                <asp:ImageButton ID="Button5" runat="server" ImageUrl="~/Imagenes/Iconos/Up.png" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:TextBox ID="TextBox1" runat="server" AutoPostBack="true" OnTextChanged="TextBox1_TextChanged" CssClass="input + centro" Width="35" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:ImageButton ID="Button6" runat="server" ImageUrl="~/Imagenes/Iconos/Down.png" />
                                                            </td>
                                                        </tr>
                                                    </table>

                                                    <asp:NumericUpDownExtender ID="NumericUpDownExtender2" runat="server" Minimum="1" Maximum="100" TargetControlID="TextBox1" Width="60" TargetButtonDownID="Button6" TargetButtonUpID="Button5" />
                                                    <%--                                    <asp:SliderExtender ID="SliderExtender3" runat="server"
                                        TargetControlID="TextBox1"
                                        Minimum="1"
                                        Maximum="100"
                                        Length="200"
                                        BoundControlID="Label5"
                                        Steps="100"
                                        EnableHandleAnimation="true" />--%>
                                                </td>
                                                <td class="alineado_centro_izq" style="width: auto">
                                                    <asp:Image runat="server" ID="Image4" ImageUrl="~/Imagenes/Iconos/Rect_Yellow.png" />
                                                </td>
                                                <td class="alineado_centro_izq">

                                                    <table class="alineado_centro_izq" align="center" style="width: 35px">
                                                        <tr>
                                                            <td>
                                                                <asp:ImageButton ID="Button7" runat="server" ImageUrl="~/Imagenes/Iconos/Up.png" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:TextBox ID="TextBox2" runat="server" AutoPostBack="true" OnTextChanged="TextBox2_TextChanged" CssClass="input + centro" Width="35" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:ImageButton ID="Button8" runat="server" ImageUrl="~/Imagenes/Iconos/Down.png" />
                                                            </td>
                                                        </tr>
                                                    </table>

                                                    <asp:NumericUpDownExtender ID="NumericUpDownExtender3" runat="server" TargetControlID="TextBox2" Width="60" TargetButtonDownID="Button8" TargetButtonUpID="Button7" />
                                                    <%--                                    <asp:SliderExtender ID="SliderExtender4" runat="server"
                                        TargetControlID="TextBox2"
                                        Minimum="1"
                                        Maximum="150"
                                        Length="200"
                                        BoundControlID="Label7"
                                        Steps="100"
                                        EnableHandleAnimation="true" />--%>
                                                </td>
                                                <td class="alineado_centro_izq" style="width: auto">
                                                    <asp:Image runat="server" ID="Image5" ImageUrl="~/Imagenes/Iconos/Rect_Red.png" /></td>
                                            </tr>
                                            <tr>
                                                <td class="alineado_centro_izq"></td>
                                                <td class="alineado_centro" style="width: 35px">
                                                    <asp:Label runat="server" ID="Label12" ForeColor="#0066cc" Font-Bold="false" Text="Rango Inferior: "></asp:Label>
                                                    <asp:Label runat="server" ID="cargavalor1" ForeColor="White" Font-Bold="true" Text="2000"></asp:Label></td>
                                                <td class="alineado_centro_izq">&nbsp;</td>
                                                <td class="alineado_centro" style="width: 35px">
                                                    <asp:Label runat="server" ID="Label13" ForeColor="#0066cc" Font-Bold="false" Text="Rango Superior: "></asp:Label>
                                                    <asp:Label runat="server" ID="cargavalor2" ForeColor="White" Font-Bold="true" Text="2000"></asp:Label></td>
                                                <td class="alineado_centro_izq"></td>
                                            </tr>
                                        </table>

                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div class="contenedorDetalle" style="width: 100%;">
                                        <table align="center">
                                            <tr>
                                                <td class="alineado_centro_izq">
                                                    <asp:Label runat="server" ID="Label16" ForeColor="White" Font-Bold="true" Text="Configuracion variable de temperatura"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="alineado_centro" style="height: 10px">
                                                    <asp:CheckBox ID="check_temp" runat="server" OnCheckedChanged="check_temp_CheckedChanged" AutoPostBack="true" />
                                                    <asp:Label runat="server" ID="Label17" ForeColor="White" Font-Bold="false" Text="Temperatura automatica "></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label runat="server" ID="Label19" ForeColor="White" Font-Bold="false" Visible="false" Text="Ingrese temperatura constante: "></asp:Label>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator15" Text="*" ControlToValidate="temperatura" runat="server" Display="Dynamic" ValidationExpression="\d{1,9}(,\d{1,9})?" ValidationGroup="Save" ForeColor="Red"></asp:RegularExpressionValidator>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator15" Text="*" ControlToValidate="temperatura" runat="server" Display="Dynamic" ValidationGroup="Save" ForeColor="Red"></asp:RequiredFieldValidator>
                                                    <asp:TextBox ID="temperatura" runat="server" AutoPostBack="false" CssClass="input + centro" Visible="false" Width="50"> </asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>

                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>

                    <h1></h1>
                    <div>
                        <asp:Button ID="guardar" runat="server" CssClass="input" AutoPostBack="true" Text="Guardar Cambios" OnClick="guardar_Click" ValidationGroup="Save" />
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div>
            <asp:Timer runat="server" ID="Timer" Interval="2000" Enabled="false" OnTick="Timer_Tick">
            </asp:Timer>
            <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="guardar" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="Timer" EventName="Tick" />
                </Triggers>
                <ContentTemplate>
                    <asp:Panel ID="Panel1" runat="server" HorizontalAlign="Center">
                        <br />
                        <asp:Label ID="Mens" runat="server" Text="" CssClass="Input_err" Visible="false" Font-Bold="true" />
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
