﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ReportFour.aspx.cs" Inherits="EPMS_WEB.WebPage.Reports.TransporteMineral.ReportFour" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="BodyC" runat="server">

    <div id="contenedor">
        <asp:UpdatePanel runat="server" ID="upTotal" UpdateMode="Conditional">
            <Triggers>
                <asp:PostBackTrigger ControlID="btnExportar" />
                <asp:AsyncPostBackTrigger ControlID="btn2Hrs" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="btn4Hrs" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="btn8Hrs" EventName="Click" />
            </Triggers>
            <ContentTemplate>
                <asp:Timer ID="TimerAct" runat="server" Interval="600000" Enabled="true" OnTick="TimerAct_Tick" />
                <ul class="superior">
                    <li>
                        <asp:Label runat="server" ID="lblIdCam" Text="Camión" CssClass="titulo"></asp:Label>
                    </li>
                    <li>
                        <asp:Button ID="btn2Hrs" runat="server" Text="últimas 2 Horas" CssClass="input" OnClick="btn2Hrs_Click" />
                    </li>
                    <li>
                        <asp:Button ID="btn4Hrs" runat="server" Text="últimas 4 Horas" CssClass="input" OnClick="btn4Hrs_Click" />
                    </li>
                    <li>
                        <asp:Button ID="btn8Hrs" runat="server" Text="últimas 8 Horas" CssClass="input" OnClick="btn8Hrs_Click" />
                    </li>
                    <li class="centro" style="height: 24px;">
                        <asp:UpdateProgress ID="UpdateProgress" runat="server">
                            <ProgressTemplate>
                                <asp:Image runat="server" ImageUrl="~/Imagenes/Iconos/Cargando2.gif" Width="24" Height="24" ImageAlign="AbsMiddle" />
                                <asp:Label runat="server" Text="Cargando Datos..." />
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </li>
                    <li class="der">
                        <asp:Label ID="lblIntervalo" runat="server" Visible="false" />
                    </li>
                    <li class="der">
                        <asp:Button ID="btnExportar" runat="server" Text="Exportar a Excel" OnClick="ExportToExcel" CssClass="input" />
                    </li>
                </ul>
                <div class="contenedorGraficos">
                    <div>
                        <br />
                        <asp:Label ID="lblTitGTkph" runat="server" Text="TKPH" CssClass="titulo" />
                        <br />
                        <asp:Chart ID="gTkph" runat="server" Width="450" Height="230">
                            <ChartAreas>
                                <asp:ChartArea Name="cArea" />
                            </ChartAreas>
                        </asp:Chart>
                    </div>
                    <div>
                        <br />
                        <asp:Label ID="lblTitGCarga" runat="server" Text="Carga (Ton)" CssClass="titulo" />
                        <br />
                        <asp:Chart ID="gCarga" runat="server" Width="450" Height="230">
                            <ChartAreas>
                                <asp:ChartArea Name="cArea" />
                            </ChartAreas>
                        </asp:Chart>
                    </div>
                    <div>
                        <br />
                        <asp:Label ID="lblTitGTemp" runat="server" Text="Temperatura (C°)" CssClass="titulo" />
                        <br />
                        <asp:Chart ID="gTemp" runat="server" Width="450" Height="230">
                            <ChartAreas>
                                <asp:ChartArea Name="cArea" />
                            </ChartAreas>
                        </asp:Chart>
                    </div>
                    <div>
                        <br />
                        <asp:Label ID="lblTitGPres" runat="server" Text="Presión (Psi)" CssClass="titulo" />
                        <br />
                        <asp:Chart ID="gPres" runat="server" Width="450" Height="230">
                            <ChartAreas>
                                <asp:ChartArea Name="cArea" />
                            </ChartAreas>
                        </asp:Chart>
                    </div>
                    <br />
                    <br />
                </div>
                <asp:Label runat="server" ID="lblAct" Text="Actualizado:" CssClass="der" />
                <br />
                <asp:GridView runat="server" ID="grilla" AutoGenerateColumns="true" OnDataBound="grilla_DataBound" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>


    <asp:UpdatePanel ID="upM" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Panel ID="pMensaje" runat="server" CssClass="BordeForm" Style="display: none; width: 500px;">
                <asp:Panel ID="pCabecera" runat="server" CssClass="titulo">
                    <asp:Label ID="Label7" runat="server" Text="Integridad" />
                </asp:Panel>
                <asp:Panel ID="pBody" runat="server">
                    <br />
                    <br />
                    <asp:Image ID="imgIcono" runat="server" ImageUrl="~/StyleSheet/Icon/info-icon.png" Width="24" Height="24" />
                    <asp:Label ID="lblMen" runat="server" /><br />
                    <br />
                    <asp:Button ID="btnAceptar" runat="server" Text="Aceptar" CssClass="input" /><br />
                    <br />
                </asp:Panel>
            </asp:Panel>

            <asp:ModalPopupExtender ID="MenPOP" runat="server" TargetControlID="lblMen" PopupControlID="pMensaje" DropShadow="false" BackgroundCssClass="FondoAplicacion" OnOkScript="btnAceptar" />
        </ContentTemplate>
    </asp:UpdatePanel>


</asp:Content>
