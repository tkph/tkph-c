﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="EMPS_WEB.Index" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" type="text/css" href="Css/login.css" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>TKPH Inicio de Sesión</title>

   

</head>
<body>
    <form id="form1" runat="server">

<div class="caja_login">
    <br />
    <asp:Label runat="server" CssClass="TitApp" Text="TKPH" />
    <br />
&nbsp;<div class="loginUp">Inicio de sesión</div>
<div class="loginDown">
<br />


<ul>
<li> <img src="Imagenes/Iconos/user.png" /> <asp:TextBox runat="server" ID="txtUsuario" placeholder="Usuario" CssClass="input" Width="250" /></li>
<br />
<br />
<li> <img src="Imagenes/Iconos/pass.png" /><asp:TextBox runat="server" ID="txtPassword" TextMode="Password" placeholder="Password" CssClass="input" Width="250" /></li>

</ul>
<br />
<br />
<asp:Button runat="server" ID="btnLogin" Text="Ingresar" CssClass="input" OnClick="btnLogin_Click" />
<asp:Label runat="server" ID="lblMens" CssClass="mens" Visible="false" />
  </div>
            </div>

    </form>
</body>
</html>
