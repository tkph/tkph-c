﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DashboardCarga.aspx.cs" Inherits="EMPS_WEB.BI.DashboardCarga" Culture="Auto" UICulture="Auto" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="BodyC" runat="server">
    <div id="contenedor">
         <asp:Panel runat="server" ID="pnlFiltros" CssClass="panelFiltros">
                    <ul>
                        <li>Turno:
                            <asp:DropDownList runat="server" ID="cboTurno" CssClass="input">
                                <asp:ListItem Value="3" Text="Todos" />
                                <asp:ListItem Value="1" Text="Día" />
                                <asp:ListItem Value="2" Text="Noche" />
                            </asp:DropDownList>
                        </li>
                        <!--
                        <li>Flota:
                            <asp:DropDownList runat="server" ID="cboFlota" CssClass="input" />
                        </li>
                        -->
                        <li>Periodo:
                            <asp:TextBox runat="server" ID="txtFDesde" Width="80px" CssClass="input" />
                            
                            -
                            <asp:TextBox runat="server" ID="txtFHasta" Width="80px" CssClass="input" />
                            
                            <script type="text/javascript">
                                $(document).ready(function () {



                                    $("#<%=txtFDesde.ClientID%>").pickadate(
                                      {
                                          weekdaysShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
                                          showMonthsShort: true,
                                          format: 'dd-mm-yyyy',
                                          today: false,
                                          clear: false,
                                          close: false,
                                          firstDay: 1
                                      }
                                    );

                                    $("#<%=txtFHasta.ClientID%>").pickadate(
                                        {
                                            weekdaysShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
                                            showMonthsShort: true,
                                            format: 'dd-mm-yyyy',
                                            today: false,
                                            clear: false,
                                            close: false,
                                            firstDay: 1
                                        }
                                        );


                                }); //this was missing
                            </script> 
                        </li>
                        <li>
                            <asp:Button runat="server" ID="btnFiltro" Text="Filtrar" OnClick="btnFiltro_Click" CssClass="input" />
                        </li>
                        <li>
                            <asp:UpdateProgress ID="UpdateProgress" runat="server">
                                <ProgressTemplate>
                                    <asp:Image runat="server" ImageUrl="~/Imagenes/Iconos/Cargando2.gif" Width="24" Height="24" ImageAlign="AbsMiddle" />
                                    <asp:Label runat="server" Text="Cargando Datos..." />
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                        </li>
                    </ul>
                </asp:Panel>

        <asp:UpdatePanel runat="server" ID="upTotal" UpdateMode="Conditional" ChildrenAsTriggers="false">
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnFiltro" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="gCarga" EventName="Click" />               
                <asp:PostBackTrigger ControlID="btnExp" />
            </Triggers>
            <ContentTemplate>
               
                <div id="divGraf">

                    <div>
                        <asp:Chart ID="gCarga" runat="server" Width="400" Height="190" OnClick="gCarga_Click">
                            <ChartAreas>
                                <asp:ChartArea Name="cArea" />
                            </ChartAreas>
                        </asp:Chart>
                    </div>
                    <div>
                        <asp:Chart ID="gHistograma" runat="server" Width="599" Height="190" OnClick="gHistograma_Click" OnPostPaint="gHistograma_PostPaint">
                            <ChartAreas>
                                <asp:ChartArea Name="cArea" />
                            </ChartAreas>
                        </asp:Chart>
                    </div>
                    <div>
                        <asp:Chart ID="gTendencia" runat="server" Width="1000" Height="190" OnClick="gTendencia_Click">
                            <ChartAreas>
                                <asp:ChartArea Name="cArea" />
                            </ChartAreas>
                        </asp:Chart>
                    </div>

                    <div>
                        <asp:Chart ID="gRPala" runat="server" Width="332" Height="190" OnClick="gRPala_Click">
                            <ChartAreas>
                                <asp:ChartArea Name="cArea" />
                            </ChartAreas>
                        </asp:Chart>
                    </div>
                    <div>
                        <asp:Chart ID="gRCamion" runat="server" Width="332" Height="190" OnClick="gRCamion_Click">
                            <ChartAreas>
                                <asp:ChartArea Name="cArea" />
                            </ChartAreas>
                        </asp:Chart>
                    </div>
                    <div>
                        <asp:Chart ID="gROperador" runat="server" Width="332" Height="190" OnClick="gROperador_Click">
                            <ChartAreas>
                                <asp:ChartArea Name="cArea" />
                            </ChartAreas>
                        </asp:Chart>
                    </div>

                </div>
            </ContentTemplate>
        </asp:UpdatePanel>

        <!-- Panel Exportar-->
        <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Conditional">
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="gHistograma" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="gTendencia" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="gRPala" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="gRCamion" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="gROperador" EventName="Click" />
            </Triggers>
            <ContentTemplate>
                <asp:Panel ID="pMensaje" runat="server" CssClass="BordeForm" Style="display: none; width: 300px;">
                    <asp:Panel ID="pCabecera" runat="server" CssClass="TituloForm">
                        <asp:Label ID="lblGraf" runat="server" Text="" CssClass="titulo" />
                    </asp:Panel>
                    <asp:Panel ID="pBody" runat="server">
                        <!-- Contenido -->
                        <br />
                        Seleccione Datos a Exportar :
                        <asp:RadioButtonList ID="rblOpciones" runat="server" />
                        <br />
                        <!-- Fin Contenido -->
                        <center>
                            <asp:Button ID="btnExp" runat="server" Text="Exportar" CssClass="input" OnClick="btnExp_Click" />
                            <asp:Button ID="btnAceptar" runat="server" Text="Cerrar" CssClass="input" />
                        </center>
                        <br />
                        <br />
                    </asp:Panel>
                </asp:Panel>
                <asp:ModalPopupExtender ID="MenPOP" runat="server" TargetControlID="lblGraf" PopupControlID="pMensaje" DropShadow="false" BackgroundCssClass="FondoAplicacion" OnOkScript="btnAceptar" />
            </ContentTemplate>
        </asp:UpdatePanel>

    </div>
    <asp:GridView runat="server" ID="gvData" AutoGenerateColumns="true" />


</asp:Content>
