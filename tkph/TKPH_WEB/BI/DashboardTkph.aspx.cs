﻿using Com.Vmica.View.BI;
using Com.Vmica.View.BI.Export;
using TKPH_BUSINESS.BUSINESS_PAGE.BI;
using TKPH_BUSINESS.COMUN;
using TKPH_WEB.Comun;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using COM_TKPH_DATA.BUSINESS_PAGE.BI;

namespace TKPH_WEB.BI
{
    /// <summary>
    /// Tablero control TKPH de la Flota.
    /// </summary>
    public partial class DashboardTkph : System.Web.UI.Page
    {

        /// <summary>
        /// Llamada a instancia de Objeto "bCargaGraficos"
        /// </summary>
        bTkphGraficos GraficosTkph = bTkphGraficos.Instance;

        /// <summary>
        /// Llamada a instancia de Objeto "bCargaExport"
        /// </summary>
        bTkphExport ExportTkph = bTkphExport.Instance;

        IList<vTkphFlota> ListaFlota = new List<vTkphFlota>();
        IList<vTkphDestino> ListaDestino = new List<vTkphDestino>();
        IList<vTkphHistograma> ListaHistograma = new List<vTkphHistograma>();
        IList<vTkphRanking> ListaRanking = new List<vTkphRanking>();
        IList<vTkphTendencia> ListaTendencia = new List<vTkphTendencia>();

        IList<vTkphHistogramaExp> ExpHistograma = new List<vTkphHistogramaExp>();
        IList<vTkphTendenciaExp> ExpTendencia = new List<vTkphTendenciaExp>();
        IList<vTkphCamionExp> ExpCamion = new List<vTkphCamionExp>();
        IList<vTkphOperadorExp> ExpOperador = new List<vTkphOperadorExp>();
        IList<vTkphDestinoExp> ExpDestino = new List<vTkphDestinoExp>();

        public String Flota = "";
        public Int64 Turno = 3;
        public String FIni = "01-04-2016";
        public String FHas = "31-05-2016";


        /// <summary>
        /// Metodo de carga Inicial de la Página
        /// </summary>
        /// <param name="sender">La fuente del evento.</param>
        /// <param name="e">La Instancia que contiene los datos del evento.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                HttpContext.Current.Session["Titulo"] = "Dashboard Tkph";

                FIni = DateTime.Today.AddDays(-14).ToString("dd-MM-yyyy");
                FHas = DateTime.Today.ToString("dd-MM-yyyy");

                txtFDesde.Text = FIni.ToString() ;
                txtFHasta.Text = FHas.ToString();

                CargaListas();
                Flota = ListaFlota.First().FlotaCamion.ToString();

                CargaGraficos();
                
            }

        }

        /// <summary>
        /// Metodo que inserta datos en interfaces de lista para desplegar datos en la GUI. 
        /// </summary>
        public void CargaListas()
        {
            ListaFlota = GraficosTkph.getTkphFlota(1, Turno, FIni, FHas,"");
            ListaDestino = GraficosTkph.getTkphDestino(2, Turno, FIni, FHas,"");
            ListaHistograma = GraficosTkph.getTkphHistograma(3, Turno, FIni, FHas,"");
            ListaRanking = GraficosTkph.getTkphRanking(4, Turno, FIni, FHas,"");
            ListaTendencia = GraficosTkph.getTkphTendencia(5, Turno, FIni, FHas,"");
        }

        /// <summary>
        /// Metodo que invoca los metodos para generacion de Graficos en la GUI. 
        /// </summary>
        public void CargaGraficos()
        {
            GrafFlota();
            GrafHistograma();
            GrafTendencia();
            GrafDestino();
            GrafRanking();
        }


        /// <summary>
        /// Metodo de generacion de graficos historico de Flota. 
        /// </summary>
        public void GrafFlota()
        {
            Series s1 = Comun.Comun.CreateSerie("", SeriesChartType.Column);

            Comun.Comun.CreatePoint(ListaFlota, s1, Flota);

            gFlota.Series.Add(s1);

            Comun.Comun.FormatoGraficos(gFlota, -45, 0, "%", "", "Promedio en Relación a Tkph Nominal", false);

            gFlota.Series[0].PostBackValue = "#AXISLABEL";

            int tp = gFlota.Series[0].Points.Count();

           StripLine Lcien = new StripLine();
           Lcien.StripWidth = 0;
           Lcien.BorderColor = Color.FromArgb(216, 66, 55);
           Lcien.BorderWidth = 1;
           Lcien.Interval = 99.9;

           gFlota.ChartAreas[0].AxisY.StripLines.Add(Lcien);
        }

        /// <summary>
        /// Metodo de generacion de graficos Histograma. 
        /// </summary>
        public void GrafHistograma()
        {
            Series s2 = Comun.Comun.CreateSerie("", SeriesChartType.Column);

            Comun.Comun.CreatePoint(ListaHistograma.Where(f => f.FlotaCamion.Equals(Flota)).ToList(), s2);

            gHistograma.Series.Add(s2);

            Comun.Comun.FormatoGraficos(gHistograma, -45, 0, "Cant. Eventos", "", "Histograma distribución de Tkph", false);

            gHistograma.Series[0]["PointWidth"] = "1";
            gHistograma.Series[0].BorderColor = Color.White;
            gHistograma.Series[0].BorderWidth = 1;

            gHistograma.Series[0].PostBackValue = "#AXISLABEL".ToString();

        }

        /// <summary>
        /// Metodo de generacion de grafico Ranking Destino. 
        /// </summary>
        public void GrafDestino()
        {
            Series t1 = Comun.Comun.CreateSerie("Destino", SeriesChartType.Bar);

            Comun.Comun.CreatePoint(ListaDestino.Where(f => f.Flota.Equals(Flota)).Take(10).OrderBy(v => v.Porcentaje).ToList(), t1);
         
            gRDestino.Series.Add(t1);

            Comun.Comun.FormatoGraficos(gRDestino, 0, 0, "", "Destino", "% Tkph",false);

            gRDestino.Series[0]["PointWidth"] = "0.6";

            gRDestino.Series[0]["BarLabelStyle"] = "Left";
            gRDestino.Series[0].LabelBackColor = Color.FromArgb(255, 30, 30, 30);

            Font fo = new Font("Calibri", 7.5f, FontStyle.Regular);
            t1.Font = fo;

            gRDestino.Series[0].PostBackValue = "#AXISLABEL";
        }


        /// <summary>
        /// Metodo de generacion de grafico Ranking Operador y Camión. 
        /// </summary>
        public void GrafRanking()
        {
            Series r1 = Comun.Comun.CreateSerie("Operador", SeriesChartType.Bar);
            Series r2 = Comun.Comun.CreateSerie("Camión", SeriesChartType.Bar);

            Comun.Comun.CreatePoint(ListaRanking.Where(f => f.FlotaCamion.Equals(Flota)).Where(t => t.Tipo.Equals("RANKING OPERADOR")).Take(10).OrderBy(v => v.Promedio).ToList(), r1,true);
            Comun.Comun.CreatePoint(ListaRanking.Where(f => f.FlotaCamion.Equals(Flota)).Where(t => t.Tipo.Equals("RANKING CAMION")).Take(10).OrderBy(v => v.Promedio).ToList(), r2,true);
           
            gROperador.Series.Add(r1);
            gRCamion.Series.Add(r2);

            Comun.Comun.FormatoGraficos(gROperador, 0, 0, "", "Operador","% sobre Tkph",false);
            Comun.Comun.FormatoGraficos(gRCamion, 0, 0, "", "Camión", "% sobre Tkph", false);

            gROperador.Series[0]["PointWidth"] = "0.6";
            gRCamion.Series[0]["PointWidth"] = "0.6";

            gROperador.Series[0]["BarLabelStyle"] = gRCamion.Series[0]["BarLabelStyle"] = "Left";
            gROperador.Series[0].LabelBackColor = gRCamion.Series[0].LabelBackColor = Color.FromArgb(255, 30, 30, 30);

            Font fo = new Font("Calibri", 7.5f, FontStyle.Regular);
            r1.Font = r2.Font = fo;

            gROperador.Series[0].PostBackValue = "#AXISLABEL";
            gRCamion.Series[0].PostBackValue = "#AXISLABEL";
            
        }

        /// <summary>
        /// Metodo de generacion de graficos de Tendencia. 
        /// </summary>

        public void GrafTendencia()
        {
            Series r1 = Comun.Comun.CreateSerie("Promedio Eventos", SeriesChartType.Line);

            Comun.Comun.CreatePoint(ListaTendencia.Where(f => f.FlotaCamion.Equals(Flota)).ToList(), r1);
            
            gTendencia.Series.Add(r1);

            Comun.Comun.FormatoGraficos(gTendencia, -45, 0, "%", "","Promedio TKPH",true);

            //gTendencia.Series[0].Color = Comun.Colores(13, 255);

            gTendencia.Series[0].PostBackValue = "#AXISLABEL";

        }

        /// <summary>
        /// Evento Click del Botón Filtrar para aplicar filtro según parametros ingresados.
        /// </summary>
        /// <param name="sender">La fuente del evento.</param>
        /// <param name="e">La Instancia que contiene los datos del evento.</param>
        protected void btnFiltro_Click(object sender, EventArgs e)
        {
            //Flota = cboFlota.SelectedValue.ToString();
            Turno = Int64.Parse(cboTurno.SelectedValue);

            FIni = txtFDesde.Text.ToString();
            FHas = txtFHasta.Text.ToString();

            CargaListas();
            CargaGraficos();

        }


        /// <summary>
        /// Evento Click del Grafico de flota para aplicar filtro según Flota seleccionada.
        /// </summary>
        /// <param name="sender">La fuente del evento.</param>
        /// <param name="e">La Instancia que contiene los datos del evento.</param>
        protected void gFlota_Click(object sender, ImageMapEventArgs e)
        {
            Flota = e.PostBackValue.ToString();
            Turno = Int64.Parse(cboTurno.SelectedValue);

            FIni = txtFDesde.Text.ToString();
            FHas = txtFHasta.Text.ToString();

            CargaGraficos();
        }

        protected override void LoadViewState(object SavedState)
        {
            if (SavedState != null)
            {
                object[] state = (object[])SavedState;

                if (state[0] != null) base.LoadViewState(state[0]);
                if (state[1] != null) ListaFlota = Serializador.DesSerializeObjeto<vTkphFlota[]>((string)state[1]).ToList();
                if (state[2] != null) ListaDestino = Serializador.DesSerializeObjeto<vTkphDestino[]>((string)state[2]).ToList();
                if (state[3] != null) ListaHistograma = Serializador.DesSerializeObjeto<vTkphHistograma[]>((string)state[3]).ToList();
                if (state[4] != null) ListaRanking = Serializador.DesSerializeObjeto<vTkphRanking[]>((string)state[4]).ToList();
                if (state[5] != null) ListaTendencia = Serializador.DesSerializeObjeto<vTkphTendencia[]>((string)state[5]).ToList();
                if (state[6] != null) Flota = state[6].ToString();
               
            }
        }

        protected override object SaveViewState()
        {
            object[] SavedState = new object[18];
            SavedState[0] = base.SaveViewState();

            if (ListaFlota != null) SavedState[1] = Serializador.SerializeObjeto<vTkphFlota[]>(ListaFlota.ToArray());
            if (ListaDestino != null) SavedState[2] = Serializador.SerializeObjeto<vTkphDestino[]>(ListaDestino.ToArray());
            if (ListaHistograma != null) SavedState[3] = Serializador.SerializeObjeto<vTkphHistograma[]>(ListaHistograma.ToArray());
            if (ListaRanking != null) SavedState[4] = Serializador.SerializeObjeto<vTkphRanking[]>(ListaRanking.ToArray());
            if (ListaTendencia != null) SavedState[5] = Serializador.SerializeObjeto<vTkphTendencia[]>(ListaTendencia.ToArray());
            if (Flota != null) SavedState[6] = Flota.ToString();

            return SavedState;
        }

        /// <summary>
        /// Evento Click del Grafico Histograma para desplegar Popup de seleccion de rango a Exportar.
        /// </summary>
        /// <param name="sender">La fuente del evento.</param>
        /// <param name="e">La Instancia que contiene los datos del evento.</param>
        protected void gHistograma_Click(object sender, ImageMapEventArgs e)  
        {
            String[] Valor = e.PostBackValue.Split('%');
            MostrarExp("Histograma", Valor[0].ToString());
        }

        /// <summary>
        /// Evento Click del Grafico de tendencia para desplegar Popup de seleccion de rango a Exportar.
        /// </summary>
        /// <param name="sender">La fuente del evento.</param>
        /// <param name="e">La Instancia que contiene los datos del evento.</param>
        protected void gTendencia_Click(object sender, ImageMapEventArgs e)
        {
            MostrarExp("Tendencia", e.PostBackValue.ToString());
        }


        /// <summary>
        /// Evento Click del Grafico Ranking Operador para desplegar Popup de seleccion de rango a Exportar.
        /// </summary>
        /// <param name="sender">La fuente del evento.</param>
        /// <param name="e">La Instancia que contiene los datos del evento.</param>
        protected void gROperador_Click(object sender, ImageMapEventArgs e)
        {
            MostrarExp("Ranking Operador", e.PostBackValue.ToString());
        }


        /// <summary>
        /// Evento Click del Grafico Ranking Camión para desplegar Popup de seleccion de rango a Exportar.
        /// </summary>
        /// <param name="sender">La fuente del evento.</param>
        /// <param name="e">La Instancia que contiene los datos del evento.</param>
        protected void gRCamion_Click(object sender, ImageMapEventArgs e)
        {
            MostrarExp("Ranking Camión", e.PostBackValue.ToString());
        }


        /// <summary>
        /// Evento Click del Grafico Ranking Destino para desplegar Popup de seleccion de rango a Exportar.
        /// </summary>
        /// <param name="sender">La fuente del evento.</param>
        /// <param name="e">La Instancia que contiene los datos del evento.</param>
        protected void gRDestino_Click(object sender, ImageMapEventArgs e)
        {
            MostrarExp("Ranking Destino", e.PostBackValue.ToString());
        }


        /// <summary>
        /// Metodo que captura los parametros de origen para exportar datos a Excel.
        /// </summary>
        /// <param name="Grafico">Titulo de grafico a Mostrar.</param>
        /// <param name="ValorS">Valor para exportar datos.</param>
        /// <param name="Valor2">Valor por Defecto.</param>
        public void MostrarExp(String Grafico, String ValorS)
        {
            rblOpciones.Items.Clear();

            lblGraf.Text = Grafico;
            rblOpciones.Items.Add(new ListItem("Valor Seleccionado (" + ValorS.ToString() + ")", ValorS.ToString()));
            rblOpciones.Items.Add(new ListItem("Toda la Serie", ""));
            rblOpciones.SelectedIndex = 0;
            MenPOP.Show();
        }


        /// <summary>
        /// Metodo que realiza acción para exportar Grilla a Archivo Excel.
        /// </summary>
        protected void ExportarExcel()
        {
            String Tit = lblGraf.Text;

            String Seleccion = rblOpciones.SelectedValue.ToString();

            FIni = txtFDesde.Text.ToString();
            FHas = txtFHasta.Text.ToString();

            switch (Tit)
            {
                case "Histograma":
                    
                    if (Seleccion != "")
                    {
                        if (Seleccion == "120+") { Seleccion = "130"; }
                        Seleccion = (Int32.Parse(Seleccion) / 10).ToString();
                    }

                    ExpHistograma = ExportTkph.getTkphHistogramaData(6, Turno, FIni, FHas, Seleccion);
                    gvData.DataSource = ExpHistograma.Where(f => f.Flota.Equals(Flota)).ToList();
                    break;
                case "Tendencia":
                    if (Seleccion != "")
                    {
                        DateTime fe = DateTime.Parse(Seleccion);
                        Seleccion = fe.ToString("yyyy-MM-dd");
                    }
                    ExpTendencia = ExportTkph.getTkphTendenciaData(7, Turno, FIni, FHas, Seleccion);
                    gvData.DataSource = ExpTendencia.Where(f => f.Flota.Equals(Flota)).ToList();
                    break;
                case "Ranking Operador":
                    ExpOperador = ExportTkph.getTkphOperadorData(10, Turno, FIni, FHas, Seleccion);
                    gvData.DataSource = ExpOperador.Where(f => f.Flota.Equals(Flota)).ToList();
                    break;
                case "Ranking Camión":
                    ExpCamion = ExportTkph.getTkphCamionData(9, Turno, FIni, FHas, Seleccion);
                    gvData.DataSource = ExpCamion.Where(f => f.Flota.Equals(Flota)).ToList();
                    break;
                case "Ranking Destino":
                    ExpDestino = ExportTkph.getTkphDestinoData(8, Turno, FIni, FHas, Seleccion);
                    gvData.DataSource = ExpDestino.Where(f => f.Flota.Equals(Flota)).ToList();
                    break;
            }


            gvData.DataBind();

            gvData.Visible = true;

            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename=Data_Tkph_" + Tit + ".xls");
            Response.Charset = "UFT-8";
            this.EnableViewState = false;
            // Si se desea que el archivo no se guarde y solo se muestre comentar la linea siguiente.
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "application/vnd.ms-excel";
            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
            gvData.RenderControl(htmlWrite);
            Response.Write(stringWrite.ToString());
            Response.End();

            gvData.Visible = false;

        }

        /// <summary>
        /// Confirms that an <see cref="T:System.Web.UI.HtmlControls.HtmlForm" /> control is rendered for the specified ASP.NET server control at run time.
        /// </summary>
        /// <param name="control">The ASP.NET server control that is required in the <see cref="T:System.Web.UI.HtmlControls.HtmlForm" /> control.</param>
        public override void VerifyRenderingInServerForm(System.Web.UI.Control control)
        {
            /* Verifies that the control is rendered */
        }


        /// <summary>
        /// Evento Click del Botón Exportar.
        /// </summary>
        /// <param name="sender">La fuente del evento.</param>
        /// <param name="e">La Instancia que contiene los datos del evento.</param>
        protected void btnExp_Click(object sender, EventArgs e)
        {
            ExportarExcel();
            MenPOP.Hide();
        }


    }
}