﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DetallePro.aspx.cs" Inherits="TKPH_WEB.WebPage.Reports.ControlSistema.DetallePro" %>

<asp:Content ID="Content1" ContentPlaceHolderID="BodyC" runat="server"> 
    <div class="alineado_centro_izq" style="width: 100%;">
        <br />
        <table border="0" cellspacing="0" cellpadding="0" width="600" class="BordeForm">
            <tr>
                <td class="alineado_centro" width="64">
                    <asp:Image runat="server" ID="imgKPI" /></td>
                <td class="alineado_centro_izq">
                    <asp:Label runat="server" ID="lblProce" CssClass="SubTitApp"></asp:Label></td>
                <td class="alineado_centro">
                    <asp:Label runat="server" ID="lblPorc" CssClass="TitApp"></asp:Label></td>
            </tr>
        </table>
        <table border="0" cellspacing="10" cellpadding="0" width="100%">
            <tr>
                <td class="BordeForm" width="50%">
                    <asp:Label runat="server" Text="Registros Cargados Ultima Semana" CssClass="txtNegroBold"></asp:Label><br />
                    <asp:Chart ID="grafRegistros" runat="server" Width="500" Height="150">
                        <ChartAreas>
                            <asp:ChartArea Name="cArea">
                            </asp:ChartArea>
                        </ChartAreas>
                    </asp:Chart>
                </td>
                <td class="BordeForm">
                    <asp:Label runat="server" Text="Detalle Proceso" CssClass="txtNegroBold"></asp:Label><br />
                    <div class="alineado_up_izq" style="padding-left: 4px;">
                        <br />
                        <br />
                        <asp:Label runat="server" Text="" ID="lblFuente"></asp:Label>
                        <br />
                        <br />
                        <asp:Label runat="server" Text="" ID="lblSalida"></asp:Label>
                        <br />
                        <br />
                        <asp:Label runat="server" Text="" ID="lblPeriodicidad"></asp:Label>
                        <br />
                        <br />
                        <asp:Label runat="server" Text="" ID="lblGerencia"></asp:Label>
                    </div>

                </td>
            </tr>
            <tr>
                <td class="BordeForm">
                    <asp:Label runat="server" Text="Ejecuciones v/s Error Ultima Semana" CssClass="txtNegroBold"></asp:Label><br />
                    <asp:Chart ID="grafVersus" runat="server" Width="500" Height="200">
                        <ChartAreas>
                            <asp:ChartArea Name="cArea">
                            </asp:ChartArea>
                        </ChartAreas>
                    </asp:Chart>
                </td>
                <td class="BordeForm">
                    <asp:Label runat="server" Text="Errores de Ejecución" ID="lblErrSem" CssClass="txtNegroBold"></asp:Label>
                    <br />
                    <br />
                    <asp:Panel ID="Panel1" runat="server" ScrollBars="Vertical" Height="200" Width="100%">
                        <asp:GridView runat="server" ID="gridErrores" HorizontalAlign="Center" AutoGenerateColumns="False" HeaderStyle-CssClass="cabeceras_1" RowStyle-CssClass="txtNegroBold10">
                            <Columns>
                                <asp:BoundField DataField="Fecha" HeaderText="Fecha" ItemStyle-Width="70" />
                                <asp:BoundField DataField="Hora" HeaderText="Hora" />
                                <asp:BoundField DataField="DescError" HeaderText="Error" />
                                <asp:BoundField DataField="Ejecutable" HeaderText="Subproceso" />
                            </Columns>
                        </asp:GridView>
                    </asp:Panel>
                    <br />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
