﻿using Com.Vmica.View;
using Com.Vmica.View.ControlSistema;
using TKPH_BUSINESS.BUSINESS_PAGE.ControlSistema;
using TKPH_BUSINESS.COMUN;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EMPS_WEB.WebPage.Reports.ControlSistema
{
    public partial class Menu : System.Web.UI.Page
    {

        bControlSistema crtlSis = bControlSistema.Instance;

        public static IList<vEstadisticas> _procesos;
        public static IList<vEstadisticas> _procesos_menu;
        public static String Idarea = "0";

        protected void Page_Load(object sender, EventArgs e)
        {

                HttpContext.Current.Session["Titulo"] = "Control Sistema";
                HttpContext.Current.Session["SubTitulo"] = "";

                if (!IsPostBack)
                {
                    CargaResumen(Idarea);
                    Cargamenu();
                }

       }

        protected void dropProceso_SelectedIndexChanged(object sender, EventArgs e)
        {
            CargaResumen(Idarea);
        }

        protected void CargaResumen(String IdArea)
        {

            _procesos = crtlSis.GetEstadisticas("0", Convert.ToInt64(dropProceso.SelectedValue.ToString()), IdArea);

            _procesos_menu = _procesos; //Copia de Consulta para metodo Cargamenu();

            IList<vEstadisticas> listado = _procesos.GroupBy(test => test.Proceso).Select(grp => grp.First()).OrderBy(or => or.IdArea).ToList();
            //String Hoy = DateTime.Today.ToString("dd-mm-yyyy");
            //IList<vEstadisticas> listado = _procesos.Where(p => p.Fecha.Equals(Hoy)).ToList();

            # region tabla HTML sin Uso.
            /*
            int cnt = 0;
            String Tabla = "<table border='0' cellspacing='0' cellpadding='0' width='800'>";

            foreach (vEstadisticas data in _procesos)
            {
                cnt++;

                String Ima = "";

                switch(data.Estado)
                {
                    case 1:
                        Ima = "/StyleSheet/Icon/formularios/proR.png";
                        break;
                    case 3:
                        Ima = "/StyleSheet/Icon/formularios/proV.png";
                        break;
                    case 4:
                        Ima = "/StyleSheet/Icon/formularios/proD.png";
                        break;
                }


                switch(cnt)
                {
                    case 1:
                        Tabla += "<tr><td class='BordeForm' width='200'><img src='" + Ima + "'><br/>" + data.Proceso + "</td>";
                        break;
                    case 4:
                        cnt = 0;
                        Tabla += "</tr>";
                        break;
                    default:
                        Tabla += "<td class='BordeForm' width='200'><img src='" + Ima + "'><br/>" + data.Proceso + "</td>";
                        break;
                }

            }

            Tabla += "</table><br/><br/>";
            divProcesos.InnerHtml = Tabla;
             * */
            # endregion

            int cnt = 0;
            int cFilas = 0;

            foreach (vEstadisticas data in listado)
            {
                cnt++;
                String Ima = "";
                TableCell td = new TableCell();
                Label lbl = new Label();
                Label lblPor = new Label();
                HyperLink img = new HyperLink();

                Table TBP = new Table();
                TableRow TRP = new TableRow();
                TableCell TDokerr = new TableCell();
                TableCell TDtrc = new TableCell();
                Label lblOk = new Label();
                Label lblErr = new Label();
                Label regCar = new Label();

                Image imgOk = new Image();
                imgOk.ImageUrl = "~/StyleSheet/Icon/formularios/proOk.png";
                imgOk.ToolTip = "Procesos Ejecutados";
                imgOk.ImageAlign = ImageAlign.AbsMiddle;
                Image imgErr = new Image();
                imgErr.ImageUrl = "~/StyleSheet/Icon/formularios/proErr.png";
                imgErr.ToolTip = "Procesos No Ejecutados";
                imgErr.ImageAlign = ImageAlign.AbsMiddle;
                Image imgCar = new Image();
                imgCar.ImageUrl = "~/StyleSheet/Icon/formularios/proCar.png";
                imgCar.ToolTip = "Registros Cargados";


                lblOk.Text = data.ProcesosOk.ToString();
                lblOk.CssClass = "txtNegroBold";
                lblErr.Text = data.ProcesosError.ToString();
                lblErr.CssClass = "txtNegroBold";

                regCar.Text = data.RegCargados.ToString();
                regCar.CssClass = "txtNegroBold";
                TDtrc.Controls.Add(imgCar);
                TDtrc.Controls.Add(regCar);
                TDtrc.Width = 150;
                TDtrc.HorizontalAlign = HorizontalAlign.Center;


                TDokerr.Controls.Add(imgOk);
                TDokerr.Controls.Add(lblOk);
                TDokerr.Controls.Add(imgErr);
                TDokerr.Controls.Add(lblErr);
                TDokerr.Width = 150;
                TDokerr.HorizontalAlign = HorizontalAlign.Center;

                TRP.Cells.Add(TDokerr);
                TRP.Cells.Add(TDtrc);


                TBP.Controls.Add(TRP);


                if (dropProceso.SelectedValue == "2")
                {
                    imgOk.Visible = false;
                    lblOk.Visible = false;
                    TDtrc.Visible = false;
                }
                else
                {
                    imgOk.Visible = true;
                    lblOk.Visible = true;
                    TDtrc.Visible = true;
                }


                switch (data.Estado)
                {
                    case 1:
                        Ima = "~/StyleSheet/Icon/formularios/proR.png";
                        break;
                    case 2:
                        Ima = "~/StyleSheet/Icon/formularios/proA.png";
                        break;
                    case 3:
                        Ima = "~/StyleSheet/Icon/formularios/proV.png";
                        break;
                    case 4:
                        Ima = "~/StyleSheet/Icon/formularios/proD.png";
                        break;
                }

                lbl.Text = data.Proceso;
                lbl.CssClass = "TitApp";
                lblPor.Text = "Disponibilidad : <b>" + Math.Round(data.Porcentaje, 2) + "%</b>";
                //lblPor.CssClass = "txtNegroBold";
                img.ImageUrl = Ima;

                if (dropProceso.SelectedValue == "1")
                {
                    img.NavigateUrl = "DetallePro.aspx?proaq=" + Seguridad.EncriptarString(data.IdProceso.ToString());
                }

                td.CssClass = "BordeForm";
                td.Controls.Add(lbl);
                td.Controls.Add(new Literal { Text = "<br/>" });
                td.Controls.Add(img);
                td.Controls.Add(lblPor);
                td.Controls.Add(new Literal { Text = "<br/><br/>" });
                td.Controls.Add(TBP);

                switch (cnt)
                {
                    case 1:
                        cFilas++;
                        TableRow tr = new TableRow();
                        tblProcesos.Rows.Add(tr);
                        tr.Cells.Add(td);
                        break;
                    case 3:
                        cnt = 0;
                        TableRow tr2 = tblProcesos.Rows[cFilas-1];
                        tr2.Cells.Add(td);
                        break;
                    default:
                        TableRow tr3 = tblProcesos.Rows[cFilas-1];
                        tr3.Cells.Add(td);
                        break;
                }


            }

            if (listado.Count() == 0)
            {
                TableRow tr = new TableRow();
                TableCell td = new TableCell();
                td.Text = "No se registran Procesos <b>" + dropProceso.SelectedItem.ToString() + ".</b>";
                td.CssClass = "btnForm";
                tr.Controls.Add(td);
                tblProcesos.Controls.Add(tr);
            }
            else
            {
                lblLeyenda.Text = "Estado de procesos para el día : " + listado.First().Fecha + ".";
            }

        }


        protected void Cargamenu()
        {
            IList<vEstadisticas> listado1 = _procesos_menu.GroupBy(test => test.Area).Select(grp => grp.Last()).ToList();

            listado1.Add(new vEstadisticas() { IdArea = 0, Area = "Todos" });

            GridView1.DataSource = listado1;
            GridView1.DataBind();

        }


        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int valorcomando = int.Parse(e.CommandName.ToString());
            Idarea = Convert.ToString(valorcomando);

            CargaResumen(Idarea);
            Cargamenu();
        }

        #region ViewState: Manejo de variables estables en view State
        protected override object SaveViewState()
        {
            object[] SavedState = new object[18];

            SavedState[0] = base.SaveViewState();

            if (Idarea != null) SavedState[1] = Idarea;

            return SavedState;
        }

        protected override void LoadViewState(object SavedState)
        {
            if (SavedState != null)
            {
                object[] state = (object[])SavedState;

                if (state[0] != null) base.LoadViewState(state[0]);
                if (state[1] != null) Idarea = state[1].ToString();
            }
        }
        #endregion

        protected void Actu_Tick(object sender, EventArgs e)
        {
            CargaResumen(Idarea);
            Cargamenu();
        }


    }
}