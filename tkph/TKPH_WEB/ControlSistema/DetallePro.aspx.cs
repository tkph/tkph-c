﻿using Com.Vmica.View.ControlSistema;
using TKPH_BUSINESS.BUSINESS_PAGE.ControlSistema;
using TKPH_BUSINESS.COMUN;
using TKPH_WEB.Comun;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;

namespace EMPS_WEB.WebPage.Reports.ControlSistema
{
    public partial class DetallePro : System.Web.UI.Page
    {

        bControlSistema crtlSis = bControlSistema.Instance;

        public static IList<vEstadisticas> _procesos;
        public static IList<vDetallePro> _EjecExito;
        public static IList<vDetallePro> _EjecError;

        public static String idpro = "";

        protected void Page_Load(object sender, EventArgs e)
        {

                HttpContext.Current.Session["Titulo"] = "Detalle Proceso";
                HttpContext.Current.Session["SubTitulo"] = "";

                if (!IsPostBack)
                {
                    if (Request.QueryString["proaq"] == null)
                    {
                        Response.Redirect("Menu.aspx");
                    }

                    idpro = Seguridad.DesEncriptarString(Request.QueryString["proaq"]);

                    CargarDatos();

                }

 
        }


        public void CargarDatos()
        {
            _procesos = crtlSis.GetEstadisticas(idpro, 1, "0");
            HttpContext.Current.Session["Titulo"] = "Detalle Proceso " + _procesos.First().Proceso.ToString();
            _procesos = _procesos.OrderBy(f => f.Fecha).ToList();
            lblProce.Text = _procesos.First().Proceso.ToString();
            lblPorc.Text = "Disponibilidad (Hoy) : " + Math.Round(_procesos.Last().Porcentaje, 2).ToString() + "%";
            //lblPorc.CssClass = "tar_" + _procesos.First().Estado;

            String Ima = "";

            switch (_procesos.First().Estado)
            {
                case 1:
                    Ima = "~/StyleSheet/Icon/formularios/proR.png";
                    break;
                case 2:
                    Ima = "~/StyleSheet/Icon/formularios/proA.png";
                    break;
                case 3:
                    Ima = "~/StyleSheet/Icon/formularios/proV.png";
                    break;
                case 4:
                    Ima = "~/StyleSheet/Icon/formularios/proD.png";
                    break;
            }

            imgKPI.ImageUrl = Ima;

            #region Graficos

            grafRegistros.Series.Clear();
            grafVersus.Series.Clear();

            Series _slin1 = Comun.CreateSerie("Cargados", SeriesChartType.Line);
            Comun.CreatePoint(_procesos, _slin1);
            grafRegistros.Series.Add(_slin1);


            Series _slin2 = Comun.CreateSerie("Exitoso", SeriesChartType.Column);
            Comun.CreatePoint(_procesos, _slin2, 1);
            grafVersus.Series.Add(_slin2);
            grafVersus.Series[0].Color = Color.Green;

            Series _slin3 = Comun.CreateSerie("Erroneo", SeriesChartType.Column);
            Comun.CreatePoint(_procesos, _slin3, 2);
            grafVersus.Series.Add(_slin3);
            grafVersus.Series[1].Color = Color.Red;


            grafRegistros.ChartAreas[0].AxisX.MajorGrid.LineColor = Color.WhiteSmoke;
            grafRegistros.ChartAreas[0].AxisY.MajorGrid.LineColor = Color.WhiteSmoke;
            grafRegistros.ChartAreas[0].AxisY.Title = "Q Registros";
            grafRegistros.ChartAreas[0].AxisX.Interval = 1;
            grafRegistros.ChartAreas[0].AxisX.LabelStyle.Angle = -45;

            grafVersus.ChartAreas[0].AxisX.MajorGrid.LineColor = Color.WhiteSmoke;
            grafVersus.ChartAreas[0].AxisY.MajorGrid.LineColor = Color.WhiteSmoke;
            grafVersus.ChartAreas[0].AxisY.Title = "Ejecuciones";
            grafVersus.ChartAreas[0].AxisX.Interval = 1;
            grafVersus.ChartAreas[0].AxisX.LabelStyle.Angle = -45;



            #endregion

            _EjecExito = crtlSis.GetProceso(1, idpro);
            _EjecError = crtlSis.GetProceso(2, idpro);

            lblFuente.Text = "<b>Fuente:</b> " + _EjecExito.First().FuenteEntrada;
            lblSalida.Text = "<b>Salida:</b> " + _EjecExito.First().FuenteSalida;
            lblPeriodicidad.Text = "<b>Periodicidad:</b> " + _EjecExito.First().IntervaloAct + " " + _EjecExito.First().PeriodicidadAct;
            lblGerencia.Text = "<b>Gerencia:</b> " + _EjecExito.First().RespArea + " - " + _EjecExito.First().DescArea;

            lblErrSem.Text = _EjecError.Count() + " Errores de Ejecución";

            gridErrores.DataSource = _EjecError;
            gridErrores.DataBind();

        }


    }
}