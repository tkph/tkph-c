﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Menu.aspx.cs" Inherits="TKPH_WEB.WebPage.Reports.ControlSistema.Menu" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="BodyC" runat="server">
        <asp:Timer runat="server" ID="Actu" Interval="600000" Enabled="true" OnTick="Actu_Tick">
        </asp:Timer>
    <br />
    <table cellspaccing="0" cellpadding="0" border="0" width="98%">
        <tr>
            <td class="alineado_up_centro" width="180">
                <div class="BordeForm">
                    <asp:Label runat="server" Text="Gerencia" CssClass="txtNegroBold"></asp:Label>
                    <br />
                    <br />
                    <asp:UpdatePanel runat="server" ID="updatemenu" UpdateMode="Conditional">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="GridView1" EventName="RowCommand" />
                        </Triggers>
                        <ContentTemplate>
                            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" Width="180" ShowHeader="false" OnRowCommand="GridView1_RowCommand" GridLines="None" CssClass="alineado_down_izq">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="Boton" runat="server" CommandName='<%# DataBinder.Eval(Container, "DataItem.IdArea") %>' CssClass="txtNegroBold" Text="<li>"><%# DataBinder.Eval(Container, "DataItem.Area") %></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <br />
                    <br />
                </div>
            </td>
            <td width="*">
                <div class="BordeForm">
                    <br />
                    <div class="alineado_centro_izq">
                        &nbsp; &nbsp; &nbsp; 
            <asp:Label ID="Label1" runat="server" Text="Tipo Proceso: "></asp:Label>
                        <asp:DropDownList ID="dropProceso" runat="server" AutoPostBack="true" OnSelectedIndexChanged="dropProceso_SelectedIndexChanged" CssClass="InputForm">
                            <asp:ListItem Value="1">ETL&#39;s</asp:ListItem>
                            <asp:ListItem Value="2">Reporteria</asp:ListItem>
                        </asp:DropDownList>
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
            <asp:Label runat="server" ID="lblLeyenda" Text="" CssClass="txtAzulBold"></asp:Label>
                    </div>
                    <br />
                    <asp:UpdateProgress ID="UpdateProgress" runat="server">
                        <ProgressTemplate>
                            <asp:Panel runat="server">
                                <img src="../../../StyleSheet/Image/Cargando.GIF" width="16" height="16" />
                                <asp:Label runat="server" Text="Cargando Datos..."></asp:Label>
                            </asp:Panel>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="dropProceso" EventName="SelectedIndexChanged" />
                            <asp:AsyncPostBackTrigger ControlID="GridView1" EventName="RowCommand" />
                            <asp:AsyncPostBackTrigger ControlID="Actu" EventName="Tick" />            
                        </Triggers>
                        <ContentTemplate>

                            <div runat="server" id="divProcesos" style="overflow: scroll; overflow-x: hidden; height: 400px;">
                                <asp:Table runat="server" ID="tblProcesos" HorizontalAlign="Center" Width="100%">
                                </asp:Table>
                                <br />
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                </div>
                <div class="alineado_centro_der" style="width: 100%;">
                    <asp:Image runat="server" ImageUrl="~/StyleSheet/Icon/formularios/proOk.png" />
                    <asp:Label runat="server" Text=" = Ejecuciones Exitosas." CssClass="txtAzulBold" />
                    &nbsp; &nbsp; &nbsp; 
                        <asp:Image runat="server" ImageUrl="~/StyleSheet/Icon/formularios/proErr.png" />
                    <asp:Label runat="server" Text=" = Ejecuciones Erroneas." CssClass="txtAzulBold" />
                    &nbsp; &nbsp; &nbsp; 
                        <asp:Image runat="server" ImageUrl="~/StyleSheet/Icon/formularios/proCar.png" />
                    <asp:Label runat="server" Text=" = N° Registros Cargados." CssClass="txtAzulBold" />
                </div>

            </td>
        </tr>

    </table>
</asp:Content>
