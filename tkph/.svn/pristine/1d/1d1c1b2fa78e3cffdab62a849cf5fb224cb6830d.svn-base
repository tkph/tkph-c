﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using TKPH_DATA;
using TKPH_DATA.ACCESS;
using TKPH_DATA.DTO.Camiones;

namespace TKPH_DATA.SP_SQL.Camiones
{
    /// <summary>
    /// Clase con metodos para llenado de listas con resultados de consultas a SQL para despliege de Datos Disponibilidad Camión 
    /// </summary>
    public class Disponibilidad
    {
        /// <summary>
        /// Metodo que realiza llamada a Stored Procedure con parametros para listar datos de Disponibilidad Camión 
        /// </summary>
        /// <returns>Retorna Interfaz de Lista tipo "DisponibilidadDTO" con datos a deplegar.</returns>
        public static IList<DisponibilidadDTO> DisponibilidadCamionGround()
        {
            Acceso _con = new Acceso();

            IList<DisponibilidadDTO> _l = new List<DisponibilidadDTO>();

            try
            {
                DataTable _t = new DataTable();

                _con.CreateCommandSelect("exec " + NombreSP.Nombre_SP(10).ToString(), _t);

                _l = EntityList(_t);
            }
            catch (Exception e)
            {
                throw e;
            }

            return _l;
        }


        /// Metodo que realiza asignación del valores desde la consulta a la Interfaz de Lista
        /// </summary>
        /// <param name="table">Objeto tipo DataTable que contiene el resultado de la consulta SQL</param>
        /// <returns>Retorna objeto de tipo "DisponibilidadDTO"</returns>
        /// <exception cref="System.Exception">ERR.t01: Convert to Entity DTO Disponibilidad Camion</exception>
        private static IList<DisponibilidadDTO> EntityList(DataTable table)
        {
            IList<DisponibilidadDTO> _l = new List<DisponibilidadDTO>();

            try
            {
                foreach (DataRow row in table.Rows)
                {
                    DisponibilidadDTO _r = new DisponibilidadDTO()
                    {
                        Fecha = row["Fecha"].ToString(),
                        Turno = row["Turno"].ToString(),
                        Flota = row["Flota"].ToString(),
                        Operativo = Convert.ToDouble(row["Camiones_Operativos"].ToString()),
                        Armado = Convert.ToDouble(row["Camiones_Armados"].ToString()),
                        Porcentaje = Convert.ToDouble(row["porc"].ToString())
                    };

                    _l.Add(_r);
                }
            }
            catch (Exception e)
            {
                throw new Exception("ERR.t01: Convert to Entity DTO Disponibilidad Camion", e.InnerException);
            }

            return _l;
        }
    }
}
