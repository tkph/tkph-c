﻿using Com.Vmica.View.BI;
using Com.Vmica.View.BI.Export;
using TKPH_BUSINESS.BUSINESS_PAGE.BI;
using TKPH_BUSINESS.COMUN;
using TKPH_WEB.Comun;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;

namespace TKPH_WEB.BI
{
    /// <summary>
    /// Tablero control Carga de la Flota.
    /// </summary>
    public partial class DashboardCarga : System.Web.UI.Page
    {
        // Llamada a instancia de Objeto "bCargaGraficos"
        bCargaGraficos GraficosCarga = bCargaGraficos.Instance;

        // Llamada a instancia de Objeto "bCargaExport"
        bCargaExport ExportCarga = bCargaExport.Instance;

        IList<vCargaFlota> ListaFlota = new List<vCargaFlota>();
        IList<vCargaTendencia> ListaTendencia = new List<vCargaTendencia>();
        IList<vCargaHistograma> ListaHistograma = new List<vCargaHistograma>();
        IList<vCargaRanking> ListaRanking = new List<vCargaRanking>();

        IList<vCargaHistogramaExp> ExpHistograma = new List<vCargaHistogramaExp>();
        IList<vCargaTendenciaExp> ExpTendencia = new List<vCargaTendenciaExp>();
        IList<vCargaCamionExp> ExpCamion = new List<vCargaCamionExp>();
        IList<vCargaPalaExp> ExpPala = new List<vCargaPalaExp>();


        public String Flota = ""; 
        public Int64 Turno = 3;                 // Por defecto 3: Todos los Turnos
        public String FIni = DateTime.Now.AddDays(-DateTime.Now.Day + 1).ToString("dd-mm-yyyy");
        public String FHas = DateTime.Now.AddDays(-DateTime.Now.Day + 1).AddMonths(1).AddDays(-1).ToString("dd-mm-yyyy");

        /// <summary>
        /// Metodo de carga Inicial de la Página
        /// </summary>
        /// <param name="sender">La fuente del evento.</param>
        /// <param name="e">La Instancia que contiene los datos del evento.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                HttpContext.Current.Session["Titulo"] = "Dashboard Carga";

                FIni = DateTime.Today.AddDays(-14).ToString("dd-MM-yyyy");
                FHas = DateTime.Today.ToString("dd-MM-yyyy");

                txtFDesde.Text = FIni.ToString() ;
                txtFHasta.Text = FHas.ToString();

                CargaListas();


                Flota = ListaFlota.First().FlotaCamion.ToString();

                CargaGraficos();
                
            }

        }


        /// <summary>
        /// Metodo que inserta datos en interfaces de lista para desplegar datos en la GUI. 
        /// </summary>
         public void CargaListas()
        {
            ListaFlota = GraficosCarga.getCargaFlota(1, Turno, FIni, FHas,"");
            ListaTendencia = GraficosCarga.getCargaTendencia(2, Turno, FIni, FHas,"");
            ListaHistograma = GraficosCarga.getCargaHistograma(3, Turno, FIni, FHas, "");
            ListaRanking = GraficosCarga.getCargaRanking(4, Turno, FIni, FHas, "");
          
        }

         /// <summary>
         /// Metodo que invoca los metodos para generacion de Graficos en la GUI. 
         /// </summary>
        public void CargaGraficos()
        {
            GrafFlota();
            GrafHistograma();
            GrafTendencia();
            GrafRanking();
        }


        /// <summary>
        /// Metodo de generacion de graficos historico de Flota. 
        /// </summary>
        public void GrafFlota()
        {
            Series s1 = Comun.Comun.CreateSerie("", SeriesChartType.Column);

            Comun.Comun.CreatePoint(ListaFlota, s1, Flota);

            gCarga.Series.Add(s1);

            Comun.Comun.FormatoGraficos(gCarga, -45, 0, "%", "", "Promedio en Relación a Carga Nominal", false);

            gCarga.Series[0].PostBackValue = "#AXISLABEL";

           int tp= gCarga.Series[0].Points.Count();

           StripLine Lcien = new StripLine();
           Lcien.StripWidth = 0;
           Lcien.BorderColor = Color.FromArgb(216, 66, 55);
           Lcien.BorderWidth = 1;
           Lcien.Interval = 99.9;

           gCarga.ChartAreas[0].AxisY.StripLines.Add(Lcien);

        }

        /// <summary>
        /// Metodo de generacion de graficos Histograma. 
        /// </summary>
        public void GrafHistograma()
        {
            Series s2 = Comun.Comun.CreateSerie("", SeriesChartType.Column);

            Comun.Comun.CreatePoint(ListaHistograma.Where(f => f.FlotaCamion.Equals(Flota)).ToList(), s2);

            gHistograma.Series.Add(s2);

            Comun.Comun.FormatoGraficos(gHistograma, -45, 0, "Cant. Eventos", "", "Histograma distribución de Cargas", false);

            gHistograma.Series[0]["PointWidth"] = "1";
            gHistograma.Series[0].BorderColor = Color.White;
            gHistograma.Series[0].BorderWidth = 1;

            gHistograma.Series[0].PostBackValue = "#AXISLABEL";
            }


        /// <summary>
        /// Metodo de generacion de graficos de Tendencia. 
        /// </summary>
        public void GrafTendencia()
        {
            Series t1 = Comun.Comun.CreateSerie("Bajo Carga", SeriesChartType.Line);
            Series t2 = Comun.Comun.CreateSerie("Sobre Carga", SeriesChartType.Line);
            Series t3 = Comun.Comun.CreateSerie("Optimo", SeriesChartType.Line);

            Comun.Comun.CreatePoint(ListaTendencia.Where(f => f.FlotaCamion.Equals(Flota)).Where(t => t.Tipo.Equals("BAJO_CARGA")).ToList(), t1);
            Comun.Comun.CreatePoint(ListaTendencia.Where(f => f.FlotaCamion.Equals(Flota)).Where(t => t.Tipo.Equals("SOBRE_CARGA")).ToList(), t2);
            Comun.Comun.CreatePoint(ListaTendencia.Where(f => f.FlotaCamion.Equals(Flota)).Where(t => t.Tipo.Equals("CARGA_OPTIMA")).ToList(), t3);

            gTendencia.Series.Add(t1);
            gTendencia.Series.Add(t2);
            gTendencia.Series.Add(t3);

            Comun.Comun.FormatoGraficos(gTendencia, -45, 0, "Cant. Eventos", "", "Tendencias de Carga", true);

            gTendencia.Series[0].Color = Comun.Comun.Colores(12, 255);
            gTendencia.Series[1].Color = Comun.Comun.Colores(11, 255);
            gTendencia.Series[2].Color = Comun.Comun.Colores(13, 255);

            gTendencia.Series[0].PostBackValue = "#AXISLABEL" + "%BAJO";
            gTendencia.Series[1].PostBackValue = "#AXISLABEL" + "%SOBRE";
            gTendencia.Series[2].PostBackValue = "#AXISLABEL" + "%OPTIMO";

        }

        /// <summary>
        /// Metodo de generacion de grafico Ranking. 
        /// </summary>
        public void GrafRanking()
        {
            Series r1 = Comun.Comun.CreateSerie("Pala", SeriesChartType.Bar);
            Series r2 = Comun.Comun.CreateSerie("Camión", SeriesChartType.Bar);
            Series r3 = Comun.Comun.CreateSerie("Camión", SeriesChartType.Bar);

            Comun.Comun.CreatePoint(ListaRanking.Where(f => f.Flota.Equals(Flota)).Where(t => t.Tipo.Equals("RANKING OPE.PALA")).Take(10).OrderBy(v => v.Valor).ToList(), r1,false);
            Comun.Comun.CreatePoint(ListaRanking.Where(f => f.Flota.Equals(Flota)).Where(t => t.Tipo.Equals("RANKING CAMION")).Take(10).OrderBy(v => v.Valor).ToList(), r2,false);
            Comun.Comun.CreatePoint(ListaRanking.Where(f => f.Flota.Equals(Flota)).Where(t => t.Tipo.Equals("RANKING CAMION PORC")).Take(10).OrderBy(v => v.Valor).ToList(), r3,true);
            
            gRPala.Series.Add(r1);
            gRCamion.Series.Add(r2);
            gROperador.Series.Add(r3);
          
            Comun.Comun.FormatoGraficos(gRPala, 0, 0, "", "Pala", "Cantidad Eventos Sobre Carga", false);
            Comun.Comun.FormatoGraficos(gRCamion, 0, 0, "", "Camión", "Cantidad Eventos Sobre Carga", false);
            Comun.Comun.FormatoGraficos(gROperador, 0, 100, "", "Camión", "% Sobre Carga Nominal", false);

            gRPala.Series[0]["PointWidth"] = "0.6";
            gRCamion.Series[0]["PointWidth"] = "0.6";
            gROperador.Series[0]["PointWidth"] = "0.6";

            gRPala.Series[0]["BarLabelStyle"] = gRCamion.Series[0]["BarLabelStyle"] = gROperador.Series[0]["BarLabelStyle"] = "Left";
            gRPala.Series[0].LabelBackColor = gRCamion.Series[0].LabelBackColor = gROperador.Series[0].LabelBackColor = Color.FromArgb(255, 30, 30, 30);
            
            Font fo = new Font("Calibri", 7.5f, FontStyle.Regular);
            r1.Font = r2.Font = r3.Font = fo;


            gRPala.Series[0].PostBackValue = "#AXISLABEL";
            gRCamion.Series[0].PostBackValue = "#AXISLABEL";
            gROperador.Series[0].PostBackValue = "#AXISLABEL";


        }

        /// <summary>
        /// Evento Click del Botón Filtrar para aplicar filtro según parametros ingresados.
        /// </summary>
        /// <param name="sender">La fuente del evento.</param>
        /// <param name="e">La Instancia que contiene los datos del evento.</param>
        protected void btnFiltro_Click(object sender, EventArgs e)
        {
            //Flota = cboFlota.SelectedValue.ToString();
            Turno = Int64.Parse(cboTurno.SelectedValue);

            FIni = txtFDesde.Text.ToString();
            FHas = txtFHasta.Text.ToString();

            CargaListas();
            CargaGraficos();
        }


        /// <summary>
        /// Evento Click del Grafico de Carga para aplicar filtro según Flota seleccionada.
        /// </summary>
        /// <param name="sender">La fuente del evento.</param>
        /// <param name="e">La Instancia que contiene los datos del evento.</param>
        protected void gCarga_Click(object sender, ImageMapEventArgs e)
        {       
            Flota = e.PostBackValue.ToString();
            Turno = Int64.Parse(cboTurno.SelectedValue);

            FIni = txtFDesde.Text.ToString();
            FHas = txtFHasta.Text.ToString();

            CargaGraficos();
        }

       
        protected override void LoadViewState(object SavedState)
        {
            if (SavedState != null)
            {
                object[] state = (object[])SavedState;

                if (state[0] != null) base.LoadViewState(state[0]);
                if (state[1] != null) ListaFlota = Serializador.DesSerializeObjeto<vCargaFlota[]>((string)state[1]).ToList();
                if (state[2] != null) ListaTendencia = Serializador.DesSerializeObjeto<vCargaTendencia[]>((string)state[2]).ToList();
                if (state[3] != null) ListaHistograma = Serializador.DesSerializeObjeto<vCargaHistograma[]>((string)state[3]).ToList();
                if (state[4] != null) ListaRanking = Serializador.DesSerializeObjeto<vCargaRanking[]>((string)state[4]).ToList();
                if (state[5] != null) Flota = state[5].ToString();
               
            }
        }

        protected override object SaveViewState()
        {
            object[] SavedState = new object[18];
            SavedState[0] = base.SaveViewState();

            if (ListaFlota != null) SavedState[1] = Serializador.SerializeObjeto<vCargaFlota[]>(ListaFlota.ToArray());
            if (ListaTendencia != null) SavedState[2] = Serializador.SerializeObjeto<vCargaTendencia[]>(ListaTendencia.ToArray());
            if (ListaHistograma != null) SavedState[3] = Serializador.SerializeObjeto<vCargaHistograma[]>(ListaHistograma.ToArray());
            if (ListaRanking != null) SavedState[4] = Serializador.SerializeObjeto<vCargaRanking[]>(ListaRanking.ToArray());
            if (Flota != null) SavedState[5] = Flota.ToString();

            return SavedState;
        }

        /* Sin implementar no se han realizado pruebas
         *
        protected void gHistograma_PostPaint(object sender, ChartPaintEventArgs e)
        {
            ChartArea a = sender as ChartArea;
            if (a != null)
            {
                Graphics g = e.ChartGraphics.Graphics;
                RectangleF r = e.ChartGraphics.GetAbsoluteRectangle(new RectangleF(1500, 10, 10, 50)); // the rect you need
                g.FillRectangle(Brushes.Red, r);
            }
        }
        */


        /// <summary>
        /// Evento Click del Grafico Histograma para desplegar Popup de seleccion de rango a Exportar.
        /// </summary>
        /// <param name="sender">La fuente del evento.</param>
        /// <param name="e">La Instancia que contiene los datos del evento.</param>
        protected void gHistograma_Click(object sender, ImageMapEventArgs e)
        {
            String[] Valor = e.PostBackValue.Split('%');
            MostrarExp("Histograma", Valor[0].ToString(),"");
        }

        /// <summary>
        /// Evento Click del Grafico de tendencia para desplegar Popup de seleccion de rango a Exportar.
        /// </summary>
        /// <param name="sender">La fuente del evento.</param>
        /// <param name="e">La Instancia que contiene los datos del evento.</param>
        protected void gTendencia_Click(object sender, ImageMapEventArgs e)
        {
            String[] Valor = e.PostBackValue.Split('%');
            MostrarExp("Tendencia", Valor[0].ToString(), Valor[1].ToString());
        }


        /// <summary>
        /// Evento Click del Grafico Ranking Palas para desplegar Popup de seleccion de rango a Exportar.
        /// </summary>
        /// <param name="sender">La fuente del evento.</param>
        /// <param name="e">La Instancia que contiene los datos del evento.</param>
        protected void gRPala_Click(object sender, ImageMapEventArgs e)
        {
            MostrarExp("Ranking Pala", e.PostBackValue.ToString(),"");
        }


        /// <summary>
        /// Evento Click del Grafico Ranking Camiones para desplegar Popup de seleccion de rango a Exportar.
        /// </summary>
        /// <param name="sender">La fuente del evento.</param>
        /// <param name="e">La Instancia que contiene los datos del evento.</param>
        protected void gRCamion_Click(object sender, ImageMapEventArgs e)
        {
            MostrarExp("Ranking Camión", e.PostBackValue.ToString(),"");
        }

        /// <summary>
        /// Evento Click del Grafico Ranking Operador para desplegar Popup de seleccion de rango a Exportar.
        /// </summary>
        /// <param name="sender">La fuente del evento.</param>
        /// <param name="e">La Instancia que contiene los datos del evento.</param>
        protected void gROperador_Click(object sender, ImageMapEventArgs e)
        {
            MostrarExp("Ranking Operador", e.PostBackValue.ToString(),"");
        }


        /// <summary>
        /// Metodo que captura los parametros de origen para exportar datos a Excel.
        /// </summary>
        /// <param name="Grafico">Titulo de grafico a Mostrar.</param>
        /// <param name="ValorS">Valor para exportar datos.</param>
        /// <param name="Valor2">Valor por Defecto.</param>
        public void MostrarExp(String Grafico, String ValorS, String Valor2)
        {
            rblOpciones.Items.Clear();

            lblGraf.Text = Grafico;
            String va=(Valor2.ToString()=="")?ValorS.ToString():ValorS.ToString()+"%"+Valor2.ToString();
            rblOpciones.Items.Add(new ListItem("Valor Seleccionado (" + ValorS.ToString() + ")",va));
            rblOpciones.Items.Add(new ListItem("Toda la Serie", Valor2.ToString()));
            rblOpciones.SelectedIndex = 0;
            MenPOP.Show();
        }
        
        /// <summary>
        /// Metodo que realiza acción para exportar Grilla a Archivo Excel.
        /// </summary>
        protected void ExportarExcel()
        {
            String Tit = lblGraf.Text;

            String Seleccion = rblOpciones.SelectedValue.ToString();

            FIni = txtFDesde.Text.ToString();
            FHas = txtFHasta.Text.ToString();

            switch (Tit)
            {
                case "Histograma": 
                    if (Seleccion != "")
                    {
                        if (Seleccion == "120+") { Seleccion = "130"; }
                        Seleccion = (Int32.Parse(Seleccion) / 10).ToString();
                    }

                    ExpHistograma = ExportCarga.getCargaHistogramaData(8, Turno, FIni, FHas, Seleccion);
                    gvData.DataSource = ExpHistograma.Where(f => f.FlotaCamion.Equals(Flota)).ToList();
                    break;
                case "Tendencia":
                    String[] t = Seleccion.Split('%');
                    Int64 ops = 0;

                    if (t.Count() > 1)
                    {
                        DateTime fe = DateTime.Parse(t[0].ToString());
                        Seleccion = fe.ToString("yyyy-MM-dd");
                        switch (t[1].ToString())
                        {
                            case "BAJO": ops = 6; break;
                            case "SOBRE": ops = 5; break;
                            case "OPTIMO": ops = 7; break;
                        }
                        Tit = Tit + " " + t[1].ToString() + " CARGA";
                    }
                    else
                    {
                        Seleccion = "";
                        switch (t[0].ToString())
                        {
                            case "BAJO": ops = 6; break;
                            case "SOBRE": ops = 5; break;
                            case "OPTIMO": ops = 7; break;
                        }
                        Tit = Tit + " " + t[0].ToString() + " CARGA";
                    }

                    ExpTendencia = ExportCarga.getCargaTendenciaData(ops, Turno, FIni, FHas, Seleccion);
                    gvData.DataSource = ExpTendencia.Where(f => f.FlotaCamion.Equals(Flota)).ToList();
                    break;
                case "Ranking Pala":
                    ExpPala = ExportCarga.getCargaPalaData(9, Turno, FIni, FHas, Seleccion);
                    gvData.DataSource = ExpPala.Where(f => f.Flota.Equals(Flota)).ToList();
                    break;
                case "Ranking Camión":
                     ExpCamion = ExportCarga.getCargaCamionData(10, Turno, FIni, FHas, Seleccion);
                     gvData.DataSource = ExpCamion.Where(f => f.Flota.Equals(Flota)).ToList();
                    break;
                case "Ranking Operador":
                    ExpCamion = ExportCarga.getCargaCamionData(10, Turno, FIni, FHas, Seleccion);
                     gvData.DataSource = ExpCamion.Where(f => f.Flota.Equals(Flota)).ToList();
                    break;
            }


            gvData.DataBind();

            gvData.Visible = true;

            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename=Data_Carga_" + Tit + ".xls");
            Response.Charset = "UFT-8";
            this.EnableViewState = false;
            // Si se desea que el archivo no se guarde y solo se muestre comentar la linea siguiente.
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "application/vnd.ms-excel";
            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
            gvData.RenderControl(htmlWrite);
            Response.Write(stringWrite.ToString());
            Response.End();

            gvData.Visible = false;

        }

        /// <summary>
        /// Confirms that an <see cref="T:System.Web.UI.HtmlControls.HtmlForm" /> control is rendered for the specified ASP.NET server control at run time.
        /// </summary>
        /// <param name="control">The ASP.NET server control that is required in the <see cref="T:System.Web.UI.HtmlControls.HtmlForm" /> control.</param>
        public override void VerifyRenderingInServerForm(System.Web.UI.Control control)
        {
            /* Verifies that the control is rendered */
        }


        /// <summary>
        /// Evento Click del Botón Exportar.
        /// </summary>
        /// <param name="sender">La fuente del evento.</param>
        /// <param name="e">La Instancia que contiene los datos del evento.</param>
        protected void btnExp_Click(object sender, EventArgs e)
        {
            ExportarExcel();
            MenPOP.Hide();
        }


    }


}