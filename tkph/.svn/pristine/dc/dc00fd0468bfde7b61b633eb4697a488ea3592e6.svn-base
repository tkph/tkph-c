﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using Com.Vmica.View;
using Com.Vmica.Wrapper;
using System.Drawing;
using TKPH_DATA;
using TKPH_DATA.DTO.Camiones;

namespace TKPH_BUSINESS.BUSINESS_PAGE
{
    /// <summary>
    /// Clase con metodos y definiciones para obtener datos asociados al Camión en Pantalla Detalle Camión
    /// </summary>
    public class bReporteThree
    {
        /// <summary>
        /// Llamada a instancia clase "Orquestador" que administra las llamadas desde otros objetos.
        /// </summary>
        Orquestador _data = Orquestador.Instance;

        /// <summary>
        /// Encapsulamiento de instancia para clase "bReporteThree"
        /// </summary>
        #region Encapsula Instancia
        private const string CLASS_NAME = "bReporteThree";

        private static readonly bReporteThree instance = new bReporteThree();
        private bReporteThree() { }
        public static bReporteThree Instance
        {
            get { return instance; }
        }
        #endregion

        #region Metodos       
        /// <summary>
        /// Metodo para obtener los datos asociados al Camión consultado.
        /// </summary>
        /// <param name="StatusRequestSingle">Objeto "vStatusRequest" que contiene la información del Camión.</param>
        /// <returns>Retorna Objeto tipo "vCamionSingle" que contiene el detalle del Camión</returns>
        public vCamionSingle GetDataSingle(vStatusRequest StatusRequestSingle)
        {
            vCamionSingle _l = new vCamionSingle();

            try
            {
                CamionSingleDTO _Response = _data.SingleByStatus(wStatusRequest.ConvertData(StatusRequestSingle));

                _l = wCamionSingle.ConvertData(_Response);
            }
            catch (Exception e)
            {
                throw e;
            }

            return _l;
        }

        /// <summary>
        /// Metodo para obtener el color asociado a la rueda del camion segun su alarma.
        /// </summary>
        /// <param name="Data">Objeto que contiene los datos del camión.</param>
        /// <param name="Rueda">Variable que contiene la rueda consultada.</param>
        /// <returns>Retorna el valor del color correspondiente a los parametros ingresados.</returns>
        public String GetColorRueda(vCamionSingle Data, Int64 Rueda)
        {
            String ColorRueda = String.Empty;

            switch (Rueda)
            {
                case 1:
                    ColorRueda = AlertaColor(Convert.ToInt64(Data.AlertaRueda_1));
                    break;
                case 2:
                    ColorRueda = AlertaColor(Convert.ToInt64(Data.AlertaRueda_2));
                    break;
                case 3:
                    ColorRueda = AlertaColor(Convert.ToInt64(Data.AlertaRueda_3));
                    break;
                case 4:
                    ColorRueda = AlertaColor(Convert.ToInt64(Data.AlertaRueda_4));
                    break;
                case 5:
                    ColorRueda = AlertaColor(Convert.ToInt64(Data.AlertaRueda_5));
                    break;
                case 6:
                    ColorRueda = AlertaColor(Convert.ToInt64(Data.AlertaRueda_6));
                    break;
            }

            return ColorRueda;
        }

        /// <summary>
        /// Metodo para obtener la imagen asociada a la rueda del camion segun su alarma.
        /// </summary>
        /// <param name="Alerta">Valor correspondiente a la alarma de la rueda.</param>
        /// <returns>Retorna la ruta de la ubicacion del archivo de imagen correspondiente.</returns>
        private String AlertaColor(Int64 Alerta)
        {
            String _color = "~/Imagenes/Rueda_Gris.png";

            switch (Alerta)
            {
                case 1:
                    _color = "~/Imagenes/Rueda_Roja.png";
                    break;
                case 2:
                    _color = "~/Imagenes/Rueda_Amarilla.png";
                    break;
                case 3:
                    _color = "~/Imagenes/Rueda_Verde.png";
                    break;
            }

            return _color;
        }


        /// <summary>
        /// Metodo para obtener la imagen asociada a la cabina del camion segun su alarma.
        /// </summary>
        /// <param name="Alerta">Valor correspondiente a la alarma de la Cabina.</param>
        /// <returns>Retorna la ruta de la ubicacion del archivo de imagen correspondiente.</returns>
        public String ColorCabina(Int64 Alerta)
        {
            String _colorTo = "~/Imagenes/Eje_DT_V_T.png";

            switch (Alerta)
            {
                case 1:
                    _colorTo = "~/Imagenes/Eje_DT_R_T.png";
                    break;
                case 2:
                    _colorTo = "~/Imagenes/Eje_DT_A_T.png";
                    break;
                case 3:
                    _colorTo = "~/Imagenes/Eje_DT_V_T.png";
                    break;
            }

            return _colorTo;
        }


        /// <summary>
        /// Metodo para obtener la imagen asociada al eje del camion segun los parametros ingresados.
        /// </summary>
        /// <param name="Alerta">Valor correspondiente a la alarma del Eje.</param>
        /// <param name="Eje">Valor correspondiente a la ubicacion del eje (D: Delantero, T: Trasero).</param>
        /// <param name="ColCam">Valor correspondiente a la ubicacion del eje para efectos de aplicación de estilos CSS.</param>
        /// <returns>Retorna la ruta de la ubicacion del archivo de imagen correspondiente. </returns>
        public String ColorEje(Int64 Alerta, String Eje, Int64 ColCam)
        {

            String CssEje = null;
            String CCam = (ColCam == 0) ? "_T" : "_C";

            if (Eje == "D")
            {
                switch (Alerta)
                {
                    case 1: CssEje = "eje_D_r" + CCam; break;
                    case 2: CssEje = "eje_D_a" + CCam; break;
                    case 3: CssEje = "eje_D_v" + CCam; break;
                    default: CssEje = "camion_frente" + CCam; break;
                }
            }
            else 
            {
                switch (Alerta)
                {
                    case 1: CssEje = "eje_T_r"; break;
                    case 2: CssEje = "eje_T_a"; break;
                    case 3: CssEje = "eje_T_v"; break;
                    default: CssEje = "camion_atras" + CCam; break;
                }
            }

            return CssEje;
        }


        /// <summary>
        /// Metodo para obtener la imagen del camión dependiendo de la posicion del visualización (Delantera o Trasera).
        /// </summary>
        /// <param name="Alerta">Valor correspondiente a la alarma general del Camión.</param>
        /// <param name="ColCam">Valor correspondiente a la visualizacion de la posicion del camión. (0: Trasera, 1: Delantera).</param>
        /// <returns></returns>
        public String ColorCamionA(Int64 Alerta, Int64 ColCam)
        {
            String CssEje = null;
            String CCam = (ColCam == 0) ? "_T" : "_C";

            switch (Alerta)
            {
                case 1: CssEje = "camion_atras_rojo" + CCam; break;
                case 2: CssEje = "camion_atras_amarillo" + CCam; break;
                case 3: CssEje = "camion_atras_verde" + CCam; break;
                default: CssEje = "camion_atras" + CCam; break;
            }

            return CssEje;
        }

        /// <summary>
        /// Metodo que obtiene la Temperatura de una rueda en especifico.
        /// </summary>
        /// <param name="Data">Objeto que contiene los datos del camión.</param>
        /// <param name="Rueda">Variable que contiene la rueda consultada.</param>
        /// <returns>Retorna el valor de la temperatura correspondiente a los parametros ingresados.</returns>
        public String GetTemperatura(vCamionSingle Data, Int64 Rueda)
        {
            String TempRueda = String.Empty;

            switch (Rueda)
            {
                case 1:
                    TempRueda = Data.TemperaturaCarga_1;
                    break;
                case 2:
                    TempRueda = Data.TemperaturaCarga_2;
                    break;
                case 3:
                    TempRueda = Data.TemperaturaCarga_3;
                    break;
                case 4:
                    TempRueda = Data.TemperaturaCarga_4;
                    break;
                case 5:
                    TempRueda = Data.TemperaturaCarga_5;
                    break;
                case 6:
                    TempRueda = Data.TemperaturaCarga_6;
                    break;
            }

            return TempRueda;
        }

        /// <summary>
        /// Metodo que obtiene la Presión de una rueda en especifico.
        /// </summary>
        /// <param name="Data">Objeto que contiene los datos del camión.</param>
        /// <param name="Rueda">Variable que contiene la rueda consultada.</param>
        /// <returns>Retorna el valor de la Presión correspondiente a los parametros ingresados.</returns>
        public string GetPresion(vCamionSingle Data, Int64 Rueda)
        {
            String PresionRueda = String.Empty;

            switch (Rueda)
            {
                case 1:
                    PresionRueda = Data.Presion_1;
                    break;
                case 2:
                    PresionRueda = Data.Presion_2;
                    break;
                case 3:
                    PresionRueda = Data.Presion_3;
                    break;
                case 4:
                    PresionRueda = Data.Presion_4;
                    break;
                case 5:
                    PresionRueda = Data.Presion_5;
                    break;
                case 6:
                    PresionRueda = Data.Presion_6;
                    break;
            }

            return PresionRueda;
        }

        /// <summary>
        /// Metodo que obtiene el color del texto estandarizado según alarma.
        /// </summary>
        /// <param name="Status">Valor correspondiente a la alarma o estado.</param>
        /// <returns>Retorna Color correspondiente a los parametros ingresados. </returns>
        public Color GetColorText(String Status)
        {
            Int32 ind = Convert.ToInt32(Status);

            switch (ind)
            {
                case 1:
                    return Color.FromArgb(255,255,37,37);
                case 2:
                    return Color.FromArgb(255, 255, 216, 0);
                case 3:
                    return Color.FromArgb(255,122,185,0);
                default:
                    return Color.FromArgb(255,255,255,255);
            }
        }

        /// <summary>
        /// Metodo que obtiene las fechas maximas de información para el camión.
        /// </summary>
        /// <param name="Camion">Valor correspondiente al identificador de Camión.</param>
        /// <returns>Retorna un objeto tipo "vFechaMaxima" con la fecha minima y maxima.</returns>
        public vFechaMaxima GetFechasMaximas(String Camion)
        {
            vFechaMaxima _resp;

            try
            {
                vFechaMaxima dta = new vFechaMaxima() { IdCamion = Camion };

                _resp = wFechaMaxima.ConvertData(_data.GetFechas(wFechaMaxima.ConvertData(dta)));
            }
            catch (Exception e)
            {
                throw e;
            }

            return _resp;
        }

        /// <summary>
        /// Metodo para obtener los datos especificos del camión.
        /// </summary>
        /// <param name="IdCamion">Valor correspondiente al identificador de Camión.</param>
        /// <param name="flota">Valor correspondiente al identificador de Camión.</param>
        /// <returns>Retorna un objeto tipo "vCamionDatos" con los datos del camión.</returns>
        public vCamionDatos GetCamionDato(String IdCamion, String flota)
        {
            CamionDatosDTO _res;

            try
            {
                vCamionDatos _d = new vCamionDatos()
                {
                    IdCamion = IdCamion,
                    Flota = flota
                };

                _res = _data.GetCamionDato(wCamionDatos.ConvertData(_d));
            }
            catch (Exception e)
            {
                throw e;
            }

            return wCamionDatos.ConvertData(_res);
        }
        #endregion
    }
}
