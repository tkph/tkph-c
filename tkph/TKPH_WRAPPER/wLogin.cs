﻿using Com.Vmica.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TKPH_DATA.DTO;

namespace Com.Vmica.Wrapper
{
    /// <summary>
    /// Clase con metodos para transformación y transporte de objetos para datos de Login a la aplicación entre la capa View y DTO
    /// </summary>
    public class wLogin
    {
        /// <summary>
        /// Metodo de conversion de objeto "LoginDTO" a "vLogin".
        /// </summary>
        /// <param name="Objeto">Objeto de tipo "LoginDTO".</param>
        /// <returns>Retorna Objeto de tipo "vLogin".</returns>
        public static vLogin ConvertData(LoginDTO Objeto)
        {
            vLogin _list = new vLogin();

            _list.Nombre = Objeto.Nombre;
            _list.Usuario = Objeto.Usuario;
            _list.Vigente = Objeto.Vigente;


            return _list;
        }


        /// <summary>
        /// Metodo de conversion de objeto "vLogin" a "LoginDTO".
        /// </summary>
        /// <param name="Objeto">Objeto de tipo "vLogin".</param>
        /// <returns>Retorna Objeto de tipo "LoginDTO".</returns>
        public static LoginDTO ConvertData(vLogin Objeto)
        {
            LoginDTO _list = new LoginDTO();

            _list.Nombre = Objeto.Nombre;
            _list.Usuario = Objeto.Usuario;
            _list.Vigente = Objeto.Vigente;

            return _list;

        }
    }
}
