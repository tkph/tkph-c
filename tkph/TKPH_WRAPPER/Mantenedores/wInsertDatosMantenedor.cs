﻿using Com.Vmica.View.Mantenedores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TKPH_DATA.DTO.Mantenedores;

namespace Com.Vmica.Wrapper.Mantenedores
{
    /// <summary>
    /// Clase con metodos para transformación y transporte de objetos para persistencia de datos de Mantendores entre la capa View y DTO
    /// </summary>
    public class wInsertDatosMantenedor
    {
        /// <summary>
        /// Metodo de conversion de objeto "InsertDatosMantenedorDTO" a "vInsertDatosMantenedor".
        /// </summary>
        /// <param name="Data">Objeto de tipo "InsertDatosMantenedorDTO".</param>
        /// <returns>Retorna Objeto de tipo "vInsertDatosMantenedor".</returns>
        public static vInsertDatosMantenedor ConvertData(InsertDatosMantenedorDTO Data)
        {
            return new vInsertDatosMantenedor
            {
                Ident = Data.Ident,
                ValorT = Data.ValorT,
                Afisc = Data.Afisc
            };
        }


        /// <summary>
        /// Metodo de conversion de objeto "vInsertDatosMantenedor" a "InsertDatosMantenedorDTO".
        /// </summary>
        /// <param name="Data">Objeto de tipo "vInsertDatosMantenedor".</param>
        /// <returns>Retorna Objeto de tipo "InsertDatosMantenedorDTO".</returns>
        public static InsertDatosMantenedorDTO ConvertData(vInsertDatosMantenedor Data)
        {
            return new InsertDatosMantenedorDTO
            {
                Ident = Data.Ident,
                ValorT = Data.ValorT,
                Afisc = Data.Afisc
            };
        }

        /// <summary>
        /// Metodo de conversion de objeto "InsertDatosMantenedorDTO" a "vInsertDatosMantenedor".
        /// </summary>
        /// <param name="Data">Interfaz de Lista de tipo "InsertDatosMantenedorDTO".</param>
        /// <returns>Retorna Interfaz de Lista de tipo "vInsertDatosMantenedor".</returns>
        public static IList<vInsertDatosMantenedor> ConvertData(IList<InsertDatosMantenedorDTO> Data)
        {
            IList<vInsertDatosMantenedor> _l = new List<vInsertDatosMantenedor>();

            try
            {
                foreach (InsertDatosMantenedorDTO r in Data)
                {
                    vInsertDatosMantenedor _r = new vInsertDatosMantenedor()
                    {
                        Ident = r.Ident,
                        ValorT = r.ValorT,
                        Afisc = r.Afisc
                    };

                    _l.Add(_r);
                }
            }
            catch
            {
                throw new Exception("Err.W01: Problemas para convertir Datos [wInsertDatosMantenedor]");
            }

            return _l;

        }


        /// <summary>
        /// Metodo de conversion de objeto "vInsertDatosMantenedor" a "InsertDatosMantenedorDTO".
        /// </summary>
        /// <param name="Data">Interfaz de Lista de tipo "vInsertDatosMantenedor".</param>
        /// <returns>Retorna Interfaz de Lista de tipo "InsertDatosMantenedorDTO".</returns>
        public static IList<InsertDatosMantenedorDTO> ConvertData(IList<vInsertDatosMantenedor> Data)
        {
            IList<InsertDatosMantenedorDTO> _l = new List<InsertDatosMantenedorDTO>();

            try
            {
                foreach (vInsertDatosMantenedor r in Data)
                {
                    InsertDatosMantenedorDTO _r = new InsertDatosMantenedorDTO()
                    {
                        Ident = r.Ident,
                        ValorT = r.ValorT,
                        Afisc = r.Afisc
                    };

                    _l.Add(_r);
                }
            }
            catch
            {
                throw new Exception("Err.W01: Problemas para convertir Datos [wInsertDatosMantenedor]");
            }

            return _l;

        }


    }
}
