﻿using Com.Vmica.View.Mantenedores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TKPH_DATA.DTO.Mantenedores;

namespace Com.Vmica.Wrapper.Mantenedores
{
    /// <summary>
    /// Clase con metodos para transformación y transporte de objetos para persistencia de datos de Mantendores de Carga entre la capa View y DTO
    /// </summary>
    public class wMantenedorCarga
    {
        /// <summary>
        /// Metodo de conversion de objeto "MantenedorCargaDTO" a "vMantenedorCarga".
        /// </summary>
        /// <param name="Data">Objeto de tipo "MantenedorCargaDTO".</param>
        /// <returns>Retorna Objeto de tipo "vMantenedorCarga".</returns>
        public static vMantenedorCarga ConvertData(MantenedorCargaDTO Data)
        {
            return new vMantenedorCarga
            {
                Mes = Data.Mes,
                Dia = Data.Dia,
                Item = Data.Item,
                Tipo = Data.Tipo,
                Nivel_1 = Data.Nivel_1,
                Nivel_2 = Data.Nivel_2,
                Nivel_3 = Data.Nivel_3,
                valor = Data.valor,
                AFiscal = Data.AFiscal,
                Mes_pl = Data.Mes_pl,
                Id_tipo = Data.Id_tipo,
                Id_nivel_1 = Data.Id_nivel_1,
                Unidad = Data.Unidad,
                Period = Data.Period

            };
        }


        /// <summary>
        /// Metodo de conversion de objeto "vMantenedorCarga" a "MantenedorCargaDTO".
        /// </summary>
        /// <param name="Data">Objeto de tipo "vMantenedorCarga".</param>
        /// <returns>Retorna Objeto de tipo "MantenedorCargaDTO".</returns>
        public static MantenedorCargaDTO ConvertData(vMantenedorCarga Data)
        {
            return new MantenedorCargaDTO
            {
                Mes = Data.Mes,
                Dia = Data.Dia,
                Item = Data.Item,
                Tipo = Data.Tipo,
                Nivel_1 = Data.Nivel_1,
                Nivel_2 = Data.Nivel_2,
                Nivel_3 = Data.Nivel_3,
                valor = Data.valor,
                AFiscal = Data.AFiscal,
                Mes_pl = Data.Mes_pl,
                Id_tipo = Data.Id_tipo,
                Id_nivel_1 = Data.Id_nivel_1,
                Unidad = Data.Unidad,
                Period = Data.Period

            };
        }


        /// <summary>
        /// Metodo de conversion de objeto "MantenedorCargaDTO" a "vMantenedorCarga".
        /// </summary>
        /// <param name="Data">Interfaz de Lista de tipo "MantenedorCargaDTO".</param>
        /// <returns>Retorna Interfaz de Lista de tipo "vMantenedorCarga".</returns>
        public static IList<vMantenedorCarga> ConvertData(IList<MantenedorCargaDTO> Data)
        {
            IList<vMantenedorCarga> _l = new List<vMantenedorCarga>();

            try
            {
                foreach (MantenedorCargaDTO r in Data)
                {
                    vMantenedorCarga _r = new vMantenedorCarga()
                    {
                        Mes = r.Mes,
                        Dia = r.Dia,
                        Item = r.Item,
                        Tipo = r.Tipo,
                        Nivel_1 = r.Nivel_1,
                        Nivel_2 = r.Nivel_2,
                        Nivel_3 = r.Nivel_3,
                        valor = r.valor,
                        AFiscal = r.AFiscal,
                        Mes_pl = r.Mes_pl,
                        Id_tipo = r.Id_tipo,
                        Id_nivel_1 = r.Id_nivel_1,
                        Unidad = r.Unidad,
                        Period = r.Period
                    };

                    _l.Add(_r);
                }
            }
            catch
            {
                throw new Exception("Err.W01: Problemas para convertir Datos [wMantenedorCarga]");
            }

            return _l;

        }


        /// <summary>
        /// Metodo de conversion de objeto "vMantenedorCarga" a "MantenedorCargaDTO".
        /// </summary>
        /// <param name="Data">Interfaz de Lista de tipo "vMantenedorCarga".</param>
        /// <returns>Retorna Interfaz de Lista de tipo "MantenedorCargaDTO".</returns>
        public static IList<MantenedorCargaDTO> ConvertData(IList<vMantenedorCarga> Data)
        {
            IList<MantenedorCargaDTO> _l = new List<MantenedorCargaDTO>();

            try
            {
                foreach (vMantenedorCarga r in Data)
                {
                    MantenedorCargaDTO _r = new MantenedorCargaDTO()
                    {
                        Mes = r.Mes,
                        Dia = r.Dia,
                        Item = r.Item,
                        Tipo = r.Tipo,
                        Nivel_1 = r.Nivel_1,
                        Nivel_2 = r.Nivel_2,
                        Nivel_3 = r.Nivel_3,
                        valor = r.valor,
                        AFiscal = r.AFiscal,
                        Mes_pl = r.Mes_pl,
                        Id_tipo = r.Id_tipo,
                        Id_nivel_1 = r.Id_nivel_1,
                        Unidad = r.Unidad,
                        Period = r.Period
                    };

                    _l.Add(_r);
                }
            }
            catch
            {
                throw new Exception("Err.W01: Problemas para convertir Datos [wMantenedorCarga]");
            }

            return _l;

        }

    }
}
