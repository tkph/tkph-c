﻿using Com.Vmica.View.BI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TKPH_DATA.DTO.BI;

namespace Com.Vmica.Wrapper.BI
{
    /// <summary>
    /// Clase con metodos para transformación y transporte de objetos para datos de TKPH Flota entre la capa View y DTO
    /// </summary>
    public class wTkphFlota
    {

        /// <summary>
        /// Metodo de conversion de objeto "TkphFlotaDTO" a "vTkphFlota".
        /// </summary>
        /// <param name="Lista">Interfaz de Lista de tipo "TkphFlotaDTO".</param>
        /// <returns>Retorna Interfaz de Lista de tipo "vTkphFlota".</returns>
        public static IList<vTkphFlota> ConvertData(IList<TkphFlotaDTO> Lista)
        {
            IList<vTkphFlota> _list = new List<vTkphFlota>();

            foreach (TkphFlotaDTO row in Lista)
            {
                vTkphFlota _row = new vTkphFlota()
                {
                    FlotaCamion = row.FlotaCamion,
                    Promedio = row.Promedio,
                    TkphNominal = row.TkphNominal,
                    Porcentaje = row.Porcentaje,
                    AlarmaPunto = row.AlarmaPunto
                };

                _list.Add(_row);
            }

            return _list;
        }


        /// <summary>
        /// Metodo de conversion de objeto "vTkphFlota" a "TkphFlotaDTO".
        /// </summary>
        /// <param name="Lista">Interfaz de Lista de tipo "vTkphFlota".</param>
        /// <returns>Retorna Interfaz de Lista de tipo "TkphFlotaDTO".</returns>
        public static IList<TkphFlotaDTO> ConvertData(IList<vTkphFlota> Lista)
        {
            IList<TkphFlotaDTO> _list = new List<TkphFlotaDTO>();

            foreach (vTkphFlota row in Lista)
            {
                TkphFlotaDTO _row = new TkphFlotaDTO()
                {
                    FlotaCamion = row.FlotaCamion,
                    Promedio = row.Promedio,
                    TkphNominal = row.TkphNominal,
                    Porcentaje = row.Porcentaje,
                    AlarmaPunto = row.AlarmaPunto
                };

                _list.Add(_row);
            }

            return _list;
        }
    }
}
