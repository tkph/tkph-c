﻿using Com.Vmica.View.BI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TKPH_DATA.DTO.BI;

namespace Com.Vmica.Wrapper.BI
{
    /// <summary>
    /// Clase con metodos para transformación y transporte de objetos para datos de Carga Tendencia entre la capa View y DTO
    /// </summary>
    public class wCargaTendencia
    {

        /// <summary>
        /// Metodo de conversion de objeto "CargaTendenciaDTO" a "vCargaTendencia".
        /// </summary>
        /// <param name="Lista">Interfaz de Lista de tipo "CargaTendenciaDTO".</param>
        /// <returns>Retorna Interfaz de Lista de tipo "vCargaTendencia".</returns>
        public static IList<vCargaTendencia> ConvertData(IList<CargaTendenciaDTO> Lista)
        {
            IList<vCargaTendencia> _list = new List<vCargaTendencia>();

            foreach (CargaTendenciaDTO row in Lista)
            {
                vCargaTendencia _row = new vCargaTendencia()
                {
                    FlotaCamion = row.FlotaCamion,
                    Fecha = row.Fecha,
                    Valor = row.Valor,
                    Tipo = row.Tipo
                };

                _list.Add(_row);
            }

            return _list;
        }


        /// <summary>
        /// Metodo de conversion de objeto "vCargaTendencia" a "CargaTendenciaDTO".
        /// </summary>
        /// <param name="Lista">Interfaz de Lista de tipo "vCargaTendencia".</param>
        /// <returns>Retorna Interfaz de Lista de tipo "CargaTendenciaDTO".</returns>
        public static IList<CargaTendenciaDTO> ConvertData(IList<vCargaTendencia> Lista)
        {
            IList<CargaTendenciaDTO> _list = new List<CargaTendenciaDTO>();

            foreach (vCargaTendencia row in Lista)
            {
                CargaTendenciaDTO _row = new CargaTendenciaDTO()
                {
                    FlotaCamion = row.FlotaCamion,
                    Fecha = row.Fecha,
                    Valor = row.Valor,
                    Tipo = row.Tipo
                };

                _list.Add(_row);
            }

            return _list;
        }
    }
}
