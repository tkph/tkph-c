﻿using Com.Vmica.View.BI.Export;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TKPH_DATA.DTO.BI.Export;

namespace Com.Vmica.Wrapper.BI.Export
{
    /// <summary>
    /// Clase con metodos para transformación y transporte de objetos para Exportar datos de Tkph Camión entre la capa View y DTO
    /// </summary>
    public class wTkphCamionExp
    {
        /// <summary>
        /// Metodo de conversion de objeto "TkphCamionExpDTO" a "vTkphCamionExp".
        /// </summary>
        /// <param name="Lista">Interfaz de Lista de tipo "TkphCamionExpDTO".</param>
        /// <returns>Retorna Interfaz de Lista de tipo "vTkphCamionExp".</returns>
        public static IList<vTkphCamionExp> ConvertData(IList<TkphCamionExpDTO> Lista)
        {
            IList<vTkphCamionExp> _list = new List<vTkphCamionExp>();

            foreach (TkphCamionExpDTO row in Lista)
            {
                vTkphCamionExp _row = new vTkphCamionExp()
                {
                    Fecha = row.Fecha,
                    FechaCarguio = row.FechaCarguio,
                    Flota = row.Flota,
                    IdCamion = row.IdCamion,
                    Carga = row.Carga,
                    CargaMedia = row.CargaMedia,
                    TiempoTotal = row.TiempoTotal,
                    ConstanteK2 = row.ConstanteK2,
                    VelocidadMedia = row.VelocidadMedia,
                    Temperatura = row.Temperatura,
                    Operacional = row.Operacional,
                    Nominal = row.Nominal                       
                };

                _list.Add(_row);
            }

            return _list;
        }


        /// <summary>
        /// Metodo de conversion de objeto "vTkphCamionExp" a "TkphCamionExpDTO".
        /// </summary>
        /// <param name="Lista">Interfaz de Lista de tipo "vTkphCamionExp".</param>
        /// <returns>Retorna Interfaz de Lista de tipo "TkphCamionExpDTO".</returns>
        public static IList<TkphCamionExpDTO> ConvertData(IList<vTkphCamionExp> Lista)
        {
            IList<TkphCamionExpDTO> _list = new List<TkphCamionExpDTO>();

            foreach (vTkphCamionExp row in Lista)
            {
                TkphCamionExpDTO _row = new TkphCamionExpDTO()
                {
                    Fecha = row.Fecha,
                    FechaCarguio = row.FechaCarguio,
                    Flota = row.Flota,
                    IdCamion = row.IdCamion,
                    Carga = row.Carga,
                    CargaMedia = row.CargaMedia,
                    TiempoTotal = row.TiempoTotal,
                    ConstanteK2 = row.ConstanteK2,
                    VelocidadMedia = row.VelocidadMedia,
                    Temperatura = row.Temperatura,
                    Operacional = row.Operacional,
                    Nominal = row.Nominal
                };

                _list.Add(_row);
            }

            return _list;
        }
    }
}
