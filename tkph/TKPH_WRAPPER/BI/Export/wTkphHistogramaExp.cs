﻿using Com.Vmica.View.BI;
using Com.Vmica.View.BI.Export;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TKPH_DATA.DTO.BI.Export;

namespace Com.Vmica.Wrapper.BI.Export
{
    /// <summary>
    /// Clase con metodos para transformación y transporte de objetos para Exportar datos de Tkph Histograma entre la capa View y DTO
    /// </summary>
    public class wTkphHistogramaExp
    {
        /// <summary>
        /// Metodo de conversion de objeto "TkphHistogramaExpDTO" a "vTkphHistogramaExp".
        /// </summary>
        /// <param name="Lista">Interfaz de Lista de tipo "TkphHistogramaExpDTO".</param>
        /// <returns>Retorna Interfaz de Lista de tipo "vTkphHistogramaExp".</returns>
        public static IList<vTkphHistogramaExp> ConvertData(IList<TkphHistogramaExpDTO> Lista)
        {
            IList<vTkphHistogramaExp> _list = new List<vTkphHistogramaExp>();

            foreach (TkphHistogramaExpDTO row in Lista)
            {
                vTkphHistogramaExp _row = new vTkphHistogramaExp()
                {
                    IdCamion = row.IdCamion,
                    Flota = row.Flota,
                    Fecha = row.Fecha,
                    Operacional = row.Operacional,
                    Nominal = row.Nominal
                };

                _list.Add(_row);
            }

            return _list;
        }


        /// <summary>
        /// Metodo de conversion de objeto "vTkphHistogramaExp" a "TkphHistogramaExpDTO".
        /// </summary>
        /// <param name="Lista">Interfaz de Lista de tipo "vTkphHistogramaExp".</param>
        /// <returns>Retorna Interfaz de Lista de tipo "TkphHistogramaExpDTO".</returns>
        public static IList<TkphHistogramaExpDTO> ConvertData(IList<vTkphHistogramaExp> Lista)
        {
            IList<TkphHistogramaExpDTO> _list = new List<TkphHistogramaExpDTO>();

            foreach (vTkphHistogramaExp row in Lista)
            {
                TkphHistogramaExpDTO _row = new TkphHistogramaExpDTO()
                {
                    IdCamion = row.IdCamion,
                    Flota = row.Flota,
                    Fecha = row.Fecha,
                    Operacional = row.Operacional,
                    Nominal = row.Nominal
                };

                _list.Add(_row);
            }

            return _list;
        }
    }
}
