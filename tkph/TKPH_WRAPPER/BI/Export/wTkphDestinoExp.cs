﻿using Com.Vmica.View.BI.Export;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TKPH_DATA.DTO.BI.Export;

namespace Com.Vmica.Wrapper.BI.Export
{
    /// <summary>
    /// Clase con metodos para transformación y transporte de objetos para Exportar datos de Tkph Destino entre la capa View y DTO
    /// </summary>
    public class wTkphDestinoExp
    {
        /// <summary>
        /// Metodo de conversion de objeto "TkphDestinoExpDTO" a "vTkphDestinoExp".
        /// </summary>
        /// <param name="Lista">Interfaz de Lista de tipo "TkphDestinoExpDTO".</param>
        /// <returns>Retorna Interfaz de Lista de tipo "vTkphDestinoExp".</returns>
        public static IList<vTkphDestinoExp> ConvertData(IList<TkphDestinoExpDTO> Lista)
        {
            IList<vTkphDestinoExp> _list = new List<vTkphDestinoExp>();

            foreach (TkphDestinoExpDTO row in Lista)
            {
                vTkphDestinoExp _row = new vTkphDestinoExp()
                {
                    Fecha = row.Fecha,
                    Flota = row.Flota,
                    IdDestino = row.IdDestino,
                    Operacional = row.Operacional,
                    Nominal = row.Nominal
                };

                _list.Add(_row);
            }

            return _list;
        }


        /// <summary>
        /// Metodo de conversion de objeto "vTkphDestinoExp" a "TkphDestinoExpDTO".
        /// </summary>
        /// <param name="Lista">Interfaz de Lista de tipo "vTkphDestinoExp".</param>
        /// <returns>Retorna Interfaz de Lista de tipo "TkphDestinoExpDTO".</returns>
        public static IList<TkphDestinoExpDTO> ConvertData(IList<vTkphDestinoExp> Lista)
        {
            IList<TkphDestinoExpDTO> _list = new List<TkphDestinoExpDTO>();

            foreach (vTkphDestinoExp row in Lista)
            {
                TkphDestinoExpDTO _row = new TkphDestinoExpDTO()
                {
                    Fecha = row.Fecha,
                    Flota = row.Flota,
                    IdDestino = row.IdDestino,
                    Operacional = row.Operacional,
                    Nominal = row.Nominal
                };

                _list.Add(_row);
            }

            return _list;
        }
    }
}
