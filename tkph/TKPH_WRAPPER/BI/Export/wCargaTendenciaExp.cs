﻿using Com.Vmica.View.BI.Export;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TKPH_DATA.DTO.BI.Export;

namespace Com.Vmica.Wrapper.BI.Export
{
    /// <summary>
    /// Clase con metodos para transformación y transporte de objetos para Exportar datos de Carga Tendencia entre la capa View y DTO
    /// </summary>
    public class wCargaTendenciaExp
    {
        /// <summary>
        /// Metodo de conversion de objeto "CargaTendenciaExpDTO" a "vCargaTendenciaExp".
        /// </summary>
        /// <param name="Lista">Interfaz de Lista de tipo "CargaTendenciaExpDTO".</param>
        /// <returns>Retorna Interfaz de Lista de tipo "vCargaTendenciaExp".</returns>
        public static IList<vCargaTendenciaExp> ConvertData(IList<CargaTendenciaExpDTO> Lista)
        {
            IList<vCargaTendenciaExp> _list = new List<vCargaTendenciaExp>();

            foreach (CargaTendenciaExpDTO row in Lista)
            {
                vCargaTendenciaExp _row = new vCargaTendenciaExp()
                {
                    IdCamion = row.IdCamion,
                    FlotaCamion = row.FlotaCamion,
                    Fecha = row.Fecha,
                    Carga = row.Carga,
                    Nominal = row.Nominal
                };

                _list.Add(_row);
            }

            return _list;
        }


        /// <summary>
        /// Metodo de conversion de objeto "vCargaTendenciaExp" a "CargaTendenciaExpDTO".
        /// </summary>
        /// <param name="Lista">Interfaz de Lista de tipo "vCargaTendenciaExp".</param>
        /// <returns>Retorna Interfaz de Lista de tipo "CargaTendenciaExpDTO".</returns>
        public static IList<CargaTendenciaExpDTO> ConvertData(IList<vCargaTendenciaExp> Lista)
        {
            IList<CargaTendenciaExpDTO> _list = new List<CargaTendenciaExpDTO>();

            foreach (vCargaTendenciaExp row in Lista)
            {
                CargaTendenciaExpDTO _row = new CargaTendenciaExpDTO()
                {
                    IdCamion = row.IdCamion,
                    FlotaCamion = row.FlotaCamion,
                    Fecha = row.Fecha,
                    Carga = row.Carga,
                    Nominal = row.Nominal
                };

                _list.Add(_row);
            }

            return _list;
        }
    }
}
