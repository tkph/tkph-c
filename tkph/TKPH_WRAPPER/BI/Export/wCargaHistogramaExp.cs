﻿using Com.Vmica.View.BI.Export;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TKPH_DATA.DTO.BI.Export;

namespace Com.Vmica.Wrapper.BI.Export
{
    /// <summary>
    /// Clase con metodos para transformación y transporte de objetos para Exportar datos de Carga Histograma entre la capa View y DTO
    /// </summary>
    public class wCargaHistogramaExp
    {
        /// <summary>
        /// Metodo de conversion de objeto "CargaHistogramaExpDTO" a "vCargaHistogramaExp".
        /// </summary>
        /// <param name="Lista">Interfaz de Lista de tipo "CargaHistogramaExpDTO".</param>
        /// <returns>Retorna Interfaz de Lista de tipo "vCargaHistogramaExp".</returns>
        public static IList<vCargaHistogramaExp> ConvertData(IList<CargaHistogramaExpDTO> Lista)
        {
            IList<vCargaHistogramaExp> _list = new List<vCargaHistogramaExp>();

            foreach (CargaHistogramaExpDTO row in Lista)
            {
                vCargaHistogramaExp _row = new vCargaHistogramaExp()
                {
                    IdCamion = row.IdCamion,
                     FlotaCamion = row.FlotaCamion,
                    Fecha = row.Fecha,
                    Carga = row.Carga,
                    Nominal = row.Nominal
                };

                _list.Add(_row);
            }

            return _list;
        }


        /// <summary>
        /// Metodo de conversion de objeto "vCargaHistogramaExp" a "CargaHistogramaExpDTO".
        /// </summary>
        /// <param name="Lista">Interfaz de Lista de tipo "vCargaHistogramaExp".</param>
        /// <returns>Retorna Interfaz de Lista de tipo "CargaHistogramaExpDTO".</returns>
        public static IList<CargaHistogramaExpDTO> ConvertData(IList<vCargaHistogramaExp> Lista)
        {
            IList<CargaHistogramaExpDTO> _list = new List<CargaHistogramaExpDTO>();

            foreach (vCargaHistogramaExp row in Lista)
            {
                CargaHistogramaExpDTO _row = new CargaHistogramaExpDTO()
                {
                    IdCamion = row.IdCamion,
                    FlotaCamion = row.FlotaCamion,
                    Fecha = row.Fecha,
                    Carga = row.Carga,
                    Nominal = row.Nominal
                };

                _list.Add(_row);
            }

            return _list;
        }
    }
}
