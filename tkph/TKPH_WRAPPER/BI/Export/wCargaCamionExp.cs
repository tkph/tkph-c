﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Com.Vmica.View.BI.Export;
using TKPH_DATA.DTO.BI.Export;

namespace Com.Vmica.Wrapper.BI.Export
{
    /// <summary>
    /// Clase con metodos para transformación y transporte de objetos para Exportar datos de Carga Camión entre la capa View y DTO
    /// </summary>
    public class wCargaCamionExp
    {

        /// <summary>
        /// Metodo de conversion de objeto "CargaCamionExpDTO" a "vCargaCamionExp".
        /// </summary>
        /// <param name="Lista">Interfaz de Lista de tipo "CargaCamionExpDTO".</param>
        /// <returns>Retorna Interfaz de Lista de tipo "vCargaCamionExp".</returns>
        public static IList<vCargaCamionExp> ConvertData(IList<CargaCamionExpDTO> Lista)
        {
            IList<vCargaCamionExp> _list = new List<vCargaCamionExp>();

            foreach (CargaCamionExpDTO row in Lista)
            {
                vCargaCamionExp _row = new vCargaCamionExp()
                {
                    Fecha = row.Fecha,
                    IdCamion = row.IdCamion,
                    Carga = row.Carga,
                    Nominal = row.Nominal,
                    Flota = row.Flota
                };

                _list.Add(_row);
            }

            return _list;
        }


        /// <summary>
        /// Metodo de conversion de objeto "vCargaCamionExp" a "CargaCamionExpDTO".
        /// </summary>
        /// <param name="Lista">Interfaz de Lista de tipo "vCargaCamionExp".</param>
        /// <returns>Retorna Interfaz de Lista de tipo "CargaCamionExpDTO".</returns>
        public static IList<CargaCamionExpDTO> ConvertData(IList<vCargaCamionExp> Lista)
        {
            IList<CargaCamionExpDTO> _list = new List<CargaCamionExpDTO>();

            foreach (vCargaCamionExp row in Lista)
            {
                CargaCamionExpDTO _row = new CargaCamionExpDTO()
                {
                    Fecha = row.Fecha,
                    IdCamion = row.IdCamion,
                    Carga = row.Carga,
                    Nominal = row.Nominal,
                    Flota = row.Flota
                };

                _list.Add(_row);
            }

            return _list;
        }
    }
}
