﻿using Com.Vmica.View.BI.Export;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TKPH_DATA.DTO.BI.Export;

namespace Com.Vmica.Wrapper.BI.Export
{
    /// <summary>
    /// Clase con metodos para transformación y transporte de objetos para Exportar datos de Carga Pala entre la capa View y DTO
    /// </summary>
    public class wCargaPalaExp
    {
        /// <summary>
        /// Metodo de conversion de objeto "CargaPalaExpDTO" a "vCargaPalaExp".
        /// </summary>
        /// <param name="Lista">Interfaz de Lista de tipo "CargaPalaExpDTO".</param>
        /// <returns>Retorna Interfaz de Lista de tipo "vCargaPalaExp".</returns>
        public static IList<vCargaPalaExp> ConvertData(IList<CargaPalaExpDTO> Lista)
        {
            IList<vCargaPalaExp> _list = new List<vCargaPalaExp>();

            foreach (CargaPalaExpDTO row in Lista)
            {
                vCargaPalaExp _row = new vCargaPalaExp()
                {
                    Fecha = row.Fecha,
                    Pala = row.Pala,
                    Carga = row.Carga,
                    Flota = row.Flota
                };

                _list.Add(_row);
            }

            return _list;
        }

        /// <summary>
        /// Metodo de conversion de objeto "vCargaPalaExp" a "CargaPalaExpDTO".
        /// </summary>
        /// <param name="Lista">Interfaz de Lista de tipo "vCargaPalaExp".</param>
        /// <returns>Retorna Interfaz de Lista de tipo "CargaPalaExpDTO".</returns>
        public static IList<CargaPalaExpDTO> ConvertData(IList<vCargaPalaExp> Lista)
        {
            IList<CargaPalaExpDTO> _list = new List<CargaPalaExpDTO>();

            foreach (vCargaPalaExp row in Lista)
            {
                CargaPalaExpDTO _row = new CargaPalaExpDTO()
                {
                    Fecha = row.Fecha,
                    Pala = row.Pala,
                    Carga = row.Carga,
                    Flota = row.Flota
                };

                _list.Add(_row);
            }

            return _list;
        }
    }
}
