﻿using Com.Vmica.View.BI.Export;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TKPH_DATA.DTO.BI.Export;

namespace Com.Vmica.Wrapper.BI.Export
{
    /// <summary>
    /// Clase con metodos para transformación y transporte de objetos para Exportar datos de Tkph Operador entre la capa View y DTO
    /// </summary>
    public class wTkphOperadorExp
    {
        /// <summary>
        /// Metodo de conversion de objeto "TkphOperadorExpDTO" a "vTkphOperadorExp".
        /// </summary>
        /// <param name="Lista">Interfaz de Lista de tipo "TkphOperadorExpDTO".</param>
        /// <returns>Retorna Interfaz de Lista de tipo "vTkphOperadorExp".</returns>
        public static IList<vTkphOperadorExp> ConvertData(IList<TkphOperadorExpDTO> Lista)
        {
            IList<vTkphOperadorExp> _list = new List<vTkphOperadorExp>();

            foreach (TkphOperadorExpDTO row in Lista)
            {
                vTkphOperadorExp _row = new vTkphOperadorExp()
                {
                    Fecha = row.Fecha,
                    FechaCarguio = row.FechaCarguio,
                    Flota = row.Flota,
                    Operador = row.Operador,
                    Carga = row.Carga,
                    CargaMedia = row.CargaMedia,
                    TiempoTotal = row.TiempoTotal,
                    ConstanteK2 = row.ConstanteK2,
                    VelocidadMedia = row.VelocidadMedia,
                    Temperatura = row.Temperatura,
                    Operacional = row.Operacional,
                    Nominal = row.Nominal                       
                };

                _list.Add(_row);
            }

            return _list;
        }


        /// <summary>
        /// Metodo de conversion de objeto "vTkphOperadorExp" a "TkphOperadorExpDTO".
        /// </summary>
        /// <param name="Lista">Interfaz de Lista de tipo "vTkphOperadorExp".</param>
        /// <returns>Retorna Interfaz de Lista de tipo "TkphOperadorExpDTO".</returns>
        public static IList<TkphOperadorExpDTO> ConvertData(IList<vTkphOperadorExp> Lista)
        {
            IList<TkphOperadorExpDTO> _list = new List<TkphOperadorExpDTO>();

            foreach (vTkphOperadorExp row in Lista)
            {
                TkphOperadorExpDTO _row = new TkphOperadorExpDTO()
                {
                    Fecha = row.Fecha,
                    FechaCarguio = row.FechaCarguio,
                    Flota = row.Flota,
                    Operador = row.Operador,
                    Carga = row.Carga,
                    CargaMedia = row.CargaMedia,
                    TiempoTotal = row.TiempoTotal,
                    ConstanteK2 = row.ConstanteK2,
                    VelocidadMedia = row.VelocidadMedia,
                    Temperatura = row.Temperatura,
                    Operacional = row.Operacional,
                    Nominal = row.Nominal
                };

                _list.Add(_row);
            }

            return _list;
        }
    }
}
