﻿using Com.Vmica.View.BI;
using Com.Vmica.View.BI.Export;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TKPH_DATA.DTO.BI.Export;

namespace Com.Vmica.Wrapper.BI.Export
{
    /// <summary>
    /// Clase con metodos para transformación y transporte de objetos para Exportar datos de Tkph Tendencia entre la capa View y DTO
    /// </summary>
    public class wTkphTendenciaExp
    {
        /// <summary>
        /// Metodo de conversion de objeto "TkphTendenciaExpDTO" a "vTkphTendenciaExp".
        /// </summary>
        /// <param name="Lista">Interfaz de Lista de tipo "TkphTendenciaExpDTO".</param>
        /// <returns>Retorna Interfaz de Lista de tipo "vTkphTendenciaExp".</returns>
        public static IList<vTkphTendenciaExp> ConvertData(IList<TkphTendenciaExpDTO> Lista)
        {
            IList<vTkphTendenciaExp> _list = new List<vTkphTendenciaExp>();

            foreach (TkphTendenciaExpDTO row in Lista)
            {
                vTkphTendenciaExp _row = new vTkphTendenciaExp()
                {
                    IdCamion = row.IdCamion,
                    Flota = row.Flota,
                    Fecha = row.Fecha,
                    FechaCarguio = row.FechaCarguio,
                    Operacional = row.Operacional,
                    Nominal = row.Nominal
                };

                _list.Add(_row);
            }

            return _list;
        }


        /// <summary>
        /// Metodo de conversion de objeto "vTkphTendenciaExp" a "TkphTendenciaExpDTO".
        /// </summary>
        /// <param name="Lista">Interfaz de Lista de tipo "vTkphTendenciaExp".</param>
        /// <returns>Retorna Interfaz de Lista de tipo "TkphTendenciaExpDTO".</returns>
        public static IList<TkphTendenciaExpDTO> ConvertData(IList<vTkphTendenciaExp> Lista)
        {
            IList<TkphTendenciaExpDTO> _list = new List<TkphTendenciaExpDTO>();

            foreach (vTkphTendenciaExp row in Lista)
            {
                TkphTendenciaExpDTO _row = new TkphTendenciaExpDTO()
                {
                    IdCamion = row.IdCamion,
                    Flota = row.Flota,
                    Fecha = row.Fecha,
                    FechaCarguio = row.FechaCarguio,
                    Operacional = row.Operacional,
                    Nominal = row.Nominal
                };

                _list.Add(_row);
            }

            return _list;
        }
    }
}
