﻿using Com.Vmica.View.BI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TKPH_DATA.DTO.BI;

namespace Com.Vmica.Wrapper.BI
{
    /// <summary>
    /// Clase con metodos para transformación y transporte de objetos para datos de Carga Ranking entre la capa View y DTO
    /// </summary>
    public class wCargaRanking
    {
        /// <summary>
        /// Metodo de conversion de objeto "CargaRankingDTO" a "vCargaRanking".
        /// </summary>
        /// <param name="Lista">Interfaz de Lista de tipo "CargaRankingDTO".</param>
        /// <returns>Retorna Interfaz de Lista de tipo "vCargaRanking".</returns>
        public static IList<vCargaRanking> ConvertData(IList<CargaRankingDTO> Lista)
        {
            IList<vCargaRanking> _list = new List<vCargaRanking>();

            foreach (CargaRankingDTO row in Lista)
            {
                vCargaRanking _row = new vCargaRanking()
                {
                    FlotaCamion = row.FlotaCamion,
                    Valor = row.Valor,
                    Tipo = row.Tipo,
                    Flota = row.Flota,
                    AlarmaPunto = row.AlarmaPunto
                };

                _list.Add(_row);
            }

            return _list;
        }


        /// <summary>
        /// Metodo de conversion de objeto "vCargaRanking" a "CargaRankingDTO".
        /// </summary>
        /// <param name="Lista">Interfaz de Lista de tipo "vCargaRanking".</param>
        /// <returns>Retorna Interfaz de Lista de tipo "CargaRankingDTO".</returns>
        public static IList<CargaRankingDTO> ConvertData(IList<vCargaRanking> Lista)
        {
            IList<CargaRankingDTO> _list = new List<CargaRankingDTO>();

            foreach (vCargaRanking row in Lista)
            {
                CargaRankingDTO _row = new CargaRankingDTO()
                {
                    FlotaCamion = row.FlotaCamion,
                    Valor = row.Valor,
                    Tipo = row.Tipo,
                    Flota = row.Flota,
                    AlarmaPunto = row.AlarmaPunto
                };

                _list.Add(_row);
            }

            return _list;
        }
    }
}
