﻿using Com.Vmica.View.BI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TKPH_DATA.DTO.BI;

namespace Com.Vmica.Wrapper.BI
{
    /// <summary>
    /// Clase con metodos para transformación y transporte de objetos para datos de TKPH Destino entre la capa View y DTO
    /// </summary>
    public class wTkphDestino
    {

        /// <summary>
        /// Metodo de conversion de objeto "TkphDestinoDTO" a "vTkphDestino".
        /// </summary>
        /// <param name="Lista">Interfaz de Lista de tipo "TkphDestinoDTO".</param>
        /// <returns>Retorna Interfaz de Lista de tipo "vTkphDestino".</returns>
        public static IList<vTkphDestino> ConvertData(IList<TkphDestinoDTO> Lista)
        {
            IList<vTkphDestino> _list = new List<vTkphDestino>();

            foreach (TkphDestinoDTO row in Lista)
            {
                vTkphDestino _row = new vTkphDestino()
                {
                    Flota = row.Flota,
                    Destino = row.Destino,
                    Porcentaje = row.Porcentaje,
                    PromTkphOpe = row.PromTkphOpe,
                    Nominal = row.Nominal,
                    Tipo = row.Tipo,
                    AlarmaPunto=row.AlarmaPunto
                };

                _list.Add(_row);
            }

            return _list;
        }


        /// <summary>
        /// Metodo de conversion de objeto "vTkphDestino" a "TkphDestinoDTO".
        /// </summary>
        /// <param name="Lista">Interfaz de Lista de tipo "vTkphDestino".</param>
        /// <returns>Retorna Interfaz de Lista de tipo "TkphDestinoDTO".</returns>
        public static IList<TkphDestinoDTO> ConvertData(IList<vTkphDestino> Lista)
        {
            IList<TkphDestinoDTO> _list = new List<TkphDestinoDTO>();

            foreach (vTkphDestino row in Lista)
            {
                TkphDestinoDTO _row = new TkphDestinoDTO()
                {
                    Flota = row.Flota,
                    Destino = row.Destino,
                    Porcentaje = row.Porcentaje,
                    PromTkphOpe = row.PromTkphOpe,
                    Nominal = row.Nominal,
                    Tipo = row.Tipo,
                    AlarmaPunto = row.AlarmaPunto
                };

                _list.Add(_row);
            }

            return _list;
        }
    }
}
