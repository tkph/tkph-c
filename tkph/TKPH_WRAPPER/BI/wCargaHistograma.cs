﻿using Com.Vmica.View.BI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TKPH_DATA.DTO.BI;

namespace Com.Vmica.Wrapper.BI
{
    /// <summary>
    /// Clase con metodos para transformación y transporte de objetos para datos de Carga Histograma entre la capa View y DTO
    /// </summary>
    public class wCargaHistograma
    {
        /// <summary>
        /// Metodo de conversion de objeto "CargaHistogramaDTO" a "vCargaHistograma".
        /// </summary>
        /// <param name="Lista">Interfaz de Lista de tipo "CargaHistogramaDTO".</param>
        /// <returns>Retorna Interfaz de Lista de tipo "vCargaHistograma".</returns>
        public static IList<vCargaHistograma> ConvertData(IList<CargaHistogramaDTO> Lista)
        {
            IList<vCargaHistograma> _list = new List<vCargaHistograma>();

            foreach (CargaHistogramaDTO row in Lista)
            {
                vCargaHistograma _row = new vCargaHistograma()
                {
                    FlotaCamion = row.FlotaCamion,
                    Valor = row.Valor,
                    Tipo = row.Tipo,
                    Orden = row.Orden,
                    AlarmaPunto = row.AlarmaPunto
                };

                _list.Add(_row);
            }

            return _list;
        }


        /// <summary>
        /// Metodo de conversion de objeto "vCargaHistograma" a "CargaHistogramaDTO".
        /// </summary>
        /// <param name="Lista">Interfaz de Lista de tipo "vCargaHistograma".</param>
        /// <returns>Retorna Interfaz de Lista de tipo "CargaHistogramaDTO".</returns>
        public static IList<CargaHistogramaDTO> ConvertData(IList<vCargaHistograma> Lista)
        {
            IList<CargaHistogramaDTO> _list = new List<CargaHistogramaDTO>();

            foreach (vCargaHistograma row in Lista)
            {
                CargaHistogramaDTO _row = new CargaHistogramaDTO()
                {
                    FlotaCamion = row.FlotaCamion,
                    Valor = row.Valor,
                    Tipo = row.Tipo,
                    Orden = row.Orden,
                    AlarmaPunto = row.AlarmaPunto
                };

                _list.Add(_row);
            }

            return _list;
        }
    }
}
