﻿using Com.Vmica.View.BI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TKPH_DATA.DTO.BI;

namespace Com.Vmica.Wrapper.BI
{
    /// <summary>
    /// Clase con metodos para transformación y transporte de objetos para datos de TKPH Ranking entre la capa View y DTO
    /// </summary>
    public class wTkphRanking
    {

        /// <summary>
        /// Metodo de conversion de objeto "TkphRankingDTO" a "vTkphRanking".
        /// </summary>
        /// <param name="Lista">Interfaz de Lista de tipo "TkphRankingDTO".</param>
        /// <returns>Retorna Interfaz de Lista de tipo "vTkphRanking".</returns>
        public static IList<vTkphRanking> ConvertData(IList<TkphRankingDTO> Lista)
        {
            IList<vTkphRanking> _list = new List<vTkphRanking>();

            foreach (TkphRankingDTO row in Lista)
            {
                vTkphRanking _row = new vTkphRanking()
                {
                    FlotaCamion = row.FlotaCamion,
                    Objeto = row.Objeto,
                    QEvento = row.QEvento,
                    Promedio = row.Promedio,
                    Tipo = row.Tipo,
                    AlarmaPunto = row.AlarmaPunto
                };

                _list.Add(_row);
            }

            return _list;
        }


        /// <summary>
        /// Metodo de conversion de objeto "vTkphRanking" a "TkphRankingDTO".
        /// </summary>
        /// <param name="Lista">Interfaz de Lista de tipo "vTkphRanking".</param>
        /// <returns>Retorna Interfaz de Lista de tipo "TkphRankingDTO".</returns>
        public static IList<TkphRankingDTO> ConvertData(IList<vTkphRanking> Lista)
        {
            IList<TkphRankingDTO> _list = new List<TkphRankingDTO>();

            foreach (vTkphRanking row in Lista)
            {
                TkphRankingDTO _row = new TkphRankingDTO()
                {
                    FlotaCamion = row.FlotaCamion,
                    Objeto = row.Objeto,
                    QEvento = row.QEvento,
                    Promedio = row.Promedio,
                    Tipo = row.Tipo,
                    AlarmaPunto = row.AlarmaPunto
                };

                _list.Add(_row);
            }

            return _list;
        }
    }
}
