﻿using Com.Vmica.View.BI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TKPH_DATA.DTO.BI;

namespace Com.Vmica.Wrapper.BI
{
    /// <summary>
    /// Clase con metodos para transformación y transporte de objetos para datos de Carga Flota entre la capa View y DTO
    /// </summary>
    public class wCargaFlota
    {
        /// <summary>
        /// Metodo de conversion de objeto "CargaFlotaDTO" a "vCargaFlota".
        /// </summary>
        /// <param name="Lista">Interfaz de Lista de tipo "CargaFlotaDTO".</param>
        /// <returns>Retorna Interfaz de Lista de tipo "vCargaFlota".</returns>
        public static IList<vCargaFlota> ConvertData(IList<CargaFlotaDTO> Lista)
        {
            IList<vCargaFlota> _list = new List<vCargaFlota>();

            foreach (CargaFlotaDTO row in Lista)
            {
                vCargaFlota _row = new vCargaFlota()
                {
                    FlotaCamion = row.FlotaCamion,
                    Promedio = row.Promedio,
                    CargaTotal = row.CargaTotal,
                    Porcentaje = row.Porcentaje,
                    AlarmaPunto = row.AlarmaPunto
                };

                _list.Add(_row);
            }

            return _list;
        }


        /// <summary>
        /// Metodo de conversion de objeto "vCargaFlota" a "CargaFlotaDTO".
        /// </summary>
        /// <param name="Lista">Interfaz de Lista de tipo "vCargaFlota".</param>
        /// <returns>Retorna Interfaz de Lista de tipo "CargaFlotaDTO".</returns>
        public static IList<CargaFlotaDTO> ConvertData(IList<vCargaFlota> Lista)
        {
            IList<CargaFlotaDTO> _list = new List<CargaFlotaDTO>();

            foreach (vCargaFlota row in Lista)
            {
                CargaFlotaDTO _row = new CargaFlotaDTO()
                {
                    FlotaCamion = row.FlotaCamion,
                    Promedio = row.Promedio,
                    CargaTotal = row.CargaTotal,
                    Porcentaje = row.Porcentaje,
                    AlarmaPunto = row.AlarmaPunto
                };

                _list.Add(_row);
            }

            return _list;
        }
    }
}
