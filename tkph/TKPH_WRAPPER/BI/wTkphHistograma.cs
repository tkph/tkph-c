﻿using Com.Vmica.View.BI;
using Com.Vmica.View.BI.Export;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TKPH_DATA.DTO.BI;

namespace Com.Vmica.Wrapper.BI
{
    /// <summary>
    /// Clase con metodos para transformación y transporte de objetos para datos de TKPH Histograma entre la capa View y DTO
    /// </summary>
    public class wTkphHistograma
    {
        /// <summary>
        /// Metodo de conversion de objeto "TkphHistogramaDTO" a "vTkphHistograma".
        /// </summary>
        /// <param name="Lista">Interfaz de Lista de tipo "TkphHistogramaDTO".</param>
        /// <returns>Retorna Interfaz de Lista de tipo "vTkphHistograma".</returns>
        public static IList<vTkphHistograma> ConvertData(IList<TkphHistogramaDTO> Lista)
        {
            IList<vTkphHistograma> _list = new List<vTkphHistograma>();

            foreach (TkphHistogramaDTO row in Lista)
            {
                vTkphHistograma _row = new vTkphHistograma()
                {
                    FlotaCamion = row.FlotaCamion,
                    Valor = row.Valor,
                    Tipo = row.Tipo,
                    Orden = row.Orden,
                    AlarmaPunto = row.AlarmaPunto
                };

                _list.Add(_row);
            }

            return _list;
        }


        /// <summary>
        /// Metodo de conversion de objeto "vTkphHistograma" a "TkphHistogramaDTO".
        /// </summary>
        /// <param name="Lista">Interfaz de Lista de tipo "vTkphHistograma".</param>
        /// <returns>Retorna Interfaz de Lista de tipo "TkphHistogramaDTO".</returns>
        public static IList<TkphHistogramaDTO> ConvertData(IList<vTkphHistograma> Lista)
        {
            IList<TkphHistogramaDTO> _list = new List<TkphHistogramaDTO>();

            foreach (vTkphHistograma row in Lista)
            {
                TkphHistogramaDTO _row = new TkphHistogramaDTO()
                {
                    FlotaCamion = row.FlotaCamion,
                    Valor = row.Valor,
                    Tipo = row.Tipo,
                    Orden = row.Orden,
                    AlarmaPunto = row.AlarmaPunto
                };

                _list.Add(_row);
            }

            return _list;
        }

    }
}
