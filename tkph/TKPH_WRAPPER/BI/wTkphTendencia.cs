﻿using Com.Vmica.View.BI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TKPH_DATA.DTO.BI;

namespace Com.Vmica.Wrapper.BI
{
    /// <summary>
    /// Clase con metodos para transformación y transporte de objetos para datos de TKPH Tendencia entre la capa View y DTO
    /// </summary>
    public class wTkphTendencia
    {

        /// <summary>
        /// Metodo de conversion de objeto "TkphTendenciaDTO" a "vTkphTendencia".
        /// </summary>
        /// <param name="Lista">Interfaz de Lista de tipo "TkphTendenciaDTO".</param>
        /// <returns>Retorna Interfaz de Lista de tipo "vTkphTendencia".</returns>
        public static IList<vTkphTendencia> ConvertData(IList<TkphTendenciaDTO> Lista)
        {
            IList<vTkphTendencia> _list = new List<vTkphTendencia>();

            foreach (TkphTendenciaDTO row in Lista)
            {
                vTkphTendencia _row = new vTkphTendencia()
                {
                    FlotaCamion = row.FlotaCamion,
                    Fecha = row.Fecha,
                    Promedio = row.Promedio,
                    Nominal = row.Nominal,
                    Porcentaje = row.Porcentaje,
                    AlarmaPunto = row.AlarmaPunto
                };

                _list.Add(_row);
            }

            return _list;
        }


        /// <summary>
        /// Metodo de conversion de objeto "vTkphTendencia" a "TkphTendenciaDTO".
        /// </summary>
        /// <param name="Lista">Interfaz de Lista de tipo "vTkphTendencia".</param>
        /// <returns>Retorna Interfaz de Lista de tipo "TkphTendenciaDTO".</returns>
        public static IList<TkphTendenciaDTO> ConvertData(IList<vTkphTendencia> Lista)
        {
            IList<TkphTendenciaDTO> _list = new List<TkphTendenciaDTO>();

            foreach (vTkphTendencia row in Lista)
            {
                TkphTendenciaDTO _row = new TkphTendenciaDTO()
                {
                    FlotaCamion = row.FlotaCamion,
                    Fecha = row.Fecha,
                    Promedio = row.Promedio,
                    Nominal = row.Nominal,
                    Porcentaje = row.Porcentaje,
                    AlarmaPunto = row.AlarmaPunto
                };

                _list.Add(_row);
            }

            return _list;
        }
    }
}
