﻿
using Com.Vmica.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TKPH_DATA.DTO.Camiones;

namespace Com.Vmica.Wrapper
{

    /// <summary>
    /// Clase con metodos para transformación y transporte de objetos para datos de Ciclo Camion entre la capa View y DTO
    /// </summary>
    class wCamionCiclo
    {

        /// <summary>
        /// Metodo de conversion de objeto "CamionCicloDTO" a "vCamionCiclo".
        /// </summary>
        /// <param name="Lista">Interfaz de Lista de tipo "CamionCicloDTO".</param>
        /// <returns>Retorna Interfaz de Lista de tipo "vCamionCiclo".</returns>
        public static IList<vCamionCiclo> ConvertData(IList<CamionCicloDTO> ListSatus)
        {
            IList<vCamionCiclo> _List = new List<vCamionCiclo>();

            foreach(CamionCicloDTO row in ListSatus)
            {
                vCamionCiclo _row = new vCamionCiclo()
                {
                    IdCamion = row.IdCamion,
                    IdCamionDispatch = row.IdCamionDispatch,
                    Fecha = row.Fecha,
                    Hora = row.Hora,
                    CargaTotal = row.CargaTotal,
                    Carga = row.Carga,
                    Ciclo = row.Ciclo,
                    K2 = row.K2,
                    Vm = row.Vm,
                    Qm = row.Qm,
                    K1 = row.K1,    
                    F = row.F,
                    TkphOperacional = row.TkphOperacional,
                    TipoCalculo = row.TipoCalculo,
                    DescPosicionEje = row.DescPosicionEje,
                    TkphNominalC = row.TkphNominalC,
                };
                _List.Add(_row);
            }
            return _List;
        }
    }
}
