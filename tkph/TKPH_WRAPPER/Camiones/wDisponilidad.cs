﻿using Com.Vmica.Dto;
using Com.Vmica.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Com.Vmica.Wrapper
{
    public class wDisponilidad
    {
        public static IList<vDisponible> ConvertData(IList<DisponibilidadDTO> List)
        {
            IList<vDisponible> _list = new List<vDisponible>();

            foreach (DisponibilidadDTO row in List)
            {
                vDisponible r = new vDisponible()
                {
                    Fecha = row.Fecha,
                    Turno = row.Turno,
                    Flota = row.Flota,
                    Operativo = row.Operativo,
                    Armado = row.Armado,
                    Porcentaje = row.Porcentaje
                };

                _list.Add(r);
            }

            return _list;
        }

        public static IList<DisponibilidadDTO> ConvertData(IList<vDisponible> List)
        {
            IList<DisponibilidadDTO> _list = new List<DisponibilidadDTO>();

            foreach (vDisponible row in List)
            {
                DisponibilidadDTO r = new DisponibilidadDTO()
                {
                    Fecha = row.Fecha,
                    Turno = row.Turno,
                    Flota = row.Flota,
                    Operativo = row.Operativo,
                    Armado = row.Armado,
                    Porcentaje = row.Porcentaje
                };

                _list.Add(r);
            }

            return _list;
        }

        public static DisponibilidadDTO ConvertData(vDisponible Data)
        {
            return new DisponibilidadDTO()
            {
                Fecha = Data.Fecha,
                Turno = Data.Turno,
                Flota = Data.Flota,
                Operativo = Data.Operativo,
                Armado = Data.Armado,
                Porcentaje = Data.Porcentaje
            };
        }

        public static vDisponible ConvertData(DisponibilidadDTO Data)
        {
            return new vDisponible()
            {
                Fecha = Data.Fecha,
                Turno = Data.Turno,
                Flota = Data.Flota,
                Operativo = Data.Operativo,
                Armado = Data.Armado,
                Porcentaje = Data.Porcentaje
            };
        }
    }
}
