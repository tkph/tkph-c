﻿
using Com.Vmica.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TKPH_DATA.DTO.Camiones;

namespace Com.Vmica.Wrapper
{
    /// <summary>
    /// Clase con metodos para transformación y transporte de objetos para datos de Detalle Camion entre la capa View y DTO
    /// </summary>
    public class wCamionDetails
    {

        /// <summary>
        /// Metodo de conversion de objeto "CamionDetailsDTO" a "vCamionDetails".
        /// </summary>
        /// <param name="Lista">Interfaz de Lista de tipo "CamionDetailsDTO".</param>
        /// <returns>Retorna Interfaz de Lista de tipo "vCamionDetails".</returns>
        public static IList<vCamionDetails> ConvertData(IList<CamionDetailsDTO> ListDetails)
        {
            IList<vCamionDetails> _list = new List<vCamionDetails>();

            foreach (CamionDetailsDTO row in ListDetails)
            {
                vCamionDetails _row = new vCamionDetails()
                {
                    IdCamion = row.IdCamion,
                    Temperatura = row.Temperatura,
                    Presion = row.Presion,
                    Carga = row.Carga,
                    Tkph = row.Tkph,
                    Neumatico = row.Neumatico,
                    Alerta = row.Alerta,
                    DescripcionFlota = row.DescripcionFlota,
                    DescripcionFlotaAbre = row.DescripcionFlotaAbre,
                };

                _list.Add(_row);
            }

            return _list;
        }
    }
}
