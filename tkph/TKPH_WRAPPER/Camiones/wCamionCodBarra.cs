﻿using Com.Vmica.View.Camiones;
using COM_EPMS_DATA.DTO.Camiones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Com.Vmica.Wrapper.Camiones
{
    public class wCamionCodBarra
    {
        public static vCamionCodBarra ConvertData(CamionCodBarraDTO Data)
        {
            return new vCamionCodBarra 
            {
                id_camion = Data.id_camion
            };
        }

        public static CamionCodBarraDTO ConvertData(vCamionCodBarra Data)
        {
            return new CamionCodBarraDTO
            {
                id_camion = Data.id_camion
            };
        }

    }
}
