﻿
using Com.Vmica.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TKPH_DATA.DTO.Camiones;

namespace Com.Vmica.Wrapper
{
    /// <summary>
    /// Clase con metodos para transformación y transporte de objetos para datos Historicos de Presion y Temperatura entre la capa View y DTO
    /// </summary>
    public class wHistoricoPT
    {
        /// <summary>
        /// Metodo de conversion de objeto "vHistoricoPT" a "HitoricoPresionTempDTO".
        /// </summary>
        /// <param name="Data">Objeto de tipo "vHistoricoPT".</param>
        /// <returns>Retorna Objeto de tipo "HitoricoPresionTempDTO".</returns>
        public static HitoricoPresionTempDTO ConvertData(vHistoricoPT Data)
        {
            HitoricoPresionTempDTO _row = new HitoricoPresionTempDTO()
            {
                FECHA = Data.FECHA,
                IDCAMION = Data.IDCAMION,
                LOGICALID = Data.LOGICALID,
                TEMPERATURA = Data.TEMPERATURA,
                PRESION = Data.PRESION,
                FechaDesde = Data.FechaDesde,
                FechaHasta = Data.FechaHasta
            };

            return _row;
        }


        /// <summary>
        /// Metodo de conversion de objeto "HitoricoPresionTempDTO" a "vHistoricoPT".
        /// </summary>
        /// <param name="Data">Objeto de tipo "vHistoricoPT".</param>
        /// <returns>Retorna Objeto de tipo "HitoricoPresionTempDTO".</returns>
        public static vHistoricoPT ConvertData(HitoricoPresionTempDTO Data)
        {
            vHistoricoPT _row = new vHistoricoPT()
            {
                FECHA = Data.FECHA,
                IDCAMION = Data.IDCAMION,
                LOGICALID = Data.LOGICALID,
                TEMPERATURA = Data.TEMPERATURA,
                PRESION = Data.PRESION,
                FechaDesde = Data.FechaDesde,
                FechaHasta = Data.FechaHasta
            };

            return _row;
        }


        /// <summary>
        /// Metodo de conversion de objeto "vHistoricoPT" a "HitoricoPresionTempDTO".
        /// </summary>
        /// <param name="Data">Interfaz de Lista de tipo "vHistoricoPT".</param>
        /// <returns>Retorna Interfaz de Lista de tipo "HitoricoPresionTempDTO".</returns>
        public static IList<HitoricoPresionTempDTO> ConvertData(IList<vHistoricoPT> Data)
        {
            IList<HitoricoPresionTempDTO> _list = new List<HitoricoPresionTempDTO>();

            foreach (vHistoricoPT row in Data)
            {
                HitoricoPresionTempDTO _row = new HitoricoPresionTempDTO()
                {
                    FECHA = row.FECHA,
                    IDCAMION = row.IDCAMION,
                    LOGICALID = row.LOGICALID,
                    TEMPERATURA = row.TEMPERATURA,
                    PRESION = row.PRESION,
                    FechaDesde = row.FechaDesde,
                    FechaHasta = row.FechaHasta
                };

                _list.Add(_row);
            }

            return _list;
        }


        /// <summary>
        /// Metodo de conversion de objeto "HitoricoPresionTempDTO" a "vHistoricoPT".
        /// </summary>
        /// <param name="Data">Interfaz de Lista de tipo "HitoricoPresionTempDTO".</param>
        /// <returns>Retorna Interfaz de Lista de tipo "vHistoricoPT".</returns>
        public static IList<vHistoricoPT> ConvertData(IList<HitoricoPresionTempDTO> Data)
        {
            IList<vHistoricoPT> _list = new List<vHistoricoPT>();

            foreach (HitoricoPresionTempDTO row in Data)
            {
                vHistoricoPT _row = new vHistoricoPT()
                {
                    FECHA = row.FECHA,
                    IDCAMION = row.IDCAMION,
                    LOGICALID = row.LOGICALID,
                    TEMPERATURA = row.TEMPERATURA,
                    PRESION = row.PRESION,
                    FechaDesde = row.FechaDesde,
                    FechaHasta = row.FechaHasta
                };

                _list.Add(_row);
            }

            return _list;
        }

    }
}
