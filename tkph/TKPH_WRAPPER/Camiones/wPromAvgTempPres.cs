﻿using Com.Vmica.View.Camiones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TKPH_DATA.DTO.Camiones;

namespace Com.Vmica.Wrapper.Camiones
{
    /// <summary>
    /// Clase con metodos para transformación y transporte de objetos para datos Promedio Presión y Temperatura entre la capa View y DTO
    /// </summary>
    public class wPromAvgTempPres
    {

        /// <summary>
        /// Metodo de conversion de objeto "vPromAvgTempPres" a "PromAvgTempPresDTO".
        /// </summary>
        /// <param name="Data">Interfaz de Lista de tipo "vPromAvgTempPres".</param>
        /// <returns>Retorna Interfaz de Lista de tipo "PromAvgTempPresDTO".</returns>
        public static IList<PromAvgTempPresDTO> ConvertData(IList<vPromAvgTempPres> Data)
        {

             IList<PromAvgTempPresDTO> _list = new List<PromAvgTempPresDTO>();


             foreach (vPromAvgTempPres _r in Data)
             {
                 PromAvgTempPresDTO lst = new PromAvgTempPresDTO
                 {
                     id_camion = _r.id_camion,
                     max_presion_psi_1 = _r.max_presion_psi_1,
                     max_presion_psi_2 = _r.max_presion_psi_2,
                     max_presion_psi_3 = _r.max_presion_psi_3,
                     max_presion_psi_4 = _r.max_presion_psi_4,
                     max_presion_psi_5 = _r.max_presion_psi_5,
                     max_presion_psi_6 = _r.max_presion_psi_6,

                     min_presion_psi_1 = _r.min_presion_psi_1,
                     min_presion_psi_2 = _r.min_presion_psi_2,
                     min_presion_psi_3 = _r.min_presion_psi_3,
                     min_presion_psi_4 = _r.min_presion_psi_4,
                     min_presion_psi_5 = _r.min_presion_psi_5,
                     min_presion_psi_6 = _r.min_presion_psi_6,

                     avg_presion_psi_1 = _r.avg_presion_psi_1,
                     avg_presion_psi_2 = _r.avg_presion_psi_2,
                     avg_presion_psi_3 = _r.avg_presion_psi_3,
                     avg_presion_psi_4 = _r.avg_presion_psi_4,
                     avg_presion_psi_5 = _r.avg_presion_psi_5,
                     avg_presion_psi_6 = _r.avg_presion_psi_6,

                     max_temperatura_c_1 = _r.max_temperatura_c_1,
                     max_temperatura_c_2 = _r.max_temperatura_c_2,
                     max_temperatura_c_3 = _r.max_temperatura_c_3,
                     max_temperatura_c_4 = _r.max_temperatura_c_4,
                     max_temperatura_c_5 = _r.max_temperatura_c_5,
                     max_temperatura_c_6 = _r.max_temperatura_c_6,

                     min_temperatura_c_1 = _r.min_temperatura_c_1,
                     min_temperatura_c_2 = _r.min_temperatura_c_2,
                     min_temperatura_c_3 = _r.min_temperatura_c_3,
                     min_temperatura_c_4 = _r.min_temperatura_c_4,
                     min_temperatura_c_5 = _r.min_temperatura_c_5,
                     min_temperatura_c_6 = _r.min_temperatura_c_6,

                     avg_temperatura_c_1 = _r.avg_temperatura_c_1,
                     avg_temperatura_c_2 = _r.avg_temperatura_c_2,
                     avg_temperatura_c_3 = _r.avg_temperatura_c_3,
                     avg_temperatura_c_4 = _r.avg_temperatura_c_4,
                     avg_temperatura_c_5 = _r.avg_temperatura_c_5,
                     avg_temperatura_c_6 = _r.avg_temperatura_c_6,

                     fecha_ini = _r.fecha_ini,
                     fecha_fin = _r.fecha_fin
                 };
                 _list.Add(lst);
             }
             return _list;
        }


        /// <summary>
        /// Metodo de conversion de objeto "PromAvgTempPresDTO" a "vPromAvgTempPres".
        /// </summary>
        /// <param name="Data">Interfaz de Lista de tipo "PromAvgTempPresDTO".</param>
        /// <returns>Retorna Interfaz de Lista de tipo "vPromAvgTempPres".</returns>
        public static IList<vPromAvgTempPres> ConvertData(IList<PromAvgTempPresDTO> Data)
        {

            IList<vPromAvgTempPres> _list = new List<vPromAvgTempPres>();


            foreach (PromAvgTempPresDTO _r in Data)
            {
                vPromAvgTempPres lst = new vPromAvgTempPres
                {
                    id_camion = _r.id_camion,
                    max_presion_psi_1 = _r.max_presion_psi_1,
                    max_presion_psi_2 = _r.max_presion_psi_2,
                    max_presion_psi_3 = _r.max_presion_psi_3,
                    max_presion_psi_4 = _r.max_presion_psi_4,
                    max_presion_psi_5 = _r.max_presion_psi_5,
                    max_presion_psi_6 = _r.max_presion_psi_6,

                    min_presion_psi_1 = _r.min_presion_psi_1,
                    min_presion_psi_2 = _r.min_presion_psi_2,
                    min_presion_psi_3 = _r.min_presion_psi_3,
                    min_presion_psi_4 = _r.min_presion_psi_4,
                    min_presion_psi_5 = _r.min_presion_psi_5,
                    min_presion_psi_6 = _r.min_presion_psi_6,

                    avg_presion_psi_1 = _r.avg_presion_psi_1,
                    avg_presion_psi_2 = _r.avg_presion_psi_2,
                    avg_presion_psi_3 = _r.avg_presion_psi_3,
                    avg_presion_psi_4 = _r.avg_presion_psi_4,
                    avg_presion_psi_5 = _r.avg_presion_psi_5,
                    avg_presion_psi_6 = _r.avg_presion_psi_6,

                    max_temperatura_c_1 = _r.max_temperatura_c_1,
                    max_temperatura_c_2 = _r.max_temperatura_c_2,
                    max_temperatura_c_3 = _r.max_temperatura_c_3,
                    max_temperatura_c_4 = _r.max_temperatura_c_4,
                    max_temperatura_c_5 = _r.max_temperatura_c_5,
                    max_temperatura_c_6 = _r.max_temperatura_c_6,

                    min_temperatura_c_1 = _r.min_temperatura_c_1,
                    min_temperatura_c_2 = _r.min_temperatura_c_2,
                    min_temperatura_c_3 = _r.min_temperatura_c_3,
                    min_temperatura_c_4 = _r.min_temperatura_c_4,
                    min_temperatura_c_5 = _r.min_temperatura_c_5,
                    min_temperatura_c_6 = _r.min_temperatura_c_6,

                    avg_temperatura_c_1 = _r.avg_temperatura_c_1,
                    avg_temperatura_c_2 = _r.avg_temperatura_c_2,
                    avg_temperatura_c_3 = _r.avg_temperatura_c_3,
                    avg_temperatura_c_4 = _r.avg_temperatura_c_4,
                    avg_temperatura_c_5 = _r.avg_temperatura_c_5,
                    avg_temperatura_c_6 = _r.avg_temperatura_c_6,

                    fecha_ini = _r.fecha_ini,
                    fecha_fin = _r.fecha_fin
                };
                _list.Add(lst);
            }
            return _list;
        }

    }
}
