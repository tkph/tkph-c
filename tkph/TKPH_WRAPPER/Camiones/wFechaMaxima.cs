﻿
using Com.Vmica.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TKPH_DATA.DTO.Camiones;

namespace Com.Vmica.Wrapper
{
    /// <summary>
    /// Clase con metodos para transformación y transporte de objetos para fechas minimas y maximas de información de Camión entre la capa View y DTO
    /// </summary>
    public class wFechaMaxima
    {
        /// <summary>
        /// Metodo de conversion de objeto "FechasMaximasDTO" a "vFechaMaxima".
        /// </summary>
        /// <param name="Fecha">Objeto de tipo "FechasMaximasDTO".</param>
        /// <returns>Retorna Objeto de tipo "vFechaMaxima".</returns>
        static public vFechaMaxima ConvertData(FechasMaximasDTO Fecha)
        {
            return new vFechaMaxima()
            {
                IdCamion = Fecha.IdCamion != null ? Fecha.IdCamion : String.Empty,
                FUAME = Fecha.FUAME,
                FUAMI = Fecha.FUAMI
            };
        }


        /// <summary>
        /// Metodo de conversion de objeto "vFechaMaxima" a "FechasMaximasDTO".
        /// </summary>
        /// <param name="Fecha">Objeto de tipo "vFechaMaxima".</param>
        /// <returns>Retorna Objeto de tipo "FechasMaximasDTO".</returns>
        static public FechasMaximasDTO ConvertData(vFechaMaxima Fecha)
        {
            return new FechasMaximasDTO()
            {
                IdCamion = Fecha.IdCamion != null ? Fecha.IdCamion : String.Empty,
                FUAME = Fecha.FUAME,
                FUAMI = Fecha.FUAMI
            };
        }
    }
}
