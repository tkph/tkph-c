﻿
using Com.Vmica.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TKPH_DATA.DTO.Camiones;

namespace Com.Vmica.Wrapper
{
    /// <summary>
    /// Clase con metodos para transformación y transporte de objetos para datos de notificación entre la capa View y DTO
    /// </summary>
    public class wNotificacionCamion
    {
        /// <summary>
        /// Metodo de conversion de objeto "vNotificacionCamion" a "BitacoraCamionDTO".
        /// </summary>
        /// <param name="Notificacion">Objeto de tipo "vNotificacionCamion".</param>
        /// <returns>Retorna Objeto de tipo "BitacoraCamionDTO".</returns>
        public static BitacoraCamionDTO ConvertData(vNotificacionCamion Notificacion)
        {
            BitacoraCamionDTO _row = new BitacoraCamionDTO()
            {
                Fecha = Notificacion.Fecha,
                IdHora = Notificacion.IdHora,
                IdCamion = Notificacion.IdCamion,
                Operador = Notificacion.Operador,
                DescAlarma = Notificacion.DescAlarma,
            };

            return _row;
        }

        /// <summary>
        /// Metodo de conversion de objeto "BitacoraCamionDTO" a "vNotificacionCamion".
        /// </summary>
        /// <param name="Notificacion">Objeto de tipo "BitacoraCamionDTO".</param>
        /// <returns>Retorna Objeto de tipo "vNotificacionCamion".</returns>
        public static vNotificacionCamion ConvertData(BitacoraCamionDTO Notificacion)
        {
            vNotificacionCamion _row = new vNotificacionCamion()
            {
                Fecha = Notificacion.Fecha,
                IdHora = Notificacion.IdHora,
                IdCamion = Notificacion.IdCamion,
                Operador = Notificacion.Operador,
                DescAlarma = Notificacion.DescAlarma,
            };

            return _row;
        }


        /// <summary>
        /// Metodo de conversion de objeto "vNotificacionCamion" a "BitacoraCamionDTO".
        /// </summary>
        /// <param name="Notificacion">Interfaz de Lista de tipo "BitacoraCamionDTO".</param>
        /// <returns>Retorna Interfaz de Lista de tipo "vNotificacionCamion".</returns>
        public static IList<BitacoraCamionDTO> ConvertData(IList<vNotificacionCamion> Notificacion)
        {
            IList<BitacoraCamionDTO> _list = new List<BitacoraCamionDTO>();

            foreach (vNotificacionCamion row in Notificacion)
            {
                BitacoraCamionDTO _row = new BitacoraCamionDTO()
                {
                    Fecha = row.Fecha,
                    IdHora = row.IdHora,
                    IdCamion = row.IdCamion,
                    Operador = row.Operador,
                    DescAlarma = row.DescAlarma,
                };

                _list.Add(_row);
            }
            return _list;
        }

        /// <summary>
        /// Metodo de conversion de objeto "BitacoraCamionDTO" a "vNotificacionCamion".
        /// </summary>
        /// <param name="Notificacion">Interfaz de Lista de tipo "BitacoraCamionDTO".</param>
        /// <returns>Retorna Interfaz de Lista de tipo "vNotificacionCamion".</returns>
        public static IList<vNotificacionCamion> ConvertData(IList<BitacoraCamionDTO> Notificacion)
        {
            IList<vNotificacionCamion> _list = new List<vNotificacionCamion>();

            foreach (BitacoraCamionDTO row in Notificacion)
            {
                vNotificacionCamion _row = new vNotificacionCamion()
                {
                    Fecha = row.Fecha,
                    IdHora = row.IdHora,
                    IdCamion = row.IdCamion,
                    Operador = row.Operador,
                    DescAlarma = row.DescAlarma,
                };

                _list.Add(_row);
            }
            return _list;
        }
    }
}
