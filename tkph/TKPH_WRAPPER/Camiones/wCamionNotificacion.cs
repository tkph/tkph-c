﻿using Com.Vmica.View;
using Com.Vmica.View.Camiones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TKPH_DATA.DTO.Camiones;

namespace Com.Vmica.Wrapper
{
    /// <summary>
    /// Clase con metodos para transformación y transporte de objetos para datos de Notificación Camion entre la capa View y DTO
    /// </summary>
    public class wCamionNotificacion
    {
        /// <summary>
        /// Metodo de conversion de objeto "CamionNotificacionDTO" a "vCamionNotificacion".
        /// </summary>
        /// <param name="Data">Objeto de tipo "CamionNotificacionDTO".</param>
        /// <returns>Retorna Objeto de tipo "vCamionNotificacion".</returns>
        public static vCamionNotificacion ConvertData(CamionNotificacionDTO Data)
        {
            return new vCamionNotificacion
            {
                ID_CAMION = Data.ID_CAMION,
                TIPO_ALARMA = Data.TIPO_ALARMA,
                UBICACION_ALARMA = Data.UBICACION_ALARMA,
                GRAVEDAD_ALARMA = Data.GRAVEDAD_ALARMA,
                VALOR = Data.VALOR,
                FECHA_ALARMA = Data.FECHA_ALARMA,
                PALA = Data.PALA,
                OPERADOR_PALA = Data.OPERADOR_PALA,
                OPERADOR_CAMION = Data.OPERADOR_CAMION,
                ID_DESTINO = Data.ID_DESTINO,
                ID_TURNO = Data.ID_TURNO,
                GENERACION = Data.GENERACION
            };
        }


        /// <summary>
        /// Metodo de conversion de objeto "vCamionNotificacion" a "CamionNotificacionDTO".
        /// </summary>
        /// <param name="Data">Objeto de tipo "vCamionNotificacion".</param>
        /// <returns>Retorna Objeto de tipo "CamionNotificacionDTO".</returns>
        public static CamionNotificacionDTO ConvertData(vCamionNotificacion Data)
        {
            return new CamionNotificacionDTO
            {
                ID_CAMION = Data.ID_CAMION,
                TIPO_ALARMA = Data.TIPO_ALARMA,
                UBICACION_ALARMA = Data.UBICACION_ALARMA,
                GRAVEDAD_ALARMA = Data.GRAVEDAD_ALARMA,
                VALOR = Data.VALOR,
                FECHA_ALARMA = Data.FECHA_ALARMA,
                PALA = Data.PALA,
                OPERADOR_PALA = Data.OPERADOR_PALA,
                OPERADOR_CAMION = Data.OPERADOR_CAMION,
                ID_DESTINO = Data.ID_DESTINO,
                ID_TURNO = Data.ID_TURNO,
                GENERACION = Data.GENERACION
            };
        }


        /// <summary>
        /// Metodo de conversion de objeto "vCamionNotificacion" a "CamionNotificacionDTO".
        /// </summary>
        /// <param name="Data">Interfaz de Lista de tipo "vCamionNotificacion".</param>
        /// <returns>Retorna Interfaz de Lista de tipo "CamionNotificacionDTO".</returns>
        public static IList<CamionNotificacionDTO> ConvertData(IList<vCamionNotificacion> Data)
        {

            IList<CamionNotificacionDTO> _list = new List<CamionNotificacionDTO>();


            foreach (vCamionNotificacion _r in Data)
            {
                CamionNotificacionDTO lst = new CamionNotificacionDTO
                {
                    ID_CAMION = _r.ID_CAMION,
                    TIPO_ALARMA = _r.TIPO_ALARMA,
                    UBICACION_ALARMA = _r.UBICACION_ALARMA,
                    GRAVEDAD_ALARMA = _r.GRAVEDAD_ALARMA,
                    VALOR = _r.VALOR,
                    FECHA_ALARMA = _r.FECHA_ALARMA,
                    PALA = _r.PALA,
                    OPERADOR_PALA = _r.OPERADOR_PALA,
                    OPERADOR_CAMION = _r.OPERADOR_CAMION,
                    ID_DESTINO = _r.ID_DESTINO,
                    ID_TURNO = _r.ID_TURNO,
                    GENERACION = _r.GENERACION
                };
                _list.Add(lst);
            }
            return _list;
        }


        /// <summary>
        /// Metodo de conversion de objeto "CamionNotificacionDTO" a "vCamionNotificacion".
        /// </summary>
        /// <param name="Data">Interfaz de Lista de tipo "CamionNotificacionDTO".</param>
        /// <returns>Retorna Interfaz de Lista de tipo "vCamionNotificacion".</returns>
        public static IList<vCamionNotificacion> ConvertData(IList<CamionNotificacionDTO> Data)
        {

            IList<vCamionNotificacion> _list = new List<vCamionNotificacion>();


            foreach (CamionNotificacionDTO _r in Data)
            {
                vCamionNotificacion lst = new vCamionNotificacion
                {
                    ID_CAMION = _r.ID_CAMION,
                    TIPO_ALARMA = _r.TIPO_ALARMA,
                    UBICACION_ALARMA = _r.UBICACION_ALARMA,
                    GRAVEDAD_ALARMA = _r.GRAVEDAD_ALARMA,
                    VALOR = _r.VALOR,
                    FECHA_ALARMA = _r.FECHA_ALARMA,
                    PALA = _r.PALA,
                    OPERADOR_PALA = _r.OPERADOR_PALA,
                    OPERADOR_CAMION = _r.OPERADOR_CAMION,
                    ID_DESTINO = _r.ID_DESTINO,
                    ID_TURNO = _r.ID_TURNO,
                    GENERACION = _r.GENERACION
                };
                _list.Add(lst);
            }
            return _list;
        }
    }
}
