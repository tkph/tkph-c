﻿
using Com.Vmica.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TKPH_DATA.DTO.Camiones;

namespace Com.Vmica.Wrapper
{
    /// <summary>
    /// Clase con metodos para transformación y transporte de objetos para datos de Status entre la capa View y DTO
    /// </summary>
    public class wStatusRequest
    {
        /// <summary>
        /// Metodo de conversion de objeto "vStatusRequest" a "StatusRequest".
        /// </summary>
        /// <param name="Status">Objeto de tipo "vStatusRequest".</param>
        /// <returns>Retorna Objeto de tipo "StatusRequest".</returns>
        static public StatusRequest ConvertData(vStatusRequest Status)
        {
            return new StatusRequest() 
            {
                IdCamion = Status.IdCamion != null ? Status.IdCamion : String.Empty,
                DescripcionFlota = Status.DescripcionFlota != null ? Status.DescripcionFlota : String.Empty,
                AlertaCarga = Status.AlertaCarga != null && !Status.AlertaCarga.Equals("") ? Status.AlertaCarga : "0",
                AlertaGeneral = Status.AlertaGeneral != null && !Status.AlertaGeneral.Equals("") ? Status.AlertaGeneral : "0",
                AlertaPresion = Status.AlertaPresion != null && !Status.AlertaPresion.Equals("") ? Status.AlertaPresion : "0",
                AlertaTemperatura = Status.AlertaTemperatura != null && !Status.AlertaTemperatura.Equals("") ? Status.AlertaTemperatura : "0",
                AlertaTkph = Status.AlertaTkph != null && !Status.AlertaTkph.Equals("") ? Status.AlertaTkph : "0",
            };
        }
    }
}
