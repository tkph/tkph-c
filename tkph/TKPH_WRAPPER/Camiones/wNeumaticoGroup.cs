﻿using Com.Vmica.Dto;
using Com.Vmica.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Vmica.Wrapper
{
    public class wNeumaticoGroup
    {
        static public IList<vEstadoNeumatico> ConvertData(IList<EstadoCamionDTO> ListStatus)
        {
            IList<vEstadoNeumatico> _list = new List<vEstadoNeumatico>();

            foreach (EstadoCamionDTO row in ListStatus)
            {
                vEstadoNeumatico _row = new vEstadoNeumatico()
                {
                    Valor = row.Valor,
                    Alerta = row.Alerta,
                    Total = Convert.ToInt64(row.Total)
                };

                _list.Add(_row);
            }

            return _list;
        }
    }
}
