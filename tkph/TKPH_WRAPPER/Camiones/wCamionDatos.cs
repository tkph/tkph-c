﻿
using Com.Vmica.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TKPH_DATA.DTO.Camiones;

namespace Com.Vmica.Wrapper
{

    /// <summary>
    /// Clase con metodos para transformación y transporte de objetos para datos de Camión entre la capa View y DTO
    /// </summary>
    public class wCamionDatos
    {

        /// <summary>
        /// Metodo de conversion de objeto "vCamionDatos" a "CamionDatosDTO".
        /// </summary>
        /// <param name="Data">Objeto de tipo "vCamionDatos".</param>
        /// <returns>Retorna Objeto de tipo "CamionDatosDTO".</returns>
        public static CamionDatosDTO ConvertData(vCamionDatos Data)
        {
            return  new CamionDatosDTO() 
            {
                IdCamion = Data.IdCamion,
                Carga = Data.Carga,
                CargaEsperada = Data.CargaEsperada,
                AlertaCarga = Data.AlertaCarga,
                VM = Data.VM,
                DistRec = Data.DistRec,
                TKPHOperacionalT = Data.TKPHOperacionalT,
                AlertaTKPH_T = Data.AlertaTKPH_T,
                TKPHOperacionalD = Data.TKPHOperacionalD,
                AlertaTKPH_D = Data.AlertaTKPH_D,
                TipoCalculo = Data.TipoCalculo,
                DescPosicionEjeT = Data.DescPosicionEjeT,
                DescPosicionEjeD = Data.DescPosicionEjeD,
                TKPHNominal_C = Data.TKPHNominal_C,
                Flota = Data.Flota,
                Turno = Data.Turno,
                Operador = Data.Operador,
                OpePala = Data.OpePala,
                Pala = Data.Pala,
                CargaActual= Data.CargaActual,
                VelocidadActual = Data.VelocidadActual,
                id_destino = Data.id_destino,
                carga_turno = Data.carga_turno,
                km_turno = Data.km_turno,
                TKPH_avg_D = Data.TKPH_avg_D,
                TKPH_avg_T = Data.TKPH_avg_T,
                f_ult_lectura = Data.f_ult_lectura,
                Temp_ambiente = Data.Temp_ambiente,
                fecha_fin_de_ciclo = Data.fecha_fin_de_ciclo,
                aler_not_alto = Data.aler_not_alto,
                aler_not_medio = Data.aler_not_medio
            };
        }


        /// <summary>
        /// Metodo de conversion de objeto "CamionDatosDTO" a "vCamionDatos".
        /// </summary>
        /// <param name="Data">Objeto de tipo "CamionDatosDTO".</param>
        /// <returns>Retorna Objeto de tipo "vCamionDatos".</returns>
        public static vCamionDatos ConvertData(CamionDatosDTO Data)
        {
            return new vCamionDatos()
            {
                IdCamion = Data.IdCamion,
                Carga = Data.Carga,
                CargaEsperada = Data.CargaEsperada,
                AlertaCarga = Data.AlertaCarga,
                VM = Data.VM,
                DistRec = Data.DistRec,
                TKPHOperacionalT = Data.TKPHOperacionalT,
                AlertaTKPH_T = Data.AlertaTKPH_T,
                TKPHOperacionalD = Data.TKPHOperacionalD,
                AlertaTKPH_D = Data.AlertaTKPH_D,
                TipoCalculo = Data.TipoCalculo,
                DescPosicionEjeT = Data.DescPosicionEjeT,
                DescPosicionEjeD = Data.DescPosicionEjeD,
                TKPHNominal_C = Data.TKPHNominal_C,
                Flota = Data.Flota,
                Turno = Data.Turno,
                Operador = Data.Operador,
                OpePala = Data.OpePala,
                Pala = Data.Pala,
                CargaActual = Data.CargaActual,
                VelocidadActual = Data.VelocidadActual,
                id_destino = Data.id_destino,
                carga_turno = Data.carga_turno,
                km_turno = Data.km_turno,
                TKPH_avg_D = Data.TKPH_avg_D,
                TKPH_avg_T = Data.TKPH_avg_T,
                f_ult_lectura = Data.f_ult_lectura,
                Temp_ambiente = Data.Temp_ambiente,
                fecha_fin_de_ciclo = Data.fecha_fin_de_ciclo,
                aler_not_alto = Data.aler_not_alto,
                aler_not_medio = Data.aler_not_medio
            };
        }
    }
}
