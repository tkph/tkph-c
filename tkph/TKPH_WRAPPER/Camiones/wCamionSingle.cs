﻿
using Com.Vmica.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TKPH_DATA.DTO.Camiones;

namespace Com.Vmica.Wrapper
{

    /// <summary>
    /// Clase con metodos para transformación y transporte de objetos para datos individuales de Camión  entre la capa View y DTO
    /// </summary>
    public class wCamionSingle
    {
        /// <summary>
        /// Metodo de conversion de objeto "CamionSingleDTO" a "vCamionSingle".
        /// </summary>
        /// <param name="Status">Objeto de tipo "CamionSingleDTO".</param>
        /// <returns>Retorna Objeto de tipo "vCamionSingle".</returns>
        static public vCamionSingle ConvertData(CamionSingleDTO Status)
        {
            vCamionSingle _row = new vCamionSingle()
            {
                IdCamion = Status.IdCamion,
                AlertaRueda_1 = Status.AlertaRueda_1,
                AlertaRueda_2 = Status.AlertaRueda_2,
                AlertaRueda_3 = Status.AlertaRueda_3,
                AlertaRueda_4 = Status.AlertaRueda_4,
                AlertaRueda_5 = Status.AlertaRueda_5,
                AlertaRueda_6 = Status.AlertaRueda_6,
                AlertaTemp_1 = Status.AlertaTemp_1,
                AlertaTemp_2 = Status.AlertaTemp_2,
                AlertaTemp_3 = Status.AlertaTemp_3,
                AlertaTemp_4 = Status.AlertaTemp_4,
                AlertaTemp_5 = Status.AlertaTemp_5,
                AlertaTemp_6 = Status.AlertaTemp_6,
                AlertaPres_1 = Status.AlertaPres_1,
                AlertaPres_2 = Status.AlertaPres_2,
                AlertaPres_3 = Status.AlertaPres_3,
                AlertaPres_4 = Status.AlertaPres_4,
                AlertaPres_5 = Status.AlertaPres_5,
                AlertaPres_6 = Status.AlertaPres_6,
                TemperaturaCarga_1 = Status.TemperaturaCarga_1,
                TemperaturaCarga_2 = Status.TemperaturaCarga_2,
                TemperaturaCarga_3 = Status.TemperaturaCarga_3,
                TemperaturaCarga_4 = Status.TemperaturaCarga_4,
                TemperaturaCarga_5 = Status.TemperaturaCarga_5,
                TemperaturaCarga_6 = Status.TemperaturaCarga_6,
                Presion_1 = Status.Presion_1,
                Presion_2 = Status.Presion_2,
                Presion_3 = Status.Presion_3,
                Presion_4 = Status.Presion_4,
                Presion_5 = Status.Presion_5,
                Presion_6 = Status.Presion_6,
                Fecha = Status.Fecha,
                DescFlota = Status.DescFlota,
            };

            return _row;
        }
    }
}
