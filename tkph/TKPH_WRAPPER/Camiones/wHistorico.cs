﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Com.Vmica.View;
using TKPH_DATA.DTO.Camiones;

namespace Com.Vmica.Wrapper
{
    /// <summary>
    /// Clase con metodos para transformación y transporte de objetos para datos Historicos de TKPH y Carga entre la capa View y DTO
    /// </summary>
    public class wHistorico
    {
        /// <summary>
        /// Metodo de conversion de objeto "vHistorico" a "HistoricoTKPHCargaDTO".
        /// </summary>
        /// <param name="Data">Objeto de tipo "vHistorico".</param>
        /// <returns>Retorna Objeto de tipo "HistoricoTKPHCargaDTO".</returns>
        public static HistoricoTKPHCargaDTO ConvertData(vHistorico Data)
        {
            HistoricoTKPHCargaDTO _row = new HistoricoTKPHCargaDTO()
            {
                Fecha = Data.Fecha,
                IdCamion = Data.IdCamion,
                Nonimal = Data.Nonimal,
                Operacional = Data.Operacional,
                Flag = Data.Flag,
                FechaDesde = Data.FechaDesde,
                FechaHasta = Data.FechaHasta
            };

            return _row;
        }


        /// <summary>
        /// Metodo de conversion de objeto "HistoricoTKPHCargaDTO" a "vHistorico".
        /// </summary>
        /// <param name="Data">Objeto de tipo "HistoricoTKPHCargaDTO".</param>
        /// <returns>Retorna Objeto de tipo "vHistorico".</returns>
        public static vHistorico ConvertData(HistoricoTKPHCargaDTO Data)
        {
            vHistorico _row = new vHistorico()
            {
                Fecha = Data.Fecha,
                IdCamion = Data.IdCamion,
                Nonimal = Data.Nonimal,
                Operacional = Data.Operacional,
                Flag = Data.Flag,
                FechaDesde = Data.FechaDesde,
                FechaHasta = Data.FechaHasta
            };

            return _row;
        }

        /// <summary>
        /// Metodo de conversion de objeto "vHistorico" a "HistoricoTKPHCargaDTO".
        /// </summary>
        /// <param name="Data">Interfaz de Lista de tipo "vHistorico".</param>
        /// <returns>Retorna Interfaz de Lista de tipo "HistoricoTKPHCargaDTO".</returns>
        public static IList<HistoricoTKPHCargaDTO> ConvertData(IList<vHistorico> Data)
        {
            IList<HistoricoTKPHCargaDTO> _list = new List<HistoricoTKPHCargaDTO>();

            foreach (vHistorico row in Data)
            {
                HistoricoTKPHCargaDTO _row = new HistoricoTKPHCargaDTO()
                {
                    Fecha = row.Fecha,
                    IdCamion = row.IdCamion,
                    Nonimal = row.Nonimal,
                    Operacional = row.Operacional,
                    Flag = row.Flag,
                    FechaDesde = row.FechaDesde,
                    FechaHasta = row.FechaHasta
                };

                _list.Add(_row);
            }
            return _list;
        }


        /// <summary>
        /// Metodo de conversion de objeto "HistoricoTKPHCargaDTO" a "vHistorico".
        /// </summary>
        /// <param name="Data">Interfaz de Lista de tipo "HistoricoTKPHCargaDTO".</param>
        /// <returns>Retorna Interfaz de Lista de tipo "vHistorico".</returns>
        public static IList<vHistorico> ConvertData(IList<HistoricoTKPHCargaDTO> Data)
        {
            IList<vHistorico> _list = new List<vHistorico>();

            foreach (HistoricoTKPHCargaDTO row in Data)
            {
                vHistorico _row = new vHistorico()
                {
                    Fecha = row.Fecha,
                    IdCamion = row.IdCamion,
                    Nonimal = row.Nonimal,
                    Operacional = row.Operacional,
                    Flag = row.Flag,
                    FechaDesde = row.FechaDesde,
                    FechaHasta = row.FechaHasta
                };

                _list.Add(_row);
            }
            return _list;
        }
    }
}
