﻿using Com.Vmica.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TKPH_DATA.DTO;

namespace Com.Vmica.Wrapper
{
    /// <summary>
    /// Clase con metodos para transformación y transporte de objetos para datos de persistencia de notificación entre la capa View y DTO
    /// </summary>
    public class wInsertCamionNotificacion
    {
        /// <summary>
        /// Metodo de conversion de objeto "InsertCamionNotificacionDTO" a "vInsertCamionNotificacion".
        /// </summary>
        /// <param name="Data">Objeto de tipo "InsertCamionNotificacionDTO".</param>
        /// <returns>Retorna Objeto de tipo "vInsertCamionNotificacion".</returns>
        public static vInsertCamionNotificacion ConvertData(InsertCamionNotificacionDTO Data)
        {
            return new vInsertCamionNotificacion
            {
                ID_CAMION = Data.ID_CAMION,
                COMENTARIO = Data.COMENTARIO
            };
        }

        /// <summary>
        /// Metodo de conversion de objeto "vInsertCamionNotificacion" a "InsertCamionNotificacionDTO".
        /// </summary>
        /// <param name="Data">Objeto de tipo "vInsertCamionNotificacion".</param>
        /// <returns>Retorna Objeto de tipo "InsertCamionNotificacionDTO".</returns>
        public static InsertCamionNotificacionDTO ConvertData(vInsertCamionNotificacion Data)
        {
            return new InsertCamionNotificacionDTO
            {
                ID_CAMION = Data.ID_CAMION,
                COMENTARIO = Data.COMENTARIO
            };
        }


        /// <summary>
        /// Metodo de conversion de objeto "vInsertCamionNotificacion" a "InsertCamionNotificacionDTO".
        /// </summary>
        /// <param name="Data">Interfaz de Lista de tipo "vInsertCamionNotificacion".</param>
        /// <returns>Retorna Interfaz de Lista de tipo "InsertCamionNotificacionDTO".</returns>
        public static IList<InsertCamionNotificacionDTO> ConvertData(IList<vInsertCamionNotificacion> Data)
        {

            IList<InsertCamionNotificacionDTO> _list = new List<InsertCamionNotificacionDTO>();


            foreach (vInsertCamionNotificacion _r in Data)
            {
                InsertCamionNotificacionDTO lst = new InsertCamionNotificacionDTO
                {
                    ID_CAMION = _r.ID_CAMION,
                    COMENTARIO = _r.COMENTARIO
                };
                _list.Add(lst);
            }
            return _list;
        }


        /// <summary>
        /// Metodo de conversion de objeto "InsertCamionNotificacionDTO" a "vInsertCamionNotificacion".
        /// </summary>
        /// <param name="Data">Interfaz de Lista de tipo "InsertCamionNotificacionDTO".</param>
        /// <returns>Retorna Interfaz de Lista de tipo "vInsertCamionNotificacion".</returns>
        public static IList<vInsertCamionNotificacion> ConvertData(IList<InsertCamionNotificacionDTO> Data)
        {

            IList<vInsertCamionNotificacion> _list = new List<vInsertCamionNotificacion>();


            foreach (InsertCamionNotificacionDTO _r in Data)
            {
                vInsertCamionNotificacion lst = new vInsertCamionNotificacion
                {
                    ID_CAMION = _r.ID_CAMION,
                    COMENTARIO = _r.COMENTARIO
                };
                _list.Add(lst);
            }
            return _list;
        }
    }
}
