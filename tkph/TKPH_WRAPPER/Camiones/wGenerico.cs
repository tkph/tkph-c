﻿using Com.Vmica.Dto;
using Com.Vmica.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Com.Vmica.Wrapper
{
    public class wGenerico
    {
        static public IList<vGenerico> ConvertData(IList<GenericoDTO> List)
        {
            IList<vGenerico> l = new List<vGenerico>();

            foreach (GenericoDTO r in List)
            {
                vGenerico _row = new vGenerico()
                {
                    TipoParam = r.TipoParam,
                    OrigenParam = r.OrigenParam,
                    DescTipoParam = r.DescTipoParam,
                    DescInternaParam = r.DescInternaParam,
                    DescOrigenParam = r.DescOrigenParam,
                    Gerencia = r.Gerencia,
                    Nivel = r.Nivel,
                    Estado = r.Estado
                };

                l.Add(_row);
            }

            return l;
        }
        static public IList<GenericoDTO> ConvertData(IList<vGenerico> List)
        {
            IList<GenericoDTO> l = new List<GenericoDTO>();

            foreach (vGenerico r in List)
            {
                GenericoDTO _row = new GenericoDTO()
                {
                    TipoParam = r.TipoParam,
                    OrigenParam = r.OrigenParam,
                    DescTipoParam = r.DescTipoParam,
                    DescInternaParam = r.DescInternaParam,
                    DescOrigenParam = r.DescOrigenParam,
                    Gerencia = r.Gerencia,
                    Nivel = r.Nivel,
                    Estado = r.Estado
                };

                l.Add(_row);
            }

            return l;
        }
    }
}
