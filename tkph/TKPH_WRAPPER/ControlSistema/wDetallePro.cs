﻿using Com.Vmica.View.ControlSistema;
using COM_EPMS_DATA.DTO.ControlSistema;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Com.Vmica.Wrapper.ControlSistema
{
    public class wDetallePro
    {
        public static vDetallePro ConvertData(DetalleProDTO Data)
        {
            return new vDetallePro
            {
                Proceso = Data.Proceso,
                Fecha = Data.Fecha,
                Hora = Data.Hora,
                RegLeidos = Data.RegLeidos,
                RegCargados = Data.RegCargados,
                RegRechazados = Data.RegRechazados,
                FuenteEntrada = Data.FuenteEntrada,
                FuenteSalida = Data.FuenteSalida,
                PeriodicidadAct = Data.PeriodicidadAct,
                IntervaloAct = Data.IntervaloAct,
                DescArea = Data.DescArea,
                RespArea = Data.RespArea,
                DescError = Data.DescError,
                CodError = Data.CodError,
                Ejecutable = Data.Ejecutable
            };
        }

        public static DetalleProDTO ConvertData(vDetallePro Data)
        {
            return new DetalleProDTO
            {
                Proceso = Data.Proceso,
                Fecha = Data.Fecha,
                Hora = Data.Hora,
                RegLeidos = Data.RegLeidos,
                RegCargados = Data.RegCargados,
                RegRechazados = Data.RegRechazados,
                FuenteEntrada = Data.FuenteEntrada,
                FuenteSalida = Data.FuenteSalida,
                PeriodicidadAct = Data.PeriodicidadAct,
                IntervaloAct = Data.IntervaloAct,
                DescArea = Data.DescArea,
                RespArea = Data.RespArea,
                DescError = Data.DescError,
                CodError = Data.CodError,
                Ejecutable = Data.Ejecutable
            };
        }

        public static IList<vDetallePro> ConvertData(IList<DetalleProDTO> Data)
        {
            IList<vDetallePro> _l = new List<vDetallePro>();

            try
            {
                foreach (DetalleProDTO r in Data)
                {
                    vDetallePro _r = new vDetallePro()
                    {
                        Proceso = r.Proceso,
                        Fecha = r.Fecha,
                        Hora = r.Hora,
                        RegLeidos = r.RegLeidos,
                        RegCargados = r.RegCargados,
                        RegRechazados = r.RegRechazados,
                        FuenteEntrada = r.FuenteEntrada,
                        FuenteSalida = r.FuenteSalida,
                        PeriodicidadAct = r.PeriodicidadAct,
                        IntervaloAct = r.IntervaloAct,
                        DescArea = r.DescArea,
                        RespArea = r.RespArea,
                        DescError = r.DescError,
                        CodError = r.CodError,
                        Ejecutable = r.Ejecutable
                    };

                    _l.Add(_r);
                }
            }
            catch
            {
                throw new Exception("Err.W01: Problemas para convertir Datos [wDetalleProceso]");
            }

            return _l;

        }

        public static IList<DetalleProDTO> ConvertData(IList<vDetallePro> Data)
        {
            IList<DetalleProDTO> _l = new List<DetalleProDTO>();

            try
            {
                foreach (vDetallePro r in Data)
                {
                    DetalleProDTO _r = new DetalleProDTO()
                    {
                        Proceso = r.Proceso,
                        Fecha = r.Fecha,
                        Hora = r.Hora,
                        RegLeidos = r.RegLeidos,
                        RegCargados = r.RegCargados,
                        RegRechazados = r.RegRechazados,
                        FuenteEntrada = r.FuenteEntrada,
                        FuenteSalida = r.FuenteSalida,
                        PeriodicidadAct = r.PeriodicidadAct,
                        IntervaloAct = r.IntervaloAct,
                        DescArea = r.DescArea,
                        RespArea = r.RespArea,
                        DescError = r.DescError,
                        CodError = r.CodError,
                        Ejecutable = r.Ejecutable
                    };

                    _l.Add(_r);
                }
            }
            catch
            {
                throw new Exception("Err.W01: Problemas para convertir Datos [wDetalleProceso]");
            }

            return _l;

        }
    }
}
