﻿using Com.Vmica.View.ControlSistema;
using COM_EPMS_DATA.DTO.ControlSistema;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Com.Vmica.Wrapper.ControlSistema
{
    public class wEstadisticas
    {
        public static vEstadisticas ConvertData(EstadisticasDTO Data)
        {
            return new vEstadisticas
            {
                IdProceso=Data.IdProceso,
                Proceso=Data.Proceso,
                Fecha=Data.Fecha,
                ProcesosOk=Data.ProcesosOk,
                ProcesosError=Data.ProcesosError,
                Estado=Data.Estado,
                Porcentaje=Data.Porcentaje,
                RegCargados = Data.RegCargados,
                IdArea = Data.IdArea,
                Area = Data.Area
            };
        }

        public static EstadisticasDTO ConvertData(vEstadisticas Data)
        {
            return new EstadisticasDTO
            {
                IdProceso = Data.IdProceso,
                Proceso = Data.Proceso,
                Fecha = Data.Fecha,
                ProcesosOk = Data.ProcesosOk,
                ProcesosError = Data.ProcesosError,
                Estado = Data.Estado,
                Porcentaje = Data.Porcentaje,
                RegCargados=Data.RegCargados,
                IdArea = Data.IdArea,
                Area = Data.Area
            };
        }

        public static IList<vEstadisticas> ConvertData(IList<EstadisticasDTO> Data)
        {
            IList<vEstadisticas> _l = new List<vEstadisticas>();

            try
            {
                foreach (EstadisticasDTO r in Data)
                {
                    vEstadisticas _r = new vEstadisticas()
                    {
                        IdProceso = r.IdProceso,
                        Proceso = r.Proceso,
                        Fecha = r.Fecha,
                        ProcesosOk = r.ProcesosOk,
                        ProcesosError = r.ProcesosError,
                        Estado = r.Estado,
                        Porcentaje = r.Porcentaje,
                        RegCargados = r.RegCargados,
                        IdArea = r.IdArea,
                        Area = r.Area
                    };

                    _l.Add(_r);
                }
            }
            catch
            {
                throw new Exception("Err.W01: Problemas para convertir Datos [wAccesoAplicacion]");
            }

            return _l;

        }

        public static IList<EstadisticasDTO> ConvertData(IList<vEstadisticas> Data)
        {
            IList<EstadisticasDTO> _l = new List<EstadisticasDTO>();

            try
            {
                foreach (vEstadisticas r in Data)
                {
                    EstadisticasDTO _r = new EstadisticasDTO()
                    {
                        IdProceso = r.IdProceso,
                        Proceso = r.Proceso,
                        Fecha = r.Fecha,
                        ProcesosOk = r.ProcesosOk,
                        ProcesosError = r.ProcesosError,
                        Estado = r.Estado,
                        Porcentaje = r.Porcentaje,
                        RegCargados = r.RegCargados,
                        IdArea = r.IdArea,
                        Area = r.Area
                    };

                    _l.Add(_r);
                }
            }
            catch
            {
                throw new Exception("Err.W01: Problemas para convertir Datos [wAccesoAplicacion]");
            }

            return _l;

        }
    }
}
