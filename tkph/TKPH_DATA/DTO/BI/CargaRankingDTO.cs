﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TKPH_DATA.DTO.BI
{
    /// <summary>
    /// Clase Base con atributos para mostrar Datos de Ranking Carga 
    /// </summary>
    [Serializable]
    public class CargaRankingDTO
    {
        public String FlotaCamion { get; set; }
        public Double Valor { get; set; }
        public String Tipo { get; set; }
        public String Flota { get; set; }
        public Int64 AlarmaPunto { get; set; }
    }
}
