﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TKPH_DATA.DTO.BI.Export
{
    /// <summary>
    /// Clase Base con atributos para exportar Datos de Histograma TKPH 
    /// </summary>
    [Serializable]
    public class TkphHistogramaExpDTO
    {
        public Int64 IdCamion { get; set; }
        public String Flota { get; set; }
        public DateTime Fecha { get; set; }
        public Double Operacional { get; set; }
        public Double Nominal { get; set; }
    }
}
