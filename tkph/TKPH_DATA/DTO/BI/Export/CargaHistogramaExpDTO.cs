﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TKPH_DATA.DTO.BI.Export
{
    /// <summary>
    /// Clase Base con atributos para exportar Datos de Histograma de Carga 
    /// </summary>
    [Serializable]
    public class CargaHistogramaExpDTO
    {
        public String IdCamion { get; set; }
        public String FlotaCamion { get; set; }
        public DateTime Fecha { get; set; }
        public Int64 Carga { get; set; }
        public Int64 Nominal { get; set; }
    }
}
