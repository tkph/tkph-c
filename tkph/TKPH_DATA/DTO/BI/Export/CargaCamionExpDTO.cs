﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TKPH_DATA.DTO.BI.Export
{
    /// <summary>
    /// Clase Base con atributos para exportar Datos de Carga Camión 
    /// </summary>
    public class CargaCamionExpDTO
    {
        public DateTime Fecha { get; set; }
        public String IdCamion { get; set; }
        public Int64 Carga { get; set; }
        public Double Nominal { get; set; }
        public String Flota { get; set; }
    }
}
