﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TKPH_DATA.DTO.BI
{
    /// <summary>
    /// Clase Base con atributos para mostrar Datos de TKPH por Flota 
    /// </summary>
    [Serializable]
    public class TkphFlotaDTO
    {
        public String FlotaCamion { get; set; }
        public Double Promedio { get; set; }
        public Int64 TkphNominal { get; set; }
        public Double Porcentaje { get; set; }
        public Int64 AlarmaPunto { get; set; }
    }
}
