﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TKPH_DATA.DTO.BI
{
    /// <summary>
    /// Clase Base con atributos para mostrar Datos de Tkph por Destino 
    /// </summary>
    [Serializable]
    public class TkphDestinoDTO
    {
        public String Flota { get; set; }
        public String Destino { get; set; }
        public Double Porcentaje { get; set; }
        public Double PromTkphOpe { get; set; }
        public Int64 Nominal { get; set; }
        public String Tipo { get; set; }
        public Int64 AlarmaPunto { get; set; }
    }
}
