﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TKPH_DATA.DTO.BI
{
    /// <summary>
    /// Clase Base con atributos para mostrar Datos de Ranking TKPH 
    /// </summary>
    [Serializable]
    public class TkphRankingDTO
    {
        public String FlotaCamion { get; set; }
        public String Objeto { get; set; }
        public Int64 QEvento { get; set; }
        public Double Promedio { get; set; }
        public String Tipo { get; set; }
        public Int64 AlarmaPunto { get; set; }
    }
}
