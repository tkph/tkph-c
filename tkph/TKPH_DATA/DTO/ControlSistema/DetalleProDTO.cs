﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace COM_EPMS_DATA.DTO.ControlSistema
{
    public class DetalleProDTO
    {
        public String Proceso { get; set; }
        public String Fecha { get; set; }
        public String Hora { get; set; }
        public Int64 RegLeidos { get; set; }
        public Int64 RegCargados { get; set; }
        public Int64 RegRechazados { get; set; }
        public String FuenteEntrada { get; set; }
        public String FuenteSalida { get; set; }
        public String PeriodicidadAct { get; set; }
        public Int64 IntervaloAct { get; set; }
        public String DescArea { get; set; }
        public String RespArea { get; set; }
        public String DescError { get; set; }
        public String CodError { get; set; }
        public String Ejecutable { get; set; }
    }
}
