﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TKPH_DATA.DTO.Mantenedores
{
    /// <summary>
    /// Clase Base con atributos para Persistencia de valores mantenedores de Configuración
    /// </summary>
    public class InsertDatosMantenedorDTO
    {
        public String Ident { get; set; }
        public String ValorT { get; set; }
        public String Afisc { get; set; }

    }
}
