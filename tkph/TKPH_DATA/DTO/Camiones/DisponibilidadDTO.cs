﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TKPH_DATA.DTO.Camiones
{
    /// <summary>
    /// Clase Base con atributos para mostrar Disponibilidad Camión 
    /// </summary>
    public class DisponibilidadDTO
    {
        public String Fecha { get; set; }
        public String Turno { get; set; }
        public String Flota { get; set; }
        public Double Operativo { get; set; }
        public Double Armado { get; set; }
        public Double Porcentaje { get; set; }
    }
}
