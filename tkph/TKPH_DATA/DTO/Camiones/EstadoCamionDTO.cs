﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TKPH_DATA.DTO.Camiones
{
    /// <summary>
    /// Clase Base con atributos para mostrar Datos de Estado Camión 
    /// </summary>
    public class EstadoCamionDTO
    {
        public Int64 Valor { get; set; }
        public String Alerta { get; set; }
        public Int64 Total { get; set; }
    }
}
