﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Com.Vmica.Dto
{
    public class GenericoDTO
    {
        public String TipoParam { get; set; }
        public String OrigenParam { get; set; }
        public String DescOrigenParam { get; set; }
        public String DescInternaParam { get; set; }
        public String DescTipoParam { get; set; }
        public String Gerencia { get; set; }
        public Int64 Nivel { get; set; }
        public String Estado { get; set; }
    }
}
