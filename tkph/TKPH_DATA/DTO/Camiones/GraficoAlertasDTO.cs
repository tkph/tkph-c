﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TKPH_DATA.DTO.Camiones
{
    /// <summary>
    /// Clase Base con atributos para Generar graficos de alertas 
    /// </summary>
    public class GraficoAlertasDTO
    {
        public String Tagname { get; set; }
        public DateTime Fecha { get; set; }
        public String Units { get; set; }
        public Double Value { get; set; }
        public Int64 RangoInferior { get; set; }
        public Int64 RangoSuperior { get; set; }
        public Int32 Alerta { get; set; }
        public Int32 Flag { get; set; }
    }
}