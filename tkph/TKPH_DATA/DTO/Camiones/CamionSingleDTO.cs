﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TKPH_DATA.DTO.Camiones
{
    /// <summary>
    /// Clase Base con atributos para mostrar Datos de especificos del Camión 
    /// </summary>
    public class CamionSingleDTO
    {
       public string IdCamion { get; set; }
       public string AlertaRueda_1 { get; set; }
       public string AlertaRueda_2 { get; set; }
       public string AlertaRueda_3 { get; set; }
       public string AlertaRueda_4 { get; set; }
       public string AlertaRueda_5 { get; set; }
       public string AlertaRueda_6 { get; set; }
       public string AlertaTemp_1 { get; set; }
       public string AlertaTemp_2 { get; set; }
       public string AlertaTemp_3 { get; set; }
       public string AlertaTemp_4 { get; set; }
       public string AlertaTemp_5 { get; set; }
       public string AlertaTemp_6 { get; set; }
       public string AlertaPres_1 { get; set; }
       public string AlertaPres_2 { get; set; }
       public string AlertaPres_3 { get; set; }
       public string AlertaPres_4 { get; set; }
       public string AlertaPres_5 { get; set; }
       public string AlertaPres_6 { get; set; }
       public string TemperaturaCarga_1 { get; set; }
       public string TemperaturaCarga_2 { get; set; }
       public string TemperaturaCarga_3 { get; set; }
       public string TemperaturaCarga_4 { get; set; }
       public string TemperaturaCarga_5 { get; set; }
       public string TemperaturaCarga_6 { get; set; }
       public string Presion_1 { get; set; }
       public string Presion_2 { get; set; }
       public string Presion_3 { get; set; }
       public string Presion_4 { get; set; }
       public string Presion_5 { get; set; }
       public string Presion_6 { get; set; }
       public string Fecha { get; set; }
       public string DescFlota { get; set; }
    }
}
