﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TKPH_DATA.DTO.Camiones
{
    /// <summary>
    /// 
    /// </summary>
    public class StatusRequest
    {
        public String IdCamion { get; set; }
        public String DescripcionFlota { get; set; }
        public String AlertaGeneral { get; set; }
        public String AlertaTemperatura { get; set; }
        public String AlertaPresion { get; set; }
        public String AlertaCarga { get; set; }
        public String AlertaTkph { get; set; }
    }
}
