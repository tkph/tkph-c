﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TKPH_DATA.DTO.Camiones
{
    /// <summary>
    /// Clase Base con atributos para mostrar Datos de Notificación Camión 
    /// </summary>
    public class CamionNotificacionDTO
    {
        public String ID_CAMION { get; set; }
        public String TIPO_ALARMA { get; set; }
        public String UBICACION_ALARMA { get; set; }
        public String GRAVEDAD_ALARMA { get; set; }
        public String VALOR { get; set; }
        public String FECHA_ALARMA { get; set; }
        public String PALA { get; set; }
        public String OPERADOR_PALA { get; set; }
        public String OPERADOR_CAMION { get; set; }
        public String ID_DESTINO { get; set; }
        public String ID_TURNO { get; set; }
        public String GENERACION { get; set; }
    }
}
