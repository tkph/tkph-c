﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TKPH_DATA.DTO.Camiones
{
    /// <summary>
    /// Clase Base con atributos para mostrar Datos de Fechas minimas y maximas de información Camión 
    /// </summary>
    public class FechasMaximasDTO
    {
        public String IdCamion { get; set; }
        public String FUAME { get; set; }
        public String FUAMI { get; set; }
    }
}
