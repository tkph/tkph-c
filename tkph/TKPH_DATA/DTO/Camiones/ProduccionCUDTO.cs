﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Com.Vmica.Dto
{
    public class ProduccionCUDTO
    {
        public DateTime Fecha { get; set; }
        public Double ValueRel { get; set; }
        public Double ValueFor { get; set; }
        public Int32 Flag { get; set; }
        public Int32 Status { get; set; }
    }
}
