﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Com.Vmica.Dto
{
    public class ProduccionCatodosDTO
    {
        public DateTime Fecha { get; set; }
        public Double Esox { get; set; }
        public Double Off1 { get; set; }
        public Double Scrap { get; set; }

    }
}
