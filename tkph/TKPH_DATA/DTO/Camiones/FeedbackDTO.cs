﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace COM_EPMS_DATA.DTO
{
    public class FeedbackDTO
    {
        public String Comentario { get; set; }
        public String Url { get; set; }
        public String Nombre { get; set; }
        public Int64 Usuario { get; set; }
        public DateTime Fecha { get; set; }
    }
}
