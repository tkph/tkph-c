﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TKPH_DATA.DTO.Camiones
{
    /// <summary>
    /// Clase Base con atributos para mostrar Datos de Bitacora Camión 
    /// </summary>
    public class BitacoraCamionDTO
    {
        public String Fecha { get; set; }
        public String IdHora { get; set; }
        public String IdCamion { get; set; }
        public String Operador { get; set; }
        public String DescAlarma { get; set; }
        public Int64 RowN { get; set; }
    }
}
