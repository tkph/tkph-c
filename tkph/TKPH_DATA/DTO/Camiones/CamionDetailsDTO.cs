﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TKPH_DATA.DTO.Camiones
{
    /// <summary>
    /// Clase Base con atributos para mostrar Datos de Detalle Camión 
    /// </summary>
    public class CamionDetailsDTO
    {
        public Int64 IdCamion { get; set; }
        public Int64 Temperatura { get; set; }
        public Int64 Presion { get; set; }
        public Int64 Carga { get; set; }
        public Int64 Tkph { get; set; }
        public Int64 Neumatico { get; set; }
        public Int64 Alerta { get; set; }
        public String DescripcionFlota { get; set; }
        public String DescripcionFlotaAbre  { get; set; }
    }
}
