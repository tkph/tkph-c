﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TKPH_DATA.DTO.Camiones
{
    public class OxidosThroughputOLDTO
    {
        public String Tagname { get; set; }
        public String Fecha { get; set; }
        public String Units { get; set; }
        public Double Value { get; set; }
    }
}
