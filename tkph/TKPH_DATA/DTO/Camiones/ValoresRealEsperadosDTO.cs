﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Com.Vmica.Dto
{
    public class ValoresRealEsperadosDTO
    {
        public Int64 Car { get; set; }
        public String UniCar { get; set; }
        public Int64 Cae { get; set; }
        public String UniCae { get; set; }
        public Int64 Tcr { get; set; }
        public String UniTcr { get; set; }
        public Int32 Flag { get; set; }
    }
}
