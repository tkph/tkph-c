﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TKPH_DATA.DTO.Camiones
{
    /// <summary>
    /// Clase Base con atributos para mostrar Datos Promedio de Presión y Temperatura del Camión 
    /// </summary>
    [Serializable]
    public class PromAvgTempPresDTO
    {
        public Int64 id_camion { get; set; }
        public String max_presion_psi_1 { get; set; }
        public String max_presion_psi_2 { get; set; }
        public String max_presion_psi_3 { get; set; }
        public String max_presion_psi_4 { get; set; }
        public String max_presion_psi_5 { get; set; }
        public String max_presion_psi_6 { get; set; }
        public String min_presion_psi_1 { get; set; }
        public String min_presion_psi_2 { get; set; }
        public String min_presion_psi_3 { get; set; }
        public String min_presion_psi_4 { get; set; }
        public String min_presion_psi_5 { get; set; }
        public String min_presion_psi_6 { get; set; }
        public String avg_presion_psi_1 { get; set; }
        public String avg_presion_psi_2 { get; set; }
        public String avg_presion_psi_3 { get; set; }
        public String avg_presion_psi_4 { get; set; }
        public String avg_presion_psi_5 { get; set; }
        public String avg_presion_psi_6 { get; set; }
        public String max_temperatura_c_1 { get; set; }
        public String max_temperatura_c_2 { get; set; }
        public String max_temperatura_c_3 { get; set; }
        public String max_temperatura_c_4 { get; set; }
        public String max_temperatura_c_5 { get; set; }
        public String max_temperatura_c_6 { get; set; }
        public String min_temperatura_c_1 { get; set; }
        public String min_temperatura_c_2 { get; set; }
        public String min_temperatura_c_3 { get; set; }
        public String min_temperatura_c_4 { get; set; }
        public String min_temperatura_c_5 { get; set; }
        public String min_temperatura_c_6 { get; set; }
        public String avg_temperatura_c_1 { get; set; }
        public String avg_temperatura_c_2 { get; set; }
        public String avg_temperatura_c_3 { get; set; }
        public String avg_temperatura_c_4 { get; set; }
        public String avg_temperatura_c_5 { get; set; }
        public String avg_temperatura_c_6 { get; set; }
        public DateTime fecha_ini { get; set; }
        public DateTime fecha_fin { get; set; }
    }
}
