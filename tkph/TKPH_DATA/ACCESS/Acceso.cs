﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace TKPH_DATA.ACCESS
{
    /// <summary>
    /// Clase con metodos para Conexion y ejecucion de consultas SQL
    /// </summary>
    public class Acceso
    {
        /// <summary>
        /// Variable que contiene la cadena de conexion
        /// </summary>
        private String _connectionString;

        /// <summary>
        /// Iniciliza una instancia de la clase
        /// </summary>
        public Acceso()
        {
            _connectionString = ConfigurationManager.AppSettings["ConexionDB"].ToString();
        }

        /// <summary>
        /// Metodo para Ejecucion de consultas y Store Procedure sin Parametros
        /// </summary>
        /// <param name="_script">Valor correspondiente a la consulta SQL</param>
        /// <param name="_table">Objeto DataTable donde se almacenara el resultado de la consulta.</param>
        /// <exception cref="System.Exception">Error: Problemas de conexión o SP no existente</exception>
        public void CreateCommandSelect(String _script, DataTable _table)
        {
            try
            {
                using (SqlConnection _connection = new SqlConnection(_connectionString))
                {
                    _connection.Open();

                    using (SqlDataAdapter _adapter = new SqlDataAdapter(_script, _connection))
                    {
                        _adapter.Fill(_table);
                    }

                    _connection.Close();
                }
            }
            catch
            {
                throw new Exception("Error: Problemas de conexión o SP no existente");
            }
        }

        /// <summary>
        /// Metodo para Ejecucion de consultas y Store Procedure sin Parametros
        /// </summary>
        /// <param name="_script">Valor correspondiente a la consulta SQL</param>
        /// <param name="_parameter">Array de tipo SqlParameter que contiene los parametros a pasar a la consulta.</param>
        /// <param name="_table">Objeto DataTable donde se almacenara el resultado de la consulta.</param>
        /// <exception cref="System.Exception">Error: Problemas de conexión o SP no existente</exception>
        public void CreateCommandSelect(String _script, SqlParameter[] _parameter, DataTable _table)
        {
            try
            {
                using (SqlConnection _sqlconnection = new SqlConnection(_connectionString))
                {
                    _sqlconnection.Open();
                    using (SqlCommand _command = new SqlCommand(_script, _sqlconnection))
                    {
                        foreach (SqlParameter prm in _parameter)
                        {
                            _command.Parameters.AddWithValue(prm.ParameterName, prm.Value);
                        }
                        using (SqlDataReader _reader = _command.ExecuteReader(CommandBehavior.CloseConnection))
                        {
                            _table.Load(_reader);
                            _reader.Close();
                        }
                    }
                    _sqlconnection.Close();
                }
            }
            catch
            {
                throw new Exception("Error: Problemas de conexión o SP no existente");
            }
        }

        public string ConfigurationManger { get; set; }
    }
}
