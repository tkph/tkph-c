﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TKPH_DATA.DTO;
//using TKPH_DATA.DTO.ControlSistema;
//using TKPH_DATA.SP_SQL.ControlSistema;
using TKPH_DATA.DTO.Camiones;
using TKPH_DATA.SP_SQL.Camiones;
using TKPH_DATA.DTO.Mantenedores;
using TKPH_DATA.SP_SQL;
using TKPH_DATA.DTO.BI;
using TKPH_DATA.SP_SQL.BI;
using TKPH_DATA.DTO.BI.Export;
using TKPH_DATA.SP_SQL.Camiones;
using TKPH_DATA.SP_SQL.Mantendores;
using TKPH_DATA.SP_SQL.BI.Export;

namespace TKPH_DATA
{
    /// <summary>
    /// Clase con metodos de control de flujo de informacion entre llamadas de clases
    /// </summary>
    public class Orquestador
    {
        /// <summary>
        /// Encapsulamiento de instancia para clase "Orquestador"
        /// </summary>
        #region Encapsula Instancia
        private const string CLASS_NAME = "Orquestador";

        private static readonly Orquestador instance = new Orquestador();
        private Orquestador() { }
        public static Orquestador Instance
        {
            get { return instance; }
        }
        #endregion

        #region Metodos Status

        /// <summary>
        /// Control de llamada a metodo detalle Camión
        /// </summary>
        /// <param name="SingleStatus">Objeto de tipo "StatusRequest" con valores de entrada</param>
        /// <returns>Retorna Interfaz de lista de tipo "CamionDetailsDTO" con datos a desplegar.</returns>
        public IList<CamionDetailsDTO> DetailsByStatus(StatusRequest SingleStatus)
        {
            IList<CamionDetailsDTO> _list = new List<CamionDetailsDTO>();

            try
            {
                _list = Status.EstadoCamionDetails(SingleStatus);
            }
            catch (Exception e)
            {
                throw e;
            }

            return _list;
        }


        /// <summary>
        /// Control de llamada a metodo estado Camión
        /// </summary>
        /// <param name="SingleStatus">Objeto de tipo "StatusRequest" con valores de entrada</param>
        /// <returns>Retorna Objeto de tipo "CamionSingleDTO" con datos a desplegar.</returns>
        public CamionSingleDTO SingleByStatus(StatusRequest SingleStatus)
        {
            CamionSingleDTO _l = new CamionSingleDTO();

            try
            {
                _l = Status.EstadoCamionSingle(SingleStatus);
            }
            catch (Exception e)
            {
                throw e;
            }

            return _l;
        }
        #endregion

        #region Metodos Fechas

        /// <summary>
        /// Control de llamada a metodo para obtener fechas minimas y maximas
        /// </summary>
        /// <param name="Request">Objeto de tipo "FechasMaximasDTO" con valores de entrada</param>
        /// <returns>Retorna Objeto de tipo "FechasMaximasDTO" con datos a desplegar.</returns>
        public FechasMaximasDTO GetFechas(FechasMaximasDTO Request)
        {
            FechasMaximasDTO _f = new FechasMaximasDTO();
            try
            {
                _f = FechasMaximas.GetFechas(Request);
            }
            catch (Exception e)
            {
                throw e;
            }

            return _f;
        }
        #endregion

        #region Metodos Camion
        /// <summary>
        /// Control de llamada a metodo para obtener datos de Camión
        /// </summary>
        /// <param name="Request">Objeto de tipo "CamionDatosDTO" con valores de entrada</param>
        /// <returns>Retorna Objeto de tipo "CamionDatosDTO" con datos a desplegar.</returns>
        public CamionDatosDTO GetCamionDato(CamionDatosDTO Request)
        {
            CamionDatosDTO c = new CamionDatosDTO();

            try
            {
                c = CamionDato.GetCamionDatos(Request);
            }
            catch (Exception e)
            {
                throw e;
            }

            return c;
        }

        /// <summary>
        /// Control de llamada a metodo para obtener datos historicos TKPH y Carga Camión
        /// </summary>
        /// <param name="Request">Objeto de tipo "HistoricoTKPHCargaDTO" con valores de entrada</param>
        /// <returns>Retorna Interfaz de lista  de tipo "HistoricoTKPHCargaDTO" con datos a desplegar.</returns>
        public IList<HistoricoTKPHCargaDTO> GetHistorico(HistoricoTKPHCargaDTO Request)
        {
            IList<HistoricoTKPHCargaDTO> c = new List<HistoricoTKPHCargaDTO>();

            try
            {
                c = Historico.GetHistoricoCargaTKPH(Request);
            }
            catch (Exception e)
            {
                throw e;
            }

            return c;
        }

        /// <summary>
        /// Control de llamada a metodo para obtener datos historicos Presión y Temperatura Camión
        /// </summary>
        /// <param name="Request">Objeto de tipo "HitoricoPresionTempDTO" con valores de entrada</param>
        /// <returns>Retorna Interfaz de lista  de tipo "HitoricoPresionTempDTO" con datos a desplegar.</returns>
        public IList<HitoricoPresionTempDTO> GetHistoricoPTO(HitoricoPresionTempDTO Request)
        {
            IList<HitoricoPresionTempDTO> c = new List<HitoricoPresionTempDTO>();

            try
            {
                c = HistoricoPT.GetHitoricoPresionTemp(Request);
            }
            catch (Exception e)
            {
                throw e;
            }

            return c;
        }

        /// <summary>
        /// Control de llamada a metodo para obtener datos agrupados Presión y Temperatura Camión
        /// </summary>
        /// <param name="idcamion"Valor correspondiente al identificador del Camión</param>
        /// <returns>Retorna Interfaz de lista  de tipo "PromAvgTempPresDTO" con datos a desplegar.</returns>
        public IList<PromAvgTempPresDTO> getTempPres(Int64 idcamion)
        {
            IList<PromAvgTempPresDTO> c = new List<PromAvgTempPresDTO>();

            try
            {
                c = PromAvgTempPres.getTempPres(idcamion).ToList();
            }
            catch (Exception e)
            {
                throw e;
            }

            return c;
        }

        /// <summary>
        /// Control de llamada a metodo para obtener datos de notificación Camión
        /// </summary>
        /// <param name="idcamion"Valor correspondiente al identificador del Camión</param>
        /// <returns>Retorna Interfaz de lista  de tipo "CamionNotificacionDTO" con datos a desplegar.</returns>
        public IList<CamionNotificacionDTO> getCamionNotificacion(String idcamion)
        {
            IList<CamionNotificacionDTO> c = new List<CamionNotificacionDTO>();

            try
            {
                c = CamionNotificacion.getCamionNotificacion(idcamion).ToList();
            }
            catch (Exception e)
            {
                throw e;
            }

            return c;
        }

        /// <summary>
        /// Control de llamada a metodo para inserción de datos de notificación Camión
        /// </summary>
        /// <param name="idcamion"Valor correspondiente al identificador del Camión</param>
        /// <param name="comentario"Valor correspondiente al comentario o notificación</param>
        /// <returns>Retorna Interfaz de lista  de tipo "InsertCamionNotificacionDTO" con datos a desplegar.</returns>
        public IList<InsertCamionNotificacionDTO> setInsertCamionNotificacion(Int64 idcamion, String comentario)
        {
            IList<InsertCamionNotificacionDTO> c = new List<InsertCamionNotificacionDTO>();

            try
            {
                c = InsertCamionNotificacion.setInsertCamionNotificacion(idcamion, comentario).ToList();
            }
            catch (Exception e)
            {
                throw e;
            }

            return c;
        }

        #endregion

        #region Control Sistema
        /* NO IMPLEMENTADO
        public IList<EstadisticasDTO> GetEstadisticas(String IdProceso, Int64 TipoProp, String IdArea)
        {
            IList<EstadisticasDTO> _list = new List<EstadisticasDTO>();

            try
            {
                _list = Estadisticas.GetEstadisticas(IdProceso, TipoProp, IdArea);
            }
            catch (Exception e)
            {
                throw e;
            }

            return _list;
        }

        public IList<DetalleProDTO> GetProceso(Int64 Flag, String IdProceso)
        {
            IList<DetalleProDTO> _list = new List<DetalleProDTO>();

            try
            {
                _list = DetallePro.GetProceso(Flag, IdProceso);
            }
            catch (Exception e)
            {
                throw e;
            }

            return _list;
        }

         * */
        #endregion

        #region Mantenedores

        /// <summary>
        /// Control de llamada a metodo para obtener datos de Mantendor Carga
        /// </summary>
        /// <param name="Request">Objeto de tipo "CamionDatosDTO" con valores de entrada</param>
        /// <returns>Retorna Objeto de tipo "MantenedorCargaDTO" con datos a desplegar.</returns>
        public IList<MantenedorCargaDTO> GetDatosBudget(Int64 Tipo, Int64 Nivel_1, String Fiscal, String Flag, String Fecha)
        {
            IList<MantenedorCargaDTO> _list = new List<MantenedorCargaDTO>();

            try
            {
                _list = MantenedorCarga.GetDatosBudget(Tipo, Nivel_1, Fiscal, Flag, Fecha);
            }
            catch (Exception e)
            {
                throw e;
            }

            return _list;
        }


        /// <summary>
        /// Control de llamada a metodo para insertar datos en mantenedor Carga
        /// </summary>
        /// <param name="Ident">Valor correspondiente a identificador de  parametro a guardar.</param>
        /// <param name="ValorT">Valor a guardar.</param>
        /// <param name="Afisc">Año fiscal al cual corresponde.</param>
        /// <returns> Retorna Objeto de tipo "InsertDatosMantenedorDTO" con datos a desplegar.</returns>
        public IList<InsertDatosMantenedorDTO> SetDatosBudget(String Inden, String @ValorT, String Afisc)
        {
            IList<InsertDatosMantenedorDTO> _list = new List<InsertDatosMantenedorDTO>();

            try
            {
                _list = InsertDatosMantenedor.SetDatosBudget(Inden, @ValorT, Afisc);
            }
            catch (Exception e)
            {
                throw e;
            }

            return _list;
        }

        #endregion

        #region Graficos BI Carga

        /// <summary>
        /// Control de llamada a metodo para obtener datos de grafico Carga Flota
        /// </summary>
        /// <param name="flag">Valor que indica el tipo de dato a retornar.</param>
        /// <param name="idTurno">Valor correspondiente al turno.</param>
        /// <param name="fDesde">Fecha inicio para consulta de datos.</param>
        /// <param name="fHasta">Fecha termino para consulta de datos.</param>
        /// <param name="Objeto">Valor correspondiente a identificador de dato para realizar la busqueda</param>
        /// <returns>Retorna Objeto de tipo "CargaFlotaDTO" con datos a desplegar.</returns>
        public IList<CargaFlotaDTO> getCargaFlota(Int64 flag, Int64 idTurno, String fDesde, String fHasta, String Objeto)
        {
            IList<CargaFlotaDTO> c = new List<CargaFlotaDTO>();

            try
            {
                c = CargaFlota.getCargaFlota(flag, idTurno, fDesde, fHasta, Objeto);
            }
            catch (Exception e)
            {
                throw e;
            }

            return c;
        }

        /// <summary>
        /// Control de llamada a metodo para obtener datos de grafico Tendencia Carga
        /// </summary>
        /// <param name="flag">Valor que indica el tipo de dato a retornar.</param>
        /// <param name="idTurno">Valor correspondiente al turno.</param>
        /// <param name="fDesde">Fecha inicio para consulta de datos.</param>
        /// <param name="fHasta">Fecha termino para consulta de datos.</param>
        /// <param name="Objeto">Valor correspondiente a identificador de dato para realizar la busqueda</param>
        /// <returns>Retorna Objeto de tipo "CargaTendenciaDTO" con datos a desplegar.</returns>
        public IList<CargaTendenciaDTO> getCargaTendencia(Int64 flag, Int64 idTurno, String fDesde, String fHasta, String Objeto)
        {
            IList<CargaTendenciaDTO> c = new List<CargaTendenciaDTO>();

            try
            {
                c = CargaTendencia.getCargaTendencia(flag, idTurno, fDesde, fHasta, Objeto);
            }
            catch (Exception e)
            {
                throw e;
            }

            return c;
        }

        /// <summary>
        /// Control de llamada a metodo para obtener datos de grafico Histograma Carga
        /// </summary>
        /// <param name="flag">Valor que indica el tipo de dato a retornar.</param>
        /// <param name="idTurno">Valor correspondiente al turno.</param>
        /// <param name="fDesde">Fecha inicio para consulta de datos.</param>
        /// <param name="fHasta">Fecha termino para consulta de datos.</param>
        /// <param name="Objeto">Valor correspondiente a identificador de dato para realizar la busqueda</param>
        /// <returns>Retorna Objeto de tipo "CargaHistogramaDTO" con datos a desplegar.</returns>
        public IList<CargaHistogramaDTO> getCargaHistograma(Int64 flag, Int64 idTurno, String fDesde, String fHasta, String Objeto)
        {
            IList<CargaHistogramaDTO> c = new List<CargaHistogramaDTO>();

            try
            {
                c = CargaHistograma.getCargaHistograma(flag, idTurno, fDesde, fHasta, Objeto);
            }
            catch (Exception e)
            {
                throw e;
            }

            return c;
        }

        /// <summary>
        /// Control de llamada a metodo para obtener datos de grafico Ranking Carga
        /// </summary>
        /// <param name="flag">Valor que indica el tipo de dato a retornar.</param>
        /// <param name="idTurno">Valor correspondiente al turno.</param>
        /// <param name="fDesde">Fecha inicio para consulta de datos.</param>
        /// <param name="fHasta">Fecha termino para consulta de datos.</param>
        /// <param name="Objeto">Valor correspondiente a identificador de dato para realizar la busqueda</param>
        /// <returns>Retorna Objeto de tipo "CargaRankingDTO" con datos a desplegar.</returns>
        public IList<CargaRankingDTO> getCargaRanking(Int64 flag, Int64 idTurno, String fDesde, String fHasta, String Objeto)
        {
            IList<CargaRankingDTO> c = new List<CargaRankingDTO>();

            try
            {
                c = CargaRanking.getCargaRanking(flag, idTurno, fDesde, fHasta, Objeto);
            }
            catch (Exception e)
            {
                throw e;
            }

            return c;
        }


        #endregion

        #region Graficos BI Tkph

        /// <summary>
        /// Control de llamada a metodo para obtener datos de grafico Tkph Flota
        /// </summary>
        /// <param name="flag">Valor que indica el tipo de dato a retornar.</param>
        /// <param name="idTurno">Valor correspondiente al turno.</param>
        /// <param name="fDesde">Fecha inicio para consulta de datos.</param>
        /// <param name="fHasta">Fecha termino para consulta de datos.</param>
        /// <param name="Objeto">Valor correspondiente a identificador de dato para realizar la busqueda</param>
        /// <returns>Retorna Objeto de tipo "TkphFlotaDTO" con datos a desplegar.</returns>
        public IList<TkphFlotaDTO> getTkphFlota(Int64 flag, Int64 idTurno, String fDesde, String fHasta, String Objeto)
        {
            IList<TkphFlotaDTO> c = new List<TkphFlotaDTO>();

            try
            {
                c = TkphFlota.getTkphFlota(flag, idTurno, fDesde, fHasta, Objeto);
            }
            catch (Exception e)
            {
                throw e;
            }

            return c;
        }


        /// <summary>
        /// Control de llamada a metodo para obtener datos de grafico Tkph Destino
        /// </summary>
        /// <param name="flag">Valor que indica el tipo de dato a retornar.</param>
        /// <param name="idTurno">Valor correspondiente al turno.</param>
        /// <param name="fDesde">Fecha inicio para consulta de datos.</param>
        /// <param name="fHasta">Fecha termino para consulta de datos.</param>
        /// <param name="Objeto">Valor correspondiente a identificador de dato para realizar la busqueda</param>
        /// <returns>Retorna Objeto de tipo "TkphDestinoDTO" con datos a desplegar.</returns>
        public IList<TkphDestinoDTO> getTkphDestino(Int64 flag, Int64 idTurno, String fDesde, String fHasta, String Objeto)
        {
            IList<TkphDestinoDTO> c = new List<TkphDestinoDTO>();

            try
            {
                c = TkphDestino.getTkphDestino(flag, idTurno, fDesde, fHasta, Objeto);
            }
            catch (Exception e)
            {
                throw e;
            }

            return c;
        }


        /// <summary>
        /// Control de llamada a metodo para obtener datos de grafico Histograma Tkph
        /// </summary>
        /// <param name="flag">Valor que indica el tipo de dato a retornar.</param>
        /// <param name="idTurno">Valor correspondiente al turno.</param>
        /// <param name="fDesde">Fecha inicio para consulta de datos.</param>
        /// <param name="fHasta">Fecha termino para consulta de datos.</param>
        /// <param name="Objeto">Valor correspondiente a identificador de dato para realizar la busqueda</param>
        /// <returns>Retorna Objeto de tipo "TkphHistogramaDTO" con datos a desplegar.</returns>
        public IList<TkphHistogramaDTO> getTkphHistograma(Int64 flag, Int64 idTurno, String fDesde, String fHasta, String Objeto)
        {
            IList<TkphHistogramaDTO> c = new List<TkphHistogramaDTO>();

            try
            {
                c = TkphHistograma.getTkphHistograma(flag, idTurno, fDesde, fHasta, Objeto);
            }
            catch (Exception e)
            {
                throw e;
            }

            return c;
        }


        /// <summary>
        /// Control de llamada a metodo para obtener datos de grafico Ranking Tkph
        /// </summary>
        /// <param name="flag">Valor que indica el tipo de dato a retornar.</param>
        /// <param name="idTurno">Valor correspondiente al turno.</param>
        /// <param name="fDesde">Fecha inicio para consulta de datos.</param>
        /// <param name="fHasta">Fecha termino para consulta de datos.</param>
        /// <param name="Objeto">Valor correspondiente a identificador de dato para realizar la busqueda</param>
        /// <returns>Retorna Objeto de tipo "TkphRankingDTO" con datos a desplegar.</returns>
        public IList<TkphRankingDTO> getTkphRanking(Int64 flag, Int64 idTurno, String fDesde, String fHasta, String Objeto)
        {
            IList<TkphRankingDTO> c = new List<TkphRankingDTO>();

            try
            {
                c = TkphRanking.getTkphRanking(flag, idTurno, fDesde, fHasta, Objeto);
            }
            catch (Exception e)
            {
                throw e;
            }

            return c;
        }


        /// <summary>
        /// Control de llamada a metodo para obtener datos de grafico Tendencia Tkph
        /// </summary>
        /// <param name="flag">Valor que indica el tipo de dato a retornar.</param>
        /// <param name="idTurno">Valor correspondiente al turno.</param>
        /// <param name="fDesde">Fecha inicio para consulta de datos.</param>
        /// <param name="fHasta">Fecha termino para consulta de datos.</param>
        /// <param name="Objeto">Valor correspondiente a identificador de dato para realizar la busqueda</param>
        /// <returns>Retorna Objeto de tipo "TkphTendenciaDTO" con datos a desplegar.</returns>
        public IList<TkphTendenciaDTO> getTkphTendencia(Int64 flag, Int64 idTurno, String fDesde, String fHasta, String Objeto)
        {
            IList<TkphTendenciaDTO> c = new List<TkphTendenciaDTO>();

            try
            {
                c = TkphTendencia.getTkphTendencia(flag, idTurno, fDesde, fHasta, Objeto);
            }
            catch (Exception e)
            {
                throw e;
            }

            return c;
        }

        #endregion

        #region Exportar BI Carga

        /// <summary>
        /// Control de llamada a metodo para obtener datos a exportar en Histograma Carga
        /// </summary>
        /// <param name="flag">Valor que indica el tipo de dato a retornar.</param>
        /// <param name="idTurno">Valor correspondiente al turno.</param>
        /// <param name="fDesde">Fecha inicio para consulta de datos.</param>
        /// <param name="fHasta">Fecha termino para consulta de datos.</param>
        /// <param name="Objeto">Valor correspondiente a identificador de dato para realizar la busqueda</param>
        /// <returns>Retorna Objeto de tipo "CargaHistogramaExpDTO" con datos a desplegar.</returns>
        public IList<CargaHistogramaExpDTO> getCargaHistogramaData(Int64 flag, Int64 idTurno, String fDesde, String fHasta, String Objeto)
        {
            IList<CargaHistogramaExpDTO> c = new List<CargaHistogramaExpDTO>();

            try
            {
                c = CargaHistogramaExp.getCargaHistogramaData(flag, idTurno, fDesde, fHasta, Objeto);
            }
            catch (Exception e)
            {
                throw e;
            }

            return c;
        }


        /// <summary>
        /// Control de llamada a metodo para obtener datos a exportar en Tendencia Carga
        /// </summary>
        /// <param name="flag">Valor que indica el tipo de dato a retornar.</param>
        /// <param name="idTurno">Valor correspondiente al turno.</param>
        /// <param name="fDesde">Fecha inicio para consulta de datos.</param>
        /// <param name="fHasta">Fecha termino para consulta de datos.</param>
        /// <param name="Objeto">Valor correspondiente a identificador de dato para realizar la busqueda</param>
        /// <returns>Retorna Objeto de tipo "CargaTendenciaExpDTO" con datos a desplegar.</returns>
        public IList<CargaTendenciaExpDTO> getCargaTendenciaData(Int64 flag, Int64 idTurno, String fDesde, String fHasta, String Objeto)
        {
            IList<CargaTendenciaExpDTO> c = new List<CargaTendenciaExpDTO>();

            try
            {
                c = CargaTendenciaExp.getCargaTendenciaData(flag, idTurno, fDesde, fHasta, Objeto);
            }
            catch (Exception e)
            {
                throw e;
            }

            return c;
        }


        /// <summary>
        /// Control de llamada a metodo para obtener datos a exportar en Camión Carga
        /// </summary>
        /// <param name="flag">Valor que indica el tipo de dato a retornar.</param>
        /// <param name="idTurno">Valor correspondiente al turno.</param>
        /// <param name="fDesde">Fecha inicio para consulta de datos.</param>
        /// <param name="fHasta">Fecha termino para consulta de datos.</param>
        /// <param name="Objeto">Valor correspondiente a identificador de dato para realizar la busqueda</param>
        /// <returns>Retorna Objeto de tipo "CargaCamionExpDTO" con datos a desplegar.</returns>
        public IList<CargaCamionExpDTO> getCargaCamionData(Int64 flag, Int64 idTurno, String fDesde, String fHasta, String Objeto)
        {
            IList<CargaCamionExpDTO> c = new List<CargaCamionExpDTO>();

            try
            {
                c = CargaCamionExp.getCargaCamionData(flag, idTurno, fDesde, fHasta, Objeto);
            }
            catch (Exception e)
            {
                throw e;
            }

            return c;
        }


        /// <summary>
        /// Control de llamada a metodo para obtener datos a exportar en Pala Carga
        /// </summary>
        /// <param name="flag">Valor que indica el tipo de dato a retornar.</param>
        /// <param name="idTurno">Valor correspondiente al turno.</param>
        /// <param name="fDesde">Fecha inicio para consulta de datos.</param>
        /// <param name="fHasta">Fecha termino para consulta de datos.</param>
        /// <param name="Objeto">Valor correspondiente a identificador de dato para realizar la busqueda</param>
        /// <returns>Retorna Objeto de tipo "CargaPalaExpDTO" con datos a desplegar.</returns>
        public IList<CargaPalaExpDTO> getCargaPalaData(Int64 flag, Int64 idTurno, String fDesde, String fHasta, String Objeto)
        {
            IList<CargaPalaExpDTO> c = new List<CargaPalaExpDTO>();

            try
            {
                c = CargaPalaExp.getCargaPalaData(flag, idTurno, fDesde, fHasta, Objeto);
            }
            catch (Exception e)
            {
                throw e;
            }

            return c;
        }

        #endregion

        #region Exportar BI Tkph

        /// <summary>
        /// Control de llamada a metodo para obtener datos a exportar en Histograma Tkph
        /// </summary>
        /// <param name="flag">Valor que indica el tipo de dato a retornar.</param>
        /// <param name="idTurno">Valor correspondiente al turno.</param>
        /// <param name="fDesde">Fecha inicio para consulta de datos.</param>
        /// <param name="fHasta">Fecha termino para consulta de datos.</param>
        /// <param name="Objeto">Valor correspondiente a identificador de dato para realizar la busqueda</param>
        /// <returns>Retorna Objeto de tipo "TkphHistogramaExpDTO" con datos a desplegar.</returns>
        public IList<TkphHistogramaExpDTO> getTkphHistogramaData(Int64 flag, Int64 idTurno, String fDesde, String fHasta, String Objeto)
        {
            IList<TkphHistogramaExpDTO> c = new List<TkphHistogramaExpDTO>();

            try
            {
                c = TkphHistogramaExp.getTkphHistogramaData(flag, idTurno, fDesde, fHasta, Objeto);
            }
            catch (Exception e)
            {
                throw e;
            }

            return c;
        }


        /// <summary>
        /// Control de llamada a metodo para obtener datos a exportar en Tendencia Tkph
        /// </summary>
        /// <param name="flag">Valor que indica el tipo de dato a retornar.</param>
        /// <param name="idTurno">Valor correspondiente al turno.</param>
        /// <param name="fDesde">Fecha inicio para consulta de datos.</param>
        /// <param name="fHasta">Fecha termino para consulta de datos.</param>
        /// <param name="Objeto">Valor correspondiente a identificador de dato para realizar la busqueda</param>
        /// <returns>Retorna Objeto de tipo "TkphTendenciaExpDTO" con datos a desplegar.</returns>
        public IList<TkphTendenciaExpDTO> getTkphTendenciaData(Int64 flag, Int64 idTurno, String fDesde, String fHasta, String Objeto)
        {
            IList<TkphTendenciaExpDTO> c = new List<TkphTendenciaExpDTO>();

            try
            {
                c = TkphTendenciaExp.getTkphTendenciaData(flag, idTurno, fDesde, fHasta, Objeto);
            }
            catch (Exception e)
            {
                throw e;
            }

            return c;
        }



        /// <summary>
        /// Control de llamada a metodo para obtener datos a exportar en Camión Tkph
        /// </summary>
        /// <param name="flag">Valor que indica el tipo de dato a retornar.</param>
        /// <param name="idTurno">Valor correspondiente al turno.</param>
        /// <param name="fDesde">Fecha inicio para consulta de datos.</param>
        /// <param name="fHasta">Fecha termino para consulta de datos.</param>
        /// <param name="Objeto">Valor correspondiente a identificador de dato para realizar la busqueda</param>
        /// <returns>Retorna Objeto de tipo "TkphCamionExpDTO" con datos a desplegar.</returns>
        public IList<TkphCamionExpDTO> getTkphCamionData(Int64 flag, Int64 idTurno, String fDesde, String fHasta, String Objeto)
        {
            IList<TkphCamionExpDTO> c = new List<TkphCamionExpDTO>();

            try
            {
                c = TkphCamionExp.getTkphCamionData(flag, idTurno, fDesde, fHasta, Objeto);
            }
            catch (Exception e)
            {
                throw e;
            }

            return c;
        }


        /// <summary>
        /// Control de llamada a metodo para obtener datos a exportar en Operador Tkph
        /// </summary>
        /// <param name="flag">Valor que indica el tipo de dato a retornar.</param>
        /// <param name="idTurno">Valor correspondiente al turno.</param>
        /// <param name="fDesde">Fecha inicio para consulta de datos.</param>
        /// <param name="fHasta">Fecha termino para consulta de datos.</param>
        /// <param name="Objeto">Valor correspondiente a identificador de dato para realizar la busqueda</param>
        /// <returns>Retorna Objeto de tipo "TkphOperadorExpDTO" con datos a desplegar.</returns>
        public IList<TkphOperadorExpDTO> getTkphOperadorData(Int64 flag, Int64 idTurno, String fDesde, String fHasta, String Objeto)
        {
            IList<TkphOperadorExpDTO> c = new List<TkphOperadorExpDTO>();

            try
            {
                c = TkphOperadorExp.getTkphOperadorData(flag, idTurno, fDesde, fHasta, Objeto);
            }
            catch (Exception e)
            {
                throw e;
            }

            return c;
        }

        /// <summary>
        /// Control de llamada a metodo para obtener datos a exportar en Destino Tkph
        /// </summary>
        /// <param name="flag">Valor que indica el tipo de dato a retornar.</param>
        /// <param name="idTurno">Valor correspondiente al turno.</param>
        /// <param name="fDesde">Fecha inicio para consulta de datos.</param>
        /// <param name="fHasta">Fecha termino para consulta de datos.</param>
        /// <param name="Objeto">Valor correspondiente a identificador de dato para realizar la busqueda</param>
        /// <returns>Retorna Objeto de tipo "TkphDestinoExpDTO" con datos a desplegar.</returns>
        public IList<TkphDestinoExpDTO> getTkphDestinoData(Int64 flag, Int64 idTurno, String fDesde, String fHasta, String Objeto)
        {
            IList<TkphDestinoExpDTO> c = new List<TkphDestinoExpDTO>();

            try
            {
                c = TkphDestinoExp.getTkphDestinoData(flag, idTurno, fDesde, fHasta, Objeto);
            }
            catch (Exception e)
            {
                throw e;
            }

            return c;
        }
        

        #endregion

        #region Login
        /// <summary>
        /// Llamada al metodo de consulta SQL que consulta existencia de datos.
        /// </summary>
        /// <param name="Usuario">Nombre de Usuario</param>
        /// <param name="Password">Password del Usuario</param>
        /// <returns>Retorna objeto tipo "LoginDTO"</returns>
        public LoginDTO getLogin(String Usuario, String Password)
        {
           LoginDTO c = new LoginDTO();

            try
            {
                c = Login.getLogin(Usuario,Password);
            }
            catch (Exception e)
            {
                throw e;
            }

            return c;
        }

        #endregion

    }
}
