﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using TKPH_DATA.ACCESS;

namespace TKPH_DATA
{
    /// <summary>
    /// Clase con metodo para obtener nombres de Stored Procedure según parametros
    /// </summary>
    public class NombreSP
    {
        /// <summary>
        /// Metodo que obtiene el nombre de Stored Procedure según ID
        /// </summary>
        /// <param name="ID_SP">Valor correspondiente al identificador del stores procedure .</param>
        /// <returns>Retorna el nombre store procedure correspondinte al ID ingresado.</returns>
        public static String Nombre_SP(Int64 ID_SP)
        {
            Acceso _con = new Acceso();
            String _resultado = "";

            try
            {
                DataTable _tab = new DataTable();

                SqlParameter[] _sqlpara = new SqlParameter[]
                {
                    new SqlParameter("@ID_SP", ID_SP)
                };

                _con.CreateCommandSelect("exec sp_param_nombreSP @ID_SP", _sqlpara, _tab);

                DataRow row = _tab.Rows[0];

                _resultado = row["nombre_sp"].ToString();

            }
            catch (Exception e)
            {
                throw e;
            }

            return _resultado;   
        }

    }
}
