﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using TKPH_DATA.ACCESS;
using TKPH_DATA.DTO.BI;

namespace TKPH_DATA.SP_SQL.BI
{
    /// <summary>
    /// Clase con metodos para llenado de listas con resultados de consultas a SQL para despliege de datos TKPH Flota
    /// </summary>
    public class TkphFlota
    {
        /// <summary>
        /// Metodo que realiza llamada a Stored Procedure con parametros para obtener datos TKPH Flota
        /// </summary>
        /// <param name="flag">Valor que indica el tipo de dato a retornar.</param>
        /// <param name="idTurno">Valor correspondiente al turno.</param>
        /// <param name="fDesde">Fecha inicio para consulta de datos.</param>
        /// <param name="fHasta">Fecha termino para consulta de datos.</param>
        /// <param name="Objeto">Valor correspondiente a identificador de dato para realizar la busqueda</param>
        /// <returns>Retorna Interfaz de Lista de "TkphFlotaDTO" con datos a deplegar.</returns>
        public static IList<TkphFlotaDTO> getTkphFlota(Int64 flag, Int64 idTurno, String fDesde, String fHasta, String Objeto)
        {
            Acceso _con = new Acceso();

            IList<TkphFlotaDTO> _list = new List<TkphFlotaDTO>();

            try
            {
                SqlParameter[] sqlParams = new SqlParameter[]
                {
                    new SqlParameter("@flag",flag),
                    new SqlParameter("@idturno",idTurno),
                    new SqlParameter("@fecha_desde",fDesde),
                    new SqlParameter("@fecha_hasta",fHasta),
                    new SqlParameter("@idobjeto",Objeto)
                };

                DataTable _t = new DataTable();

                _con.CreateCommandSelect("exec " + NombreSP.Nombre_SP(78).ToString() + " @flag, @idturno, @fecha_desde, @fecha_hasta, @idobjeto", sqlParams, _t);

                _list = LeeFlota(_t);
            }
            catch (Exception e)
            {
                throw e;
            }

            return _list;
        }


        /// <summary>
        /// Metodo que realiza asignación del valores desde la consulta a la Interfaz de Lista
        /// </summary>
        /// <param name="table">Objeto tipo DataTable que contiene el resultado de la consulta SQL</param>
        /// <returns>Retorna Interfaz de lista de tipo "TkphFlotaDTO" </returns>
        /// <exception cref="System.Exception">ERR.t01: Convert to Entity DTO Tkph Flota</exception>
        private static IList<TkphFlotaDTO> LeeFlota(DataTable table)
        {
            IList<TkphFlotaDTO> _l = new List<TkphFlotaDTO>();

            try
            {
                foreach (DataRow row in table.Rows)
                {

                    TkphFlotaDTO _r = new TkphFlotaDTO()
                    {
                        FlotaCamion = row["DESC_FLOTACAMION"].ToString(),
                        Promedio = Double.Parse(row["PROM"].ToString()),
                        TkphNominal = Int64.Parse(row["TKPH_NOMINAL"].ToString()),
                        Porcentaje = Double.Parse(row["PORCENTAJE"].ToString()),
                        AlarmaPunto = Int64.Parse(row["ALARMA_PUNTO"].ToString())
                    };

                    _l.Add(_r);

                }
            }
            catch (Exception e)
            {
                throw new Exception("ERR.t01: Convert to Entity DTO Tkph Flota", e.InnerException);
            }

            return _l;
        }

    }
}
