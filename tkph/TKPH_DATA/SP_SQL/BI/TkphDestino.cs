﻿using TKPH_DATA.DTO.BI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using TKPH_DATA.ACCESS;

namespace TKPH_DATA.SP_SQL.BI
{
    /// <summary>
    /// Clase con metodos para llenado de listas con resultados de consultas a SQL para despliege de datos TKPH Destino
    /// </summary>
    public class TkphDestino
    {
        /// <summary>
        /// Metodo que realiza llamada a Stored Procedure con parametros para obtener datos TKPH Destino
        /// </summary>
        /// <param name="flag">Valor que indica el tipo de dato a retornar.</param>
        /// <param name="idTurno">Valor correspondiente al turno.</param>
        /// <param name="fDesde">Fecha inicio para consulta de datos.</param>
        /// <param name="fHasta">Fecha termino para consulta de datos.</param>
        /// <param name="Objeto">Valor correspondiente a identificador de dato para realizar la busqueda</param>
        /// <returns>Retorna Interfaz de Lista de "TkphDestinoDTO" con datos a deplegar.</returns>
        public static IList<TkphDestinoDTO> getTkphDestino(Int64 flag, Int64 idTurno, String fDesde, String fHasta, String Objeto)
        {
            Acceso _con = new Acceso();

            IList<TkphDestinoDTO> _list = new List<TkphDestinoDTO>();

            try
            {
                SqlParameter[] sqlParams = new SqlParameter[]
                {
                    new SqlParameter("@flag",flag),
                    new SqlParameter("@idturno",idTurno),
                    new SqlParameter("@fecha_desde",fDesde),
                    new SqlParameter("@fecha_hasta",fHasta),
                    new SqlParameter("@idobjeto",Objeto)
                };

                DataTable _t = new DataTable();

                _con.CreateCommandSelect("exec " + NombreSP.Nombre_SP(78).ToString() + " @flag, @idturno, @fecha_desde, @fecha_hasta, @idobjeto", sqlParams, _t);

                _list = LeeDestino(_t);
            }
            catch (Exception e)
            {
                throw e;
            }

            return _list;
        }


        /// <summary>
        /// Metodo que realiza asignación del valores desde la consulta a la Interfaz de Lista
        /// </summary>
        /// <param name="table">Objeto tipo DataTable que contiene el resultado de la consulta SQL</param>
        /// <returns>Retorna Interfaz de lista de tipo "TkphDestinoDTO" </returns>
        /// <exception cref="System.Exception">ERR.t01: Convert to Entity DTO Tkph Destino</exception>
        private static IList<TkphDestinoDTO> LeeDestino(DataTable table)
        {
            IList<TkphDestinoDTO> _l = new List<TkphDestinoDTO>();

            try
            {
                foreach (DataRow row in table.Rows)
                {

                    TkphDestinoDTO _r = new TkphDestinoDTO()
                    {
                        Flota = row["FLOTA"].ToString(),
                        Destino = row["ID_DESTINO"].ToString(),
                        Porcentaje = Double.Parse(row["PORCENTAJE"].ToString()),
                        PromTkphOpe = Double.Parse(row["PROM_TKPH_OPERACIONAL"].ToString()),
                        Nominal = Int64.Parse(row["NOMINAL"].ToString()),
                        Tipo = row["TIPO"].ToString(),
                        AlarmaPunto = Int64.Parse(row["ALARMA_PUNTO"].ToString())
                    };

                    _l.Add(_r);

                }
            }
            catch (Exception e)
            {
                throw new Exception("ERR.t01: Convert to Entity DTO Tkph Destino", e.InnerException);
            }

            return _l;
        }

    }
}
