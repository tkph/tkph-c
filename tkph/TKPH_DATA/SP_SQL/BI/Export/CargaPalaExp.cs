﻿using TKPH_DATA.DTO.BI;
using TKPH_DATA.DTO.BI.Export;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using TKPH_DATA.ACCESS;

namespace TKPH_DATA.SP_SQL.BI.Export
{
    /// <summary>
    /// Clase con metodos para llenado de listas con resultados de consultas a SQL para Exportar Carga Pala
    /// </summary>
    public class CargaPalaExp
    {
        /// <summary>
        /// Metodo que realiza llamada a Stored Procedure con parametros para obtener datos carga pala
        /// </summary>
        /// <param name="flag">Valor que indica el tipo de dato a retornar.</param>
        /// <param name="idTurno">Valor correspondiente al turno.</param>
        /// <param name="fDesde">Fecha inicio para consulta de datos.</param>
        /// <param name="fHasta">Fecha termino para consulta de datos.</param>
        /// <param name="Objeto">Valor correspondiente a identificador de dato para realizar la busqueda</param>
        /// <returns>Retorna Interfaz de Lista de "CargaPalaExpDTO" con datos a exportar.</returns>
        public static IList<CargaPalaExpDTO> getCargaPalaData(Int64 flag, Int64 idTurno, String fDesde, String fHasta, String Objeto)
        {
            Acceso _con = new Acceso();

            IList<CargaPalaExpDTO> _list = new List<CargaPalaExpDTO>();

            try
            {
                SqlParameter[] sqlParams = new SqlParameter[]
                {
                    new SqlParameter("@flag",flag),
                    new SqlParameter("@idturno",idTurno),
                    new SqlParameter("@fecha_desde",fDesde),
                    new SqlParameter("@fecha_hasta",fHasta),
                    new SqlParameter("@idobjeto",Objeto)
                };

                DataTable _t = new DataTable();

                _con.CreateCommandSelect("exec " + NombreSP.Nombre_SP(77).ToString() + " @flag, @idturno, @fecha_desde, @fecha_hasta, @idobjeto", sqlParams, _t);

                _list = LeePala(_t);
            }
            catch (Exception e)
            {
                throw e;
            }

            return _list;
        }


        /// <summary>
        /// Metodo que realiza asignación del valores desde la consulta a la Interfaz de Lista 
        /// </summary>
        /// <param name="table">Objeto tipo DataTable que contiene el resultado de la consulta SQL</param>
        /// <returns>Retorna Interfaz de lista de tipo "CargaPalaExpDTO"</returns>
        /// <exception cref="System.Exception">ERR.t01: Convert to Entity DTO Carga Pala</exception>
        private static IList<CargaPalaExpDTO> LeePala(DataTable table)
        {
            IList<CargaPalaExpDTO> _l = new List<CargaPalaExpDTO>();

            try
            {
                foreach (DataRow row in table.Rows)
                {

                    CargaPalaExpDTO _r = new CargaPalaExpDTO()
                    {
                        Fecha = DateTime.Parse(row["FECHA_LLEGADA_FRENTE_CARGUIO"].ToString()),
                        Pala = row["PALA"].ToString(),
                        Carga = Int64.Parse(row["CARGA"].ToString()),
                        Flota = row["FLOTA"].ToString()
                    };

                    _l.Add(_r);

                }
            }
            catch (Exception e)
            {
                throw new Exception("ERR.t01: Convert to Entity DTO Carga Pala", e.InnerException);
            }

            return _l;
        }

    }
}
