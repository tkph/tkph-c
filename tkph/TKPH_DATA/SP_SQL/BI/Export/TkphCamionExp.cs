﻿using TKPH_DATA.DTO.BI;
using TKPH_DATA.DTO.BI.Export;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using TKPH_DATA.ACCESS;

namespace TKPH_DATA.SP_SQL.BI.Export
{
    /// <summary>
    /// Clase con metodos para llenado de listas con resultados de consultas a SQL para Exportar Tkph Camión
    /// </summary>
    public class TkphCamionExp
    {
        /// <summary>
        /// Metodo que realiza llamada a Stored Procedure con parametros para obtener datos Tkph Camión
        /// </summary>
        /// <param name="flag">Valor que indica el tipo de dato a retornar.</param>
        /// <param name="idTurno">Valor correspondiente al turno.</param>
        /// <param name="fDesde">Fecha inicio para consulta de datos.</param>
        /// <param name="fHasta">Fecha termino para consulta de datos.</param>
        /// <param name="Objeto">Valor correspondiente a identificador de dato para realizar la busqueda</param>
        /// <returns>Retorna Interfaz de Lista de "TkphCamionExpDTO" con datos a exportar.</returns>
        public static IList<TkphCamionExpDTO> getTkphCamionData(Int64 flag, Int64 idTurno, String fDesde, String fHasta, String Objeto)
        {
            Acceso _con = new Acceso();

            IList<TkphCamionExpDTO> _list = new List<TkphCamionExpDTO>();

            try
            {
                SqlParameter[] sqlParams = new SqlParameter[]
                {
                    new SqlParameter("@flag",flag),
                    new SqlParameter("@idturno",idTurno),
                    new SqlParameter("@fecha_desde",fDesde),
                    new SqlParameter("@fecha_hasta",fHasta),
                    new SqlParameter("@idobjeto",Objeto)
                };

                DataTable _t = new DataTable();

                _con.CreateCommandSelect("exec " + NombreSP.Nombre_SP(78).ToString() + " @flag, @idturno, @fecha_desde, @fecha_hasta, @idobjeto", sqlParams, _t);

                _list = LeeCamion(_t);
            }
            catch (Exception e)
            {
                throw e;
            }

            return _list;
        }


        /// <summary>
        /// Metodo que realiza asignación del valores desde la consulta a la Interfaz de Lista 
        /// </summary>
        /// <param name="table">Objeto tipo DataTable que contiene el resultado de la consulta SQL</param>
        /// <returns>Retorna Interfaz de lista de tipo "TkphCamionExpDTO"</returns>
        /// <exception cref="System.Exception">ERR.t01: Convert to Entity DTO Tkph Camion</exception>
        private static IList<TkphCamionExpDTO> LeeCamion(DataTable table)
        {
            IList<TkphCamionExpDTO> _l = new List<TkphCamionExpDTO>();

            try
            {
                foreach (DataRow row in table.Rows)
                {

                    TkphCamionExpDTO _r = new TkphCamionExpDTO()
                    {
                        Fecha = DateTime.Parse(row["FECHA"].ToString()),
                        FechaCarguio = DateTime.Parse(row["FECHA_LLEGADA_FRENTE_CARGUIO"].ToString()),
                        Flota = row["DESC_FLOTACAMION"].ToString(),
                        IdCamion = Int64.Parse(row["ID_CAMION"].ToString()),
                        Carga  = Int64.Parse(row["CARGA"].ToString()),
                        CargaMedia = Double.Parse(row["QM"].ToString()),
                        TiempoTotal = Double.Parse(row["TC"].ToString()),
                        ConstanteK2 = Double.Parse(row["K2"].ToString()),
                        VelocidadMedia = Double.Parse(row["VM"].ToString()),
                        Temperatura = Int64.Parse(row["TEMPERATURA_AMBIENTE"].ToString()),
                        Operacional = Double.Parse(row["TKPH_OPERACIONAL"].ToString()),
                        Nominal = Double.Parse(row["TKPH_NOMINAL"].ToString())
                    };

                    _l.Add(_r);

                }
            }
            catch (Exception e)
            {
                throw new Exception("ERR.t01: Convert to Entity DTO Tkph Camion", e.InnerException);
            }

            return _l;
        }

    }
}
