﻿using TKPH_DATA.DTO.BI;
using TKPH_DATA.DTO.BI.Export;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using TKPH_DATA.ACCESS;

namespace TKPH_DATA.SP_SQL.BI.Export
{
    /// <summary>
    /// Clase con metodos para llenado de listas con resultados de consultas a SQL para Exportar Histograma Tkph
    /// </summary>
    public class TkphHistogramaExp
    {
        /// <summary>
        /// Metodo que realiza llamada a Stored Procedure con parametros para obtener datos Histograma Tkph
        /// </summary>
        /// <param name="flag">Valor que indica el tipo de dato a retornar.</param>
        /// <param name="idTurno">Valor correspondiente al turno.</param>
        /// <param name="fDesde">Fecha inicio para consulta de datos.</param>
        /// <param name="fHasta">Fecha termino para consulta de datos.</param>
        /// <param name="Objeto">Valor correspondiente a identificador de dato para realizar la busqueda</param>
        /// <returns>Retorna Interfaz de Lista de "TkphHistogramaExpDTO" con datos a exportar.</returns>
        public static IList<TkphHistogramaExpDTO> getTkphHistogramaData(Int64 flag, Int64 idTurno, String fDesde, String fHasta, String Objeto)
        {
            Acceso _con = new Acceso();

            IList<TkphHistogramaExpDTO> _list = new List<TkphHistogramaExpDTO>();

            try
            {
                SqlParameter[] sqlParams = new SqlParameter[]
                {
                    new SqlParameter("@flag",flag),
                    new SqlParameter("@idturno",idTurno),
                    new SqlParameter("@fecha_desde",fDesde),
                    new SqlParameter("@fecha_hasta",fHasta),
                    new SqlParameter("@idobjeto",Objeto)
                };

                DataTable _t = new DataTable();

                _con.CreateCommandSelect("exec " + NombreSP.Nombre_SP(78).ToString() + " @flag, @idturno, @fecha_desde, @fecha_hasta, @idobjeto", sqlParams, _t);

                _list = LeeHistograma(_t);
            }
            catch (Exception e)
            {
                throw e;
            }

            return _list;
        }


        /// <summary>
        /// Metodo que realiza asignación del valores desde la consulta a la Interfaz de Lista
        /// </summary>
        /// <param name="table">Objeto tipo DataTable que contiene el resultado de la consulta SQL</param>
        /// <returns>Retorna Interfaz de lista de tipo "TkphCamionExpDTO" </returns>
        /// <exception cref="System.Exception">ERR.t01: Convert to Entity DTO Tkph Histograma</exception>
        private static IList<TkphHistogramaExpDTO> LeeHistograma(DataTable table)
        {
            IList<TkphHistogramaExpDTO> _l = new List<TkphHistogramaExpDTO>();

            try
            {
                foreach (DataRow row in table.Rows)
                {

                    TkphHistogramaExpDTO _r = new TkphHistogramaExpDTO()
                    {
                        IdCamion = Int64.Parse(row["ID_CAMION"].ToString()),
                        Flota = row["DESC_FLOTACAMION"].ToString(),
                        Fecha = DateTime.Parse(row["FECHA"].ToString()),
                        Operacional = Double.Parse(row["TKPH_OPERACIONAL"].ToString()),
                        Nominal = Double.Parse(row["TKPH_NOMINAL"].ToString())
                    };

                    _l.Add(_r);

                }
            }
            catch (Exception e)
            {
                throw new Exception("ERR.t01: Convert to Entity DTO Tkph Histograma", e.InnerException);
            }

            return _l;
        }

    }
}
