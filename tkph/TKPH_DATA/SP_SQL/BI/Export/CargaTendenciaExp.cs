﻿using TKPH_DATA.DTO.BI;
using TKPH_DATA.DTO.BI.Export;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using TKPH_DATA.ACCESS;

namespace TKPH_DATA.SP_SQL.BI.Export
{
    /// <summary>
    /// Clase con metodos para llenado de listas con resultados de consultas a SQL para Exportar Tendencia Carga
    /// </summary>
    public class CargaTendenciaExp
    {
        /// <summary>
        /// Metodo que realiza llamada a Stored Procedure con parametros para obtener datos tendencia carga
        /// </summary>
        /// <param name="flag">Valor que indica el tipo de dato a retornar.</param>
        /// <param name="idTurno">Valor correspondiente al turno.</param>
        /// <param name="fDesde">Fecha inicio para consulta de datos.</param>
        /// <param name="fHasta">Fecha termino para consulta de datos.</param>
        /// <param name="Objeto">Valor correspondiente a identificador de dato para realizar la busqueda</param>
        /// <returns>Retorna Interfaz de Lista de "CargaTendenciaExpDTO" con datos a exportar.</returns>
        public static IList<CargaTendenciaExpDTO> getCargaTendenciaData(Int64 flag, Int64 idTurno, String fDesde, String fHasta, String Objeto)
        {
            Acceso _con = new Acceso();

            IList<CargaTendenciaExpDTO> _list = new List<CargaTendenciaExpDTO>();

            try
            {
                SqlParameter[] sqlParams = new SqlParameter[]
                {
                    new SqlParameter("@flag",flag),
                    new SqlParameter("@idturno",idTurno),
                    new SqlParameter("@fecha_desde",fDesde),
                    new SqlParameter("@fecha_hasta",fHasta),
                    new SqlParameter("@idobjeto",Objeto)
                };

                DataTable _t = new DataTable();

                _con.CreateCommandSelect("exec " + NombreSP.Nombre_SP(77).ToString() + " @flag, @idturno, @fecha_desde, @fecha_hasta, @idobjeto", sqlParams, _t);

                _list = LeeTendencia(_t);
            }
            catch (Exception e)
            {
                throw e;
            }

            return _list;
        }


        /// <summary>
        /// Metodo que realiza asignación del valores desde la consulta a la Interfaz de Lista 
        /// </summary>
        /// <param name="table">Objeto tipo DataTable que contiene el resultado de la consulta SQL</param>
        /// <returns>Retorna Interfaz de lista de tipo "CargaTendenciaExpDTO"</returns>
        /// <exception cref="System.Exception">ERR.t01: Convert to Entity DTO Carga Camion</exception>
        private static IList<CargaTendenciaExpDTO> LeeTendencia(DataTable table)
        {
            IList<CargaTendenciaExpDTO> _l = new List<CargaTendenciaExpDTO>();

            try
            {
                foreach (DataRow row in table.Rows)
                {

                    CargaTendenciaExpDTO _r = new CargaTendenciaExpDTO()
                    {
                        IdCamion = row["ID_CAMION"].ToString(),
                        FlotaCamion = row["DESC_FLOTACAMION"].ToString(),
                        Fecha = DateTime.Parse(row["FECHA_LLEGADA_FRENTE_CARGUIO"].ToString()),
                        Carga = Int64.Parse(row["CARGA"].ToString()),
                        Nominal = Int64.Parse(row["CARGA_NOMINAL"].ToString()),
                    };

                    _l.Add(_r);

                }
            }
            catch (Exception e)
            {
                throw new Exception("ERR.t01: Convert to Entity DTO Carga Tendencia", e.InnerException);
            }

            return _l;
        }

    }
}
