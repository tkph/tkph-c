﻿using TKPH_DATA.DTO.BI;
using TKPH_DATA.DTO.BI.Export;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using TKPH_DATA.ACCESS;

namespace TKPH_DATA.SP_SQL.BI.Export
{
    /// <summary>
    /// Clase con metodos para llenado de listas con resultados de consultas a SQL para Exportar Tkph Destino
    /// </summary>
    public class TkphDestinoExp
    {
        /// <summary>
        /// Metodo que realiza llamada a Stored Procedure con parametros para obtener datos Tkph Destino
        /// </summary>
        /// <param name="flag">Valor que indica el tipo de dato a retornar.</param>
        /// <param name="idTurno">Valor correspondiente al turno.</param>
        /// <param name="fDesde">Fecha inicio para consulta de datos.</param>
        /// <param name="fHasta">Fecha termino para consulta de datos.</param>
        /// <param name="Objeto">Valor correspondiente a identificador de dato para realizar la busqueda</param>
        /// <returns>Retorna Interfaz de Lista de "TkphDestinoExpDTO" con datos a exportar.</returns>
        public static IList<TkphDestinoExpDTO> getTkphDestinoData(Int64 flag, Int64 idTurno, String fDesde, String fHasta, String Objeto)
        {
            Acceso _con = new Acceso();

            IList<TkphDestinoExpDTO> _list = new List<TkphDestinoExpDTO>();

            try
            {
                SqlParameter[] sqlParams = new SqlParameter[]
                {
                    new SqlParameter("@flag",flag),
                    new SqlParameter("@idturno",idTurno),
                    new SqlParameter("@fecha_desde",fDesde),
                    new SqlParameter("@fecha_hasta",fHasta),
                    new SqlParameter("@idobjeto",Objeto)
                };

                DataTable _t = new DataTable();

                _con.CreateCommandSelect("exec " + NombreSP.Nombre_SP(78).ToString() + " @flag, @idturno, @fecha_desde, @fecha_hasta, @idobjeto", sqlParams, _t);

                _list = LeeDestino(_t);
            }
            catch (Exception e)
            {
                throw e;
            }

            return _list;
        }


        /// <summary>
        /// Metodo que realiza asignación del valores desde la consulta a la Interfaz de Lista
        /// </summary>
        /// <param name="table">Objeto tipo DataTable que contiene el resultado de la consulta SQL</param>
        /// <returns>Retorna Interfaz de lista de tipo "TkphCamionExpDTO"</returns>
        /// <exception cref="System.Exception">ERR.t01: Convert to Entity DTO Tkph Destino</exception>
        private static IList<TkphDestinoExpDTO> LeeDestino(DataTable table)
        {
            IList<TkphDestinoExpDTO> _l = new List<TkphDestinoExpDTO>();

            try
            {
                foreach (DataRow row in table.Rows)
                {

                    TkphDestinoExpDTO _r = new TkphDestinoExpDTO()
                    {
                        Fecha = DateTime.Parse(row["FECHA"].ToString()),
                        Flota = row["FLOTA"].ToString(),
                        IdDestino = row["ID_DESTINO"].ToString(),
                        Operacional = Double.Parse(row["TKPH_OPERACIONAL"].ToString()),
                        Nominal = Double.Parse(row["NOMINAL"].ToString())
                    };

                    _l.Add(_r);

                }
            }
            catch (Exception e)
            {
                throw new Exception("ERR.t01: Convert to Entity DTO Tkph Destino", e.InnerException);
            }

            return _l;
        }

    }
}
