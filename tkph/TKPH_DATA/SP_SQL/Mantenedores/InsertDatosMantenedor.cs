﻿using TKPH_DATA.DTO.Mantenedores;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using TKPH_DATA.ACCESS;

namespace TKPH_DATA.SP_SQL.Mantendores
{
    /// <summary>
    /// Clase con metodos para llenado de listas con resultados de consultas a SQL para inserción de datos Mantenedores
    /// </summary>
    public class InsertDatosMantenedor
    {
        /// <summary>
        /// Metodo que realiza llamada a Stored Procedure con parametros para inserción de datos Mantenedores 
        /// </summary>
        /// <param name="Ident">Valor correspondiente al identificador del valor a insertar</param>
        /// <param name="ValorT">Corresponde al valor a insertar</param>
        /// <param name="Afisc">Valor correspondiente al año fiscal al cual pertence el valor</param>
        /// <returns>Retorna interfaz de lista de tipo "InsertDatosMantenedorDTO" con datos insertados.</returns>
        public static IList<InsertDatosMantenedorDTO> SetDatosBudget(String Ident, String ValorT, String Afisc)
        {
            Acceso _con = new Acceso();

            IList<InsertDatosMantenedorDTO> _l = new List<InsertDatosMantenedorDTO>();

            try
            {
                DataTable _tab = new DataTable();

                SqlParameter[] _sqlpara = new SqlParameter[]
                {
                    new SqlParameter("@identificador", Ident),
                    new SqlParameter("@ValorT", ValorT),
                    new SqlParameter("@Afiscal", Afisc)
                };

                _con.CreateCommandSelect("exec " + NombreSP.Nombre_SP(68).ToString() + " @identificador, @ValorT, @Afiscal", _sqlpara, _tab);

            }
            catch (Exception e)
            {
                throw e;
            }
            return _l;
        }
    }
}
