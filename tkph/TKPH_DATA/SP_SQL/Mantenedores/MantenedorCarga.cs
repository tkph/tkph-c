﻿using TKPH_DATA.DTO.Mantenedores;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using TKPH_DATA.ACCESS;

namespace TKPH_DATA.SP_SQL.Mantendores
{
    /// <summary>
    /// Clase con metodos para llenado de listas con resultados de consultas a SQL para obtener datos de Mantenedores
    /// </summary>
    public class MantenedorCarga
    {
        /// <summary>
        /// Metodo que realiza llamada a Stored Procedure con parametros para obtener configuración del modulo de Carga
        /// </summary>
        /// <param name="Tipo">Valor correspondiente al tipo de Mantendor</param>
        /// <param name="Nivel_1">Valor correspondinte al primer nivel de Jerarquico</param>
        /// <param name="Fiscal">Año fiscal al cual corresponden los datos.</param>
        /// <param name="Flag">Valor correspondiente a estado de configuración</param>
        /// <param name="Fecha">Valor correspondiente a la fecha o periodo consultado</param>
        /// <returns>Retorna Interfaz de lista tipo "MantenedorCargaDTO" con datos de carga.</returns>
        public static IList<MantenedorCargaDTO> GetDatosBudget(Int64 Tipo, Int64 Nivel_1, String Fiscal, String Flag, String Fecha)
        {
            Acceso _con = new Acceso();

            IList<MantenedorCargaDTO> _l = new List<MantenedorCargaDTO>();

            try
            {
                DataTable _tab = new DataTable();

                SqlParameter[] _sqlpara = new SqlParameter[]
                {
                    new SqlParameter("@tipo", Tipo),
                    new SqlParameter("@nivel_1", Nivel_1),
                    new SqlParameter("@afiscal", Fiscal),
                    new SqlParameter("@flag", Flag),
                    new SqlParameter("@fecha", Fecha)
                };

                _con.CreateCommandSelect("exec " + NombreSP.Nombre_SP(67).ToString() + " @tipo, @nivel_1, @afiscal, @flag, @fecha", _sqlpara, _tab);

                _l = EntityList(_tab);
            }
            catch (Exception e)
            {
                throw e;
            }
            return _l;
        }


        /// <summary>
        /// Metodo que realiza asignación del valores desde la consulta a la Interfaz de Lista
        /// </summary>
        /// <param name="_t">Objeto tipo DataTable que contiene el resultado de la consulta SQL</param>
        /// <returns>Retorna Interfaz de lista de tipo "MantenedorCargaDTO"</returns>
        /// <exception cref="System.Exception">ERR.t01: Convert to Entity DTO Mantenedor Carga</exception>
        private static IList<MantenedorCargaDTO> EntityList(DataTable _t)
        {
            IList<MantenedorCargaDTO> _l = new List<MantenedorCargaDTO>();

            try
            {
                foreach (DataRow row in _t.Rows)
                {

                    MantenedorCargaDTO _r = new MantenedorCargaDTO()
                    {
                        Mes = row["ID_MES"].ToString(),
                        Dia = Convert.ToDateTime(row["ID_DIA"].ToString()),
                        Item = Convert.ToInt64(row["ID_MANT_ITEM"].ToString()),
                        Tipo = row["DESC_MANT_TIPO"].ToString(),
                        Nivel_1 = row["DESC_MANT_NIVEL_1"].ToString(),
                        Nivel_2 = row["DESC_MANT_NIVEL_2"].ToString(),
                        Nivel_3 = row["DESC_MANT_NIVEL_3"].ToString(),
                        valor = Convert.ToDouble(row["VALOR"].ToString()),
                        AFiscal = row["ANO_FISCAL"].ToString(),
                        Mes_pl = row["ID_MES_PL"].ToString(),
                        Id_tipo = row["ID_MANT_TIPO"].ToString(),
                        Id_nivel_1 = row["ID_MANT_NIVEL_1"].ToString(),
                        Unidad = row["DESC_UNIDAD_MEDIDA"].ToString(),
                        Period = row["DESC_PERIODICIDAD"].ToString()
                    };

                    _l.Add(_r);
                }
            }
            catch (Exception e)
            {
                throw new Exception("ERR.t01: Convert to Entity DTO Mantenedor Carga", e.InnerException);
            }

            return _l;
        }

    }
}
