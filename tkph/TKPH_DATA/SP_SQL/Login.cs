﻿using TKPH_DATA.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using TKPH_DATA.ACCESS;

namespace TKPH_DATA.SP_SQL
{
    public class Login
    {
        /// <summary>
        /// Metodo de consulta en la base de datos a traves de "Stored Procedure"
        /// </summary>
        /// <param name="Usuario">Nombre de Usuario</param>
        /// <param name="Password">Password del Usuario</param>
        /// <returns>Retorna objeto tipo "LoginDTO"</returns>
        /// <remarks>El N° del SP correspondiente a la consulta de datos de usuario es 79, que es enviado al metodo "Nombre_SP"</remarks>
        public static LoginDTO getLogin(String Usuario, String Password)
        {
            Acceso _con = new Acceso();

            LoginDTO login = new LoginDTO();

            try
            {
                SqlParameter[] sqlParams = new SqlParameter[]
                {
                    new SqlParameter("@Usuario",Usuario),
                    new SqlParameter("@Password",Password)
                    
                };

                DataTable _t = new DataTable();

                _con.CreateCommandSelect("exec " + NombreSP.Nombre_SP(79).ToString() + " @Usuario, @Password", sqlParams, _t);

                login = LeeLogin(_t);
            }
            catch (Exception e)
            {
                throw e;
            }

            return login;
        }

        /// <summary>
        /// Parsea y llena el objeto "LoginDTO" con el resultado de la consulta SQL.
        /// </summary>
        /// <param name="table">Corresponde al set de datos devueltos por la consulta SQL.</param>
        /// <returns>Retorna objeto tipo "LoginDTO"</returns>
        /// <exception cref="Exception">Error al ejecutar Consulta, "InnerException"</exception>
        private static LoginDTO LeeLogin(DataTable table)
        {
            LoginDTO _l = new LoginDTO();

            try
            {

                if (table.Rows.Count == 0)
                {
                    _l.Nombre = "";
                    _l.Usuario = "";
                    _l.Vigente = false;
                }
                else
                {
                    DataRow row = table.Rows[0];

                    _l.Nombre = row["nombre"].ToString();
                    _l.Usuario = row["usuario"].ToString();
                    _l.Vigente = (row["vigente"].ToString() == "1") ? true : false;
                }


            }
            catch (Exception e)
            {
                throw new Exception("Error al ejecutar Consulta, ", e.InnerException);
            }

            return _l;
        }

    }
}
