﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Com.Vmica.Con;
using Com.Vmica.Dto;
using COM_EPMS_DATA;

namespace Com.Vmica.Data
{
    public class GenericoFiltros
    {
        public static IList<GenericoDTO> ParametroGenerico()
        {
            Acceso _con = new Acceso();

            IList<GenericoDTO> _l = new List<GenericoDTO>();

            try
            {
                DataTable _t = new DataTable();

                _con.CreateCommandSelect("exec " + NombreSP.Nombre_SP(19).ToString(), _t);

                _l = EntityList(_t);
            }
            catch (Exception e)
            {
                throw e;
            }

            return _l;
        }

        private static IList<GenericoDTO> EntityList(DataTable table)
        {
            IList<GenericoDTO> _l = new List<GenericoDTO>();

            try
            {
                foreach (DataRow row in table.Rows)
                {
                    GenericoDTO _r = new GenericoDTO()
                    {
                        TipoParam = row["TIPO_PAR"].ToString(),
                        OrigenParam = row["ORIGEN_PAR"].ToString(),
                        DescOrigenParam = row["DESC_ORIGEN_PAR"].ToString(),
                        DescInternaParam = row["DESC_INT_PAR"].ToString(),
                        DescTipoParam = row["DESC_TIPO_PAR"].ToString(),
                        Gerencia = row["GERENCIA"].ToString(),
                        Nivel = Convert.ToInt64(row["NIVEL"].ToString()),
                        Estado = row["ESTADO"].ToString()
                    };

                    _l.Add(_r);
                }
            }
            catch (Exception e)
            {
                throw new Exception("ERR.t01: Convert to Entity DTO Parametros Genericos", e.InnerException);
            }

            return _l;
        }
    }
}
