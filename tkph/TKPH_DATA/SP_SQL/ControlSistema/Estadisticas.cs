﻿using COM_EPMS_DATA.ACCESS;
using COM_EPMS_DATA.DTO.ControlSistema;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace COM_EPMS_DATA.SP_SQL.ControlSistema
{
    public class Estadisticas
    {
        public static IList<EstadisticasDTO> GetEstadisticas(String IdProceso, Int64 TipoProp, String IdArea)
        {
            AccesoSistema _con = new AccesoSistema();

            IList<EstadisticasDTO> _l = new List<EstadisticasDTO>();

            try
            {
                DataTable _tab = new DataTable();

                SqlParameter[] _sqlpara = new SqlParameter[]
                {
                    new SqlParameter("@idproceso_mcp", IdProceso),
                    new SqlParameter("@tipo_procp", TipoProp),
                    new SqlParameter("@id_area", IdArea)
                };

                _con.CreateCommandSelect("exec " + NombreSP.Nombre_SP(64).ToString() + " @idproceso_mcp, @tipo_procp, @id_area", _sqlpara, _tab);

                _l = EntityList(_tab);
            }
            catch (Exception e)
            {
                throw e;
            }
            return _l;
        }

        private static IList<EstadisticasDTO> EntityList(DataTable _t)
        {
            IList<EstadisticasDTO> _l = new List<EstadisticasDTO>();

            try
            {
                foreach (DataRow row in _t.Rows)
                {
                    DateTime F = Convert.ToDateTime(row["FECHA"].ToString());
                    EstadisticasDTO _r = new EstadisticasDTO()
                    {
                        IdProceso =  Convert.ToInt64(row["ID_PROCESO_MCP"].ToString()),
                        Proceso = row["DESC_PROCESO_MCP"].ToString(),
                        Fecha = F.ToString("dd-MM-yyyy"),
                        ProcesosOk = Convert.ToInt64(row["Q_PROCESO"].ToString()),
                        ProcesosError = Convert.ToInt64(row["Q_ERRORES"].ToString()),
                        Estado = Convert.ToInt64(row["ESTADO"].ToString()),
                        Porcentaje = Convert.ToDecimal(row["F_PORCENTAJE_EJEC"].ToString()),
                        RegCargados = Convert.ToInt64(row["F_CANT_REG_CARGADOS_MCP"].ToString()),
                        IdArea = Convert.ToInt64(row["ID_AREA_MCP"].ToString()),
                        Area = row["DESC_AREA_MCP"].ToString()
                    };

                    _l.Add(_r);
                }
            }
            catch (Exception e)
            {
                throw new Exception("ERR.t01: Convert to Entity DTO ControlSistema", e.InnerException);
            }

            return _l;
        }
    }
}
