﻿using COM_EPMS_DATA.ACCESS;
using COM_EPMS_DATA.DTO.ControlSistema;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace COM_EPMS_DATA.SP_SQL.ControlSistema
{
    public class DetallePro
    {
        public static IList<DetalleProDTO> GetProceso(Int64 Flag, String IdProceso)
        {
            AccesoSistema _con = new AccesoSistema();

            IList<DetalleProDTO> _l = new List<DetalleProDTO>();

            try
            {
                DataTable _tab = new DataTable();

                SqlParameter[] _sqlpara = new SqlParameter[]
                {
                    new SqlParameter("@flag", Flag),
                    new SqlParameter("@idproceso_mcp", IdProceso)
                };

                _con.CreateCommandSelect("exec " + NombreSP.Nombre_SP(66).ToString() + " @flag, @idproceso_mcp", _sqlpara, _tab);

                _l = EntityList(_tab);
            }
            catch (Exception e)
            {
                throw e;
            }
            return _l;
        }

        private static IList<DetalleProDTO> EntityList(DataTable _t)
        {
            IList<DetalleProDTO> _l = new List<DetalleProDTO>();

            try
            {
                foreach (DataRow row in _t.Rows)
                {



                    //String[] FH = row["FECHA"].ToString().Split(' ');

                    DateTime FH = Convert.ToDateTime(row["FECHA"].ToString());

                    String F = FH.ToString("dd-MM-yyyy");
                    String H = FH.ToString("HH:mm:ss");

                    DetalleProDTO _r = new DetalleProDTO()
                    {
                        Proceso = row["DESC_PROCESO_MCP"].ToString(),
                        Fecha = F,
                        Hora = H,
                        RegLeidos = Convert.ToInt64(row["F_CANT_REG_LEIDOS_MCP"].ToString()),
                        RegCargados = Convert.ToInt64(row["F_CANT_REG_CARGADOS_MCP"].ToString()),
                        RegRechazados = Convert.ToInt64(row["F_CANT_REG_RECHAZADOS_MCP"].ToString()),
                        FuenteEntrada = row["FUENTE_ENTRADA_PRINCIPAL_MCP"].ToString(),
                        FuenteSalida = row["FUENTE_SALIDA_PRINCIPAL_MCP"].ToString(),
                        PeriodicidadAct = row["FRECUENCIA_MCP"].ToString(),
                        IntervaloAct = Convert.ToInt64(row["F_VALOR_FRECUENCIA_MCP"].ToString()),
                        DescArea = row["DESC_AREA_MCP"].ToString(),
                        RespArea = row["RESPONSABLE_AREA_MCP"].ToString(),
                        DescError = row["DESC_ERROR_MCP"].ToString(),
                        CodError = row["CODIGO_ERROR_MCP"].ToString(),
                        Ejecutable = row["EJECUTABLE_MCP"].ToString()
                    };

                    _l.Add(_r);
                }
            }
            catch (Exception e)
            {
                throw new Exception("ERR.t01: Convert to Entity DTO ControlSistema", e.InnerException);
            }

            return _l;
        }
    }
}
