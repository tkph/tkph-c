﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using TKPH_DATA;
using TKPH_DATA.ACCESS;
using TKPH_DATA.DTO.Camiones;

namespace TKPH_DATA.SP_SQL.Camiones
{
    /// <summary>
    /// Clase con metodos para llenado de listas con resultados de consultas a SQL para despliege de datos Notificaciones Camión
    /// </summary>
    public class Bitacora
    {
        /// <summary>
        /// Metodo que realiza llamada a Stored Procedure con parametros para insercion de  Notificaciones Camión
        /// </summary>
        /// <param name="Notificacion">Objeto tipo "BitacoraCamionDTO" con valores de entrada</param>
        /// <returns>Retorna Interfaz de Lista tipo "BitacoraCamionDTO" con datos a deplegar.</returns>
        public static IList<BitacoraCamionDTO> InsertNotificacion(BitacoraCamionDTO Notificacion)
        {
            Acceso _con = new Acceso();

            IList<BitacoraCamionDTO> _l = new List<BitacoraCamionDTO>();

            try
            {
                SqlParameter[] sqlParams = new SqlParameter[]
                {
                    new SqlParameter("@fecha",Notificacion.Fecha),
                    new SqlParameter("@id_hhmmss",Notificacion.IdHora),
                    new SqlParameter("@id_camion",Notificacion.IdCamion),
                    new SqlParameter("@Operador",Notificacion.Operador),
                    new SqlParameter("@Desc_alarma",Notificacion.DescAlarma)
                };

                DataTable _t = new DataTable();

                _con.CreateCommandSelect("exec " + NombreSP.Nombre_SP(2).ToString() + " @fecha, @id_hhmmss, @id_camion, @Operador, @Desc_alarma", sqlParams, _t);

                _l = ListaNotificacion(Notificacion);
            }
            catch (Exception e)
            {
                throw e;
            }

            return _l;
        }


        /// <summary>
        /// Metodo que realiza llamada a Stored Procedure con parametros para listar datos  Notificaciones Camión
        /// </summary>
        /// <param name="RequestBitacora">Objeto tipo "BitacoraCamionDTO" con valores de entrada</param>
        /// <returns>Retorna Interfaz de Lista tipo "BitacoraCamionDTO" con datos a deplegar.</returns>
        public static IList<BitacoraCamionDTO> ListaNotificacion(BitacoraCamionDTO RequestBitacora)
        {
            Acceso _con = new Acceso();

            IList<BitacoraCamionDTO> _l = new List<BitacoraCamionDTO>();

            try
            {
                DataTable _t = new DataTable();

                SqlParameter[] sqlParams = new SqlParameter[]
                {
                    new SqlParameter("@id_camion",RequestBitacora.IdCamion),
                    new SqlParameter("@Operador",RequestBitacora.Operador)
                };

                _con.CreateCommandSelect("exec " + NombreSP.Nombre_SP(1).ToString() + " @id_camion, @Operador", sqlParams, _t);

                _l = EntityList(_t);
            }
            catch (Exception e)
            {
                throw e;
            }
            return _l;
        }


        /// <summary>
        /// Metodo que realiza asignación del valores desde la consulta a la Interfaz de Lista
        /// </summary>
        /// <param name="table">Objeto tipo DataTable que contiene el resultado de la consulta SQL</param>
        /// <returns>Retorna Interfaz de lista de tipo "BitacoraCamionDTO" </returns>
        /// <exception cref="System.Exception">ERR.t01: Convert to Entity DTO Notificaciones Camion</exception>
        private static IList<BitacoraCamionDTO> EntityList(DataTable table)
        {
            IList<BitacoraCamionDTO> _l = new List<BitacoraCamionDTO>();

            try
            {
                foreach (DataRow row in table.Rows)
                {
                    BitacoraCamionDTO _r = new BitacoraCamionDTO()
                    {
                        Fecha = row["fecha"].ToString(),
                        IdHora = row["id_hhmmss"].ToString(),
                        IdCamion = row["id_camion"].ToString(),
                        Operador = row["Operador"].ToString(),
                        DescAlarma = row["Desc_alarma"].ToString()
                    };

                    _l.Add(_r);
                }
            }
            catch (Exception e)
            {
                throw new Exception("ERR.t01: Convert to Entity DTO Notificaciones Camion", e.InnerException);
            }

            return _l;
        }
    }
}
