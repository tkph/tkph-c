﻿using TKPH_DATA.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using TKPH_DATA.ACCESS;

namespace TKPH_DATA.SP_SQL.Camiones
{
    /// <summary>
    /// Clase con metodos para llenado de listas con resultados de consultas a SQL para insercion de Datos de notificación Camión 
    /// </summary>
    class InsertCamionNotificacion
    {
        /// <summary>
        /// Metodo que realiza llamada a Stored Procedure con parametros para insercion de Datos de notificación Camión 
        /// </summary>
        /// <param name="idcamion">Valor correspondiente al identificador del camión</param>
        /// <param name="comentario">Valor correspondiente a comentario o detalle de notificación</param>
        /// <returns>Retorna Interfaz de lista tipo "InsertCamionNotificacionDTO" con datos insertados</returns>
        public static IList<InsertCamionNotificacionDTO> setInsertCamionNotificacion(Int64 idcamion, String comentario)
        {
            Acceso _con = new Acceso();

            IList<InsertCamionNotificacionDTO> _ListCamionNotificacion = new List<InsertCamionNotificacionDTO>();

            try
            {
                SqlParameter[] sqlParams = new SqlParameter[]
                {
                    new SqlParameter("@idcamion",idcamion),
                    new SqlParameter("@comentario",comentario)
                };

                DataTable _t = new DataTable();

                _con.CreateCommandSelect("exec " + NombreSP.Nombre_SP(75).ToString() + " @idcamion, @comentario", sqlParams, _t);

            }
            catch (Exception e)
            {
                throw e;
            }

            return _ListCamionNotificacion;
        }
    }
}
