﻿using TKPH_DATA.DTO.Camiones;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using TKPH_DATA.ACCESS;

namespace TKPH_DATA.SP_SQL.Camiones
{
    /// <summary>
    /// Clase con metodos para llenado de listas con resultados de consultas a SQL con datos agrupados de presion y Temperatura del Camión 
    /// </summary>
    public class PromAvgTempPres
    {
        /// <summary>
        /// Metodo que realiza llamada a Stored Procedure con parametros para obtener datos agrupados de presion y Temperatura del Camión 
        /// </summary>
        /// <param name="idcamion">Valor correspondiente al identificador del Camión</param>
        /// <returns>Retorna interfaz de lista de tipo "PromAvgTempPresDTO" con datos a desplegar</returns>
        public static IList<PromAvgTempPresDTO> getTempPres(Int64 idcamion)
        {
            Acceso _con = new Acceso();

            IList<PromAvgTempPresDTO> _ListTempPres = new List<PromAvgTempPresDTO>();

            try
            {
                SqlParameter[] sqlParams = new SqlParameter[]
                {
                    new SqlParameter("@camion",idcamion)
                };

                DataTable _t = new DataTable();

                _con.CreateCommandSelect("exec " + NombreSP.Nombre_SP(70).ToString() + " @camion", sqlParams, _t);

                _ListTempPres = leeTempPres(_t);
            }
            catch (Exception e)
            {
                throw e;
            }

            return _ListTempPres;
        }


        /// <summary>
        /// Metodo que realiza asignación del valores desde la consulta a la Interfaz de Lista
        /// </summary>
        /// <param name="table">Objeto tipo DataTable que contiene el resultado de la consulta SQL</param>
        /// <returns>Retorna Interfaz de lista de tipo "PromAvgTempPresDTO"</returns>
        /// <exception cref="System.Exception">ERR.t01: Convert to Entity DTO PromAvgTempPresDTO Camion</exception>
        private static IList<PromAvgTempPresDTO> leeTempPres(DataTable table)
        {
             IList<PromAvgTempPresDTO> _l = new List<PromAvgTempPresDTO>();

             try
             {
                 foreach (DataRow row in table.Rows)
                 {

                     PromAvgTempPresDTO _r = new PromAvgTempPresDTO()
                     {
                         id_camion = Int64.Parse(row["id_camion"].ToString()),
                         max_presion_psi_1 = row["max_presion_psi_1"].ToString(),
                         max_presion_psi_2 = row["max_presion_psi_2"].ToString(),
                         max_presion_psi_3 = row["max_presion_psi_3"].ToString(),
                         max_presion_psi_4 = row["max_presion_psi_4"].ToString(),
                         max_presion_psi_5 = row["max_presion_psi_5"].ToString(),
                         max_presion_psi_6 = row["max_presion_psi_6"].ToString(),

                         min_presion_psi_1 = row["min_presion_psi_1"].ToString(),
                         min_presion_psi_2 = row["min_presion_psi_2"].ToString(),
                         min_presion_psi_3 = row["min_presion_psi_3"].ToString(),
                         min_presion_psi_4 = row["min_presion_psi_4"].ToString(),
                         min_presion_psi_5 = row["min_presion_psi_5"].ToString(),
                         min_presion_psi_6 = row["min_presion_psi_6"].ToString(),

                         avg_presion_psi_1 = row["avg_presion_psi_1"].ToString(),
                         avg_presion_psi_2 = row["avg_presion_psi_2"].ToString(),
                         avg_presion_psi_3 = row["avg_presion_psi_3"].ToString(),
                         avg_presion_psi_4 = row["avg_presion_psi_4"].ToString(),
                         avg_presion_psi_5 = row["avg_presion_psi_5"].ToString(),
                         avg_presion_psi_6 = row["avg_presion_psi_6"].ToString(),

                         max_temperatura_c_1 = row["max_temperatura_c_1"].ToString(),
                         max_temperatura_c_2 = row["max_temperatura_c_2"].ToString(),
                         max_temperatura_c_3 = row["max_temperatura_c_3"].ToString(),
                         max_temperatura_c_4 = row["max_temperatura_c_4"].ToString(),
                         max_temperatura_c_5 = row["max_temperatura_c_5"].ToString(),
                         max_temperatura_c_6 = row["max_temperatura_c_6"].ToString(),

                         min_temperatura_c_1 = row["min_temperatura_c_1"].ToString(),
                         min_temperatura_c_2 = row["min_temperatura_c_2"].ToString(),
                         min_temperatura_c_3 = row["min_temperatura_c_3"].ToString(),
                         min_temperatura_c_4 = row["min_temperatura_c_4"].ToString(),
                         min_temperatura_c_5 = row["min_temperatura_c_5"].ToString(),
                         min_temperatura_c_6 = row["min_temperatura_c_6"].ToString(),

                         avg_temperatura_c_1 = row["avg_temperatura_c_1"].ToString(),
                         avg_temperatura_c_2 = row["avg_temperatura_c_2"].ToString(),
                         avg_temperatura_c_3 = row["avg_temperatura_c_3"].ToString(),
                         avg_temperatura_c_4 = row["avg_temperatura_c_4"].ToString(),
                         avg_temperatura_c_5 = row["avg_temperatura_c_5"].ToString(),
                         avg_temperatura_c_6 = row["avg_temperatura_c_6"].ToString(),

                         fecha_ini = DateTime.Parse(row["fecha_ini"].ToString()),
                         fecha_fin = DateTime.Parse(row["fecha_fin"].ToString())
                     };

                     _l.Add(_r);

                 }
             }
             catch (Exception e)
             {
                 throw new Exception("ERR.t01: Convert to Entity DTO PromAvgTempPresDTO Camion", e.InnerException);
             }

            return _l;
        }



    }
}
