﻿using TKPH_DATA.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using TKPH_DATA.DTO.Camiones;
using TKPH_DATA.ACCESS;

namespace TKPH_DATA.SP_SQL.Camiones
{
    /// <summary>
    /// Clase con metodos para llenado de listas con resultados de consultas a SQL para despliege de Datos Notificación Camión 
    /// </summary>
    class CamionNotificacion
    {
        /// <summary>
        /// Metodo que realiza llamada a Stored Procedure con parametros para listar datos  Notificaciones Camión
        /// </summary>
        /// <param name="idcamion">Valor correspondiente al identificadofr del Camión</param>
        /// <returns>Retorna Interfaz de Lista tipo "CamionNotificacionDTO" con datos a deplegar.</returns>
        public static IList<CamionNotificacionDTO> getCamionNotificacion(String idcamion)
        {
            Acceso _con = new Acceso();

            IList<CamionNotificacionDTO> _ListCamionNotificacion = new List<CamionNotificacionDTO>();

            try
            {
                SqlParameter[] sqlParams = new SqlParameter[]
                {
                    new SqlParameter("@camion",idcamion)
                };

                DataTable _t = new DataTable();

                _con.CreateCommandSelect("exec " + NombreSP.Nombre_SP(74).ToString() + " @camion", sqlParams, _t);

                _ListCamionNotificacion = leeCamionNotificacion(_t);
            }
            catch (Exception e)
            {
                throw e;
            }

            return _ListCamionNotificacion;
        }


        /// Metodo que realiza asignación del valores desde la consulta a la Interfaz de Lista
        /// </summary>
        /// <param name="table">Objeto tipo DataTable que contiene el resultado de la consulta SQL</param>
        /// <returns>Retorna objeto de tipo "CamionNotificacionDTO"</returns>
        /// <exception cref="System.Exception">ERR.t01: Convert to Entity DTO CamionNotificacionDTO Camion</exception>
        private static IList<CamionNotificacionDTO> leeCamionNotificacion(DataTable table)
        {
            IList<CamionNotificacionDTO> _l = new List<CamionNotificacionDTO>();

            try
            {
                foreach (DataRow row in table.Rows)
                {

                    CamionNotificacionDTO _r = new CamionNotificacionDTO()
                    {
                        ID_CAMION = row["ID_CAMION"].ToString(),
                        TIPO_ALARMA = row["TIPO_ALARMA"].ToString(),
                        UBICACION_ALARMA = row["UBICACION_ALARMA"].ToString(),
                        GRAVEDAD_ALARMA = row["GRAVEDAD_ALARMA"].ToString(),
                        VALOR = row["VALOR"].ToString(),
                        FECHA_ALARMA = row["FECHA_ALARMA"].ToString(),
                        PALA = row["PALA"].ToString(),
                        OPERADOR_PALA = row["OPERADOR_PALA"].ToString(),
                        OPERADOR_CAMION = row["OPERADOR_CAMION"].ToString(),
                        ID_DESTINO = row["ID_DESTINO"].ToString(),
                        ID_TURNO = row["ID_TURNO"].ToString(),
                        GENERACION = row["GENERACION"].ToString()
                    };

                    _l.Add(_r);

                }
            }
            catch (Exception e)
            {
                throw new Exception("ERR.t01: Convert to Entity DTO CamionNotificacionDTO Camion", e.InnerException);
            }

            return _l;
        }
    }
}
