﻿using TKPH_DATA;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using TKPH_DATA.DTO.Camiones;
using TKPH_DATA.ACCESS;

namespace TKPH_DATA.SP_SQL.Camiones
{
    /// <summary>
    /// Clase con metodos para llenado de listas con resultados de consultas a SQL para despliege de Datos historicos Presión y Temperatura Camión 
    /// </summary>
    public class HistoricoPT
    {
        /// <summary>
        /// Metodo que realiza llamada a Stored Procedure con parametros para obtener de Datos Presión y Temperatura Camión 
        /// </summary>
        /// <param name="Request">Objeto tipo "HitoricoPresionTempDTO" con valores de entrada</param>
        /// <returns>Retorna Interfaz de listo de tipo "HitoricoPresionTempDTO" con datos a deplegar.</returns>
        public static IList<HitoricoPresionTempDTO> GetHitoricoPresionTemp(HitoricoPresionTempDTO Request)
        {
            Acceso _con = new Acceso();

            IList<HitoricoPresionTempDTO> _l = new List<HitoricoPresionTempDTO>();

            try
            {
                SqlParameter[] sqlParams = new SqlParameter[] {
                    new SqlParameter("@camion", Request.IDCAMION),
                    new SqlParameter("@logicalid", Request.LOGICALID),
                    new SqlParameter("@fecha_desdeIn", Request.FechaDesde),
                    new SqlParameter("@fecha_hastaIn", Request.FechaHasta)
                };

                DataTable _t = new DataTable();

                _con.CreateCommandSelect("exec "+NombreSP.Nombre_SP(16).ToString()+" @camion, @logicalid, @fecha_desdeIn, @fecha_hastaIn", sqlParams, _t);

                _l = EntityList(_t);
            }
            catch (Exception e)
            {
                throw e;
            }

            return _l;

        }


        /// <summary>
        /// Metodo que realiza asignación del valores desde la consulta a la Interfaz de Lista
        /// </summary>
        /// <param name="Table">Objeto tipo DataTable que contiene el resultado de la consulta SQL</param>
        /// <returns>Retorna Interfaz de lista de tipo "HitoricoPresionTempDTO"</returns>
        /// <exception cref="System.Exception">ERR.t03: Convert to Entity DTO Historico TempPres</exception>
        private static IList<HitoricoPresionTempDTO> EntityList(DataTable Table)
        {
            IList<HitoricoPresionTempDTO> _l = new List<HitoricoPresionTempDTO>();

            try
            {
                foreach (DataRow row in Table.Rows)
                {
                    HitoricoPresionTempDTO _f = new HitoricoPresionTempDTO()
                    {
                        FECHA = Convert.ToDateTime(row["FECHA"].ToString()),
                        IDCAMION = row["IDCAMION"].ToString(),
                        LOGICALID = row["LOGICALID"].ToString(),
                        TEMPERATURA = Convert.ToDouble(row["TEMPERATURA"].ToString()),
                        PRESION = Convert.ToDouble(row["PRESION"].ToString())
                    };

                    _l.Add(_f);
                }
            }
            catch (Exception e)
            {
                throw new Exception("ERR.t03: Convert to Entity DTO Historico TempPres", e.InnerException);
            }

            return _l;
        }
    }
}