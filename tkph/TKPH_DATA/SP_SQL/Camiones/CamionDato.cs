﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using TKPH_DATA;
using TKPH_DATA.ACCESS;
using TKPH_DATA.DTO.Camiones;

namespace TKPH_DATA.SP_SQL.Camiones
{
    /// <summary>
    /// Clase con metodos para llenado de listas con resultados de consultas a SQL para despliege de Datos Generales Camión
    /// </summary>
    public class CamionDato
    {
        /// <summary>
        /// Metodo que realiza llamada a Stored Procedure con parametros para insercion de  Datos Generales Camión
        /// </summary>
        /// <param name="Notificacion">Objeto tipo "CamionDatosDTO" con valores de entrada</param>
        /// <returns>Retorna Objeto "CamionDatosDTO" con datos a deplegar.</returns>
        public static CamionDatosDTO GetCamionDatos(CamionDatosDTO Request)
        {
            Acceso _con = new Acceso();

            CamionDatosDTO _l = new CamionDatosDTO();

            try
            {
                SqlParameter[] sqlParams = new SqlParameter[] {
                    new SqlParameter("@camion", Request.IdCamion),
                    new SqlParameter("@flota", Request.Flota)
                };

                DataTable _t = new DataTable();

                _con.CreateCommandSelect("exec " + NombreSP.Nombre_SP(9).ToString() + " @camion, @flota ", sqlParams, _t);

                _l = EntityList(_t);
            }
            catch (Exception e)
            {
                throw e;
            }

            return _l;
        }


        /// <summary>
        /// Metodo que realiza asignación del valores desde la consulta a la Interfaz de Lista
        /// </summary>
        /// <param name="Table">Objeto tipo DataTable que contiene el resultado de la consulta SQL</param>
        /// <returns>Retorna objeto de tipo "CamionDatosDTO"</returns>
        /// <exception cref="System.Exception">ERR.t01: No existen Datos: CamionDato, EntityList or ERR.t01: Convert to Entity DTO Camion Datos Generales </exception>
        private static CamionDatosDTO EntityList(DataTable Table)
        {
            CamionDatosDTO _c = new CamionDatosDTO();

            try
            {
                if (Table.Rows.Count.Equals(0))
                    throw new Exception("ERR.t01: No existen Datos: CamionDato, EntityList");
                else
                    try
                    {
                        _c = new CamionDatosDTO()
                        {
                            IdCamion = Table.Rows[0]["id_camion"].ToString(),
                            Carga = Table.Rows[0]["CARGA"].ToString(),
                            CargaEsperada = Table.Rows[0]["CARGA_ESPERADA"].ToString(),
                            AlertaCarga = Convert.ToInt32(Table.Rows[0]["alerta_carga"].ToString()),
                            VM = Table.Rows[0]["VM"].ToString(),
                            DistRec = Table.Rows[0]["DistRec"].ToString(),
                            TKPHOperacionalT = Table.Rows[0]["TKPH_OPERACIONALT"].ToString(),
                            AlertaTKPH_T = Convert.ToInt32(Table.Rows[0]["alerta_tkph_t"].ToString()),
                            TKPHOperacionalD = Table.Rows[0]["TKPH_OPERACIONALD"].ToString(),
                            AlertaTKPH_D = Convert.ToInt32(Table.Rows[0]["alerta_tkph_d"].ToString()),
                            TipoCalculo = Table.Rows[0]["TIPO_CALCULO"].ToString(),
                            DescPosicionEjeT = Table.Rows[0]["DESC_POSICION_EJET"].ToString(),
                            DescPosicionEjeD = Table.Rows[0]["DESC_POSICION_EJED"].ToString(),
                            TKPHNominal_C = Table.Rows[0]["tkph_nominal_c"].ToString(),
                            Flota = Table.Rows[0]["Flota"].ToString(),
                            Turno = Table.Rows[0]["Turno"].ToString(),
                            Operador = Table.Rows[0]["Operador"].ToString(),
                            OpePala = Table.Rows[0]["OpePala"].ToString(),
                            Pala = Table.Rows[0]["Pala"].ToString(),
                            CargaActual = Table.Rows[0]["Carga_actual"].ToString(),
                            VelocidadActual = Table.Rows[0]["Velocidad_actual"].ToString(),
                            id_destino = Table.Rows[0]["id_destino"].ToString(),
                            carga_turno = Table.Rows[0]["carga_turno"].ToString(),
                            km_turno = Table.Rows[0]["km_turno"].ToString(),
                            TKPH_avg_D = Table.Rows[0]["TKPH_OPERACIONAL_AVG_EJED"].ToString(),
                            TKPH_avg_T = Table.Rows[0]["TKPH_OPERACIONAL_AVG_EJET"].ToString(),
                            f_ult_lectura = Table.Rows[0]["f_ult_lectura"].ToString(),
                            Temp_ambiente = Table.Rows[0]["temperatura_ambiente"].ToString(),
                            fecha_fin_de_ciclo = Table.Rows[0]["FECHA_FIN_DE_CICLO"].ToString(),
                            aler_not_alto = Convert.ToInt32(Table.Rows[0]["IND_ROJO"].ToString()),
                            aler_not_medio = Convert.ToInt32(Table.Rows[0]["IND_AMARILLO"].ToString()),

                        };
                    }
                    catch (Exception e)
                    {
                        throw new Exception("ERR.t01: Convert to Entity DTO Camion Datos Generales", e.InnerException);
                    }
            }
            catch (Exception e)
            {
                throw e;
            }

            return _c;
        }
    }
}
