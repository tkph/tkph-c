﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using TKPH_DATA;
using TKPH_DATA.ACCESS;
using TKPH_DATA.DTO.Camiones;

namespace TKPH_DATA.SP_SQL.Camiones
{
    /// <summary>
    /// Clase con metodos para llenado de listas con resultados de consultas a SQL para despliege de Datos historicos TKPH y Carga Camión 
    /// </summary>
    public class Historico
    {
        /// <summary>
        /// Metodo que realiza llamada a Stored Procedure con parametros para obtener de Datos TKPH y Carga Camión 
        /// </summary>
        /// <param name="Request">Objeto tipo "HistoricoTKPHCargaDTO" con valores de entrada</param>
        /// <returns>Retorna Interfaz de listo de tipo "HistoricoTKPHCargaDTO" con datos a deplegar.</returns>
        public static IList<HistoricoTKPHCargaDTO> GetHistoricoCargaTKPH(HistoricoTKPHCargaDTO Request)
        {
            Acceso _con = new Acceso();

            IList<HistoricoTKPHCargaDTO> _l = new List<HistoricoTKPHCargaDTO>();

            try
            {
                SqlParameter[] sqlParams = new SqlParameter[] {
                    new SqlParameter("@camion", Request.IdCamion),
                    new SqlParameter("@flag", Request.Flag),
                    new SqlParameter("@fecha_desdeIn", Request.FechaDesde),
                    new SqlParameter("@fecha_hastaIn", Request.FechaHasta)
                };

                DataTable _t = new DataTable();

                _con.CreateCommandSelect("exec " + NombreSP.Nombre_SP(17).ToString() + " @camion, @flag, @fecha_desdeIn, @fecha_hastaIn", sqlParams, _t);

                _l = EntityList(_t);
            }
            catch (Exception e)
            {
                throw e;
            }

            return _l;
        }


        /// <summary>
        /// Metodo que realiza asignación del valores desde la consulta a la Interfaz de Lista
        /// </summary>
        /// <param name="table">Objeto tipo DataTable que contiene el resultado de la consulta SQL</param>
        /// <returns>Retorna Interfaz de lista de tipo "HistoricoTKPHCargaDTO"</returns>
        /// <exception cref="System.Exception">ERR.t03: Convert to Entity DTO Historico Carga TKPH</exception>
        private static IList<HistoricoTKPHCargaDTO> EntityList(DataTable Table)
        {
            IList<HistoricoTKPHCargaDTO> _l = new List<HistoricoTKPHCargaDTO>();

            try
            {
                foreach (DataRow row in Table.Rows)
                {
                    HistoricoTKPHCargaDTO _f = new HistoricoTKPHCargaDTO()
                    {
                        Fecha = Convert.ToDateTime(row["FECHA"].ToString()),
                        //Hora = row["HORA"].ToString(),
                        Nonimal = Convert.ToInt64(row["NOMINAL"].ToString()),
                        Operacional = Convert.ToInt64(row["OPERACIONAL"].ToString()),
                        IdCamion = row["IDCAMION"].ToString(),
                        Flag = Convert.ToInt64(row["FLAG"].ToString())
                    };

                    _l.Add(_f);
                }
            }
            catch (Exception e)
            {
                throw new Exception("ERR.t03: Convert to Entity DTO Historico Carga TKPH", e.InnerException);
            }

            return _l;
        }
    }
}
