﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using TKPH_DATA;
using TKPH_DATA.ACCESS;
using TKPH_DATA.DTO.Camiones;

namespace TKPH_DATA.SP_SQL.Camiones
{
    /// <summary>
    /// Clase con metodos para llenado de listas con resultados de consultas a SQL para despliege de Datos de fechas minimas y maximas de información Camión 
    /// </summary>
    public class FechasMaximas
    {
        /// <summary>
        /// Metodo que realiza llamada a Stored Procedure con parametros para obtener fechas minimas y maximas de información Camión 
        /// </summary>
        /// <param name="RequestFechas">Objeto tipo "FechasMaximasDTO" con valores de entrada</param>
        /// <returns>Retorna  Objeto tipo "FechasMaximasDTO" fecha minima y maxima.</returns>
        public static FechasMaximasDTO GetFechas(FechasMaximasDTO RequestFechas)
        {
            Acceso _con = new Acceso();

            FechasMaximasDTO _l = new FechasMaximasDTO();

            try
            {
                SqlParameter[] sqlParams = new SqlParameter[] {
                    new SqlParameter("@camion", RequestFechas.IdCamion)
                };

                DataTable _t = new DataTable();

                _con.CreateCommandSelect("exec " + NombreSP.Nombre_SP(15).ToString() + " @camion", sqlParams, _t);

                _l = EntityList(_t);
            }
            catch (Exception e)
            {
                throw e;
            }

            return _l;
        }

        /// <summary>
        /// Metodo que realiza asignación del valores desde la consulta a la Interfaz de Lista
        /// </summary>
        /// <param name="table">Objeto tipo DataTable que contiene el resultado de la consulta SQL</param>
        /// <returns>Retorna objeto de tipo "FechasMaximasDTO"</returns>
        /// <exception cref="System.Exception">ERR.t03: Convert to Entity DTO Fechas Maximas</exception>
        private static FechasMaximasDTO EntityList(DataTable Table)
        {
            FechasMaximasDTO _f;

            try
            {
                _f = new FechasMaximasDTO()
                    {
                        FUAME = Table.Rows[0]["FUA_ME"].ToString(),
                        FUAMI = Table.Rows[0]["FUA_MI"].ToString()
                    };
            }
            catch (Exception e)
            {
                throw new Exception("ERR.t03: Convert to Entity DTO Fechas Maximas", e.InnerException);
            }

            return _f;
        }
    }
}
