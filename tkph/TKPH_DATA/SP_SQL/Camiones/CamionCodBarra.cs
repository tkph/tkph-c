﻿using Com.Vmica.Con;
using COM_EPMS_DATA.DTO.Camiones;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace COM_EPMS_DATA.SP_SQL.Camiones
{
    public class CamionCodBarra
    {
        public static CamionCodBarraDTO getIDCamion(String id_camion)
        { 
            
            Acceso _con = new Acceso();

            CamionCodBarraDTO idCamionCBarra = new CamionCodBarraDTO();

            try
            {
                SqlParameter[] sqlParams = new SqlParameter[]
                {
                    new SqlParameter("@camion",id_camion)
                };

                DataTable _t = new DataTable();

                _con.CreateCommandSelect("exec " + NombreSP.Nombre_SP(76).ToString() + " @camion", sqlParams, _t);

                idCamionCBarra = leeidCamionCBarra(_t);
            }
            catch (Exception e)
            {
                throw e;
            }

            return idCamionCBarra;
        }

        private static CamionCodBarraDTO leeidCamionCBarra(DataTable table)
        {
            CamionCodBarraDTO _f;

            try
            {
                _f = new CamionCodBarraDTO()
                {
                    id_camion = table.Rows[0]["id_camion"].ToString()
                };
            }
            catch (Exception e)
            {
                throw new Exception("ERR.t01: Convert to Entity DTO CamionNotificacionDTO Camion", e.InnerException);
            }

            return _f;
        }
    }
}
