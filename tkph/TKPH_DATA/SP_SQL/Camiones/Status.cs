﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TKPH_DATA;
using TKPH_DATA.DTO.Camiones;
using TKPH_DATA.ACCESS;

namespace TKPH_DATA.SP_SQL.Camiones
{
    /// <summary>
    /// Clase con metodos para llenado de listas con resultados de consultas a SQL con datos individuales del Camión 
    /// </summary>
    public class Status
    {
        /// <summary>
        /// Metodo que realiza llamada a Stored Procedure con parametros para obtener datos individuales del Camión 
        /// </summary>
        /// <param name="SingleStatus">Objeto tipo "StatusRequest" con valores de entrada</param>
        /// <returns>Retorna objeto de tipo "CamionSingleDTO" con datos a desplegar</returns>
        public static CamionSingleDTO EstadoCamionSingle(StatusRequest SingleStatus)
        {
            Acceso _con = new Acceso();

            CamionSingleDTO _l = new CamionSingleDTO();

            try
            {
                SqlParameter[] sqlParams = new SqlParameter[] {
                    new SqlParameter("@flota", SingleStatus.DescripcionFlota),
                    new SqlParameter("@camion", SingleStatus.IdCamion)
                };

                DataTable _t = new DataTable();

                _con.CreateCommandSelect("exec " + NombreSP.Nombre_SP(6).ToString() + " @flota, @camion", sqlParams, _t);

                _l = EntityListS(_t);
            }
            catch (Exception e)
            {
                throw e;
            }

            return _l;
        }


        /// <summary>
        /// Metodo que realiza llamada a Stored Procedure con parametros para obtener datos datos especificos del Camión 
        /// </summary>
        /// <param name="SingleStatus">Objeto tipo "StatusRequest" con valores de entrada</param>
        /// <returns>Retorna objeto de tipo "CamionDetailsDTO" con datos a desplegar</returns>
        public static IList<CamionDetailsDTO> EstadoCamionDetails(StatusRequest SingleStatus)
        {
            Acceso _con = new Acceso();

            IList<CamionDetailsDTO> _l = new List<CamionDetailsDTO>();

            try
            {
                SqlParameter[] sqlParams = new SqlParameter[] {
                    new SqlParameter("@flota", SingleStatus.DescripcionFlota),
                    new SqlParameter("@status",SingleStatus.AlertaGeneral),
                    new SqlParameter("@temperatura", SingleStatus.AlertaTemperatura),
                    new SqlParameter("@presion", SingleStatus.AlertaPresion),
                    new SqlParameter("@carga", SingleStatus.AlertaCarga),
                    new SqlParameter("@tkph", SingleStatus.AlertaTkph)
                };

                DataTable _t = new DataTable();

                _con.CreateCommandSelect("exec " + NombreSP.Nombre_SP(13).ToString()  + " @flota, @status, @temperatura, @presion, @carga, @tkph", sqlParams, _t);

                _l = EntityListD(_t);
            }
            catch (Exception e)
            {
                throw e;
            }

            return _l;
        }

        public static IList<EstadoCamionDTO> EstadoCamionGround()
        {
            Acceso _con = new Acceso();

            IList<EstadoCamionDTO> _l = new List<EstadoCamionDTO>();

            try
            {
                DataTable _t = new DataTable();

                _con.CreateCommandSelect("exec " + NombreSP.Nombre_SP(12).ToString(), _t);

                _l = EntityList(_t);
            }
            catch (Exception e)
            {
                throw e;
            }

            return _l;
        }

        /// <summary>
        /// Metodo que realiza asignación del valores desde la consulta a la Interfaz de Lista
        /// </summary>
        /// <param name="table">Objeto tipo DataTable que contiene el resultado de la consulta SQL</param>
        /// <returns>Retorna Interfaz de lista de tipo "EstadoCamionDTO"</returns>
        /// <exception cref="System.Exception">ERR.t01: Convert to Entity DTO Estado Camion Ground</exception>
        private static IList<EstadoCamionDTO> EntityList(DataTable table)
        {
            IList<EstadoCamionDTO> _l = new List<EstadoCamionDTO>();

            try
            {
                foreach (DataRow row in table.Rows)
                {
                    EstadoCamionDTO _r = new EstadoCamionDTO()
                    {
                        Valor = Convert.ToInt32(row["valor"].ToString()),
                        Alerta = row["alerta"].ToString(),
                        Total = Convert.ToInt32(row["Total"].ToString())
                    };

                    _l.Add(_r);
                }
            }
            catch (Exception e)
            {
                throw new Exception("ERR.t01: Convert to Entity DTO Estado Camion Ground", e.InnerException);
            }

            return _l;
        }


        /// <summary>
        /// Metodo que realiza asignación del valores desde la consulta a la Interfaz de Lista
        /// </summary>
        /// <param name="table">Objeto tipo DataTable que contiene el resultado de la consulta SQL</param>
        /// <returns>Retorna Interfaz de lista de tipo "CamionDetailsDTO"</returns>
        /// <exception cref="System.Exception">ERR.t03: Convert to Entity DTO Estado Camion Single</exception>
        private static IList<CamionDetailsDTO> EntityListD(DataTable table)
        {
            IList<CamionDetailsDTO> _l = new List<CamionDetailsDTO>();

            try
            {
                foreach (DataRow row in table.Rows)
                {
                    CamionDetailsDTO _r = new CamionDetailsDTO()
                    {
                        IdCamion = Int64.Parse(row["id_camion"].ToString()),
                        Temperatura = Int64.Parse(row["VALOR_TEMP"].ToString()),
                        Presion = Int64.Parse(row["VALOR_P"].ToString()),
                        Carga = Int64.Parse(row["valor_c"].ToString()),
                        Tkph = Int64.Parse(row["valor_tk"].ToString()),
                        Neumatico = Int64.Parse(row["valor_n"].ToString()),
                        Alerta = Int64.Parse(row["valor"].ToString()),
                        DescripcionFlota = row["desc_flota"].ToString(),
                        DescripcionFlotaAbre = row["flota_abre"].ToString()
                    };

                    _l.Add(_r);
                }
            }
            catch (Exception e)
            {
                throw new Exception("ERR.t03: Convert to Entity DTO Estado Camion Single", e.InnerException);
            }

            return _l;
        }

        private static CamionSingleDTO EntityListS(DataTable table)
        {
            CamionSingleDTO _r = new CamionSingleDTO();

            try
            {
                _r = new CamionSingleDTO()
                 {
                     IdCamion = table.Rows[0]["id_camion"].ToString(),
                     AlertaRueda_1 = table.Rows[0]["AlertaRueda_1"].ToString(),
                     AlertaRueda_2 = table.Rows[0]["AlertaRueda_2"].ToString(),
                     AlertaRueda_3 = table.Rows[0]["AlertaRueda_3"].ToString(),
                     AlertaRueda_4 = table.Rows[0]["AlertaRueda_4"].ToString(),
                     AlertaRueda_5 = table.Rows[0]["AlertaRueda_5"].ToString(),
                     AlertaRueda_6 = table.Rows[0]["AlertaRueda_6"].ToString(),
                     AlertaTemp_1 = table.Rows[0]["AlarmaTemp_1"].ToString(),
                     AlertaTemp_2 = table.Rows[0]["AlarmaTemp_2"].ToString(),
                     AlertaTemp_3 = table.Rows[0]["AlarmaTemp_3"].ToString(),
                     AlertaTemp_4 = table.Rows[0]["AlarmaTemp_4"].ToString(),
                     AlertaTemp_5 = table.Rows[0]["AlarmaTemp_5"].ToString(),
                     AlertaTemp_6 = table.Rows[0]["AlarmaTemp_6"].ToString(),
                     AlertaPres_1 = table.Rows[0]["AlarmaPres_1"].ToString(),
                     AlertaPres_2 = table.Rows[0]["AlarmaPres_2"].ToString(),
                     AlertaPres_3 = table.Rows[0]["AlarmaPres_3"].ToString(),
                     AlertaPres_4 = table.Rows[0]["AlarmaPres_4"].ToString(),
                     AlertaPres_5 = table.Rows[0]["AlarmaPres_5"].ToString(),
                     AlertaPres_6 = table.Rows[0]["AlarmaPres_6"].ToString(),
                     Presion_1 = table.Rows[0]["presion_psi_1"].ToString(),
                     Presion_2 = table.Rows[0]["presion_psi_2"].ToString(),
                     Presion_3 = table.Rows[0]["presion_psi_3"].ToString(),
                     Presion_4 = table.Rows[0]["presion_psi_4"].ToString(),
                     Presion_5 = table.Rows[0]["presion_psi_5"].ToString(),
                     Presion_6 = table.Rows[0]["presion_psi_6"].ToString(),
                     TemperaturaCarga_1 = table.Rows[0]["temperatura_c_1"].ToString(),
                     TemperaturaCarga_2 = table.Rows[0]["temperatura_c_2"].ToString(),
                     TemperaturaCarga_3 = table.Rows[0]["temperatura_c_3"].ToString(),
                     TemperaturaCarga_4 = table.Rows[0]["temperatura_c_4"].ToString(),
                     TemperaturaCarga_5 = table.Rows[0]["temperatura_c_5"].ToString(),
                     TemperaturaCarga_6 = table.Rows[0]["temperatura_c_6"].ToString(),
                     DescFlota = table.Rows[0]["desc_flota"].ToString(),
                     Fecha = table.Rows[0]["Fecha"].ToString()
                 };
            }
            catch (Exception e)
            {
                throw new Exception("ERR.t03: Convert to Entity DTO Estado Camion Single", e.InnerException);
            }

            return _r;
        }

        
    }
}